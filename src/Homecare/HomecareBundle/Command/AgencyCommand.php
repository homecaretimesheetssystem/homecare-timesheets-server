<?php

namespace Homecare\HomecareBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Homecare\HomecareBundle\Entity\Agency;
use Doctrine\ORM\EntityManager;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;


class AgencyCommand extends ContainerAwareCommand
{


    protected function configure()
    {
        $this
            ->setName('create:agency')
            ->setDescription('Creates a new agency in the database')
            ->addArgument(
                'agencyName',
                InputArgument::REQUIRED | InputArgument::IS_ARRAY,
                'Name of the agency you want to create.(Put each name in quotations. Example: "PCA Partners". To create multiple agencies at a time, seperate multiple agency names with a space. Example: "Pca Partners" "Comforts Of Home")'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $agencyNames = $input->getArgument('agencyName');

        $string = implode("\n", $agencyNames);

        $numAgencies = count($agencyNames);


        foreach ($agencyNames as $agencyName) {

            $agency = new Agency();

            $agency->setAgencyName(strtolower($agencyName));

            $em->persist($agency);

        }

        $em->flush();


        $output->writeln("<info>Agencies Created: $numAgencies \n$string</info>");
    }
}


?>