<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 9/7/16
 * Time: 3:32 PM
 */

namespace Homecare\HomecareBundle\Command;

set_time_limit(0);

use Homecare\HomecareBundle\Entity\UnsignedTimesheetEmail;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Homecare\HomecareBundle\Entity\Agency;
use Doctrine\ORM\EntityManager;


class SendTimesheetUnsignedEmailCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('send-email:unsigned-timesheets')
            ->setDescription('Sends email to recipients with timesheets that have been unsigned for a week');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {


        // this is the infinite loop that you start up
        // it sleeps for an hour after being run
        while (1) {

            $em         = $this->getContainer()->get('doctrine.orm.entity_manager');
            $timesheets = $em->getRepository("HomecareHomecareBundle:Timesheet")->getUnsignedTimesheets();


            foreach ($timesheets as $timesheet) {


                // since you are updating an entity and querying the same entity
                // in the same loop make sure you refresh the database
                // to get the most recent
                $em->refresh($timesheet);


                // if the timesheet has never had an email sent
                // then send it right away
                if ( ! $timesheet->getUnsignedTimesheetEmail()) {

                    // comment out the email sending for now till go live
                    /*
                    $this->getContainer()->get('homecare_homecare.model.custom_swift_mailer')->sendTimesheetAvailableEmail(
                        $timesheet->getRecipient()
                    );
                    */


                    $unsignedTimesheetEmail = new UnsignedTimesheetEmail();
                    $date                   = new \DateTime();
                    $date->modify('+7 day');
                    $unsignedTimesheetEmail->setNextEmailSendDate($date);
                    $timesheet->setUnsignedTimesheetEmail($unsignedTimesheetEmail);

                    $em->persist($timesheet);
                    $em->persist($unsignedTimesheetEmail);
                    $em->flush();

                    $output->writeln("first email sent");

                } elseif ($unsignedTimesheetEmail = $timesheet->getUnsignedTimesheetEmail()) {


                    $currentDate = new \DateTime();
                    $date        = $unsignedTimesheetEmail->getNextEmailSendDate();


                    // if it is time to send the email again
                    if ($currentDate > $date) {

                        // comment out email sending for now
                        /*
                        $this->getContainer()->get('homecare_homecare.model.custom_swift_mailer')->sendTimesheetAvailableEmail(
                            $timesheet->getRecipient()
                        );
                        */

                        $date->modify("+7 days");
                        $unsignedTimesheetEmail->setNextEmailSendDate($date);
                        $em->persist($unsignedTimesheetEmail);
                        $em->flush();

                        $output->writeln("second email sent");
                    }


                }


            }

            $output->writeln("while loop completed");

            // every hour
            sleep(3600);
        }

    }


}