<?php

namespace Homecare\HomecareBundle\AuthenticationHandler;

use Homecare\HomecareBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Http\HttpUtils;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

/**
 * AuthenticationSuccessHandler
 *
 * This custom AuthenticationSuccessHandler ensures that different roles of
 * user are, by default, redirected to the appropriate (appropriate to their
 * role) landing page.  We say "by default" because if before authentication
 * they were trying to reach a specific URL, they should instead go to that
 * specific URL after login (assuming they are authorized).
 *
 * @author Josh Crawmer <joshcrawmer4@gmail.com>
 */
class AuthenticationSuccessHandler extends DefaultAuthenticationSuccessHandler
{


    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;


    /**
     * @param $authorizationChecker
     */
    public function setAuthorizationChecker($authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }


    /**
     * Redirect user to appropriate landing page based on their role
     *
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $user  = $token->getUser();
        $roles = $user->getRoles();


        // This should never happen.  All user classes should inherit from this base
        // user class.
        if ( ! $user instanceof User) {
            throw new AuthenticationException('Invalid user authentication.');
        }


        if ($this->authorizationChecker->isGranted('ROLE_PCA')) {

            return $this->httpUtils->createRedirectResponse($request, 'fos_user_profile_show');

        }


        return parent::onAuthenticationSuccess($request, $token);

    }
}
