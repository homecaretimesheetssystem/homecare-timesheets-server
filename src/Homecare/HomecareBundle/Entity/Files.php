<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation;

/**
 * Files
 *
 * @ORM\Table(name="files")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\FilesRepository")
 * @Annotation\ExclusionPolicy("all")
 */
class Files
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Annotation\Expose
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="timesheetCsvFile", type="string", length=255, nullable=true)
     * @Annotation\Expose
     * @Type("string")
     * @SerializedName("timesheetCsvFile")
     */
    private $timesheetCsvFile;


    /**
     * @var string
     *
     * @ORM\Column(name="timesheetPdfFile", type="string", length=255, nullable=true)
     * @Annotation\Expose
     * @Type("string")
     * @SerializedName("timesheetPdfFile")
     */
    private $timesheetPdfFile;


    /**
     * @var string
     *
     * @ORM\Column(name="timesheetRecSig", type="string", length=255, nullable=true)
	 * @Annotation\Expose
     * @Type("string")
     * @SerializedName("timesheetRecSig")
     */
    private $timesheetRecSig;




    /**
     * @var string
     *
     * @ORM\Column(name="timesheetPcaSig", type="string", length=255, nullable=true)
     * @Annotation\Expose
     * @Type("string")
     * @SerializedName("timesheetPcaSig")
     */
    private $timesheetPcaSig;



		
		
		
    /**
     * @ORM\OneToOne(targetEntity="Timesheet", inversedBy="file")
     * @Type("Homecare\HomecareBundle\Entity\Timesheet")
     * @SerializedName("timesheet")
     */
    private $timesheet;
		
		
		


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set timesheetCsvFile
     *
     * @param string $timesheetCsvFile
     * @return Files
     */
    public function setTimesheetCsvFile($timesheetCsvFile)
    {
        $this->timesheetCsvFile = $timesheetCsvFile;

        return $this;
    }

    /**
     * Get timesheetCsvFile
     *
     * @return string 
     */
    public function getTimesheetCsvFile()
    {
        return $this->timesheetCsvFile;
    }

    /**
     * Set timesheetPdfFile
     *
     * @param string $timesheetPdfFile
     * @return Files
     */
    public function setTimesheetPdfFile($timesheetPdfFile)
    {
        $this->timesheetPdfFile = $timesheetPdfFile;

        return $this;
    }

    /**
     * Get timesheetPdfFile
     *
     * @return string 
     */
    public function getTimesheetPdfFile()
    {
        return $this->timesheetPdfFile;
    }

    /**
     * Set timesheetRecSig
     *
     * @param string $timesheetRecSig
     * @return Files
     */
    public function setTimesheetRecSig($timesheetRecSig)
    {
        $this->timesheetRecSig = $timesheetRecSig;

        return $this;
    }

    /**
     * Get timesheetRecSig
     *
     * @return string 
     */
    public function getTimesheetRecSig()
    {
        return $this->timesheetRecSig;
    }



    /**
     * Set timesheetPcaSig
     *
     * @param string $timesheetPcaSig
     * @return Files
     */
    public function setTimesheetPcaSig($timesheetPcaSig)
    {
        $this->timesheetPcaSig = $timesheetPcaSig;

        return $this;
    }

    /**
     * Get timesheetPcaSig
     *
     * @return string 
     */
    public function getTimesheetPcaSig()
    {
        return $this->timesheetPcaSig;
    }




    /**
     * Set timesheet
     *
     * @param \Homecare\HomecareBundle\Entity\Timesheet $timesheet
     * @return Files
     */
    public function setTimesheet(\Homecare\HomecareBundle\Entity\Timesheet $timesheet = null)
    {
        $this->timesheet = $timesheet;

        return $this;
    }

    /**
     * Get timesheet
     *
     * @return \Homecare\HomecareBundle\Entity\Timesheet 
     */
    public function getTimesheet()
    {
        return $this->timesheet;
    }
}
