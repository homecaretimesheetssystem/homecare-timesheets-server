<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;

/**
 * Services
 *
 * @ORM\Table(name="services")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\ServicesRepository")
 * @Annotation\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks()
 */
class Services
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Annotation\Expose
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="serviceNumber", type="integer")
     * @Annotation\Expose
     * @Encrypted
     */
    private $serviceNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="serviceName", type="string", length=255)
     * @Annotation\Expose
     * @Encrypted
     */
    private $serviceName;


    /**
     * @ORM\ManyToMany(targetEntity="Agency", mappedBy="services")
     */
    private $agencies;


    /**
     * @ORM\OneToMany(targetEntity="SessionData", mappedBy="service", orphanRemoval=true)
     */
    private $sessionData;


    /**
     * @ORM\ManyToMany(targetEntity="Homecare\HomecareBundle\Entity\Pca", mappedBy="services")
     */
    private $pcas;


    public function __toString()
    {
        return $this->getServiceName();
    }


    public function __construct()
    {
        $this->sessionData = new ArrayCollection();
        $this->agencies    = new ArrayCollection();
        $this->pcas        = new ArrayCollection();
    }


    /**
     * this method is necessary because we need to lowercase any encrypted properties for future database queries
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */

    public function lowercaseProperties()
    {
        $this->setServiceName(strtolower($this->getServiceName()));
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set serviceName
     *
     * @param string $serviceName
     *
     * @return Services
     */
    public function setServiceName($serviceName)
    {
        $this->serviceName = $serviceName;

        return $this;
    }

    /**
     * Get serviceName
     *
     * @return string
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }


    /**
     * Add sessionData
     *
     * @param \Homecare\HomecareBundle\Entity\SessionData $sessionData
     *
     * @return Services
     */
    public function addSessionDatum(\Homecare\HomecareBundle\Entity\SessionData $sessionData)
    {
        $this->sessionData[] = $sessionData;

        return $this;
    }

    /**
     * Remove sessionData
     *
     * @param \Homecare\HomecareBundle\Entity\SessionData $sessionData
     */
    public function removeSessionDatum(\Homecare\HomecareBundle\Entity\SessionData $sessionData)
    {
        $this->sessionData->removeElement($sessionData);
    }

    /**
     * Get sessionData
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSessionData()
    {
        return $this->sessionData;
    }


    /**
     * Add agency
     *
     * @param \Homecare\HomecareBundle\Entity\Agency $agency
     *
     * @return Services
     */
    public function addAgency(\Homecare\HomecareBundle\Entity\Agency $agency)
    {
        $this->agencies[] = $agency;

        return $this;
    }

    /**
     * Remove agency
     *
     * @param \Homecare\HomecareBundle\Entity\Agency $agency
     */
    public function removeAgency(\Homecare\HomecareBundle\Entity\Agency $agency)
    {
        $this->agencies->removeElement($agency);
    }

    /**
     * Get agencies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAgencies()
    {
        return $this->agencies;
    }

    /**
     * Set serviceNumber
     *
     * @param integer $serviceNumber
     *
     * @return Services
     */
    public function setServiceNumber($serviceNumber)
    {
        $this->serviceNumber = $serviceNumber;

        return $this;
    }

    /**
     * Get serviceNumber
     *
     * @return integer
     */
    public function getServiceNumber()
    {
        return $this->serviceNumber;
    }


    /**
     * Add pca
     *
     * @param \Homecare\HomecareBundle\Entity\Pca $pca
     *
     * @return Services
     */
    public function addPca(\Homecare\HomecareBundle\Entity\Pca $pca)
    {
        $this->pcas[] = $pca;

        return $this;
    }

    /**
     * Remove pca
     *
     * @param \Homecare\HomecareBundle\Entity\Pca $pca
     */
    public function removePca(\Homecare\HomecareBundle\Entity\Pca $pca)
    {
        $this->pcas->removeElement($pca);
    }

    /**
     * Get pcas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPcas()
    {
        return $this->pcas;
    }
}
