<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Homecare\HomecareBundle\Form\model\RegistrationData;
use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Agency
 *
 * @ORM\Table(name="agency")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\AgencyRepository")
 * @Annotation\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class Agency
{


    public static $weekStartDates = [
        'Monday'    => 'Monday',
        'Tuesday'   => 'Tuesday',
        'Wednesday' => 'Wednesday',
        'Thursday'  => 'Thursday',
        'Friday'    => 'Friday',
        'Saturday'  => 'Saturday',
        'Sunday'    => 'Sunday',
    ];


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Annotation\Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="agency_name", type="string", length=255)
     * @Assert\NotBlank(groups={"Default"})
     * @Annotation\Expose
     * @Encrypted
     */
    private $agencyName;


    /**
     * @var string
     *
     * @ORM\Column(name="week_start_date", type="string", length=255)
     * @Assert\Choice(choices = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"}, message = "Choose a valid week start date.", groups={"Default"})
     * @Annotation\Expose
     * @Encrypted
     */
    private $weekStartDate = 'Monday';


    /**
     * @var array
     *
     * @ORM\Column(name="start_time", type="array")
     * @Annotation\Expose
     */
    private $startTime = [];


    /**
     * @var string
     *
     * @ORM\Column(name="phoneNumber", type="string", length=255, nullable=true)
     * @Assert\NotBlank(groups={"Default"})
     * @Annotation\Expose
     * @Encrypted
     */
    private $phoneNumber;


    /**
     * @ORM\ManyToOne(targetEntity="States", inversedBy="agencies")
     * @Assert\NotBlank(groups={"Default"})
     * @Annotation\Expose
     */
    private $state;


    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="product_image", fileNameProperty="imageName")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     * @Annotation\Expose
     * @Encrypted
     */
    private $imageName;


    /**
     * @ORM\ManyToMany(targetEntity="Services", inversedBy="agencies")
     * @Annotation\Expose
     */
    private $services;


    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="agency")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity="CareGoals", mappedBy="setByAgency")
     */
    private $careGoals;


    /**
     * @ORM\OneToMany(targetEntity="Pca", mappedBy="agency")
     */
    private $pcas;


    /**
     * @ORM\OneToMany(targetEntity="Recipient", mappedBy="agency")
     */
    private $recipients;


    /**
     * @ORM\OneToMany(targetEntity="EmailSettings", mappedBy="agency")
     */
    private $emails;


    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    private $updatedAt;


    /**
     * @var RegistrationData
     */
    private $registrationData;


    /**
     * @var Integer
     * @ORM\Column(name="plan", type="integer", nullable=true)
     */
    private $plan;


    /**
     * @var String
     * @ORM\Column(name="stripe_customer_id", type="string", length=255, nullable=true)
     * @Encrypted
     */
    private $stripeCustomerId;


    /**
     * @var String
     * @ORM\Column(name="stripe_source", type="string", length=255, nullable=true)
     * @Encrypted
     */
    private $stripeSource;


    /**
     * @var String
     * @ORM\Column(name="stripe_subscription_id", type="string", length=255, nullable=true)
     * @Encrypted
     */
    private $stripeSubscriptionId;


    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }


    public function addUnarchivedService(\Homecare\HomecareBundle\Entity\Services $services)
    {
        $this->addService($services);
    }

    public function removeUnarchivedService(\Homecare\HomecareBundle\Entity\Services $services)
    {
        $this->removeService($services);
    }


    public function getUnarchivedServices()
    {
        $result = new ArrayCollection();
        foreach ($this->getServices() as $service) {
            if ($service->getArchived() == 0) {
                $result->add($service);
            }
        }

        return $result;
    }


    public function __construct()
    {
        $this->careGoals  = new ArrayCollection();
        $this->users      = new ArrayCollection();
        $this->recipients = new ArrayCollection();
        $this->emails     = new ArrayCollection();
        $this->services   = new ArrayCollection();
    }

    /**
     * this method is necessary because we need to lowercase any encrypted properties for future database queries
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */

    public function lowercaseProperties()
    {
        $this->setAgencyName(strtolower($this->getAgencyName()));
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set agencyName
     *
     * @param string $agencyName
     *
     * @return Agency
     */
    public function setAgencyName($agencyName)
    {
        $this->agencyName = $agencyName;

        return $this;
    }

    /**
     * Get agencyName
     *
     * @return string
     */
    public function getAgencyName()
    {
        return $this->agencyName;
    }


    /**
     * Add users
     *
     * @param \Homecare\HomecareBundle\Entity\User $users
     *
     * @return Agency
     */
    public function addUser(\Homecare\HomecareBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Homecare\HomecareBundle\Entity\User $users
     */
    public function removeUser(\Homecare\HomecareBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }


    public function __toString()
    {
        return $this->getAgencyName();
    }


    /**
     * Add careGoals
     *
     * @param \Homecare\HomecareBundle\Entity\CareGoals $careGoals
     *
     * @return Agency
     */
    public function addCareGoal(\Homecare\HomecareBundle\Entity\CareGoals $careGoals)
    {
        $this->careGoals[] = $careGoals;

        return $this;
    }

    /**
     * Remove careGoals
     *
     * @param \Homecare\HomecareBundle\Entity\CareGoals $careGoals
     */
    public function removeCareGoal(\Homecare\HomecareBundle\Entity\CareGoals $careGoals)
    {
        $this->careGoals->removeElement($careGoals);
    }

    /**
     * Get careGoals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCareGoals()
    {
        return $this->careGoals;
    }


    /**
     * Add pcas
     *
     * @param \Homecare\HomecareBundle\Entity\Pca $pcas
     *
     * @return Agency
     */
    public function addPca(\Homecare\HomecareBundle\Entity\Pca $pcas)
    {
        $this->pcas[] = $pcas;

        return $this;
    }

    /**
     * Remove pcas
     *
     * @param \Homecare\HomecareBundle\Entity\Pca $pcas
     */
    public function removePca(\Homecare\HomecareBundle\Entity\Pca $pcas)
    {
        $this->pcas->removeElement($pcas);
    }

    /**
     * Get pcas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPcas()
    {
        return $this->pcas;
    }

    /**
     * Add recipients
     *
     * @param \Homecare\HomecareBundle\Entity\Recipient $recipients
     *
     * @return Agency
     */
    public function addRecipient(\Homecare\HomecareBundle\Entity\Recipient $recipients)
    {
        $this->recipients[] = $recipients;

        return $this;
    }

    /**
     * Remove recipients
     *
     * @param \Homecare\HomecareBundle\Entity\Recipient $recipients
     */
    public function removeRecipient(\Homecare\HomecareBundle\Entity\Recipient $recipients)
    {
        $this->recipients->removeElement($recipients);
    }

    /**
     * Get recipients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecipients()
    {
        return $this->recipients;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Agency
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set state
     *
     * @param \Homecare\HomecareBundle\Entity\States $state
     *
     * @return Agency
     */
    public function setState(\Homecare\HomecareBundle\Entity\States $state = null)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return \Homecare\HomecareBundle\Entity\States
     */
    public function getState()
    {
        return $this->state;
    }


    /**
     * Add emails
     *
     * @param \Homecare\HomecareBundle\Entity\EmailSettings $emails
     *
     * @return Agency
     */
    public function addEmail(\Homecare\HomecareBundle\Entity\EmailSettings $emails)
    {
        $this->emails[] = $emails;

        return $this;
    }

    /**
     * Remove emails
     *
     * @param \Homecare\HomecareBundle\Entity\EmailSettings $emails
     */
    public function removeEmail(\Homecare\HomecareBundle\Entity\EmailSettings $emails)
    {
        $this->emails->removeElement($emails);
    }

    /**
     * Get emails
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * Set imageName
     *
     * @param string $imageName
     *
     * @return Agency
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get imageName
     *
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Agency
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add service
     *
     * @param \Homecare\HomecareBundle\Entity\Services $service
     *
     * @return Agency
     */
    public function addService(\Homecare\HomecareBundle\Entity\Services $service)
    {
        $this->services[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param \Homecare\HomecareBundle\Entity\Services $service
     */
    public function removeService(\Homecare\HomecareBundle\Entity\Services $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @return string
     */
    public function getWeekStartDate()
    {
        return $this->weekStartDate;
    }

    /**
     * @param string $weekStartDate
     */
    public function setWeekStartDate($weekStartDate)
    {
        $this->weekStartDate = $weekStartDate;
    }


    /**
     * Set startTime
     *
     * @param array $startTime
     *
     * @return Agency
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return array
     */
    public function getStartTime()
    {
        if ( ! $this->startTime) {
            return ['hour' => '12', 'minute' => '00', 'period' => 'AM'];
        }


        return $this->startTime;
    }

    /**
     * @return RegistrationData
     */
    public function getRegistrationData()
    {
        return $this->registrationData;
    }

    /**
     * @param RegistrationData $registrationData
     */
    public function setRegistrationData($registrationData)
    {
        $this->registrationData = $registrationData;
    }

    /**
     * @return int
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @param int $plan
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;
    }

    /**
     * @return String
     */
    public function getStripeCustomerId()
    {
        return $this->stripeCustomerId;
    }

    /**
     * @param String $stripeCustomerId
     */
    public function setStripeCustomerId($stripeCustomerId)
    {
        $this->stripeCustomerId = $stripeCustomerId;
    }

    /**
     * @return String
     */
    public function getStripeSource()
    {
        return $this->stripeSource;
    }

    /**
     * @param String $stripeSource
     */
    public function setStripeSource($stripeSource)
    {
        $this->stripeSource = $stripeSource;
    }

    /**
     * @return String
     */
    public function getStripeSubscriptionId()
    {
        return $this->stripeSubscriptionId;
    }

    /**
     * @param String $stripeSubscriptionId
     */
    public function setStripeSubscriptionId($stripeSubscriptionId)
    {
        $this->stripeSubscriptionId = $stripeSubscriptionId;
    }


    /**
     * @param $agencyName
     * @param $phoneNumber
     * @param States $state
     * @param $plan
     *
     * @return $this
     */
    public function initializeAgency($agencyName, $phoneNumber, States $state, $plan)
    {
        $this->agencyName  = $agencyName;
        $this->phoneNumber = $phoneNumber;
        $this->state       = $state;
        $this->plan        = $plan;

        return $this;
    }


}
