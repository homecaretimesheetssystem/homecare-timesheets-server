<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;

/**
 * TimeIn
 *
 * @ORM\Table(name="time_in")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\TimeInRepository")
 * @Annotation\ExclusionPolicy("all")
 */
class TimeIn
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     * @SerializedName("id")
     * @Annotation\Expose
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeIn", type="datetime", nullable=true)
     * @Type("DateTime")
     * @SerializedName("timeIn")
     * @Annotation\Expose
     */
    private $timeIn;

    /**
     * @var integer
     *
     * @ORM\Column(name="estimatedShiftLength", type="bigint", nullable=true)
     * @Type("integer")
     * @SerializedName("estimatedShiftLength")
     * @Annotation\Expose
     */
    private $estimatedShiftLength;

    /**
     * @var string
     *
     * @ORM\Column(name="timeInAddress", type="string", length=255, nullable=true)
     * @Type("string")
     * @SerializedName("timeInAddress")
     * @Annotation\Expose
     * @Encrypted
     */
    private $timeInAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=255, nullable=true)
     * @Type("string")
     * @SerializedName("latitude")
     * @Annotation\Expose
     * @Encrypted
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length=255, nullable=true)
     * @Type("string")
     * @SerializedName("longitude")
     * @Annotation\Expose
     * @Encrypted
     */
    private $longitude;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeInPictureTime", type="datetime", nullable=true)
     * @Type("DateTime")
     * @SerializedName("timeInPictureTime")
     * @Annotation\Expose
     */
    private $timeInPictureTime;


    /**
     * @ORM\OneToOne(targetEntity="SessionData", mappedBy="timeIn")
     * @Type("Homecare\HomecareBundle\Entity\SessionData")
     * @SerializedName("sessionData")
     */
    private $sessionData;


    public function __toString()
    {
        return $this->getTimeInAddress();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timeIn
     *
     * @param \DateTime $timeIn
     *
     * @return TimeIn
     */
    public function setTimeIn($timeIn)
    {
        $this->timeIn = $timeIn;

        return $this;
    }

    /**
     * Get timeIn
     *
     * @return \DateTime
     */
    public function getTimeIn()
    {
        return $this->timeIn;
    }


    /**
     * Set timeInAddress
     *
     * @param string $timeInAddress
     *
     * @return TimeIn
     */
    public function setTimeInAddress($timeInAddress)
    {
        $this->timeInAddress = $timeInAddress;

        return $this;
    }

    /**
     * Get timeInAddress
     *
     * @return string
     */
    public function getTimeInAddress()
    {
        return $this->timeInAddress;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return TimeIn
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return TimeIn
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set timeInPictureTime
     *
     * @param \DateTime $timeInPictureTime
     *
     * @return TimeIn
     */
    public function setTimeInPictureTime($timeInPictureTime)
    {
        $this->timeInPictureTime = $timeInPictureTime;

        return $this;
    }

    /**
     * Get timeInPictureTime
     *
     * @return \DateTime
     */
    public function getTimeInPictureTime()
    {
        return $this->timeInPictureTime;
    }

    /**
     * Set estimatedShiftLength
     *
     * @param integer $estimatedShiftLength
     *
     * @return TimeIn
     */
    public function setEstimatedShiftLength($estimatedShiftLength)
    {
        $this->estimatedShiftLength = $estimatedShiftLength;

        return $this;
    }

    /**
     * Get estimatedShiftLength
     *
     * @return integer
     */
    public function getEstimatedShiftLength()
    {
        return $this->estimatedShiftLength;
    }


    /**
     * Set sessionData
     *
     * @param \Homecare\HomecareBundle\Entity\SessionData $sessionData
     *
     * @return TimeIn
     */
    public function setSessionData(\Homecare\HomecareBundle\Entity\SessionData $sessionData = null)
    {
        $this->sessionData = $sessionData;

        return $this;
    }

    /**
     * Get sessionData
     *
     * @return \Homecare\HomecareBundle\Entity\SessionData
     */
    public function getSessionData()
    {
        return $this->sessionData;
    }
}
