<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 10/24/16
 * Time: 6:13 PM
 */

namespace Homecare\HomecareBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use Homecare\HomecareBundle\Entity\User;


/**
 * Class DeviceRepository
 * @package Homecare\HomecareBundle\Entity\Repository
 */
class DeviceRepository extends EntityRepository
{


    /**
     * This method returns all the devices for a given user
     *
     *
     * @param User $user
     *
     * @return array
     */
    public function getAllByUser(User $user)
    {


        $qb = $this->createQueryBuilder('device');

        return $qb
            ->where('device.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();

    }


}