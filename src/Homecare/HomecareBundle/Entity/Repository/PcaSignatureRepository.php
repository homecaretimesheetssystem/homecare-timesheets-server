<?php

namespace Homecare\HomecareBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * PcaSignatureRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PcaSignatureRepository extends EntityRepository
{
}
