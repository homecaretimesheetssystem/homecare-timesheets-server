<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 10/25/16
 * Time: 9:48 PM
 */

namespace Homecare\HomecareBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use Homecare\HomecareBundle\Entity\Pca;
use Homecare\HomecareBundle\Entity\Request;

/**
 * Class RsvpRepository
 * @package Homecare\HomecareBundle\Entity\Repository
 */
class RsvpRepository extends EntityRepository
{


    /**
     * This method returns all the rsvps for a given request
     *
     *
     * @param Request $request
     *
     * @return array
     */
    public function getAllByRequest(Request $request)
    {


        $qb = $this->createQueryBuilder('rsvp');

        return $qb
            ->where('rsvp.request = :request')
            ->setParameter('request', $request)
            ->getQuery()
            ->getResult();

    }


    /**
     * This method returns all the requests for a given Pca
     *
     *
     * @param Pca $pca
     *
     * @return array
     *
     */
    public function getAllByPca(Pca $pca)
    {


        $qb = $this->createQueryBuilder('rsvp');

        return $qb
            ->where('rsvp.pca = :pca')
            ->setParameter('pca', $pca)
            ->getQuery()
            ->getResult();

    }

}