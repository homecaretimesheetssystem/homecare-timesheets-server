<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 10/18/16
 * Time: 12:02 PM
 */

namespace Homecare\HomecareBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;

/**
 * Class QualificationsRepository
 * @package Homecare\HomecareBundle\Entity\Repository
 */
class QualificationsRepository extends EntityRepository
{

}