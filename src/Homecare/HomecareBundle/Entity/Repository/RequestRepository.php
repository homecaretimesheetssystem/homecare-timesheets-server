<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 10/24/16
 * Time: 6:13 PM
 */

namespace Homecare\HomecareBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use Homecare\HomecareBundle\Entity\Agency;
use Homecare\HomecareBundle\Entity\Pca;
use Homecare\HomecareBundle\Entity\Recipient;


/**
 * Class RequestRepository
 * @package Homecare\HomecareBundle\Entity\Repository
 */
class RequestRepository extends EntityRepository
{


    /**
     * This method returns all the requests for all the counties that a given Pca
     * works in and the agency that the pca works in
     *
     * @param Pca $pca
     *
     * @return array
     *
     */
    public function getAcceptedByPca(Pca $pca)
    {

        $qb = $this->createQueryBuilder('request');


        $midnightToday = (new \DateTime())->setTime(0, 0);


        $requests = $qb
            ->join('request.rsvps', 'r')
            ->where('r.pca = :pca')
            ->andWhere('r.accepted = 1')
            ->andWhere('request.startShiftTime > :midnightToday')
            ->setParameter('pca', $pca)
            ->setParameter('midnightToday', $midnightToday, \Doctrine\DBAL\Types\Type::DATETIME)
            ->addOrderBy('request.startShiftTime', 'ASC')
            ->getQuery()
            ->getResult();


        // remove any requests that have a time in

        $responseArray = [];
        foreach ($requests as $request) {

            $rsvp = $request->getRsvps()[0];

            if ($rsvp) {
                if ($session = $rsvp->getSessionData()) {

                    if ($session && ! $session->getTimeIn()) {
                        array_push($responseArray, $request);
                    }
                }
            }

        }

        return $responseArray;


    }


    /**
     * This method returns all the requests for all the counties that a given Pca
     * works in and the agency that the pca works in
     * 1. Make sure the PCA covers the county the recipient is in
     * 2. make sure the PCA belongs to the agency the recipient is in
     * 3.
     *
     * @param Pca $pca
     *
     * @return array
     * @throws \Exception
     */
    public function getAvailableForPca(Pca $pca)
    {

        $currentTime = new \DateTime();

        $query = $this->createQueryBuilder('request')
                      ->join('request.recipient', 'r')
                      ->leftJoin('request.qualifications', 'q')
                      ->leftJoin('request.rsvps', 'rs');


        if (count($pca->getCounties()) == 0) {
            throw new \Exception("Counties have not been set for the PCA yet");
        }

        $query->where('r.county IN (:counties)')
              ->setParameter('counties', $pca->getCounties());

        // shouldn't need this check cause you have the default set to 0
        /*
        if ( ! $pca->getYearsOfExperience()) {
            throw new \Exception("Years of experience have not been set for the PCA");
        }
        */

        $query->andWhere('request.yearsOfExperience <= :pcaYearsOfExperience')
              ->setParameter('pcaYearsOfExperience', $pca->getYearsOfExperience());


        // you don't need to throw an exception if the pca does not have any qualifications
        /*
        if (count($pca->getQualifications()) == 0) {
            throw new \Exception("Qualifications have not been set for the PCA");
        }
        */


        if ( ! $pca->getAgency()) {
            throw new \Exception(
                "This pca does not appear to be assigned to an agency."
            );
        }

        $query->andWhere('r.agency = :agency')
              ->setParameter('agency', $pca->getAgency())
              ->andWhere(':currentTime < request.endShiftTime')
              ->setParameter('currentTime', $currentTime, \Doctrine\DBAL\Types\Type::DATETIME);


        $requests = $query->addOrderBy('request.startShiftTime', 'ASC')
                          ->getQuery()
                          ->getResult();


        $pcaQualifications = $pca->getQualifications();
        $responseArray     = [];
        foreach ($requests as $request) {
            $requestQualifications = $request->getQualifications();

            // if there are no qualifications attached to the request
            if (count($requestQualifications) == 0) {
                array_push($responseArray, $request);
                continue;
            }


            // get the total number of matched qualifications
            $totalMatches = 0;
            foreach ($requestQualifications as $requestQualification) {
                foreach ($pcaQualifications as $pcaQualification) {
                    if ($pcaQualification == $requestQualification) {
                        $totalMatches++;
                    }
                }
            }

            if ($totalMatches >= 1) {
                array_push($responseArray, $request);
            }

        }


        return $responseArray;

    }


    /**
     * Basic utility function to remove object from array of objects
     * todo refactor this into a utility service of some sort
     *
     * @param array $array
     * @param $value
     * @param bool $strict
     *
     */
    private function unsetValue(array &$array, $value, $strict = true)
    {

        if (($key = array_search($value, $array, $strict)) !== false) {
            unset($array[$key]);
        }
    }


    public function getRequestsByRecipient(Recipient $recipient)
    {

        $qb          = $this->createQueryBuilder('request')
                            ->where('request.recipient = :recipient')
                            ->setParameter('recipient', $recipient);
        $currentTime = new \DateTime();

        $requests = $qb->andWhere(':currentTime < request.endShiftTime')
                       ->setParameter('currentTime', $currentTime, \Doctrine\DBAL\Types\Type::DATETIME)
                       ->addOrderBy('request.startShiftTime', 'ASC')
                       ->getQuery()
                       ->getResult();


        // remove any requests that have a time in

        $responseArray = [];
        foreach ($requests as $request) {

            $rsvp = $request->getRsvps()[0];

            if ($rsvp) {
                if ($session = $rsvp->getSessionData()) {

                    // if there is not a time in then store the request still
                    if ($session && ! $session->getTimeIn()) {
                        array_push($responseArray, $request);
                        continue;
                    }

                    if ($session && $session->getTimeIn()) {
                        // if there is a time in then just skip the iteration
                        continue;
                    }
                }
            }

            // default to pushing all the reqeusts onto the array
            array_push($responseArray, $request);

        }

        return $responseArray;


    }


}