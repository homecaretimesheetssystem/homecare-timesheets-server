<?php

namespace Homecare\HomecareBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * VisitRatiosRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class VisitRatiosRepository extends EntityRepository
{
}
