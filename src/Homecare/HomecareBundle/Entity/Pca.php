<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Pca
 *
 * @ORM\Table(name="pca")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\PcaRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Annotation\ExclusionPolicy("all")
 */
class Pca
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Annotation\Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pca_name", type="string", length=254, nullable=true)
     * @Encrypted
     */
    private $pcaName;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     * @Annotation\Expose
     * @Encrypted
     * @Assert\NotBlank()
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255)
     * @Annotation\Expose
     * @Encrypted
     * @Assert\NotBlank()
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="umpi", type="string", length=255, nullable=true)
     * @Annotation\Expose
     * @Encrypted
     * @Assert\NotBlank()
     */
    private $umpi;


    /**
     * @var string
     *
     * @ORM\Column(name="company_assigned_id", type="string", length=255, nullable=true)
     * @Annotation\Expose
     * @Encrypted
     */
    private $companyAssignedId;


    /**
     * @var string
     *
     * @ORM\Column(name="phoneNumber", type="string", length=255, nullable=true)
     * @Encrypted
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^[+]?([\d]{0,3})?[\(\.\-\s]?([\d]{3})[\)\.\-\s]*([\d]{3})[\.\-\s]?([\d]{4})$/", message="Phone number must be a valid phone number")
     */
    private $phoneNumber;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    private $updatedAt;


    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="pca", cascade={"persist"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    private $user;


    /**
     * @ORM\ManyToOne(targetEntity="Agency", inversedBy="pcas")
     */
    private $agency;


    /**
     * @ORM\OneToMany(targetEntity="SessionData", mappedBy="pca")
     */
    private $sessionData;


    /**
     * @ORM\OneToMany(targetEntity="RecipientPca", mappedBy="pca",orphanRemoval=true)
     */
    private $recipientPca;


    /**
     * @var string
     *
     * @ORM\Column(name="profile_photo", type="string", length=255, nullable=true)
     * @Annotation\Expose
     * @Type("string")
     * @SerializedName("profilePhoto")
     */
    private $profilePhoto;

    /**
     * @var string
     *
     * @ORM\Column(name="home_address", type="string", length=255, nullable=true)
     * @Type("string")
     * @Annotation\Expose
     * @Encrypted
     */
    private $homeAddress;


    /**
     * @ORM\ManyToMany(targetEntity="Qualifications", inversedBy="pcas", cascade={"persist"})
     * @Annotation\Expose
     */
    private $qualifications;

    /**
     * This is a bool switch which determines whether or not the pca is available for
     * on demand care
     *
     * @var boolean
     * @ORM\Column(name="available", type="boolean", nullable=true)
     * @Annotation\Expose
     */
    private $available = false;


    /**
     * @ORM\OneToMany(targetEntity="Rsvp", mappedBy="pca")
     */
    private $rsvps;


    /**
     * @ORM\ManyToMany(targetEntity="Homecare\HomecareBundle\Entity\Services", inversedBy="pcas")
     * @Annotation\Expose
     */
    private $services;


    /**
     * List of all the qualifications available
     * @var array
     * @Annotation\Expose
     */
    private $qualificationList;


    /**
     * @ORM\Column(name="years_of_experience", type="integer", nullable=true)
     * @Annotation\Expose
     * @var integer
     * @Assert\NotBlank()
     */
    private $yearsOfExperience = 0;


    /**
     *
     * 0 = no preference
     * 1 = female
     * 2 = male
     *
     * @var integer
     * @ORM\Column(name="gender", type="integer", nullable=true)
     * @Type("integer")
     * @Annotation\Expose
     * @Assert\NotBlank()
     */
    private $gender;


    /**
     * @ORM\ManyToMany(targetEntity="Homecare\HomecareBundle\Entity\County", inversedBy="pcas")
     * @Annotation\Expose
     */
    private $counties;


    /**
     * @ORM\OneToMany(targetEntity="Homecare\HomecareBundle\Entity\Verification", mappedBy="pca")
     */
    private $archivedVerifications;


    public function __toString()
    {
        return $this->getFirstName();
    }


    public function __construct()
    {
        $this->recipientPca = new ArrayCollection();
        //this is the new timesheet row you are adding
        //delete the timesheets one later
        $this->sessionData           = new ArrayCollection();
        $this->qualifications        = new ArrayCollection();
        $this->rsvps                 = new ArrayCollection();
        $this->services              = new ArrayCollection();
        $this->qualificationList     = new ArrayCollection();
        $this->counties              = new ArrayCollection();
        $this->archivedVerifications = new ArrayCollection();
    }


    /**
     * this method allows the form builder to return multiple properties for the fields names
     * @return string
     */
    function getUniqueName()
    {
        return sprintf('%s %s - UMPI# %s', $this->firstName, $this->lastName, $this->umpi);
    }


    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateTimestamps()
    {

        $this->setUpdatedAt(new \DateTime());

        if ($this->getCreatedAt() == null) {
            $this->setCreatedAt(new \DateTime());
        }

    }


    /**
     * this method is necessary because we need to lowercase any encrypted properties for future database queries
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function lowercaseProperties()
    {
        $this->setFirstName(strtolower($this->getFirstName()));
        $this->setLastName(strtolower($this->getLastName()));
        $this->setPcaName(strtolower($this->getFirstName().' '.$this->getLastName()));
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Pca
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Pca
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set umpi
     *
     * @param string $umpi
     *
     * @return Pca
     */
    public function setUmpi($umpi)
    {
        $this->umpi = $umpi;

        return $this;
    }

    /**
     * Get umpi
     *
     * @return string
     */
    public function getUmpi()
    {
        return $this->umpi;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Pca
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Pca
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


    /**
     * Set agency
     *
     * @param \Homecare\HomecareBundle\Entity\Agency $agency
     *
     * @return Pca
     */
    public function setAgency(\Homecare\HomecareBundle\Entity\Agency $agency = null)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get agency
     *
     * @return \Homecare\HomecareBundle\Entity\Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }


    /**
     * Add sessionData
     *
     * @param \Homecare\HomecareBundle\Entity\SessionData $sessionData
     *
     * @return Pca
     */
    public function addSessionDatum(\Homecare\HomecareBundle\Entity\SessionData $sessionData)
    {
        $this->sessionData[] = $sessionData;

        return $this;
    }

    /**
     * Remove sessionData
     *
     * @param \Homecare\HomecareBundle\Entity\SessionData $sessionData
     */
    public function removeSessionDatum(\Homecare\HomecareBundle\Entity\SessionData $sessionData)
    {
        $this->sessionData->removeElement($sessionData);
    }

    /**
     * Get sessionData
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSessionData()
    {
        return $this->sessionData;
    }


    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Pca
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }


    /**
     * Add recipientPca
     *
     * @param \Homecare\HomecareBundle\Entity\RecipientPca $recipientPca
     *
     * @return Pca
     */
    public function addRecipientPca(\Homecare\HomecareBundle\Entity\RecipientPca $recipientPca)
    {
        $this->recipientPca[] = $recipientPca;

        return $this;
    }

    /**
     * Remove recipientPca
     *
     * @param \Homecare\HomecareBundle\Entity\RecipientPca $recipientPca
     */
    public function removeRecipientPca(\Homecare\HomecareBundle\Entity\RecipientPca $recipientPca)
    {
        $this->recipientPca->removeElement($recipientPca);
    }

    /**
     * Get recipientPca
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecipientPca()
    {
        return $this->recipientPca;
    }


    /**
     * Set pcaName
     *
     * @param string $pcaName
     *
     * @return Pca
     */
    public function setPcaName($pcaName)
    {
        $this->pcaName = $pcaName;

        return $this;
    }

    /**
     * Get pcaName
     *
     * @return string
     */
    public function getPcaName()
    {
        return $this->pcaName;
    }

    /**
     * Set companyAssignedId
     *
     * @param string $companyAssignedId
     *
     * @return Pca
     */
    public function setCompanyAssignedId($companyAssignedId)
    {
        $this->companyAssignedId = $companyAssignedId;

        return $this;
    }

    /**
     * Get companyAssignedId
     *
     * @return string
     */
    public function getCompanyAssignedId()
    {
        return $this->companyAssignedId;
    }

    /**
     * Add qualification
     *
     * @param \Homecare\HomecareBundle\Entity\Qualifications $qualification
     *
     * @return Pca
     */
    public function addQualification(\Homecare\HomecareBundle\Entity\Qualifications $qualification)
    {
        $this->qualifications[] = $qualification;

        $qualification->addPca($this);

        return $this;
    }

    /**
     * Remove qualification
     *
     * @param \Homecare\HomecareBundle\Entity\Qualifications $qualification
     */
    public function removeQualification(\Homecare\HomecareBundle\Entity\Qualifications $qualification)
    {
        $this->qualifications->removeElement($qualification);
    }

    /**
     * Get qualifications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQualifications()
    {
        return $this->qualifications;
    }

    /**
     * Set homeAddress
     *
     * @param string $homeAddress
     *
     * @return Pca
     */
    public function setHomeAddress($homeAddress)
    {
        $this->homeAddress = $homeAddress;

        return $this;
    }

    /**
     * Get homeAddress
     *
     * @return string
     */
    public function getHomeAddress()
    {
        return $this->homeAddress;
    }

    /**
     * Set available
     *
     * @param boolean $available
     *
     * @return Pca
     */
    public function setAvailable($available)
    {
        $this->available = $available;

        return $this;
    }

    /**
     * Get available
     *
     * @return boolean
     */
    public function getAvailable()
    {
        return $this->available;
    }


    /**
     * Add rsvp
     *
     * @param \Homecare\HomecareBundle\Entity\Rsvp $rsvp
     *
     * @return Pca
     */
    public function addRsvp(\Homecare\HomecareBundle\Entity\Rsvp $rsvp)
    {
        $this->rsvps[] = $rsvp;

        return $this;
    }

    /**
     * Remove rsvp
     *
     * @param \Homecare\HomecareBundle\Entity\Rsvp $rsvp
     */
    public function removeRsvp(\Homecare\HomecareBundle\Entity\Rsvp $rsvp)
    {
        $this->rsvps->removeElement($rsvp);
    }

    /**
     * Get rsvps
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRsvps()
    {
        return $this->rsvps;
    }

    /**
     * Set user
     *
     * @param \Homecare\HomecareBundle\Entity\User $user
     *
     * @return Pca
     */
    public function setUser(\Homecare\HomecareBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Homecare\HomecareBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add service
     *
     * @param \Homecare\HomecareBundle\Entity\Services $service
     *
     * @return Pca
     */
    public function addService(\Homecare\HomecareBundle\Entity\Services $service)
    {
        $this->services[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param \Homecare\HomecareBundle\Entity\Services $service
     */
    public function removeService(\Homecare\HomecareBundle\Entity\Services $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @return array
     */
    public function getQualificationList()
    {
        return $this->qualificationList;
    }

    /**
     * @param array $qualificationList
     */
    public function setQualificationList($qualificationList)
    {
        $this->qualificationList = $qualificationList;
    }


    /**
     * Set yearsOfExperience
     *
     * @param integer $yearsOfExperience
     *
     * @return Pca
     */
    public function setYearsOfExperience($yearsOfExperience)
    {
        $this->yearsOfExperience = $yearsOfExperience;

        return $this;
    }

    /**
     * Get yearsOfExperience
     *
     * @return integer
     */
    public function getYearsOfExperience()
    {
        return $this->yearsOfExperience;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return Pca
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }


    /**
     * Add archivedVerification
     *
     * @param \Homecare\HomecareBundle\Entity\Verification $archivedVerification
     *
     * @return Pca
     */
    public function addArchivedVerification(\Homecare\HomecareBundle\Entity\Verification $archivedVerification)
    {
        $this->archivedVerifications[] = $archivedVerification;

        return $this;
    }

    /**
     * Remove archivedVerification
     *
     * @param \Homecare\HomecareBundle\Entity\Verification $archivedVerification
     */
    public function removeArchivedVerification(\Homecare\HomecareBundle\Entity\Verification $archivedVerification)
    {
        $this->archivedVerifications->removeElement($archivedVerification);
    }

    /**
     * Get archivedVerifications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArchivedVerifications()
    {
        return $this->archivedVerifications;
    }


    /**
     * Add county
     *
     * @param \Homecare\HomecareBundle\Entity\County $county
     *
     * @return Pca
     */
    public function addCounty(\Homecare\HomecareBundle\Entity\County $county)
    {
        $this->counties[] = $county;

        return $this;
    }

    /**
     * Remove county
     *
     * @param \Homecare\HomecareBundle\Entity\County $county
     */
    public function removeCounty(\Homecare\HomecareBundle\Entity\County $county)
    {
        $this->counties->removeElement($county);
    }

    /**
     * Get counties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCounties()
    {
        return $this->counties;
    }

    /**
     * @return mixed
     */
    public function getProfilePhoto()
    {
        return $this->profilePhoto;
    }

    /**
     * @param mixed $profilePhoto
     */
    public function setProfilePhoto($profilePhoto)
    {
        $this->profilePhoto = $profilePhoto;
    }


}
