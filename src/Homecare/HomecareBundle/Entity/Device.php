<?php

namespace Homecare\HomecareBundle\Entity;

use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;

use Doctrine\ORM\Mapping as ORM;

/**
 * Device
 *
 * @ORM\Table(name="device")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\DeviceRepository")
 * @Annotation\ExclusionPolicy("all")
 */
class Device
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isAndroid", type="boolean")
     * @Annotation\Expose
     */
    private $isAndroid;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isIos", type="boolean")
     * @Annotation\Expose
     */
    private $isIos;

    /**
     * If the Ios or Andriod device has blocked or disabled
     * push notifications from being sent, you should set this value to false.
     *
     * @var boolean
     *
     * @ORM\Column(name="isActive", type="boolean")
     * @Annotation\Expose
     */
    private $isActive = true;

    /**
     * @var string
     *
     * @ORM\Column(name="deviceToken", type="string", length=255)
     * @Annotation\Expose
     */
    private $deviceToken;


    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="devices")
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isAndroid
     *
     * @param boolean $isAndroid
     *
     * @return Device
     */
    public function setIsAndroid($isAndroid)
    {
        $this->isAndroid = $isAndroid;

        return $this;
    }

    /**
     * Get isAndroid
     *
     * @return boolean
     */
    public function getIsAndroid()
    {
        return $this->isAndroid;
    }

    /**
     * Set isIos
     *
     * @param boolean $isIos
     *
     * @return Device
     */
    public function setIsIos($isIos)
    {
        $this->isIos = $isIos;

        return $this;
    }

    /**
     * Get isIos
     *
     * @return boolean
     */
    public function getIsIos()
    {
        return $this->isIos;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Device
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set deviceToken
     *
     * @param string $deviceToken
     *
     * @return Device
     */
    public function setDeviceToken($deviceToken)
    {
        $this->deviceToken = $deviceToken;

        return $this;
    }

    /**
     * Get deviceToken
     *
     * @return string
     */
    public function getDeviceToken()
    {
        return $this->deviceToken;
    }

    /**
     * Set user
     *
     * @param \Homecare\HomecareBundle\Entity\User $user
     *
     * @return Device
     */
    public function setUser(\Homecare\HomecareBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Homecare\HomecareBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
