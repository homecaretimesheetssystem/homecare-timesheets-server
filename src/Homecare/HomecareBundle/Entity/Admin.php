<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Admin
 *
 * @ORM\Table(name="admin")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\AdminRepository")
 */
class Admin
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     */
    private $firstName;


    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255)
     */
    private $lastName;
		
		
		/**
		* @ORM\OneToMany(targetEntity="CareGoals", mappedBy="setByAdmin")
		*/
		private $careGoals;
		
		
    /**
     * @ORM\OneToMany(targetEntity="User", mappedBy="admin")
		 */
		private $users;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->careGoals = new \Doctrine\Common\Collections\ArrayCollection();
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add careGoals
     *
     * @param \Homecare\HomecareBundle\Entity\CareGoals $careGoals
     * @return Admin
     */
    public function addCareGoal(\Homecare\HomecareBundle\Entity\CareGoals $careGoals)
    {
        $this->careGoals[] = $careGoals;

        return $this;
    }

    /**
     * Remove careGoals
     *
     * @param \Homecare\HomecareBundle\Entity\CareGoals $careGoals
     */
    public function removeCareGoal(\Homecare\HomecareBundle\Entity\CareGoals $careGoals)
    {
        $this->careGoals->removeElement($careGoals);
    }

    /**
     * Get careGoals
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCareGoals()
    {
        return $this->careGoals;
    }

    /**
     * Add users
     *
     * @param \Homecare\HomecareBundle\Entity\User $users
     * @return Admin
     */
    public function addUser(\Homecare\HomecareBundle\Entity\User $users)
    {
        $users->setAdmin( $this );

        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Homecare\HomecareBundle\Entity\User $users
     */
    public function removeUser(\Homecare\HomecareBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }
		
		
		public function __toString() {
		return $this->firstName;
		}
		

		

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Admin
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Admin
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }
}
