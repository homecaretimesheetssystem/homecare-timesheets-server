<?php

namespace Homecare\HomecareBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;

/**
 * Services
 *
 * @ORM\Table(name="rsvp")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\RsvpRepository")
 * @Annotation\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks()
 */
class Rsvp
{

    //use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Annotation\Expose
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Pca", inversedBy="rsvps")
     * @SerializedName("pca")
     * @Annotation\Expose
     */
    private $pca;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="eta", type="datetime", nullable=true)
     * @Type("DateTime")
     * @SerializedName("eta")
     *
     * @Annotation\Expose
     */
    private $eta;


    /**
     * @var boolean
     *
     * @ORM\Column(name="accepted", type="boolean", nullable=true)
     * @Type("boolean")
     * @SerializedName("accepted")
     * @Annotation\Expose
     */
    private $accepted;


    /**
     * @var boolean
     *
     * @ORM\Column(name="declined", type="boolean", nullable=true)
     * @Type("boolean")
     * @SerializedName("declined")
     * @Annotation\Expose
     */
    private $declined;


    /**
     * @var string
     *
     * @ORM\Column(name="cancellation_reason", type="text", nullable=true)
     * @Type("string")
     * @SerializedName("cancellationReason")
     * @Annotation\Expose
     */
    private $cancellationReason;


    /**
     * @ORM\ManyToOne(targetEntity="Request", inversedBy="rsvps")
     * @SerializedName("request")
     */
    private $request;


    /**
     * When an RSVP is created a sessionData object gets created
     * If an RSVP gets deleted all the data associated with it gets
     * deleted as well. (sessionData, timesheets, etc)
     *
     * @ORM\OneToOne(targetEntity="SessionData", inversedBy="rsvp")
     */
    private $sessionData;


    /**
     * @var
     * @Annotation\Expose
     */
    private $sessionDataId;
    

    /**
     * @ORM\PostLoad
     */
    public function setSessionDataId()
    {

        if ($this->getSessionData()) {
            $this->sessionDataId = $this->getSessionData()->getId();
        }

    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eta
     *
     * @param \DateTime $eta
     *
     * @return Rsvp
     */
    public function setEta($eta)
    {
        $this->eta = $eta;

        return $this;
    }

    /**
     * Get eta
     *
     * @return \DateTime
     */
    public function getEta()
    {
        return $this->eta;
    }

    /**
     * Set accepted
     *
     * @param boolean $accepted
     *
     * @return Rsvp
     */
    public function setAccepted($accepted)
    {
        $this->accepted = $accepted;

        return $this;
    }

    /**
     * Get accepted
     *
     * @return boolean
     */
    public function getAccepted()
    {
        return $this->accepted;
    }

    /**
     * Set declined
     *
     * @param boolean $declined
     *
     * @return Rsvp
     */
    public function setDeclined($declined)
    {
        $this->declined = $declined;

        return $this;
    }

    /**
     * Get declined
     *
     * @return boolean
     */
    public function getDeclined()
    {
        return $this->declined;
    }

    /**
     * Set cancellationReason
     *
     * @param string $cancellationReason
     *
     * @return Rsvp
     */
    public function setCancellationReason($cancellationReason)
    {
        $this->cancellationReason = $cancellationReason;

        return $this;
    }

    /**
     * Get cancellationReason
     *
     * @return string
     */
    public function getCancellationReason()
    {
        return $this->cancellationReason;
    }

    /**
     * Set pca
     *
     * @param \Homecare\HomecareBundle\Entity\Pca $pca
     *
     * @return Rsvp
     */
    public function setPca(\Homecare\HomecareBundle\Entity\Pca $pca = null)
    {
        $this->pca = $pca;

        return $this;
    }

    /**
     * Get pca
     *
     * @return \Homecare\HomecareBundle\Entity\Pca
     */
    public function getPca()
    {
        return $this->pca;
    }

    /**
     * Set request
     *
     * @param \Homecare\HomecareBundle\Entity\Request $request
     *
     * @return Rsvp
     */
    public function setRequest(\Homecare\HomecareBundle\Entity\Request $request = null)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return \Homecare\HomecareBundle\Entity\Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set sessionData
     *
     * @param \Homecare\HomecareBundle\Entity\SessionData $sessionData
     *
     * @return Rsvp
     */
    public function setSessionData(\Homecare\HomecareBundle\Entity\SessionData $sessionData = null)
    {
        $this->sessionData = $sessionData;

        return $this;
    }

    /**
     * Get sessionData
     *
     * @return \Homecare\HomecareBundle\Entity\SessionData
     */
    public function getSessionData()
    {
        return $this->sessionData;
    }


}



