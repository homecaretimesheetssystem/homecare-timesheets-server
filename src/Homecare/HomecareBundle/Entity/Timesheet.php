<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Timesheet
 *
 * @ORM\Table(name="timesheet")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\TimesheetRepository")
 * @Annotation\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks()
 */
class Timesheet
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     * @SerializedName("id")
     * @Annotation\Expose
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Recipient", inversedBy="actualTimesheets")
     * @ORM\JoinColumn(name="recipient_id", referencedColumnName="id")
     * @Type("Homecare\HomecareBundle\Entity\Recipient")
     * @SerializedName("recipient")
     * @Annotation\Expose
     */
    private $recipient;


    /**
     * @ORM\OneToOne(targetEntity="Files", mappedBy="timesheet", orphanRemoval=true)
     * @Type("Homecare\HomecareBundle\Entity\Files")
     * @SerializedName("file")
     * @Annotation\Expose
     */
    private $file;


    /**
     * @ORM\OneToMany(targetEntity="CareOptionTimesheet", mappedBy="timesheet", cascade={"persist"}, orphanRemoval=true)
     * @Type("Homecare\HomecareBundle\Entity\CareOptionTimesheet")
     * @SerializedName("careOptionTimesheets")
     * @Annotation\Expose
     */
    private $careOptionTimesheets;


    /**
     * this property is for whether or not the timesheet has been finished or not
     *
     * @var integer will hold a 0 for no and a 1 for finished
     *
     *
     * @ORM\Column(name="finished",type="integer")
     * @Type("integer")
     * @SerializedName("finished")
     * @Annotation\Expose
     */
    private $finished;


    /**
     * @ORM\OneToOne(targetEntity="RecipientSignature", mappedBy="timesheet", orphanRemoval=true)
     * @Annotation\Expose
     */
    private $recipientSignature;


    /**
     * @ORM\OneToOne(targetEntity="UnsignedTimesheetEmail", mappedBy="timesheet", orphanRemoval=true, cascade={"persist"})
     */
    private $unsignedTimesheetEmail;


    /**
     * @ORM\OneToOne(targetEntity="PcaSignature", mappedBy="timesheet", orphanRemoval=true)
     * @Annotation\Expose
     */
    private $pcaSignature;


    /**
     * @ORM\ManyToOne(targetEntity="SessionData", inversedBy="timesheets")
     * @Type("Homecare\HomecareBundle\Entity\SessionData")
     * @SerializedName("sessionData")
     */
    private $sessionData;


    /**
     * @var
     * @ORM\Column(type="boolean")
     */
    private $archived = 0;


    /**
     * @ORM\OneToOne(targetEntity="Verification", inversedBy="timesheet")
     * @SerializedName("verification")
     */
    private $verification;


    private $dateFilter;


    public function getOwner()
    {
        return $this->getSessionData()->getPca()->getAgency();
    }


    public function setDateFilter($dateFilter)
    {
        $this->dateFilter = $dateFilter;

        return $this;
    }

    public function getDateFilter()
    {
        return $this->dateFilter;
    }


    /**
     * had to set this value to zero automaticaly cause doctrine default: 0 was not working with api
     *
     * @ORM\PrePersist
     */
    public function setFinishedValue()
    {
        $this->finished = 0;
    }


    public function __construct()
    {
        $this->careOptionTimesheets = new ArrayCollection();

    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set finished
     *
     * @param integer $finished
     *
     * @return Timesheet
     */
    public function setFinished($finished)
    {
        $this->finished = $finished;

        return $this;
    }

    /**
     * Get finished
     *
     * @return integer
     */
    public function getFinished()
    {
        return $this->finished;
    }

    /**
     * Set recipient
     *
     * @param \Homecare\HomecareBundle\Entity\Recipient $recipient
     *
     * @return Timesheet
     */
    public function setRecipient(\Homecare\HomecareBundle\Entity\Recipient $recipient = null)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return \Homecare\HomecareBundle\Entity\Recipient
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Set file
     *
     * @param \Homecare\HomecareBundle\Entity\Files $file
     *
     * @return Timesheet
     */
    public function setFile(\Homecare\HomecareBundle\Entity\Files $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return \Homecare\HomecareBundle\Entity\Files
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Add careOptionTimesheets
     *
     * @param \Homecare\HomecareBundle\Entity\CareOptionTimesheet $careOptionTimesheets
     *
     * @return Timesheet
     */
    public function addCareOptionTimesheet(\Homecare\HomecareBundle\Entity\CareOptionTimesheet $careOptionTimesheets)
    {
        $this->careOptionTimesheets[] = $careOptionTimesheets;

        return $this;
    }

    /**
     * Remove careOptionTimesheets
     *
     * @param \Homecare\HomecareBundle\Entity\CareOptionTimesheet $careOptionTimesheets
     */
    public function removeCareOptionTimesheet(\Homecare\HomecareBundle\Entity\CareOptionTimesheet $careOptionTimesheets)
    {
        $this->careOptionTimesheets->removeElement($careOptionTimesheets);
    }

    /**
     * Get careOptionTimesheets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCareOptionTimesheets()
    {
        return $this->careOptionTimesheets;
    }

    /**
     * Set recipientSignature
     *
     * @param \Homecare\HomecareBundle\Entity\RecipientSignature $recipientSignature
     *
     * @return Timesheet
     */
    public function setRecipientSignature(\Homecare\HomecareBundle\Entity\RecipientSignature $recipientSignature = null)
    {
        $this->recipientSignature = $recipientSignature;

        return $this;
    }

    /**
     * Get recipientSignature
     *
     * @return \Homecare\HomecareBundle\Entity\RecipientSignature
     */
    public function getRecipientSignature()
    {
        return $this->recipientSignature;
    }

    /**
     * Set pcaSignature
     *
     * @param \Homecare\HomecareBundle\Entity\PcaSignature $pcaSignature
     *
     * @return Timesheet
     */
    public function setPcaSignature(\Homecare\HomecareBundle\Entity\PcaSignature $pcaSignature = null)
    {
        $this->pcaSignature = $pcaSignature;

        return $this;
    }

    /**
     * Get pcaSignature
     *
     * @return \Homecare\HomecareBundle\Entity\PcaSignature
     */
    public function getPcaSignature()
    {
        return $this->pcaSignature;
    }

    /**
     * Set sessionData
     *
     * @param \Homecare\HomecareBundle\Entity\SessionData $sessionData
     *
     * @return Timesheet
     */
    public function setSessionData(\Homecare\HomecareBundle\Entity\SessionData $sessionData = null)
    {
        $this->sessionData = $sessionData;

        return $this;
    }

    /**
     * Get sessionData
     *
     * @return \Homecare\HomecareBundle\Entity\SessionData
     */
    public function getSessionData()
    {
        return $this->sessionData;
    }

    /**
     * Set archived
     *
     * @param boolean $archived
     *
     * @return Timesheet
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;

        return $this;
    }

    /**
     * Get archived
     *
     * @return boolean
     */
    public function getArchived()
    {
        return $this->archived;
    }


    /**
     * Set verification
     *
     * @param \Homecare\HomecareBundle\Entity\Verification $verification
     *
     * @return Timesheet
     */
    public function setVerification(\Homecare\HomecareBundle\Entity\Verification $verification = null)
    {
        $this->verification = $verification;

        return $this;
    }

    /**
     * Get verification
     *
     * @return \Homecare\HomecareBundle\Entity\Verification
     */
    public function getVerification()
    {
        return $this->verification;
    }

    /**
     * Set unsignedTimesheetEmail
     *
     * @param \Homecare\HomecareBundle\Entity\UnsignedTimesheetEmail $unsignedTimesheetEmail
     *
     * @return Timesheet
     */
    public function setUnsignedTimesheetEmail(\Homecare\HomecareBundle\Entity\UnsignedTimesheetEmail $unsignedTimesheetEmail = null)
    {
        $this->unsignedTimesheetEmail = $unsignedTimesheetEmail;

        $unsignedTimesheetEmail->setTimesheet($this);

        return $this;
    }

    /**
     * Get unsignedTimesheetEmail
     *
     * @return \Homecare\HomecareBundle\Entity\UnsignedTimesheetEmail
     */
    public function getUnsignedTimesheetEmail()
    {
        return $this->unsignedTimesheetEmail;
    }
}
