<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation;

/**
 * RecipientPca
 *
 * @ORM\Table(name="recipient_pca")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\RecipientPcaRepository")
 * @Annotation\ExclusionPolicy("all")
 */
class RecipientPca
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Pca", inversedBy="recipientPca")
     * @Annotation\Expose
     */
    private $pca;


    /**
     * @ORM\ManyToOne(targetEntity="Recipient", inversedBy="recipientPca")
     */
    private $recipient;


    /**
     * @ORM\OneToMany(targetEntity="Verification", mappedBy="recipientPca")
     * @Annotation\Expose
     */
    private $verification;


    /**
     * @ORM\Column(name="is_temp", type="boolean", nullable=true)
     *
     * @Annotation\Expose
     */
    private $isTemp = false;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set pca
     *
     * @param \Homecare\HomecareBundle\Entity\Pca $pca
     *
     * @return RecipientPca
     */
    public function setPca(\Homecare\HomecareBundle\Entity\Pca $pca = null)
    {
        $this->pca = $pca;

        return $this;
    }

    /**
     * Get pca
     *
     * @return \Homecare\HomecareBundle\Entity\Pca
     */
    public function getPca()
    {
        return $this->pca;
    }

    /**
     * Set recipient
     *
     * @param \Homecare\HomecareBundle\Entity\Recipient $recipient
     *
     * @return RecipientPca
     */
    public function setRecipient(\Homecare\HomecareBundle\Entity\Recipient $recipient = null)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return \Homecare\HomecareBundle\Entity\Recipient
     */
    public function getRecipient()
    {
        return $this->recipient;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->verification = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add verification
     *
     * @param \Homecare\HomecareBundle\Entity\Verification $verification
     *
     * @return RecipientPca
     */
    public function addVerification(\Homecare\HomecareBundle\Entity\Verification $verification)
    {
        $this->verification[] = $verification;

        return $this;
    }

    /**
     * Remove verification
     *
     * @param \Homecare\HomecareBundle\Entity\Verification $verification
     */
    public function removeVerification(\Homecare\HomecareBundle\Entity\Verification $verification)
    {
        $this->verification->removeElement($verification);
    }

    /**
     * Get verification
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVerification()
    {
        return $this->verification;
    }

    /**
     * @return mixed
     */
    public function getIsTemp()
    {
        return $this->isTemp;
    }

    /**
     * @param mixed $isTemp
     */
    public function setIsTemp($isTemp)
    {
        $this->isTemp = $isTemp;
    }


}
