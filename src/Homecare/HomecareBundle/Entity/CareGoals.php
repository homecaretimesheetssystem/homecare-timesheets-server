<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CareGoals
 *
 * @ORM\Table(name="care_goals")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\CareGoalsRepository")
 */
class CareGoals
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
		
		
		/**
		* @ORM\ManyToMany(targetEntity="CareOptions", inversedBy="careGoals")
		*/
		private $careOption;
		
		/**
		* @ORM\ManyToOne(targetEntity="Recipient", inversedBy="careGoals")
		*/
		private $recipient;
		
		
		/**
		* @ORM\Column(name="set_by_recipient", type="boolean", nullable=true)
		*/
		private $setByRecipient;
		
		/**
		* @ORM\ManyToOne(targetEntity="Agency", inversedBy="careGoals")
		* @ORM\JoinColumn(name="setByAgency_id", referencedColumnName="id", nullable=true)
		*/
		private $setByAgency;
		
		/**
		* @ORM\ManyToOne(targetEntity="Admin", inversedBy="careGoals")
		* @ORM\JoinColumn(name="setByAdmin_id", referencedColumnName="id", nullable=true)
		*/
		private $setByAdmin;
		
		
		
		
		
		
		


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set careOption
     *
     * @param \Homecare\HomecareBundle\Entity\CareOptions $careOption
     * @return CareGoals
     */
    public function setCareOption(\Homecare\HomecareBundle\Entity\CareOptions $careOption = null)
    {
        $this->careOption = $careOption;

        return $this;
    }

    /**
     * Get careOption
     *
     * @return \Homecare\HomecareBundle\Entity\CareOptions 
     */
    public function getCareOption()
    {
        return $this->careOption;
    }

    /**
     * Set recipient
     *
     * @param \Homecare\HomecareBundle\Entity\Recipient $recipient
     * @return CareGoals
     */
    public function setRecipient(\Homecare\HomecareBundle\Entity\Recipient $recipient = null)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return \Homecare\HomecareBundle\Entity\Recipient 
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Set setByRecipient
     * @return CareGoals
     */
    public function setSetByRecipient($setByRecipient = null)
    {
        $this->setByRecipient = $setByRecipient;

        return $this;
    }

    /**
     * Get setByRecipient
     *
     * @return \Homecare\HomecareBundle\Entity\Recipient 
     */
    public function getSetByRecipient()
    {
        return $this->setByRecipient;
    }

    /**
     * Set setByAgency
     *
     * @param \Homecare\HomecareBundle\Entity\Agency $setByAgency
     * @return CareGoals
     */
    public function setSetByAgency(\Homecare\HomecareBundle\Entity\Agency $setByAgency = null)
    {
        $this->setByAgency = $setByAgency;

        return $this;
    }

    /**
     * Get setByAgency
     *
     * @return \Homecare\HomecareBundle\Entity\Agency 
     */
    public function getSetByAgency()
    {
        return $this->setByAgency;
    }

    /**
     * Set setByAdmin
     *
     * @param \Homecare\HomecareBundle\Entity\Admin $setByAdmin
     * @return CareGoals
     */
    public function setSetByAdmin(\Homecare\HomecareBundle\Entity\Admin $setByAdmin = null)
    {
        $this->setByAdmin = $setByAdmin;

        return $this;
    }

    /**
     * Get setByAdmin
     *
     * @return \Homecare\HomecareBundle\Entity\Admin 
     */
    public function getSetByAdmin()
    {
        return $this->setByAdmin;
    }
		
		
		
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->careOption = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add careOption
     *
     * @param \Homecare\HomecareBundle\Entity\CareOptions $careOption
     * @return CareGoals
     */
    public function addCareOption(\Homecare\HomecareBundle\Entity\CareOptions $careOption)
    {
        $this->careOption[] = $careOption;

        return $this;
    }

    /**
     * Remove careOption
     *
     * @param \Homecare\HomecareBundle\Entity\CareOptions $careOption
     */
    public function removeCareOption(\Homecare\HomecareBundle\Entity\CareOptions $careOption)
    {
        $this->careOption->removeElement($careOption);
    }
		
		
}
