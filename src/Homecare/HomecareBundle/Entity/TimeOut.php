<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;

/**
 * TimeOut
 *
 * @ORM\Table(name="time_out")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\TimeOutRepository")
 * @Annotation\ExclusionPolicy("all")
 */
class TimeOut
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
		 * @Type("integer")
		 * @SerializedName("id")
		 * @Annotation\Expose
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeOut", type="datetime", nullable=true)
		 * @Type("DateTime")
		 * @SerializedName("timeOut")
	 	 * @Annotation\Expose
     */
    private $timeOut;

    /**
     * @var string
     *
     * @ORM\Column(name="timeOutAddress", type="string", length=255, nullable=true)
		 * @Type("string")
		 * @SerializedName("timeOutAddress")	
		 * @Annotation\Expose
     * @Encrypted
     */
    private $timeOutAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=255, nullable=true)
		 * @Type("string")
		 * @SerializedName("latitude")
		 * @Annotation\Expose
     * @Encrypted
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length=255, nullable=true)
		 * @Type("string")
		 * @SerializedName("longitude")
		 * @Annotation\Expose
     * @Encrypted
     */
    private $longitude;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timeOutPictureTime", type="datetime", nullable=true)
		 * @Type("DateTime")
		 * @SerializedName("timeOutPictureTime")
		 * @Annotation\Expose
     */
    private $timeOutPictureTime;

    /**
     * @var string
     *
     * @ORM\Column(name="totalHours", type="bigint", nullable=true)
		 * @Type("integer")
		 * @SerializedName("totalHours")
		 * @Annotation\Expose
     */
    private $totalHours;

    /**
     * @var string
     *
     * @ORM\Column(name="billableHours", type="bigint", nullable=true)
		 * @Type("integer")
		 * @SerializedName("billableHours")
		 * @Annotation\Expose
     */
    private $billableHours;
		
		
		
		




    /**
     * @ORM\OneToOne(targetEntity="SessionData", mappedBy="timeOut")
     * @Type("Homecare\HomecareBundle\Entity\SessionData")
     * @SerializedName("sessionData")
     */
    private $sessionData;


		
		

		
		
		
		
			public function __toString() {
				return $this->getTimeOutAddress();
			}
		
		


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set timeOut
     *
     * @param \DateTime $timeOut
     * @return TimeOut
     */
    public function setTimeOut($timeOut)
    {
        $this->timeOut = $timeOut;

        return $this;
    }

    /**
     * Get timeOut
     *
     * @return \DateTime 
     */
    public function getTimeOut()
    {
        return $this->timeOut;
    }

    /**
     * Set timeOutAddress
     *
     * @param string $timeOutAddress
     * @return TimeOut
     */
    public function setTimeOutAddress($timeOutAddress)
    {
        $this->timeOutAddress = $timeOutAddress;

        return $this;
    }

    /**
     * Get timeOutAddress
     *
     * @return string 
     */
    public function getTimeOutAddress()
    {
        return $this->timeOutAddress;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     * @return TimeOut
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return TimeOut
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set timeOutPictureTime
     *
     * @param \DateTime $timeOutPictureTime
     * @return TimeOut
     */
    public function setTimeOutPictureTime($timeOutPictureTime)
    {
        $this->timeOutPictureTime = $timeOutPictureTime;

        return $this;
    }

    /**
     * Get timeOutPictureTime
     *
     * @return \DateTime 
     */
    public function getTimeOutPictureTime()
    {
        return $this->timeOutPictureTime;
    }

  




    /**
     * Set totalHours
     *
     * @param integer $totalHours
     * @return TimeOut
     */
    public function setTotalHours($totalHours)
    {
        $this->totalHours = $totalHours;

        return $this;
    }

    /**
     * Get totalHours
     *
     * @return integer 
     */
    public function getTotalHours()
    {
        return $this->totalHours;
    }

    /**
     * Set billableHours
     *
     * @param integer $billableHours
     * @return TimeOut
     */
    public function setBillableHours($billableHours)
    {
        $this->billableHours = $billableHours;

        return $this;
    }

    /**
     * Get billableHours
     *
     * @return integer 
     */
    public function getBillableHours()
    {
        return $this->billableHours;
    }

    /**
     * Set sessionData
     *
     * @param \Homecare\HomecareBundle\Entity\SessionData $sessionData
     * @return TimeOut
     */
    public function setSessionData(\Homecare\HomecareBundle\Entity\SessionData $sessionData = null)
    {
        $this->sessionData = $sessionData;

        return $this;
    }

    /**
     * Get sessionData
     *
     * @return \Homecare\HomecareBundle\Entity\SessionData 
     */
    public function getSessionData()
    {
        return $this->sessionData;
    }
}
