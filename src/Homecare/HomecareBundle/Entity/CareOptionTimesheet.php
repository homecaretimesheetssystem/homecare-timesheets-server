<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation;

/**
 * CareOptionTimesheet
 *
 * @ORM\Table(name="careoption_timesheet")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\CareOptionTimesheetRepository")
 * @Annotation\ExclusionPolicy("all")
 */
class CareOptionTimesheet
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
		
		
		
		
		/**
		* @ORM\ManyToOne(targetEntity="Timesheet", inversedBy="careOptionTimesheets")
		* @Type("Homecare\HomecareBundle\Entity\Timesheet")
		* @SerializedName("timesheet")
		*/
		private $timesheet;
		
		
		
		
		/**
		* @ORM\ManyToOne(targetEntity="CareOptions", inversedBy="careOptionTimesheets")
		* @Type("Homecare\HomecareBundle\Entity\CareOptions")
		* @SerializedName("careOption")
		* @Annotation\Expose
		*/
		private $careOption;
		
		


    /**
     * Set timesheet
     *
     * @param \Homecare\HomecareBundle\Entity\Timesheet $timesheet
     * @return CareOptionTimesheet
     */
    public function setTimesheet(\Homecare\HomecareBundle\Entity\Timesheet $timesheet = null)
    {
        $this->timesheet = $timesheet;

        return $this;
    }

    /**
     * Get timesheet
     *
     * @return \Homecare\HomecareBundle\Entity\Timesheet 
     */
    public function getTimesheet()
    {
        return $this->timesheet;
    }

    /**
     * Set careOption
     *
     * @param \Homecare\HomecareBundle\Entity\CareOptions $careOption
     * @return CareOptionTimesheet
     */
    public function setCareOption(\Homecare\HomecareBundle\Entity\CareOptions $careOption = null)
    {
        $this->careOption = $careOption;

        return $this;
    }

    /**
     * Get careOption
     *
     * @return \Homecare\HomecareBundle\Entity\CareOptions 
     */
    public function getCareOption()
    {
        return $this->careOption;
    }
}
