<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;

/**
 * SharedCareLocation
 *
 * @ORM\Table(name="shared_care_location")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\SharedCareLocationRepository")
 * @Annotation\ExclusionPolicy("all")
 */
class SharedCareLocation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     * @SerializedName("id")
     * @Annotation\Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255)
     * @Type("string")
     * @SerializedName("location")
     * @Annotation\Expose
     * @Encrypted
     */
    private $location;


    /**
     * @ORM\OneToMany(targetEntity="SessionData", mappedBy="sharedCareLocation")
     */
    private $sessionData;



    public function __construct() {
        $this->sessionData = new ArrayCollection();
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return SharedCareLocation
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string 
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Add sessionData
     *
     * @param \Homecare\HomecareBundle\Entity\SessionData $sessionData
     * @return SharedCareLocation
     */
    public function addSessionDatum(\Homecare\HomecareBundle\Entity\SessionData $sessionData)
    {
        $this->sessionData[] = $sessionData;

        return $this;
    }

    /**
     * Remove sessionData
     *
     * @param \Homecare\HomecareBundle\Entity\SessionData $sessionData
     */
    public function removeSessionDatum(\Homecare\HomecareBundle\Entity\SessionData $sessionData)
    {
        $this->sessionData->removeElement($sessionData);
    }

    /**
     * Get sessionData
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSessionData()
    {
        return $this->sessionData;
    }
}
