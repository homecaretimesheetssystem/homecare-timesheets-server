<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;

/**
 * CareGoals
 *
 * @ORM\Table(name="careoptions")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\CareOptionsRepository")
 * @Annotation\ExclusionPolicy("all")
 */
class CareOptions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Annotation\Expose
     */
    private $id;


    /**
     * @ORM\Column(name="is_iadl", type="boolean", nullable=false)
     * @Annotation\Expose
     * @Encrypted
     */
    private $isIadl;


    /**
     * @var string
     *
     * @ORM\Column(name="care_option", type="string", length=255)
     * @Annotation\Expose
     * @Encrypted
     */
    private $careOption;


    /**
     * @ORM\ManyToMany(targetEntity="CareGoals", mappedBy="careOption")
     */
    private $careGoals;


    /**
     * @ORM\OneToMany(targetEntity="CareOptionTimesheet", mappedBy="careOption", cascade={"persist"})
     * @Type("Homecare\HomecareBundle\Entity\CareOptionTimesheet")
     * @SerializedName("careOptionTimesheets")
     */
    private $careOptionTimesheets;


    public function __construct()
    {
        $this->careOptionTimesheets = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }







    //add a to string method so that the object can be displayed in the twig template
    /*
    public function __toString()
            {
                return $this->getCareGoal();
            }
    */


    /**
     * Set careOption
     *
     * @param string $careOption
     *
     * @return CareOptions
     */
    public function setCareOption($careOption)
    {
        $this->careOption = $careOption;

        return $this;
    }

    /**
     * Get careOption
     *
     * @return string
     */
    public function getCareOption()
    {
        return $this->careOption;
    }


    /**
     * Add careGoals
     *
     * @param \Homecare\HomecareBundle\Entity\CareGoals $careGoals
     *
     * @return CareOptions
     */
    public function addCareGoal(\Homecare\HomecareBundle\Entity\CareGoals $careGoals)
    {
        $this->careGoals[] = $careGoals;

        return $this;
    }

    /**
     * Remove careGoals
     *
     * @param \Homecare\HomecareBundle\Entity\CareGoals $careGoals
     */
    public function removeCareGoal(\Homecare\HomecareBundle\Entity\CareGoals $careGoals)
    {
        $this->careGoals->removeElement($careGoals);
    }

    /**
     * Get careGoals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCareGoals()
    {
        return $this->careGoals;
    }


    public function __toString()
    {
        return $this->getCareOption();
    }


    /**
     * Add careOptionTimesheets
     *
     * @param \Homecare\HomecareBundle\Entity\CareOptionTimesheet $careOptionTimesheets
     *
     * @return CareOptions
     */
    public function addCareOptionTimesheet(\Homecare\HomecareBundle\Entity\CareOptionTimesheet $careOptionTimesheets)
    {
        $this->careOptionTimesheets[] = $careOptionTimesheets;

        return $this;
    }

    /**
     * Remove careOptionTimesheets
     *
     * @param \Homecare\HomecareBundle\Entity\CareOptionTimesheet $careOptionTimesheets
     */
    public function removeCareOptionTimesheet(\Homecare\HomecareBundle\Entity\CareOptionTimesheet $careOptionTimesheets)
    {
        $this->careOptionTimesheets->removeElement($careOptionTimesheets);
    }

    /**
     * Get careOptionTimesheets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCareOptionTimesheets()
    {
        return $this->careOptionTimesheets;
    }

    /**
     * Set isIadl
     *
     * @param boolean $isIadl
     *
     * @return CareOptions
     */
    public function setIsIadl($isIadl)
    {
        $this->isIadl = $isIadl;

        return $this;
    }

    /**
     * Get isIadl
     *
     * @return boolean
     */
    public function getIsIadl()
    {
        return $this->isIadl;
    }
}
