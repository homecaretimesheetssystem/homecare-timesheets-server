<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;

/**
 * States
 *
 * @ORM\Table(name="states")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\StatesRepository")
 * @Annotation\ExclusionPolicy("all")
 */
class States
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Annotation\Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="stateName", type="string", length=255)
     * @Annotation\Expose
     * @Encrypted
     */
    private $stateName;


    /**
     * @ORM\OneToMany(targetEntity="Agency", mappedBy="state")
     */
    private $agencies;


    public function __construct()
    {

        $this->agencies = new ArrayCollection();

    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stateName
     *
     * @param string $stateName
     *
     * @return States
     */
    public function setStateName($stateName)
    {
        $this->stateName = $stateName;

        return $this;
    }

    /**
     * Get stateName
     *
     * @return string
     */
    public function getStateName()
    {
        return $this->stateName;
    }

    /**
     * Add agencies
     *
     * @param \Homecare\HomecareBundle\Entity\Agency $agencies
     *
     * @return States
     */
    public function addAgency(\Homecare\HomecareBundle\Entity\Agency $agencies)
    {
        $this->agencies[] = $agencies;

        return $this;
    }

    /**
     * Remove agencies
     *
     * @param \Homecare\HomecareBundle\Entity\Agency $agencies
     */
    public function removeAgency(\Homecare\HomecareBundle\Entity\Agency $agencies)
    {
        $this->agencies->removeElement($agencies);
    }

    /**
     * Get agencies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAgencies()
    {
        return $this->agencies;
    }


    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->stateName;
    }
}
