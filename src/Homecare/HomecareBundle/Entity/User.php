<?php
namespace Homecare\HomecareBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\UserRepository")
 * @Annotation\ExclusionPolicy("all")
 * @UniqueEntity("email")
 * @UniqueEntity("username")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var string
     * @Assert\NotBlank(groups={"EDIT_USER", "Default"})
     */
    protected $username;


    /**
     * @var string
     * @Assert\NotBlank(groups={"EDIT_USER", "Default"})
     */
    protected $email;


    /**
     * @var string
     * @Assert\NotBlank(groups={"PASSWORD", "Default"})
     */
    protected $password;


    /**
     * @ORM\OneToOne(targetEntity="Recipient", mappedBy="user")
     */
    private $recipient;


    /**
     * @ORM\ManyToOne(targetEntity="Agency", inversedBy="users")
     * @ORM\JoinColumn(name="agency_id", referencedColumnName="id")
     * @Assert\NotBlank(groups={"CREATE_AGENCY"})
     */
    private $agency;

    /**
     * @ORM\ManyToOne(targetEntity="Admin", inversedBy="users")
     * @ORM\JoinColumn(name="admin_id", referencedColumnName="id")
     */
    private $admin;


    /**
     * @ORM\OneToOne(targetEntity="Pca", mappedBy="user")
     */
    private $pca;


    /**
     * @var
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(groups={"CREATE_AGENCY"})
     *
     */
    private $firstName;

    /**
     * @var
     * @ORM\Column(type="string", nullable=true)
     * @Assert\NotBlank(groups={"CREATE_AGENCY"})
     */
    private $lastName;


    /**
     * @var
     * @ORM\Column(type="boolean")
     */
    private $archived = 0;


    /**
     * @ORM\OneToMany(targetEntity="Device", mappedBy="user")
     */
    private $devices;


    public function __construct()
    {
        $this->devices = new ArrayCollection();

        parent::__construct();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set recipient
     *
     * @param \Homecare\HomecareBundle\Entity\Recipient $recipient
     *
     * @return User
     */
    public function setRecipient(\Homecare\HomecareBundle\Entity\Recipient $recipient = null)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return \Homecare\HomecareBundle\Entity\Recipient
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Set agency
     *
     * @param \Homecare\HomecareBundle\Entity\Agency $agency
     *
     * @return User
     */
    public function setAgency(\Homecare\HomecareBundle\Entity\Agency $agency = null)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get agency
     *
     * @return \Homecare\HomecareBundle\Entity\Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * Set admin
     *
     * @param \Homecare\HomecareBundle\Entity\Admin $admin
     *
     * @return User
     */
    public function setAdmin(\Homecare\HomecareBundle\Entity\Admin $admin = null)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin
     *
     * @return \Homecare\HomecareBundle\Entity\Admin
     */
    public function getAdmin()
    {
        return $this->admin;
    }


    /**
     * Set pca
     *
     * @param \Homecare\HomecareBundle\Entity\Pca $pca
     *
     * @return User
     */
    public function setPca(\Homecare\HomecareBundle\Entity\Pca $pca = null)
    {
        $this->pca = $pca;

        return $this;
    }

    /**
     * Get pca
     *
     * @return \Homecare\HomecareBundle\Entity\Pca
     */
    public function getPca()
    {
        return $this->pca;
    }


    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set archived
     *
     * @param boolean $archived
     *
     * @return User
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;

        return $this;
    }

    /**
     * Get archived
     *
     * @return boolean
     */
    public function getArchived()
    {
        return $this->archived;
    }


    /**
     * Generic initialization of a user
     *
     * @param $firstName
     * @param $lastName
     * @param $username
     * @param $email
     * @param $password
     *
     * @return $this
     */
    public function initializeUser($firstName, $lastName, $username, $email, $password)
    {
        $this->firstName = $firstName;
        $this->lastName  = $lastName;
        $this->username  = $username;
        $this->email     = $email;
        $this->password  = $password;


        return $this;
    }


    /**
     * Convenience method to initialize a new recipient user
     *
     * @param Recipient $recipient
     */
    public function initializeRecipient(Recipient $recipient)
    {

        $this->setEnabled(true);
        $this->addRole('ROLE_RECIPIENT');
        $this->setFirstName($recipient->getFirstName());
        $this->setLastName($recipient->getLastName());
        $this->setRecipient($recipient);

    }


    /**
     * Convenience method to initialize a new pca user
     *
     * @param Pca $pca
     */
    public function initializePca(Pca $pca)
    {

        $this->setEnabled(true);
        $this->addRole('ROLE_PCA');
        $this->setFirstName($pca->getFirstName());
        $this->setLastName($pca->getLastName());
        $this->setPca($pca);

    }


    /**
     * Convenience method to initialize a new admin user
     *
     * @param Admin $admin
     */
    public function initializeAdmin(Admin $admin)
    {

        $this->setEnabled(true);
        $this->addRole('ROLE_ADMIN');
        $this->setFirstName($admin->getFirstName());
        $this->setLastName($admin->getLastName());
        $this->setAdmin($admin);

    }


    /**
     * Convenience method to initialize a new agency user.
     *
     * @param Agency $agency
     */
    public function initializeAgency(Agency $agency)
    {

        $this->setEnabled(true);
        $this->addRole('ROLE_AGENCY');
        $this->setAgency($agency);

    }


    /**
     * Add device
     *
     * @param \Homecare\HomecareBundle\Entity\Device $device
     *
     * @return User
     */
    public function addDevice(\Homecare\HomecareBundle\Entity\Device $device)
    {
        $this->devices[] = $device;

        return $this;
    }

    /**
     * Remove device
     *
     * @param \Homecare\HomecareBundle\Entity\Device $device
     */
    public function removeDevice(\Homecare\HomecareBundle\Entity\Device $device)
    {
        $this->devices->removeElement($device);
    }

    /**
     * Get devices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDevices()
    {
        return $this->devices;
    }
}
