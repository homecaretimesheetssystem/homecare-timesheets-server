<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * NewFile
 */
class NewFile
{
   

    private $newFile;




	  /**
	     * Sets file.
	     *
	     * @param UploadedFile $file
	     */
	    public function setNewFile(UploadedFile $newFile = null)
	    {
	        $this->newFile = $newFile;
	    }

	    /**
	     * Get file.
	     *
	     * @return UploadedFile
	     */
	    public function getNewFile()
	    {
	        return $this->newFile;
	    }
		
		
		
		
		
}
