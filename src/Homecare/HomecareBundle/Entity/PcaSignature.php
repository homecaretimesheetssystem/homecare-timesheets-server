<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;

/**
 * PcaSignature
 *
 * @ORM\Table(name="pca_signature")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\PcaSignatureRepository")
 * @Annotation\ExclusionPolicy("all")
 */
class PcaSignature
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     * @SerializedName("id")
     * @Annotation\Expose
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="pcaSigTime", type="datetime", nullable=true)
     * @Type("DateTime")
     * @SerializedName("pcaSigTime")
     * @Annotation\Expose
     */
    private $pcaSigTime;

    /**
     * @var string
     *
     * @ORM\Column(name="pcaSigAddress", type="string", length=255, nullable=true)
     * @Type("string")
     * @SerializedName("pcaSigAddress")
     * @Annotation\Expose
     * @Encrypted
     */
    private $pcaSigAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=255, nullable=true)
     * @Type("string")
     * @SerializedName("latitude")
     * @Annotation\Expose
     * @Encrypted
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length=255, nullable=true)
     * @Type("string")
     * @SerializedName("longitude")
     * @Annotation\Expose
     * @Encrypted
     */
    private $longitude;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="pcaPhotoTime", type="datetime", nullable=true)
     * @Type("DateTime")
     * @SerializedName("pcaPhotoTime")
     * @Annotation\Expose
     */
    private $pcaPhotoTime;


    /**
     * @ORM\OneToOne(targetEntity="Timesheet", inversedBy="pcaSignature")
     * @Type("Homecare\HomecareBundle\Entity\Timesheet")
     * @SerializedName("timesheet")
     */
    private $timesheet;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pcaSigTime
     *
     * @param \DateTime $pcaSigTime
     *
     * @return PcaSignature
     */
    public function setPcaSigTime($pcaSigTime)
    {
        $this->pcaSigTime = $pcaSigTime;

        return $this;
    }

    /**
     * Get pcaSigTime
     *
     * @return \DateTime
     */
    public function getPcaSigTime()
    {
        return $this->pcaSigTime;
    }

    /**
     * Set pcaSigAddress
     *
     * @param string $pcaSigAddress
     *
     * @return PcaSignature
     */
    public function setPcaSigAddress($pcaSigAddress)
    {
        $this->pcaSigAddress = $pcaSigAddress;

        return $this;
    }

    /**
     * Get pcaSigAddress
     *
     * @return string
     */
    public function getPcaSigAddress()
    {
        return $this->pcaSigAddress;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return PcaSignature
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return PcaSignature
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set pcaPhotoTime
     *
     * @param \DateTime $pcaPhotoTime
     *
     * @return PcaSignature
     */
    public function setPcaPhotoTime($pcaPhotoTime)
    {
        $this->pcaPhotoTime = $pcaPhotoTime;

        return $this;
    }

    /**
     * Get pcaPhotoTime
     *
     * @return \DateTime
     */
    public function getPcaPhotoTime()
    {
        return $this->pcaPhotoTime;
    }

    /**
     * Set timesheet
     *
     * @param \Homecare\HomecareBundle\Entity\Timesheet $timesheet
     *
     * @return PcaSignature
     */
    public function setTimesheet(\Homecare\HomecareBundle\Entity\Timesheet $timesheet = null)
    {
        $this->timesheet = $timesheet;

        return $this;
    }

    /**
     * Get timesheet
     *
     * @return \Homecare\HomecareBundle\Entity\Timesheet
     */
    public function getTimesheet()
    {
        return $this->timesheet;
    }
}
