<?php

namespace Homecare\HomecareBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Services
 *
 * @ORM\Table(name="request")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\RequestRepository")
 * @Annotation\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks()
 */
class Request
{

    //use TimestampableEntity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Annotation\Expose
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Recipient", inversedBy="requests")
     * @SerializedName("recipient")
     * @Annotation\Expose
     */
    private $recipient;


    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     * @Type("string")
     * @SerializedName("address")
     * @Annotation\Expose
     * @Encrypted
     */
    private $address;


    /**
     * @ORM\OneToMany(targetEntity="Rsvp", mappedBy="request", cascade={"persist", "remove"}, orphanRemoval=true)
     * @SerializedName("rsvps")
     * @Annotation\Expose
     */
    private $rsvps;


    /**
     * Don't set a default gender preference because you want to run a check to see if this was set.
     * If it was set then it overrides the default gender preference set on the dashboard.
     *
     * 0 = no preference
     * 1 = female
     * 2 = male
     *
     * @var string
     * @ORM\Column(name="gender_preference", type="integer", nullable=true)
     * @Annotation\Expose
     */
    private $genderPreference;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_shift_time", type="datetime", nullable=true)
     * @Type("DateTime")
     * @SerializedName("startShiftTime")
     * @Annotation\Expose
     */
    private $startShiftTime;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_shift_time", type="datetime", nullable=true)
     * @Type("DateTime")
     * @SerializedName("endShiftTime")
     * @Annotation\Expose
     */
    private $endShiftTime;


    /**
     * @ORM\ManyToMany(targetEntity="Qualifications", inversedBy="requests", cascade={"persist"})
     */
    private $qualifications;


    /**
     * @ORM\Column(name="years_of_experience", type="integer", nullable=true)
     * @Annotation\Expose
     * @var integer
     */
    private $yearsOfExperience;


    /**
     * Request constructor.
     */
    public function __construct()
    {
        $this->rsvps          = new ArrayCollection();
        $this->qualifications = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set recipient
     *
     * @param \Homecare\HomecareBundle\Entity\Recipient $recipient
     *
     * @return Request
     */
    public function setRecipient(\Homecare\HomecareBundle\Entity\Recipient $recipient = null)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return \Homecare\HomecareBundle\Entity\Recipient
     */
    public function getRecipient()
    {
        return $this->recipient;
    }


    /**
     * Set address
     *
     * @param string $address
     *
     * @return Request
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Add rsvp
     *
     * @param \Homecare\HomecareBundle\Entity\Rsvp $rsvp
     *
     * @return Request
     */
    public function addRsvp(\Homecare\HomecareBundle\Entity\Rsvp $rsvp)
    {
        $this->rsvps[] = $rsvp;

        return $this;
    }

    /**
     * Remove rsvp
     *
     * @param \Homecare\HomecareBundle\Entity\Rsvp $rsvp
     */
    public function removeRsvp(\Homecare\HomecareBundle\Entity\Rsvp $rsvp)
    {
        $this->rsvps->removeElement($rsvp);
    }

    /**
     * Get rsvps
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRsvps()
    {
        return $this->rsvps;
    }

    /**
     * Set genderPreference
     *
     * @param string $genderPreference
     *
     * @return Request
     */
    public function setGenderPreference($genderPreference)
    {
        $this->genderPreference = $genderPreference;

        return $this;
    }

    /**
     * Get genderPreference
     *
     * @return integer
     */
    public function getGenderPreference()
    {
        return $this->genderPreference;
    }

    /**
     * Set startShiftTime
     *
     * @param \DateTime $startShiftTime
     *
     * @return Request
     */
    public function setStartShiftTime($startShiftTime)
    {
        $this->startShiftTime = $startShiftTime;

        return $this;
    }

    /**
     * Get startShiftTime
     *
     * @return \DateTime
     */
    public function getStartShiftTime()
    {
        return $this->startShiftTime;
    }

    /**
     * Set endShiftTime
     *
     * @param \DateTime $endShiftTime
     *
     * @return Request
     */
    public function setEndShiftTime($endShiftTime)
    {
        $this->endShiftTime = $endShiftTime;

        return $this;
    }

    /**
     * Get endShiftTime
     *
     * @return \DateTime
     */
    public function getEndShiftTime()
    {
        return $this->endShiftTime;
    }


    /**
     * Add qualification
     *
     * @param \Homecare\HomecareBundle\Entity\Qualifications $qualification
     *
     * @return Request
     */
    public function addQualification(\Homecare\HomecareBundle\Entity\Qualifications $qualification)
    {
        $this->qualifications[] = $qualification;

        return $this;
    }

    /**
     * Remove qualification
     *
     * @param \Homecare\HomecareBundle\Entity\Qualifications $qualification
     */
    public function removeQualification(\Homecare\HomecareBundle\Entity\Qualifications $qualification)
    {
        $this->qualifications->removeElement($qualification);
    }

    /**
     * Get qualifications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQualifications()
    {
        return $this->qualifications;
    }


    /**
     * Set yearsOfExperience
     *
     * @param integer $yearsOfExperience
     *
     * @return Pca
     */
    public function setYearsOfExperience($yearsOfExperience)
    {
        $this->yearsOfExperience = $yearsOfExperience;

        return $this;
    }

    /**
     * Get yearsOfExperience
     *
     * @return integer
     */
    public function getYearsOfExperience()
    {
        return $this->yearsOfExperience;
    }
}
