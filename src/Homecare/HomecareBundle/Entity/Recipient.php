<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Recipient
 *
 * @ORM\Table(name="recipient")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\RecipientRepository")
 * @Annotation\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks()
 */
class Recipient
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Annotation\Expose
     */
    private $id;

    /**
     * @ORM\Column(name="skip_verification", type="boolean", nullable=false, options={"default" : 0})
     * @Annotation\Expose
     * @Encrypted
     */
    private $skipVerification = false;

    /**
     * @ORM\Column(name="skip_gps", type="boolean", nullable=false, options={"default" : 0})
     * @Annotation\Expose
     * @Encrypted
     */
    private $skipGps = false;

    /**
     * @var string
     *
     * @ORM\Column(name="recipient_name", type="string", length=254, nullable=true)
     * @Encrypted
     */
    private $recipientName;


    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=254)
     * @Annotation\Expose
     * @Encrypted
     * @Assert\NotBlank()
     */
    private $firstName;


    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255)
     * @Annotation\Expose
     * @Encrypted
     * @Assert\NotBlank()
     */
    private $lastName;


    /**
     * @var string
     *
     * @ORM\Column(name="ma_number", type="string", length=255, nullable=true)
     * @Annotation\Expose
     * @Encrypted
     * @Assert\NotBlank()
     */
    private $maNumber;


    /**
     * @var string
     *
     * @ORM\Column(name="company_assigned_id", type="string", length=255, nullable=true)
     * @Annotation\Expose
     * @Encrypted
     * @Assert\NotBlank()
     */
    private $companyAssignedId;


    //this is the timesheet you are adding to the website to keep
    //the timesheets column above this you are going to delete in the future

    /**
     * @ORM\OneToMany(targetEntity="Timesheet", mappedBy="recipient")
     */
    private $actualTimesheets;


    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="recipient", cascade={"persist"}, orphanRemoval=true)
     * @Assert\Valid()
     */
    private $user;


    /**
     * @ORM\OneToMany(targetEntity="CareGoals", mappedBy="recipient",orphanRemoval=true)
     */
    private $careGoals;


    /**
     * @ORM\ManyToOne(targetEntity="Agency", inversedBy="recipients")
     */
    private $agency;


    /**
     * @ORM\OneToMany(targetEntity="RecipientPca", mappedBy="recipient",orphanRemoval=true)
     */
    private $recipientPca;


    /**
     * @ORM\OneToMany(targetEntity="Request", mappedBy="recipient")
     */
    private $requests;


    /**
     * Overrides the gender preference on the request
     *
     * 0 = no preference
     * 1 = female
     * 2 = male
     *
     * @var string
     * @ORM\Column(name="gender_preference", type="integer", nullable=true)
     * @Annotation\Expose
     */
    private $genderPreference = 0;


    /**
     * @ORM\ManyToOne(targetEntity="County", inversedBy="recipients")
     * @Annotation\Expose
     */
    private $county;


    /**
     * @ORM\OneToMany(targetEntity="Homecare\HomecareBundle\Entity\Verification", mappedBy="recipient")
     */
    private $archivedVerifications;


    /**
     * @var string
     *
     * @ORM\Column(name="home_address", type="string", length=255, nullable=true)
     * @Type("string")
     * @SerializedName("homeAddress")
     * @Annotation\Expose
     * @Encrypted
     */
    private $homeAddress;


    public function __construct()
    {
        $this->recipientPca = new ArrayCollection();
        $this->careGoals    = new ArrayCollection();
        //this is the new timesheet row you are adding
        //delete the timesheets one later
        $this->actualTimesheets      = new ArrayCollection();
        $this->requests              = new ArrayCollection();
        $this->archivedVerifications = new ArrayCollection();
    }


    /**
     * this method is necessary because we need to lowercase any encrypted properties for future database queries
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function lowercaseProperties()
    {
        $this->setFirstName(strtolower($this->getFirstName()));
        $this->setLastName(strtolower($this->getLastName()));
        $this->setRecipientName(strtolower($this->getFirstName().' '.$this->getLastName()));
    }


    /**
     * this method allows the form builder to return multiple properties for the fields names
     * @return string
     */
    function getUniqueName()
    {
        return sprintf('%s %s - MA# %s', $this->firstName, $this->lastName, $this->maNumber);
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recipientName
     *
     * @param string $recipientName
     *
     * @return Recipient
     */
    public function setRecipientName($recipientName)
    {
        $this->recipientName = $recipientName;

        return $this;
    }

    /**
     * Get recipientName
     *
     * @return string
     */
    public function getRecipientName()
    {
        return $this->recipientName;
    }


    public function __toString()
    {
        return $this->getRecipientName();
    }


    /**
     * Add careGoals
     *
     * @param \Homecare\HomecareBundle\Entity\CareGoals $careGoals
     *
     * @return Recipient
     */
    public function addCareGoal(\Homecare\HomecareBundle\Entity\CareGoals $careGoals)
    {
        $this->careGoals[] = $careGoals;

        return $this;
    }

    /**
     * Remove careGoals
     *
     * @param \Homecare\HomecareBundle\Entity\CareGoals $careGoals
     */
    public function removeCareGoal(\Homecare\HomecareBundle\Entity\CareGoals $careGoals)
    {
        $this->careGoals->removeElement($careGoals);
    }

    /**
     * Get careGoals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCareGoals()
    {
        return $this->careGoals;
    }


    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Recipient
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Recipient
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set agency
     *
     * @param \Homecare\HomecareBundle\Entity\Agency $agency
     *
     * @return Recipient
     */
    public function setAgency(\Homecare\HomecareBundle\Entity\Agency $agency = null)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get agency
     *
     * @return \Homecare\HomecareBundle\Entity\Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * Set maNumber
     *
     * @param string $maNumber
     *
     * @return Recipient
     */
    public function setMaNumber($maNumber)
    {
        $this->maNumber = $maNumber;

        return $this;
    }

    /**
     * Get maNumber
     *
     * @return string
     */
    public function getMaNumber()
    {
        return $this->maNumber;
    }


    /**
     * Add actualTimesheets
     *
     * @param \Homecare\HomecareBundle\Entity\Timesheet $actualTimesheets
     *
     * @return Recipient
     */
    public function addActualTimesheet(\Homecare\HomecareBundle\Entity\Timesheet $actualTimesheets)
    {
        $this->actualTimesheets[] = $actualTimesheets;

        return $this;
    }

    /**
     * Remove actualTimesheets
     *
     * @param \Homecare\HomecareBundle\Entity\Timesheet $actualTimesheets
     */
    public function removeActualTimesheet(\Homecare\HomecareBundle\Entity\Timesheet $actualTimesheets)
    {
        $this->actualTimesheets->removeElement($actualTimesheets);
    }

    /**
     * Get actualTimesheets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActualTimesheets()
    {
        return $this->actualTimesheets;
    }


    /**
     * Add recipientPca
     *
     * @param \Homecare\HomecareBundle\Entity\RecipientPca $recipientPca
     *
     * @return Recipient
     */
    public function addRecipientPca(\Homecare\HomecareBundle\Entity\RecipientPca $recipientPca)
    {
        $this->recipientPca[] = $recipientPca;

        return $this;
    }

    /**
     * Remove recipientPca
     *
     * @param \Homecare\HomecareBundle\Entity\RecipientPca $recipientPca
     */
    public function removeRecipientPca(\Homecare\HomecareBundle\Entity\RecipientPca $recipientPca)
    {
        $this->recipientPca->removeElement($recipientPca);
    }

    /**
     * Get recipientPca
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecipientPca()
    {
        return $this->recipientPca;
    }


    /**
     * Set companyAssignedId
     *
     * @param string $companyAssignedId
     *
     * @return Recipient
     */
    public function setCompanyAssignedId($companyAssignedId)
    {
        $this->companyAssignedId = $companyAssignedId;

        return $this;
    }

    /**
     * Get companyAssignedId
     *
     * @return string
     */
    public function getCompanyAssignedId()
    {
        return $this->companyAssignedId;
    }

    /**
     * Set skipVerification
     *
     * @param boolean $skipVerification
     *
     * @return Recipient
     */
    public function setSkipVerification($skipVerification)
    {
        $this->skipVerification = $skipVerification;

        return $this;
    }

    /**
     * Get skipVerification
     *
     * @return boolean
     */
    public function getSkipVerification()
    {
        return $this->skipVerification;
    }

    /**
     * Set skipGps
     *
     * @param boolean $skipGps
     *
     * @return Recipient
     */
    public function setSkipGps($skipGps)
    {
        $this->skipGps = $skipGps;

        return $this;
    }

    /**
     * Get skipGps
     *
     * @return boolean
     */
    public function getSkipGps()
    {
        return $this->skipGps;
    }


    /**
     * Add request
     *
     * @param \Homecare\HomecareBundle\Entity\Request $request
     *
     * @return Recipient
     */
    public function addRequest(\Homecare\HomecareBundle\Entity\Request $request)
    {
        $this->requests[] = $request;

        return $this;
    }

    /**
     * Remove request
     *
     * @param \Homecare\HomecareBundle\Entity\Request $request
     */
    public function removeRequest(\Homecare\HomecareBundle\Entity\Request $request)
    {
        $this->requests->removeElement($request);
    }

    /**
     * Get requests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRequests()
    {
        return $this->requests;
    }


    /**
     * Set user
     *
     * @param \Homecare\HomecareBundle\Entity\User $user
     *
     * @return Recipient
     */
    public function setUser(\Homecare\HomecareBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Homecare\HomecareBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set genderPreference
     *
     * @param string $genderPreference
     *
     * @return Recipient
     */
    public function setGenderPreference($genderPreference)
    {
        $this->genderPreference = $genderPreference;

        return $this;
    }

    /**
     * Get genderPreference
     *
     * @return integer
     */
    public function getGenderPreference()
    {
        return $this->genderPreference;
    }

    /**
     * Set county
     *
     * @param \Homecare\HomecareBundle\Entity\County $county
     *
     * @return Recipient
     */
    public function setCounty(\Homecare\HomecareBundle\Entity\County $county = null)
    {
        $this->county = $county;

        return $this;
    }

    /**
     * Get county
     *
     * @return \Homecare\HomecareBundle\Entity\County
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * Add archivedVerification
     *
     * @param \Homecare\HomecareBundle\Entity\Verification $archivedVerification
     *
     * @return Recipient
     */
    public function addArchivedVerification(\Homecare\HomecareBundle\Entity\Verification $archivedVerification)
    {
        $this->archivedVerifications[] = $archivedVerification;

        return $this;
    }

    /**
     * Remove archivedVerification
     *
     * @param \Homecare\HomecareBundle\Entity\Verification $archivedVerification
     */
    public function removeArchivedVerification(\Homecare\HomecareBundle\Entity\Verification $archivedVerification)
    {
        $this->archivedVerifications->removeElement($archivedVerification);
    }

    /**
     * Get archivedVerifications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArchivedVerifications()
    {
        return $this->archivedVerifications;
    }

    /**
     * @return string
     */
    public function getHomeAddress()
    {
        return $this->homeAddress;
    }

    /**
     * @param string $homeAddress
     */
    public function setHomeAddress($homeAddress)
    {
        $this->homeAddress = $homeAddress;
    }


}
