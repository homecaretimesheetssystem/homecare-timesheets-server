<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation;

/**
 * County
 *
 * @ORM\Table(name="county")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\CountyRepository")
 * @Annotation\ExclusionPolicy("all")
 */
class County
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Annotation\Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Annotation\Expose
     */
    private $name;


    /**
     * @ORM\OneToMany(targetEntity="Recipient", mappedBy="recipient")
     */
    private $recipients;


    /**
     * @ORM\ManyToMany(targetEntity="Homecare\HomecareBundle\Entity\Pca", mappedBy="counties")
     */
    private $pcas;


    public function __construct()
    {
        $this->recipients = new ArrayCollection();
        $this->pcas       = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return County
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add recipient
     *
     * @param \Homecare\HomecareBundle\Entity\Recipient $recipient
     *
     * @return County
     */
    public function addRecipient(\Homecare\HomecareBundle\Entity\Recipient $recipient)
    {
        $this->recipients[] = $recipient;

        return $this;
    }

    /**
     * Remove recipient
     *
     * @param \Homecare\HomecareBundle\Entity\Recipient $recipient
     */
    public function removeRecipient(\Homecare\HomecareBundle\Entity\Recipient $recipient)
    {
        $this->recipients->removeElement($recipient);
    }

    /**
     * Get recipients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecipients()
    {
        return $this->recipients;
    }



    /**
     * Add pca
     *
     * @param \Homecare\HomecareBundle\Entity\Pca $pca
     *
     * @return County
     */
    public function addPca(\Homecare\HomecareBundle\Entity\Pca $pca)
    {
        $this->pcas[] = $pca;

        return $this;
    }

    /**
     * Remove pca
     *
     * @param \Homecare\HomecareBundle\Entity\Pca $pca
     */
    public function removePca(\Homecare\HomecareBundle\Entity\Pca $pca)
    {
        $this->pcas->removeElement($pca);
    }

    /**
     * Get pcas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPcas()
    {
        return $this->pcas;
    }
}
