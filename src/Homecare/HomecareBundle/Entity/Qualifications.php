<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;

/**
 * Services
 *
 * @ORM\Table(name="qualifications")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\QualificationsRepository")
 * @Annotation\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks()
 */
class Qualifications
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Annotation\Expose
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="qualification", type="string", length=255)
     * @Annotation\Expose
     * @Encrypted
     */
    private $qualification;


    /**
     * @ORM\ManyToMany(targetEntity="Pca", mappedBy="qualifications")
     */
    private $pcas;


    /**
     * @ORM\ManyToMany(targetEntity="Request", mappedBy="qualifications")
     */
    private $requests;


    public function __toString()
    {
        return $this->getQualification();
    }


    public function __construct()
    {
        $this->pcas     = new ArrayCollection();
        $this->requests = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qualification
     *
     * @param string $qualification
     *
     * @return Qualifications
     */
    public function setQualification($qualification)
    {
        $this->qualification = $qualification;

        return $this;
    }

    /**
     * Get qualification
     *
     * @return string
     */
    public function getQualification()
    {
        return $this->qualification;
    }

    /**
     * Add pca
     *
     * @param \Homecare\HomecareBundle\Entity\Pca $pca
     *
     * @return Qualifications
     */
    public function addPca(\Homecare\HomecareBundle\Entity\Pca $pca)
    {
        $this->pcas[] = $pca;

        return $this;
    }

    /**
     * Remove pca
     *
     * @param \Homecare\HomecareBundle\Entity\Pca $pca
     */
    public function removePca(\Homecare\HomecareBundle\Entity\Pca $pca)
    {
        $this->pcas->removeElement($pca);
    }

    /**
     * Get pcas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPcas()
    {
        return $this->pcas;
    }

    /**
     * Add request
     *
     * @param \Homecare\HomecareBundle\Entity\Request $request
     *
     * @return Qualifications
     */
    public function addRequest(\Homecare\HomecareBundle\Entity\Request $request)
    {
        $this->requests[] = $request;

        return $this;
    }

    /**
     * Remove request
     *
     * @param \Homecare\HomecareBundle\Entity\Request $request
     */
    public function removeRequest(\Homecare\HomecareBundle\Entity\Request $request)
    {
        $this->requests->removeElement($request);
    }

    /**
     * Get requests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRequests()
    {
        return $this->requests;
    }
}
