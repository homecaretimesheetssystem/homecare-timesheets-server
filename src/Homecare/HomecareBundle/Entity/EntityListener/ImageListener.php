<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 10/18/16
 * Time: 8:49 PM
 */

namespace Homecare\HomecareBundle\Entity\EntityListener;

use Homecare\HomecareBundle\Entity\Image;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;

/**
 * Class ImageListener
 *
 * @author Josh Crawmer <joshcrawmer4@gmail.com>
 */
class ImageListener
{


    /**
     * Make sure the file has a name before persisting
     *
     * @param Image $image
     * @param LifecycleEventArgs $args
     */
    public function prePersist(Image $image, LifecycleEventArgs $args)
    {
        $image->preUpload();
    }

    /**
     * Make sure the file has a name before updating
     *
     * @param Image $image
     * @param PreUpdateEventArgs $args
     */
    public function preUpdate(Image $image, PreUpdateEventArgs $args)
    {
        $image->preUpload();
    }

    /**
     * Upload the file after persisting the entity
     *
     * @param Image $image
     * @param LifecycleEventArgs $args
     */
    public function postPersist(Image $image, LifecycleEventArgs $args)
    {
        $image->upload();

    }

    /**
     * Upload the file after updating the entity
     *
     * @param Image $image
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(Image $image, LifecycleEventArgs $args)
    {
        $image->upload();
    }


    /**
     * Delete the file after removing the entity
     *
     * @param Image $image
     * @param LifecycleEventArgs $args
     */
    public function preRemove(Image $image, LifecycleEventArgs $args)
    {
        $actualClass = ClassUtils::getClass($image);

        if ($actualClass !== get_class($image)) {
            /*
             * working with an entity proxy, force load the properties
             * needed in postRemove() now since the proxy can't be
             * loaded once the postRemove() handler is invoked
             */
            $image->getWebPath();
            $image->getAbsolutePath();
        }
    }

    /**
     * Delete the file after removing the entity
     *
     * @param Image $image
     * @param LifecycleEventArgs $args
     */
    public function postRemove(Image $image, LifecycleEventArgs $args)
    {
        $image->removeUpload();
    }
}
