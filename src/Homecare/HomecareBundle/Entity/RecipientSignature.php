<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;

/**
 * RecipientSignature
 *
 * @ORM\Table(name="recipient_signature")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\RecipientSignatureRepository")
 * @Annotation\ExclusionPolicy("all")
 */
class RecipientSignature
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
		 * @Type("integer")
		 * @SerializedName("id")
		 * @Annotation\Expose
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="recipientSigTime", type="datetime", nullable=true)
		 * @Type("DateTime")
		 * @SerializedName("recipientSigTime")
		 * @Annotation\Expose
     */
    private $recipientSigTime;

    /**
     * @var string
     *
     * @ORM\Column(name="recipientSigAddress", type="string", length=255, nullable=true)
		 * @Type("string")
		 * @SerializedName("recipientSigAddress")
		 * @Annotation\Expose
     * @Encrypted
     */
    private $recipientSigAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=255, nullable=true)
		 * @Type("string")
		 * @SerializedName("latitude")
		 * @Annotation\Expose
     * @Encrypted
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length=255, nullable=true)
		 * @Type("string")
		 * @SerializedName("longitude")
		 * @Annotation\Expose
     * @Encrypted
     */
    private $longitude;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="recipientPhotoTime", type="datetime", nullable=true)
		 * @Type("DateTime")
		 * @SerializedName("recipientPhotoTime")
		 * @Annotation\Expose
     */
    private $recipientPhotoTime;
		
		
		
		
		
    /**
     * @ORM\OneToOne(targetEntity="Timesheet", inversedBy="recipientSignature")
     * @Type("Homecare\HomecareBundle\Entity\Timesheet")
	 * @SerializedName("timesheet")
     */
    private $timesheet;

		
		


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recipientSigTime
     *
     * @param \DateTime $recipientSigTime
     * @return RecipientSignature
     */
    public function setRecipientSigTime($recipientSigTime)
    {
        $this->recipientSigTime = $recipientSigTime;

        return $this;
    }

    /**
     * Get recipientSigTime
     *
     * @return \DateTime 
     */
    public function getRecipientSigTime()
    {
        return $this->recipientSigTime;
    }

    /**
     * Set recipientSigAddress
     *
     * @param string $recipientSigAddress
     * @return RecipientSignature
     */
    public function setRecipientSigAddress($recipientSigAddress)
    {
        $this->recipientSigAddress = $recipientSigAddress;

        return $this;
    }

    /**
     * Get recipientSigAddress
     *
     * @return string 
     */
    public function getRecipientSigAddress()
    {
        return $this->recipientSigAddress;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     * @return RecipientSignature
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return RecipientSignature
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set recipientPhotoTime
     *
     * @param \DateTime $recipientPhotoTime
     * @return RecipientSignature
     */
    public function setRecipientPhotoTime($recipientPhotoTime)
    {
        $this->recipientPhotoTime = $recipientPhotoTime;

        return $this;
    }

    /**
     * Get recipientPhotoTime
     *
     * @return \DateTime 
     */
    public function getRecipientPhotoTime()
    {
        return $this->recipientPhotoTime;
    }

    /**
     * Set timesheet
     *
     * @param \Homecare\HomecareBundle\Entity\Timesheet $timesheet
     * @return RecipientSignature
     */
    public function setTimesheet(\Homecare\HomecareBundle\Entity\Timesheet $timesheet = null)
    {
        $this->timesheet = $timesheet;

        return $this;
    }

    /**
     * Get timesheet
     *
     * @return \Homecare\HomecareBundle\Entity\Timesheet
     */
    public function getTimesheet()
    {
        return $this->timesheet;
    }
}
