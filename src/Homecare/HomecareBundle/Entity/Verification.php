<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;

/**
 * Verification
 *
 * @ORM\Table(name="verification")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\VerificationRepository")
 * @Annotation\ExclusionPolicy("all")
 * @ORM\HasLifecycleCallbacks()
 */
class Verification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     * @SerializedName("id")
     * @Annotation\Expose
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="datetime", nullable=true)
     * @Type("DateTime")
     * @SerializedName("time")
     * @Annotation\Expose
     */
    private $time;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     * @Type("string")
     * @SerializedName("address")
     * @Annotation\Expose
     * @Encrypted
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="latitude", type="string", length=255, nullable=true)
     * @Type("string")
     * @SerializedName("latitude")
     * @Annotation\Expose
     * @Encrypted
     */
    private $latitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="string", length=255, nullable=true)
     * @Type("string")
     * @SerializedName("longitude")
     * @Annotation\Expose
     * @Encrypted
     */
    private $longitude;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=true)
     * @Annotation\Expose
     */
    private $startDate;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     * @Annotation\Expose
     */
    private $endDate;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="verification_date", type="datetime", nullable=true)
     * @Annotation\Expose
     */
    private $verificationDate;


    /**
     * @var integer
     *
     * @ORM\Column(name="frequency", type="integer", nullable=true)
     * @Annotation\Expose
     */
    private $frequency;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     * @Annotation\Expose
     */
    private $createdAt;


    /**
     * @ORM\ManyToOne(targetEntity="RecipientPca", cascade={"persist"}, inversedBy="verification")
     */
    private $recipientPca;


    /**
     * @ORM\OneToOne(targetEntity="Timesheet", mappedBy="verification")
     */
    private $timesheet;


    /**
     * @var string
     *
     * @ORM\Column(name="verification_photo", type="string", length=255, nullable=true)
     * @Annotation\Expose
     * @Type("string")
     * @SerializedName("verificationPhoto")
     */
    private $verificationPhoto;


    /**
     * @var boolean
     * @ORM\Column(name="first_time", type="boolean")
     */
    private $firstTime;


    /**
     * @var boolean
     * @ORM\Column(name="current", type="boolean")
     */
    private $current;


    /**
     * @var boolean
     * @ORM\Column(name="verified", type="boolean")
     */
    private $verified;


    /**
     * @var Recipient
     * @ORM\ManyToOne(targetEntity="Recipient", inversedBy="archivedVerifications")
     * @Annotation\Expose
     */
    private $archivedRecipient;


    /**
     * @var Pca
     * @ORM\ManyToOne(targetEntity="Pca", inversedBy="archivedVerifications")
     * @Annotation\Expose
     */
    private $archivedPca;


    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->verified  = false;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return Verification
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Verification
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return Verification
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return Verification
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }


    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Verification
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Verification
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set verificationDate
     *
     * @param \DateTime $verificationDate
     *
     * @return Verification
     */
    public function setVerificationDate($verificationDate)
    {
        $this->verificationDate = $verificationDate;

        return $this;
    }

    /**
     * Get verificationDate
     *
     * @return \DateTime
     */
    public function getVerificationDate()
    {
        return $this->verificationDate;
    }


    /**
     * Set frequency
     *
     * @param integer $frequency
     *
     * @return Verification
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Get frequency
     *
     * @return integer
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Verification
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }


    /**
     * Set recipientPca
     *
     * @param \Homecare\HomecareBundle\Entity\RecipientPca $recipientPca
     *
     * @return Verification
     */
    public function setRecipientPca(\Homecare\HomecareBundle\Entity\RecipientPca $recipientPca = null)
    {
        $this->recipientPca = $recipientPca;

        return $this;
    }

    /**
     * Get recipientPca
     *
     * @return \Homecare\HomecareBundle\Entity\RecipientPca
     */
    public function getRecipientPca()
    {
        return $this->recipientPca;
    }

    /**
     * Set verificationPhoto
     *
     * @param string $verificationPhoto
     *
     * @return Verification
     */
    public function setVerificationPhoto($verificationPhoto)
    {
        $this->verificationPhoto = $verificationPhoto;

        return $this;
    }

    /**
     * Get verificationPhoto
     *
     * @return string
     */
    public function getVerificationPhoto()
    {
        return $this->verificationPhoto;
    }

    /**
     * Set firstTime
     *
     * @param boolean $firstTime
     *
     * @return Verification
     */
    public function setFirstTime($firstTime)
    {
        $this->firstTime = $firstTime;

        return $this;
    }

    /**
     * Get firstTime
     *
     * @return boolean
     */
    public function getFirstTime()
    {
        return $this->firstTime;
    }

    /**
     * Set current
     *
     * @param boolean $current
     *
     * @return Verification
     */
    public function setCurrent($current)
    {
        $this->current = $current;

        return $this;
    }

    /**
     * Get current
     *
     * @return boolean
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * Set verified
     *
     * @param boolean $verified
     *
     * @return Verification
     */
    public function setVerified($verified)
    {
        $this->verified = $verified;

        return $this;
    }

    /**
     * Get verified
     *
     * @return boolean
     */
    public function getVerified()
    {
        return $this->verified;
    }


    /**
     * Set timesheet
     *
     * @param \Homecare\HomecareBundle\Entity\Timesheet $timesheet
     *
     * @return Verification
     */
    public function setTimesheet(\Homecare\HomecareBundle\Entity\Timesheet $timesheet = null)
    {
        $this->timesheet = $timesheet;

        return $this;
    }

    /**
     * Get timesheet
     *
     * @return \Homecare\HomecareBundle\Entity\Timesheet
     */
    public function getTimesheet()
    {
        return $this->timesheet;
    }

  



    /**
     * Set archivedRecipient
     *
     * @param integer $archivedRecipient
     *
     * @return Verification
     */
    public function setArchivedRecipient($archivedRecipient)
    {
        $this->archivedRecipient = $archivedRecipient;

        return $this;
    }

    /**
     * Get archivedRecipient
     *
     * @return integer
     */
    public function getArchivedRecipient()
    {
        return $this->archivedRecipient;
    }

    /**
     * Set archivedPca
     *
     * @param integer $archivedPca
     *
     * @return Verification
     */
    public function setArchivedPca($archivedPca)
    {
        $this->archivedPca = $archivedPca;

        return $this;
    }

    /**
     * Get archivedPca
     *
     * @return integer
     */
    public function getArchivedPca()
    {
        return $this->archivedPca;
    }
}
