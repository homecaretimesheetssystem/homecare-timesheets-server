<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmailSettings
 *
 * @ORM\Table(name="email_settings")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\EmailSettingsRepository")
 */
class EmailSettings
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;


    /**
     * @ORM\ManyToOne(targetEntity="Agency", inversedBy="emails")
     */
    private $agency;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return EmailSettings
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set agency
     *
     * @param \Homecare\HomecareBundle\Entity\Agency $agency
     * @return EmailSettings
     */
    public function setAgency(\Homecare\HomecareBundle\Entity\Agency $agency = null)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get agency
     *
     * @return \Homecare\HomecareBundle\Entity\Agency 
     */
    public function getAgency()
    {
        return $this->agency;
    }
}
