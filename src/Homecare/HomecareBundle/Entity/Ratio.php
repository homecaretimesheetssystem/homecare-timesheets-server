<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;

/**
 * Ratio
 *
 * @ORM\Table(name="ratio")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\RatioRepository")
 * @Annotation\ExclusionPolicy("all")
 */
class Ratio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     * @SerializedName("id")
     * @Annotation\Expose
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ratio", type="string", length=255)
     * @Type("string")
     * @SerializedName("ratio")
     * @Annotation\Expose
     * @Encrypted
     */
    private $ratio;


    /**
     * @ORM\OneToMany(targetEntity="SessionData", mappedBy="ratio")
     */
    private $sessionData;



    public function __construct() {
    $this->sessionData = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ratio
     *
     * @param string $ratio
     * @return Ratio
     */
    public function setRatio($ratio)
    {
        $this->ratio = $ratio;

        return $this;
    }

    /**
     * Get ratio
     *
     * @return string 
     */
    public function getRatio()
    {
        return $this->ratio;
    }

    /**
     * Add sessionData
     *
     * @param \Homecare\HomecareBundle\Entity\SessionData $sessionData
     * @return Ratio
     */
    public function addSessionDatum(\Homecare\HomecareBundle\Entity\SessionData $sessionData)
    {
        $this->sessionData[] = $sessionData;

        return $this;
    }

    /**
     * Remove sessionData
     *
     * @param \Homecare\HomecareBundle\Entity\SessionData $sessionData
     */
    public function removeSessionDatum(\Homecare\HomecareBundle\Entity\SessionData $sessionData)
    {
        $this->sessionData->removeElement($sessionData);
    }

    /**
     * Get sessionData
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSessionData()
    {
        return $this->sessionData;
    }
}
