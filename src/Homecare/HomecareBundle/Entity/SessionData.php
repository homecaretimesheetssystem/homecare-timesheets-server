<?php

namespace Homecare\HomecareBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation;

/**
 * SessionData
 *
 * @ORM\Table(name="session_data")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\SessionDataRepository")
 * @Annotation\ExclusionPolicy("all")
 */
class SessionData
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
	 * @Type("integer")
	 * @SerializedName("id")
     * @Annotation\Expose
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="continueTimesheetNumber", type="integer", nullable=true)
     * @Type("integer")
	 * @SerializedName("continueTimesheetNumber")
	 * @Annotation\Expose
     */
    private $continueTimesheetNumber;
		
    /**
     * @var integer
     *
     * @ORM\Column(name="currentTimesheetNumber", type="integer", nullable=true)
	 * @Type("integer")
	 * @SerializedName("currentTimesheetNumber")
	 * @Annotation\Expose
     */
    private $currentTimesheetNumber;

    /**
     * @var boolean
     *
     * @ORM\Column(name="flagForFurtherInvestigation", type="boolean", nullable=true)
	 * @Type("boolean")
	 * @SerializedName("flagForFurtherInvestigation")
	 * @Annotation\Expose
     */
    private $flagForFurtherInvestigation;
		
		
   
    /**
     * @var integer
     *
     * @ORM\Column(name="currentTimesheetId", type="integer", nullable=true)
     * @Type("integer")
	 * @SerializedName("currentTimesheetId")
     * @Annotation\Expose
     */
    private $currentTimesheetId;



		
    /**
     * @ORM\ManyToOne(targetEntity="Pca", inversedBy="sessionData")
     * @Type("Homecare\HomecareBundle\Entity\Pca")
	 * @SerializedName("pca")
     * @Annotation\Expose
     */
    private $pca;


    /**
     * @ORM\ManyToOne(targetEntity="Services", inversedBy="sessionData")
     * @Type("Homecare\HomecareBundle\Entity\Services")
     * @SerializedName("service")
     * @Annotation\Expose
     */
    private $service;




    /**
     * @ORM\OneToOne(targetEntity="TimeIn", inversedBy="sessionData", orphanRemoval=true)
     * @Type("Homecare\HomecareBundle\Entity\TimeIn")
     * @SerializedName("timeIn")
     * @Annotation\Expose
     */
    private $timeIn;


    /**
     * @ORM\OneToOne(targetEntity="TimeOut", inversedBy="sessionData", orphanRemoval=true)
     * @Type("Homecare\HomecareBundle\Entity\TimeOut")
     * @SerializedName("timeOut")
     * @Annotation\Expose
     */
    private $timeOut;



    /**
     * @ORM\OneToMany(targetEntity="Timesheet", mappedBy="sessionData", orphanRemoval=true)
     * @Type("Homecare\HomecareBundle\Entity\Timesheet")
     * @SerializedName("timesheets")
     * @Annotation\Expose
     */
    private $timesheets;


    /**
     * @var
     * @ORM\Column(name="dateEnding",type="datetime",nullable=true)
     * @Type("DateTime")
     * @SerializedName("dateEnding")
     * @Annotation\Expose
     *
     */
    private $dateEnding;



    /**
     * @ORM\ManyToOne(targetEntity="Ratio", inversedBy="sessionData")
     * @Type("Homecare\HomecareBundle\Entity\Ratio")
     * @SerializedName("ratio")
     * @Annotation\Expose
     */
    private $ratio;


    /**
     * @ORM\ManyToOne(targetEntity="SharedCareLocation", inversedBy="sessionData")
     * @Type("Homecare\HomecareBundle\Entity\SharedCareLocation")
     * @SerializedName("sharedCareLocation")
     * @Annotation\Expose
     */
    private $sharedCareLocation;


    /**
     * If this session was created through an rsvp
     *
     * @ORM\OneToOne(targetEntity="Rsvp", mappedBy="sessionData", orphanRemoval=true)
     */
    private $rsvp;









    public function __construct() {
        $this->timesheets = new ArrayCollection();
    }



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set continueTimesheetNumber
     *
     * @param integer $continueTimesheetNumber
     * @return SessionData
     */
    public function setContinueTimesheetNumber($continueTimesheetNumber)
    {
        $this->continueTimesheetNumber = $continueTimesheetNumber;

        return $this;
    }

    /**
     * Get continueTimesheetNumber
     *
     * @return integer 
     */
    public function getContinueTimesheetNumber()
    {
        return $this->continueTimesheetNumber;
    }

    /**
     * Set flagForFurtherInvestigation
     *
     * @param boolean $flagForFurtherInvestigation
     * @return SessionData
     */
    public function setFlagForFurtherInvestigation($flagForFurtherInvestigation)
    {
        $this->flagForFurtherInvestigation = $flagForFurtherInvestigation;

        return $this;
    }

    /**
     * Get flagForFurtherInvestigation
     *
     * @return boolean 
     */
    public function getFlagForFurtherInvestigation()
    {
        return $this->flagForFurtherInvestigation;
    }

    /**
     * Set currentTimesheetId
     *
     * @param integer $currentTimesheetId
     * @return SessionData
     */
    public function setCurrentTimesheetId($currentTimesheetId)
    {
        $this->currentTimesheetId = $currentTimesheetId;

        return $this;
    }

    /**
     * Get currentTimesheetId
     *
     * @return integer 
     */
    public function getCurrentTimesheetId()
    {
        return $this->currentTimesheetId;
    }


    /**
     * Set currentTimesheetNumber
     *
     * @param integer $currentTimesheetNumber
     * @return SessionData
     */
    public function setCurrentTimesheetNumber($currentTimesheetNumber)
    {
        $this->currentTimesheetNumber = $currentTimesheetNumber;

        return $this;
    }

    /**
     * Get currentTimesheetNumber
     *
     * @return integer 
     */
    public function getCurrentTimesheetNumber()
    {
        return $this->currentTimesheetNumber;
    }

    /**
     * Set pca
     *
     * @param \Homecare\HomecareBundle\Entity\Pca $pca
     * @return SessionData
     */
    public function setPca(\Homecare\HomecareBundle\Entity\Pca $pca = null)
    {
        $this->pca = $pca;

        return $this;
    }

    /**
     * Get pca
     *
     * @return \Homecare\HomecareBundle\Entity\Pca 
     */
    public function getPca()
    {
        return $this->pca;
    }

    /**
     * Set service
     *
     * @param \Homecare\HomecareBundle\Entity\Services $service
     * @return SessionData
     */
    public function setService(\Homecare\HomecareBundle\Entity\Services $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \Homecare\HomecareBundle\Entity\Services 
     */
    public function getService()
    {
        return $this->service;
    }

   

    /**
     * Set timeIn
     *
     * @param \Homecare\HomecareBundle\Entity\timeIn $timeIn
     * @return SessionData
     */
    public function setTimeIn(\Homecare\HomecareBundle\Entity\timeIn $timeIn = null)
    {
        $this->timeIn = $timeIn;

        return $this;
    }

    /**
     * Get timeIn
     *
     * @return \Homecare\HomecareBundle\Entity\timeIn 
     */
    public function getTimeIn()
    {
        return $this->timeIn;
    }

    /**
     * Set timeOut
     *
     * @param \Homecare\HomecareBundle\Entity\timeOut $timeOut
     * @return SessionData
     */
    public function setTimeOut(\Homecare\HomecareBundle\Entity\timeOut $timeOut = null)
    {
        $this->timeOut = $timeOut;

        return $this;
    }

    /**
     * Get timeOut
     *
     * @return \Homecare\HomecareBundle\Entity\timeOut 
     */
    public function getTimeOut()
    {
        return $this->timeOut;
    }

    /**
     * Add timesheets
     *
     * @param \Homecare\HomecareBundle\Entity\Timesheet $timesheets
     * @return SessionData
     */
    public function addTimesheet(\Homecare\HomecareBundle\Entity\Timesheet $timesheets)
    {
        $this->timesheets[] = $timesheets;

        return $this;
    }

    /**
     * Remove timesheets
     *
     * @param \Homecare\HomecareBundle\Entity\Timesheet $timesheets
     */
    public function removeTimesheet(\Homecare\HomecareBundle\Entity\Timesheet $timesheets)
    {
        $this->timesheets->removeElement($timesheets);
    }

    /**
     * Get timesheets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTimesheets()
    {
        return $this->timesheets;
    }




    /**
     * Set dateEnding
     *
     * @param \DateTime $dateEnding
     * @return SessionData
     */
    public function setDateEnding($dateEnding)
    {
        $this->dateEnding = $dateEnding;

        return $this;
    }

    /**
     * Get dateEnding
     *
     * @return \DateTime 
     */
    public function getDateEnding()
    {
        return $this->dateEnding;
    }

    /**
     * Set ratio
     *
     * @param \Homecare\HomecareBundle\Entity\Ratio $ratio
     * @return SessionData
     */
    public function setRatio(\Homecare\HomecareBundle\Entity\Ratio $ratio = null)
    {
        $this->ratio = $ratio;

        return $this;
    }

    /**
     * Get ratio
     *
     * @return \Homecare\HomecareBundle\Entity\Ratio
     */
    public function getRatio()
    {
        return $this->ratio;
    }

    /**
     * Set sharedCareLocation
     *
     * @param \Homecare\HomecareBundle\Entity\SharedCareLocation $sharedCareLocation
     * @return SessionData
     */
    public function setSharedCareLocation(\Homecare\HomecareBundle\Entity\SharedCareLocation $sharedCareLocation = null)
    {
        $this->sharedCareLocation = $sharedCareLocation;

        return $this;
    }

    /**
     * Get sharedCareLocation
     *
     * @return \Homecare\HomecareBundle\Entity\SharedCareLocation
     */
    public function getSharedCareLocation()
    {
        return $this->sharedCareLocation;
    }




    /**
     * Set rsvp
     *
     * @param \Homecare\HomecareBundle\Entity\Rsvp $rsvp
     *
     * @return SessionData
     */
    public function setRsvp(\Homecare\HomecareBundle\Entity\Rsvp $rsvp = null)
    {
        $this->rsvp = $rsvp;

        return $this;
    }

    /**
     * Get rsvp
     *
     * @return \Homecare\HomecareBundle\Entity\Rsvp
     */
    public function getRsvp()
    {
        return $this->rsvp;
    }
}
