<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 9/8/16
 * Time: 9:02 PM
 */

namespace Homecare\HomecareBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use Symfony\Component\HttpFoundation\File\File;
use JMS\Serializer\Annotation\Type;


/**
 * UnsignedTimesheetEmail
 *
 * @ORM\Table(name="unsignedtimesheetemail")
 * @ORM\Entity(repositoryClass="Homecare\HomecareBundle\Entity\Repository\UnsignedTimesheetEmailRepository")
 *
 */
class UnsignedTimesheetEmail
{


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Type("integer")
     */
    private $id;


    /**
     * @ORM\OneToOne(targetEntity="Timesheet", inversedBy="unsignedTimesheetEmail")
     */
    private $timesheet;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="nextEmailSendDate", type="datetime", nullable=true)
     */
    private $nextEmailSendDate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastEmailSent
     *
     * @param \DateTime $nextEmailSendDate
     *
     * @return UnsignedTimesheetEmail
     */
    public function setNextEmailSendDate($nextEmailSendDate)
    {
        $this->nextEmailSendDate = clone $nextEmailSendDate;

        return $this;
    }

    /**
     * Get lastEmailSent
     *
     * @return \DateTime
     */
    public function getNextEmailSendDate()
    {
        return clone $this->nextEmailSendDate;
    }

    /**
     * Set timesheet
     *
     * @param \Homecare\HomecareBundle\Entity\Timesheet $timesheet
     *
     * @return UnsignedTimesheetEmail
     */
    public function setTimesheet(\Homecare\HomecareBundle\Entity\Timesheet $timesheet = null)
    {
        $this->timesheet = $timesheet;

        return $this;
    }

    /**
     * Get timesheet
     *
     * @return \Homecare\HomecareBundle\Entity\Timesheet
     */
    public function getTimesheet()
    {
        return $this->timesheet;
    }
}
