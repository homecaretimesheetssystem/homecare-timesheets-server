<?php

namespace Homecare\HomecareBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FilterController extends Controller
{
    public function showFilterAction( $dateFilter, $textToFilterBy, $startDate, $endDate )
    {
        return $this->render('HomecareHomecareBundle:Filter:filter.html.twig', array(
					'dateFilter' => $dateFilter,
					'textToFilterBy' => $textToFilterBy,
					'startDate' => $startDate,
					'endDate' => $endDate,
            ));    }

}
