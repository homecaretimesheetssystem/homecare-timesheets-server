<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 4/30/15
 * Time: 2:11 PM
 */

namespace Homecare\HomecareBundle\Controller;


use Homecare\HomecareBundle\Entity\User;
use Homecare\HomecareBundle\Form\AdminType;
use Homecare\HomecareBundle\Form\User2Type;
use Homecare\HomecareBundle\Form\UserType;
use Homecare\HomecareBundle\Entity\Admin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Homecare\HomecareBundle\Entity\Agency;
use Homecare\HomecareBundle\Entity\Services;
use Homecare\HomecareBundle\Form\AgencyType;
use Doctrine\Common\Collections\ArrayCollection;

class AdminController extends Controller
{


    public function createAgencyUserAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        //grab the current logged in user;
        $user = $this->get('security.token_storage')->getToken()->getUser();
        //create a new user object
        $newUser = new User();
        $form    = $this->createForm(
            'homecare_homecarebundle_user',
            $newUser,
            array('validation_groups' => array('Default', 'PASSWORD', 'CREATE_AGENCY'))
        );
        $form->add('submit', 'submit', ['label' => 'Submit']);

        //if the form has been submitted
        if ($request->getMethod() == "POST") {
            //If the form was submitted then bind the request variables to the form User object
            $form->handleRequest($request);

            if ($form->isValid()) {
                $newUser->addRole('ROLE_AGENCY');
                //grab the password from the form
                $plainPassword = $newUser->getPassword();
                //encode the password
                $encoded = $this->container->get('security.password_encoder')
                                           ->encodePassword($newUser, $plainPassword);
                $newUser->setPassword($encoded);
                $newUser->setEnabled(true);
                $em->persist($newUser);
                $em->flush();
                $request->getSession()->getFlashBag()->add(
                    'notice',
                    'Agency User Successfully Created!'
                );

                return $this->redirectToRoute('Homecare_HomecareBundle_admin_create_agency_user');
            }
        }

        return $this->render(
            'HomecareHomecareBundle:Admin:createAgencyUser.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }


    public function editAgencyUserAction($userId, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        //grab the current logged in user;
        $user = $this->get('security.token_storage')->getToken()->getUser();
        //create a new user object

        $editUser = $this->getDoctrine()->getRepository('HomecareHomecareBundle:User')->find($userId);
        $form     = $this->createForm(
            'homecare_homecarebundle_user',
            $editUser,
            array('validation_groups' => array('EDIT_USER', 'CREATE_AGENCY'))
        );
        $form->add('submit', 'submit', ['label' => 'Submit']);

        $originalPassword = $editUser->getPassword();

        //if the form has been submitted
        if ($request->getMethod() == "POST") {
            //If the form was submitted then bind the request variables to the form User object
            $form->handleRequest($request);

            if ($form->isValid()) {
                $editUser->addRole('ROLE_AGENCY');
                //grab the password from the form
                $plainPassword = $editUser->getPassword();
                if ( ! empty($plainPassword)) {
                    //encode the password
                    $encoded = $this->container->get('security.password_encoder')
                                               ->encodePassword($editUser, $plainPassword);
                    $editUser->setPassword($encoded);
                } else {
                    $editUser->setPassword($originalPassword);
                }

                $editUser->setEnabled(true);
                $em->persist($editUser);
                $em->flush();
                $request->getSession()->getFlashBag()->add(
                    'notice',
                    'Agency User Successfully Modified!'
                );

                return $this->redirectToRoute(
                    'Homecare_HomecareBundle_admin_edit_agency_user',
                    array('userId' => $userId)
                );
            }
        }

        return $this->render(
            'HomecareHomecareBundle:Admin:editAgencyUser.html.twig',
            array(
                'form' => $form->createView(),
            )
        );

    }


    public function createAdminUserAction(Request $request)
    {


        $adminUser = $this->get('security.token_storage')->getToken()->getUser();
        $em        = $this->getDoctrine()->getManager();
        //create a new user object
        $user = new User();

        $form = $this->createForm(
            'homecare_homecarebundle_user',
            $user,
            array('validation_groups' => array('Default', 'PASSWORD', 'CREATE_AGENCY'))
        );
        $form->add('submit', 'submit', ['label' => 'Submit']);

        //if the form has been submitted
        if ($request->getMethod() == "POST") {
            //If the form was submitted then bind the request variables to the form User object
            $form->handleRequest($request);

            if ($form->isValid()) {

                $user->addRole('ROLE_ADMIN');
                //grab the password from the form
                $plainPassword = $user->getPassword();
                //encode the password
                $encoded = $this->container->get('security.password_encoder')
                                           ->encodePassword($user, $plainPassword);
                $user->setPassword($encoded);
                $user->setEnabled(true);
                $user->setAdmin($adminUser->getAdmin());
                $em->persist($user);
                $em->flush();

                $request->getSession()->getFlashBag()->add(
                    'notice',
                    'Admin User Successfully Created!'
                );

                return $this->redirectToRoute('Homecare_HomecareBundle_create_admin_user');

            }
        }

        return $this->render(
            'HomecareHomecareBundle:Admin:createAdminUser.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }


    public function editAdminUserAction($userId, Request $request)
    {
        $adminUser = $this->get('security.token_storage')->getToken()->getUser();
        $em        = $this->getDoctrine()->getManager();

        $user = $this->getDoctrine()->getRepository('HomecareHomecareBundle:User')->find($userId);

        $form = $this->createForm(
            'homecare_homecarebundle_user',
            $user,
            array('validation_groups' => array('Default', 'CREATE_AGENCY'))
        );

        $form->add('submit', 'submit', ['label' => 'Submit']);


        $originalPassword = $user->getPassword();

        //if the form has been submitted
        if ($request->getMethod() == "POST") {
            //If the form was submitted then bind the request variables to the form User object
            $form->handleRequest($request);

            if ($form->isValid()) {

                $user->addRole('ROLE_ADMIN');
                //grab the password from the form
                $plainPassword = $user->getPassword();
                if ( ! empty($plainPassword)) {
                    //encode the password
                    $encoded = $this->container->get('security.password_encoder')
                                               ->encodePassword($user, $plainPassword);
                    $user->setPassword($encoded);
                } else {
                    $user->setPassword($originalPassword);
                }


                $user->setEnabled(true);
                $user->setAdmin($adminUser->getAdmin());
                $em->persist($user);
                $em->flush();

                $request->getSession()->getFlashBag()->add(
                    'notice',
                    'Admin User Successfully Modified!'
                );

                return $this->redirectToRoute('Homecare_HomecareBundle_edit_admin_user', array('userId' => $userId));

            }
        }

        return $this->render(
            'HomecareHomecareBundle:Admin:editAdminUser.html.twig',
            array(
                'form' => $form->createView(),
            )
        );


    }


}