<?php

namespace Homecare\HomecareBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
//grab your custom class that will calculate a score for you
use Homecare\HomecareBundle\Model\CalculateScore;

class CareGoalSpanController extends Controller
{
    public function showCareGoalSpanAction( $recipientId, $match, $timesheetDateIds )
    {

			
			// set the timesheet date ids 
			$timesheetIds = $timesheetDateIds;
			// get the entity manager
			$em= $this->getDoctrine()->getManager();
			// grab the repository
			$careGoalsRepository = $em->getRepository( "HomecareHomecareBundle:CareGoals" );
			//grab the current logged in user;
		  $user = $this->get('security.token_storage')->getToken()->getUser();
			//grab the recipient object
			$recipient = $em->getRepository( "HomecareHomecareBundle:Recipient" )->findOneById( $recipientId );
			
			
			
			/*
			grab the DESIRED CARE GOALS 
			set by the admin agency or the recipient
			*/
			if ( $match == 'adminGoals' ) 
			{
				
				$adminType = $user->getAdmin();
				
				$careGoalObj = $careGoalsRepository->findOneBy(
	    array('recipient' => $recipient, 'setByAdmin' => $adminType)
	);
	
	      $desiredCareGoals = $careGoalObj->getCareOption();
				
			} elseif ( $match == 'agencyGoals' ) 
			{
				
				$careGoalObjAgency = $this->getAgencyGoals( $recipient );
				
				$desiredCareGoals = $careGoalObjAgency->getCareOption();
				
			} elseif ( $match == 'recipientGoals' ) 
			{
				
				$careGoalObjRecipient = $this->getRecipientGoals( $recipient );
				
				$desiredCareGoals = $careGoalObjRecipient->getCareOption();
			}
			
			
			
			

			
			

        $timesheets = $em->getRepository( "HomecareHomecareBundle:Timesheet" )->getTimesheetsFromIds( $timesheetIds );


			
		/* CALCULATE A SCORE ON IF THE RECIPIENT RECIEVED THE DESIRED CARE */
		//grab the service you created 	
		$score = $this->get( 'calculateScore' );
		//set all the timesheets
		$score->setTimesheets( $timesheets );



        $desiredCareGoalsArray = [];
        foreach ( $desiredCareGoals as $desiredCareGoal ) {
            $desiredCareGoalsArray[] = $desiredCareGoal->getCareOption();
        }

        //this method sets the desired care goals for the recipient, agency, or admin
        $score->setDesiredCareGoals( $desiredCareGoalsArray );


		//grab all the care performed and sort it by date like array( "12/20/14" => ("health_related", "behavior" ),
		$datesAndServices = $score->sortCarePerformedByEachDate();



        //this method calculates the actual score
		$grade = $score->calculateTheActualScore();


		
		// combine the forgotten goals to the dates and services array
		$allCareGoalsSpanInfo = $this->getForgottenCareGoals( $datesAndServices, $desiredCareGoalsArray );

			
        return $this->render('HomecareHomecareBundle:CareGoalSpan:showCareGoalSpan.html.twig', array(
                    'allCareGoalsSpanInfo' => $allCareGoalsSpanInfo,
                    'recipient' => $recipient,
					'match' => $match,
                    'grade' => isset( $grade ) ? $grade : '',
            ));    
						
						
						}



						
						
						//this method will grab the recipient goals for the administrator and agency to view
						public function getRecipientGoals( $recipient ) {
							
							//peform your queries in this method for now
							//put it in a repository later so you can combine these queries
							$em = $this->getDoctrine()->getManager();
							
							//grab the care goal repository
							$careGoalsRepository = $em->getRepository( "HomecareHomecareBundle:CareGoals" );
							
							//use this to grab all the caregoals_careoptions
							$careGoalObj = $careGoalsRepository->findOneBy(
				    array('recipient' => $recipient, 'setByRecipient' => true)
				);
				
				
				return $careGoalObj;
				
						}
						
						
						
						
						
						
						
						//this method will grab the agency goals for the administrator to view
						public function getAgencyGoals( /*\Homecare\HomecareBundle\Entity\Recipient */ $recipient ) {

				            //peform your queries in this method for now
							//put it in a repository later so you can combine these queries
							$em = $this->getDoctrine()->getManager();

							//grab the timesheet for the recipient
							$timesheet = $em->getRepository( "HomecareHomecareBundle:Timesheet" )->findOneByRecipient( $recipient );
							
							//grab the agency related to the recipient
							$agency = $timesheet->getRecipient()->getAgency();
							
							//grab the care goal repository
							$careGoalsRepository = $em->getRepository( "HomecareHomecareBundle:CareGoals" );
							
							//use this to grab all the caregoals_careoptions
							$careGoalObj = $careGoalsRepository->findOneBy(
				    array('recipient' => $recipient, 'setByAgency' => $agency)
				);
				
				return $careGoalObj;
						}
						

						
						//this method will calculate the score based on the match
						public function getForgottenCareGoals( $datesAndServices, $desiredCareGoalsArray ) {
                            $numFailedGoals = 0;
                            foreach ( $datesAndServices as $date => $carePerformed ) {
                                foreach( $carePerformed as $care ) {
                                    foreach ( $desiredCareGoalsArray as $desiredCareGoal ) {
                                        if ( !in_array( $desiredCareGoal, $care ) ) {
                                            $numFailedGoals += 1;
                                            $datesAndServices[ $date ][ 'forgottenGoals' ][] = $desiredCareGoal;
                                        }
                                    }
                                }
                            }
                            return $datesAndServices;
						}

}
