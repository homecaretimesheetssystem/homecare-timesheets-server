<?php

namespace Homecare\HomecareBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class ManageUsersController extends Controller
{
    public function showUsersAction($typeOfUser, Request $request)
    {


        $em = $this->getDoctrine()->getManager();
        //grab the current logged in user;
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($agency = $user->getAgency()) {
            $createdUsers = $this->getUsers('agency', $typeOfUser, $agency);
        } else if ($admin = $user->getAdmin()) {
            $createdUsers = $this->getUsers('admin', $typeOfUser, $admin);
        }


        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $createdUsers,
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );


        // \Doctrine\Common\Util\Debug::dump( $createdUsers );

        //return new Response( "hi" );

        return $this->render(
            'HomecareHomecareBundle:ManageUsers:manageusers.html.twig',
            array(
                'createdUsers' => $pagination,
                'typeOfUser'   => $typeOfUser,
            )
        );
    }


    public function getUsers($loggedInUserType, $typeOfUser, $adminOrAgencyObj)
    {
        $users = [];
        if ($loggedInUserType == 'admin') {
            switch ($typeOfUser) {
                case 'admin':
                    $users = $this->getDoctrine()->getManager()->getRepository(
                        'HomecareHomecareBundle:User'
                    )->getAllAdminUsers();
                    break;
                case 'agency':
                    $users = $this->getDoctrine()->getManager()->getRepository(
                        'HomecareHomecareBundle:User'
                    )->getAllAgencyUsers();
                    break;
                case 'recipient':
                    $users = $this->getDoctrine()->getManager()->getRepository(
                        'HomecareHomecareBundle:User'
                    )->getAllRecipientUsers();
                    break;
                case 'pca':
                    $users = $this->getDoctrine()->getManager()->getRepository(
                        'HomecareHomecareBundle:User'
                    )->getAllPcaUsers();
                    break;
                case 'agencies':
                    $users = $this->getDoctrine()->getManager()->getRepository(
                        'HomecareHomecareBundle:Agency'
                    )->findAll();
                    break;
            }
        } else if ($loggedInUserType == 'agency') {
            switch ($typeOfUser) {
                case 'agency':
                    $users = $this->getDoctrine()->getManager()->getRepository('HomecareHomecareBundle:User')->findBy(
                      array('agency' => $adminOrAgencyObj->getId(), 'archived' => 0)
                    );
                    break;
                case 'recipient':
                    $users = $this->getDoctrine()->getManager()->getRepository(
                        'HomecareHomecareBundle:User'
                    )->getAllRecipientUsersForAnAgency($adminOrAgencyObj);
                    break;
                case 'pca':
                    $users = $this->getDoctrine()->getManager()->getRepository(
                        'HomecareHomecareBundle:User'
                    )->getAllPcaUsersForAnAgency($adminOrAgencyObj);
                    break;
            }
        }

        return $users;
    }


    public function deleteUserAction($typeOfUser, $userId)
    {

        $em      = $this->getDoctrine()->getManager();
        $delete  = false;
        $message = '';

        $repository = $em->getRepository('HomecareHomecareBundle:User');
        $userToDelete = $repository->findOneById($userId);

        // check for archive access: calls all voters
        $this->denyAccessUnlessGranted('archive', $userToDelete);


        switch ($typeOfUser) {
            case 'admin':
                $repository = $em->getRepository('HomecareHomecareBundle:User');
                //make sure they are not deleting the last admin user
                if (count($repository->getAllAdminUsers()) == 1) {
                    $this->addFlash('error', 'You must always have one admin user left.');
                } else {
                    $userToDelete = $repository->findOneById($userId);
                    $userToDelete->setArchived(1);
                    $userToDelete->setLocked(1);
                    $em->flush();
                    $this->addFlash('message', 'The admin user has been successfully archived.');
                }
                break;
            case 'agency':
                $userToDelete = $em->getRepository('HomecareHomecareBundle:User')->findOneById($userId);
                $userToDelete->setArchived(1);
                $userToDelete->setLocked(1);
                $em->flush();
                $this->addFlash('message', 'The agency user has been successfully archived.');
                break;
            case 'pca':
                $userToDelete = $em->getRepository('HomecareHomecareBundle:User')->findOneById($userId);
                $userToDelete->setArchived(1);
                $userToDelete->setLocked(1);
                $em->flush();
                $this->addFlash('message', 'The pca has been successfully archived.');
                break;
            case 'recipient':
                $userToDelete = $em->getRepository('HomecareHomecareBundle:User')->findOneById($userId);
                $userToDelete->setArchived(1);
                $userToDelete->setLocked(1);
                $em->flush();
                // f recalculate the frequency since user has been archived
                $recipient = $userToDelete->getRecipient();
                $recipientPcas = $recipient->getRecipientPca();
                //recalculate all the recipientPca verificaion frequencies
                foreach($recipientPcas as $rp) {
                    $this->get('homecare_homecare.model.verification')->decideFrequency($rp->getPca());
                }

                $this->addFlash('message', 'The recipient has been successfully archived');
                break;
        }



        return $this->redirectToRoute('Homecare_HomecareBundle_manage_users', array('typeOfUser' => $typeOfUser));
    }
}
