<?php

namespace Homecare\HomecareBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;


class PdfController extends Controller
{
    public function generatePdfAction( $timesheetId )
    {
        $em = $this->getDoctrine()->getManager();
        $timesheet = $em->getRepository( "HomecareHomecareBundle:Timesheet" )->find( $timesheetId );

        // check for download access: calls all voters
        $this->denyAccessUnlessGranted('download', $timesheet);

        $pdfFileName = $timesheet->getFile()->getTimesheetPdfFile();
        $amazonS3 = $this->get( 'homecare.homecarebundle.amazons3' );
        $result = $amazonS3->downloadFile( $pdfFileName );
        $response = new Response();
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $this->get( 'homecare_homecarebundle.model.filenaming' )->createFileName( $timesheet, '.pdf' ) . '";');
        $response->headers->set( 'Content-type', $result['ContentType'] );
        $response->setContent( $result['Body'] );
        return $response;
    }
}