<?php

namespace Homecare\HomecareBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use the class that generates the charts
use Ob\HighchartsBundle\Highcharts\Highchart;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CareHistoryGraphController extends Controller
{
    public function showCareHistoryGraphAction( Request $request )
    {



			
			$em = $this->getDoctrine()->getManager();
			
			// grab all the available care options
			$careOptions = $em->getRepository( "HomecareHomecareBundle:CareOptions" )->findAll();

			
			//grab the graph type
			$graphType = $request->query->get( 'graphType', 'bar' );
			
			
			// get all the timesheetIds (dates that were selected) from the form
			// if they are changing graphs then the array of timesheetIds has been serialized
			// so you need to unserialize it
			if ( $request->query->get( 'hasBeenSerialized' ) ) {
				$timesheetIds = json_decode( $request->query->get( 'timesheetIds' ) );
				//$timesheetIds = unserialize( $request->query->get( 'timesheetIds' ) );
			} else {
				$timesheetIds = $request->query->get( 'pdfsOrCsvs' );
			}


        //get all the timesheets for the given timesheet Ids
        $timesheets = $em->getRepository( "HomecareHomecareBundle:Timesheet" )->getTimesheetsFromIds( $timesheetIds );


            //grab the recipient object for the timesheets
			$recipientObj = $timesheets[0]->getRecipient();




        /* grab the CARE PERFORMED on the recipient */
		$carePerformed = array();
        foreach ( $timesheets as $timesheet ) {

	        // check for access: calls all voters
	        $this->denyAccessUnlessGranted('compare', $timesheet);

            foreach ( $timesheet->getCareOptionTimesheets() as $careOption ) {
                $carePerformed[] = $careOption->getCareOption()->getCareOption();
            }
        }

      //\Doctrine\Common\Util\Debug::dump( $carePerformed );

        //return new Response("hi");
		
		
		//calculate care performed weights 
		$carePerformedWeights = array();
		foreach ( $carePerformed as $care ) {
		$carePerformedWeights[$care] = isset( $carePerformedWeights[$care] ) ? 	( $carePerformedWeights[$care] + 1 ) : 1;
		}
		
			
			
			
			
			
			//create the graph info dynamically
			$data = array();
			foreach ( $carePerformedWeights as $care => $count ) {
				array_push( $data, array('name' => $care, 'y' => $count) );
			}
			
			
			
			
			
			
			
			
			
			$graph;
			switch ( $graphType ) {
				case 'bar':
	     $series = array(
	         array("name" => "Number Of Times Care Was Performed", "type" => "bar" , "data" => $data)
	     );
					break;
				case 'pie':
 	     $series = array(
 	         array("name" => "Number Of Times Care Was Performed", "type" => "pie" , "data" => $data)
 	     );
					break;
				case 'line':
 	     $series = array(
 	         array("name" => "Number Of Times Care Was Performed", "data" => $data)
 	     );
					break;
			}
			
		
			


		     $ob = new Highchart();
		     $ob->chart->renderTo('linechart');  // The #id of the div where to render the chart
		     $ob->title->text('Care History Graph For ' . $recipientObj->getRecipientName() );
		     $ob->xAxis->title(array('text'  => "Care Performed"));
		     $ob->yAxis->title(array('text'  => "Number Of Times Care Was Performed"));
				 
				 $ob->xAxis->categories( $carePerformed );
				 
		     $ob->series($series);

		     
			
        return $this->render('HomecareHomecareBundle:CareHistoryGraph:showCareHistoryGraph.html.twig', array(
             'chart' => $ob,
						 'graphType' => $graphType,
						 'timesheetIds' => $timesheetIds,
						 'timesheets' => $timesheets,
						 'recipientObj' => $recipientObj,
            ));    }













						public function convertCareOptionsToStringArray( $careOptions ) {
							
							$newCareOptions = array();
							foreach ( $careOptions as $careOption ) {
							$newCareOptions[] =	$careOption->getCareOption();
							}
							return $newCareOptions;
						}
						
						
						
						
						
					
						
						
						
						
						






}
