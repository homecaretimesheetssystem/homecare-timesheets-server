<?php

namespace Homecare\HomecareBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Homecare\HomecareBundle\Form\CareGoalsType;
use Homecare\HomecareBundle\Entity\CareGoals;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Homecare\HomecareBundle\Entity\User;

class RecipientProfileController extends Controller
{


    public function showProfileAction(Request $request, $recipientId, $timesheetDateIds)
    {


        //grab the entity manager
        $em = $this->getDoctrine()->getManager();
        //grab the repository
        $careGoalsRepository = $em->getRepository("HomecareHomecareBundle:CareGoals");

        //grab the recipient object
        $recipient = $em->getRepository("HomecareHomecareBundle:Recipient")->findOneById($recipientId);


        //grab the current logged in user;
        $user = $this->get('security.token_storage')->getToken()->getUser();


        if ($user->hasRole('ROLE_ADMIN')) {
            $adminType = $user->getAdmin();
            //use this to grab all the caregoals_careoptions
            $careGoalObj = $careGoalsRepository->findOneBy(
                array('recipient' => $recipient, 'setByAdmin' => $adminType)
            );
            //grab all the agency goals for the recipient
            $careGoalObjAgency = $this->getAgencyGoals($recipient);
            //grab all the recipient goals for himself
            $careGoalObjRecipient = $this->getRecipientGoals($recipient);

        } elseif ($user->hasRole('ROLE_AGENCY')) {
            $agencyType = $user->getAgency();
            //use this to grab all the caregoals_careoptions
            $careGoalObj = $careGoalsRepository->findOneBy(
                array('recipient' => $recipient, 'setByAgency' => $agencyType)
            );
            //grab all the recipient goals for himself
            $careGoalObjRecipient = $this->getRecipientGoals($recipient);
        } elseif ($user->hasRole('ROLE_RECIPIENT')) {
            $careGoalObj = $careGoalsRepository->findOneBy(
                array('recipient' => $recipient, 'setByRecipient' => true)
            );
        }


        //create the form
        $form = $this->createForm(new CareGoalsType(), $careGoalObj);


        $form->handleRequest($request);


        //if the form has been submitted then run this code
        if ($request->getMethod() == 'POST') {
            //if the form is submitted and valid
            //tie the request variables into the form object as properties


            if ($form->isValid()) {

                //if this is the first time the form is being submitted then enter this info
                $careGoals = $form->getData();

                if ($user->hasRole('ROLE_ADMIN')) {
                    $adminType = $user->getAdmin();
                    $careGoals->setSetByAdmin($adminType);
                } elseif ($user->hasRole('ROLE_AGENCY')) {
                    $agencyType = $user->getAgency();
                    $careGoals->setSetByAgency($agencyType);
                } elseif ($user->hasRole('ROLE_RECIPIENT')) {
                    $careGoals->setSetByRecipient(true);
                }
                //perform some action such as persist objects to database
                $careGoals->setRecipient($recipient);
                $em->persist($careGoals);


                $em->flush();

                return $this->redirect(
                    $this->generateUrl(
                        'Homecare_HomecareBundle_recipient_profile',
                        array('recipientId' => $recipientId, 'timesheetDateIds' => $timesheetDateIds)
                    )
                );
            }

        }


        return $this->render(
            'HomecareHomecareBundle:RecipientProfile:showProfile.html.twig',
            array(
                'form'                 => $form->createView(),
                'recipient'            => $recipient,
                'careGoalObjAgency'    => isset($careGoalObjAgency) ? $careGoalObjAgency : '',
                'careGoalObjRecipient' => isset($careGoalObjRecipient) ? $careGoalObjRecipient : '',
                'timesheetDateIds'     => $timesheetDateIds,
            )
        );
    }


    //this method will grab the agency goals for the administrator to view
    public function getAgencyGoals( /*\Homecare\HomecareBundle\Entity\Recipient */
        $recipient
    ) {


        //peform your queries in this method for now
        //put it in a repository later so you can combine these queries
        $em = $this->getDoctrine()->getManager();
        //grab the timesheet for the recipient
        $timesheet = $em->getRepository("HomecareHomecareBundle:Timesheet")->findOneByRecipient($recipient);

        //grab the agency related to the recipient
        $agency = $timesheet->getSessionData()->getPca()->getAgency();

        //grab the care goal repository
        $careGoalsRepository = $em->getRepository("HomecareHomecareBundle:CareGoals");

        //use this to grab all the caregoals_careoptions
        $careGoalObj = $careGoalsRepository->findOneBy(
            array('recipient' => $recipient, 'setByAgency' => $agency)
        );

        return $careGoalObj;
    }


    //this method will grab the recipient goals for the administrator and agency to view
    public function getRecipientGoals($recipientId)
    {

        //peform your queries in this method for now
        //put it in a repository later so you can combine these queries
        $em = $this->getDoctrine()->getManager();


        //grab the recipient object
        $recipient = $em->getRepository("HomecareHomecareBundle:Recipient")->findOneById($recipientId);

        //grab the care goal repository
        $careGoalsRepository = $em->getRepository("HomecareHomecareBundle:CareGoals");

        //use this to grab all the caregoals_careoptions
        $careGoalObj = $careGoalsRepository->findOneBy(
            array('recipient' => $recipient, 'setByRecipient' => true)
        );


        return $careGoalObj;

    }


}
