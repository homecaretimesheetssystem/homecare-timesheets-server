<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 4/30/15
 * Time: 2:11 PM
 */

namespace Homecare\HomecareBundle\Controller;


use Homecare\HomecareBundle\Entity\User;
use Homecare\HomecareBundle\Form\AdminType;
use Homecare\HomecareBundle\Form\User2Type;
use Homecare\HomecareBundle\Form\UserType;
use Homecare\HomecareBundle\Entity\Admin;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Homecare\HomecareBundle\Entity\Agency;
use Homecare\HomecareBundle\Entity\Services;
use Homecare\HomecareBundle\Form\AgencyType;
use Doctrine\Common\Collections\ArrayCollection;

class ArchiveController extends Controller {



    public function archiveTimesheetsAction(Request $request)
    {
        $timesheetIds = $request->query->get( 'pdfsOrCsvs' );
        $em = $this->getDoctrine()->getManager();

        //download s3 files to uploads directory
        foreach ( $timesheetIds as $timesheetId ) {
            $timesheet = $em->getRepository( "HomecareHomecareBundle:Timesheet" )->find( $timesheetId );

            // check for download access: calls all voters
            $this->denyAccessUnlessGranted('archive', $timesheet);

            $pdfFileName = $timesheet->setArchived(true);
            $em->persist($timesheet);
            $em->flush();
        }

        $session = $request->getSession();
        $session->getFlashBag()->add(
            'message',
            'Timesheets Archived Successfully.'
        );

        return $this->redirectToRoute('Homecare_HomecareBundle_dashboard_timesheets_loader');
    }



}