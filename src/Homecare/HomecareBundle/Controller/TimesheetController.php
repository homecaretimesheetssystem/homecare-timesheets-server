<?php

namespace Homecare\HomecareBundle\Controller;

use Homecare\HomecareBundle\Form\TimesheetFilterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Homecare\HomecareBundle\Entity\CareGoals;
use Homecare\HomecareBundle\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class TimesheetController extends Controller
{


    public function indexAction(Request $request)
    {


        $agency = $this->getDoctrine()->getRepository("HomecareHomecareBundle:Agency")->find(271);



        $form = $this->createForm(
            new TimesheetFilterType(),
            null,
            [
                'action' => $this->generateUrl('Homecare_HomecareBundle_dashboard_timesheets'),
                'method' => 'GET',
            ]
        );


        $filterBuilder = $this->getDoctrine()->getManager()->getRepository(
            'HomecareHomecareBundle:Timesheet'
        )->createQueryBuilder('t')
         ->where('t.finished = :finished')
         ->setParameter('finished', 1)
         ->orderBy('sd.dateEnding', 'DESC');




        //dump($filterBuilder->getQuery()->getResult());

        //dump($filterBuilder->getDQL());


        $user = $this->getUser();


        // Secure the query and make sure it related to the logged in agency or recipient
        if ($user->hasRole('ROLE_AGENCY')) {
            $filterBuilder->andWhere('r.agency = :agency')
                          ->setParameter('agency', $user->getAgency());
        } elseif ($user->hasRole('ROLE_RECIPIENT')) {
            $filterBuilder->andWhere('t.recipient = :recipient')
                          ->setParameter('recipient', $user->getRecipient());
        }


        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->get('lexik_form_filter.query_builder_updater')
                 ->addFilterConditions($form, $filterBuilder);

        }

        $query = $filterBuilder->getQuery();


       //dump($filterBuilder->getDQL());

        //dump($filterBuilder->getParameters());


        $paginator  = $this->get('knp_paginator');
        $timesheets = $paginator->paginate(
            $query,
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );


        return $this->render(
            'HomecareHomecareBundle:Timesheet:index.html.twig',
            array(
                'form'       => $form->createView(),
                'timesheets' => $timesheets,
            )
        );

    }




    public function loaderAction()
    {
        $url = $this->generateUrl(
                'Homecare_HomecareBundle_dashboard_timesheets'
            ).'?timesheet_filter%5Barchived%5D=n&submit-filter=filter';

        return $this->redirect($url);
    }


    /*
        public function deleteTimesheetAction( $timesheetId ) {


            //get the timesheet Obj and make sure it exists in the database
            $em = $this->getDoctrine()->getManager();
            $timesheet = $em->getRepository( "HomecareHomecareBundle:Timesheet" )->find( $timesheetId );
            if ( !$timesheet ) {
                $this->addFlash(
                    'error', 'That timesheet does not exist in the database'
                );
                return $this->redirectToRoute('Homecare_HomecareBundle_dashboard_timesheets_loader');
            }

            //delete the sessionData if there is only 1 timesheet left that belongs to this session
            $timesheetsThatBelongToSession = $timesheet->getSessionData()->getTimesheets();
            if ( count( $timesheetsThatBelongToSession ) == 1 ) {
                $em->remove( $timesheet->getSessionData() );
            }

            //delete the timesheet and all its info and then flush the changes
            $em->remove( $timesheet );
            $em->flush();
            $this->addFlash(
                'message', 'Timesheet has been successfully archived'
            );
            return $this->redirectToRoute('Homecare_HomecareBundle_dashboard_timesheets_loader');
        }

    */


    public function archiveTimesheetAction($timesheetId)
    {


        //get the timesheet Obj and make sure it exists in the database
        $em        = $this->getDoctrine()->getManager();
        $timesheet = $em->getRepository("HomecareHomecareBundle:Timesheet")->find($timesheetId);
        if ( ! $timesheet) {
            $this->addFlash(
                'error',
                'That time entry does not exist in the database'
            );

            return $this->redirectToRoute('Homecare_HomecareBundle_dashboard_timesheets_loader');
        }

        // check for archive access: calls all voters
        $this->denyAccessUnlessGranted('archive', $timesheet);

        $timesheet->setArchived(true);
        $em->persist($timesheet);
        $em->flush();

        $this->addFlash(
            'message',
            'Time entry has been successfully archived'
        );

        return $this->redirectToRoute('Homecare_HomecareBundle_dashboard_timesheets_loader');
    }


}