<?php

namespace Homecare\HomecareBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use Homecare\HomecareBundle\Model\PcaCallDate;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Homecare\HomecareBundle\Entity\Pca;
use Homecare\HomecareBundle\Entity\User;
use Homecare\HomecareBundle\Form\PcaType;
use Homecare\HomecareBundle\Entity\PcaReview;

class PcaController extends Controller
{

    const SESSION_EMAIL = 'fos_user_send_resetting_email/email';


    public function createPcaAction(Request $request)
    {

        $pca  = new Pca();
        $user = new User();
        $pca->setUser($user);
        $form = $this->buildCreatePcaForm($pca);


        if ($request->getMethod() == 'POST') {

            $form->handleRequest($request);

            if ($form->isValid()) {

                $pca    = $form->getData();
                $agency = ($pca->getAgency()) ? $pca->getAgency() : $this->get('security.token_storage')->getToken(
                )->getUser()->getAgency();

                $pca->setAgency($agency);


                $user          = $pca->getUser();
                $plainPassword = $user->getPassword();
                $encoded       = $this->container->get('security.password_encoder')
                                                 ->encodePassword($user, $plainPassword);
                $user->setPassword($encoded);
                $user->initializePca($pca);


                $em = $this->getDoctrine()->getEntityManager();

                $em->persist($pca);
                $em->flush();
                //set a flash message
                $session = $request->getSession();
                $session->getFlashBag()->add(
                    'message',
                    'Pca Successfully Created!'
                );


                $this->sendPasswordResetEmail($user);

                $this->container->get('homecare_homecare.model.custom_swift_mailer')->sendAppDownloads($user);

                // send password reset email

                return $this->redirect($this->generateUrl('Homecare_HomecareBundle_create_pca'));
            }
        }

        return $this->render(
            'HomecareHomecareBundle:Pca:createPca.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }


    public function editPcaAction($pcaId, Request $request)
    {

        $pca = $this->getDoctrine()->getRepository('HomecareHomecareBundle:Pca')->find($pcaId);


        // check for edit access: calls all voters
        $this->denyAccessUnlessGranted('edit', $pca);


        $form = $this->buildEditPcaForm($pca);

        $originalPassword = $pca->getUser()->getPassword();

        if ($request->getMethod() == 'POST') {

            $form->handleRequest($request);

            if ($form->isValid()) {

                $pca = $form->getData();

                $user          = $pca->getUser();
                $plainPassword = $user->getPassword();
                if ( ! empty($plainPassword)) {
                    //encode the password
                    $encoded = $this->container->get('security.password_encoder')
                                               ->encodePassword($user, $plainPassword);
                    $user->setPassword($encoded);
                } else {
                    $user->setPassword($originalPassword);
                }

                $user->initializePca($pca);
                $em = $this->getDoctrine()->getEntityManager();


                $em->persist($pca);
                $em->flush();
                //set a flash message
                $session = $request->getSession();
                $session->getFlashBag()->add(
                    'message',
                    'Pca Successfully Modified!'
                );

                return $this->redirect(
                    $this->generateUrl('Homecare_HomecareBundle_edit_pca', array('pcaId' => $pcaId))
                );
            }
        }

        return $this->render(
            'HomecareHomecareBundle:Pca:editPca.html.twig',
            array(
                'form' => $form->createView(),
            )
        );

    }


    /**
     * Builds a form to create a pca .
     *
     * @param Pca $pca
     *
     * @return Form The form
     *
     */
    private function buildCreatePcaForm(Pca $pca)
    {
        $form = $this->createForm(
            'homecare_pca',
            $pca,
            array(
                'action' => $this->generateUrl('Homecare_HomecareBundle_create_pca'),
                'method' => 'POST',
                'validation_groups' => ['Default', 'PASSWORD'],
            )
        );

        $form->add(
            'submit',
            'submit',
            [
                'label' => 'Create Worker',
                'attr'  => ['class' => 'btn btn-default'],
            ]
        );

        return $form;
    }


    /**
     * Builds a form to edit a Course entity.
     *
     * @param Pca $pca
     *
     * @return Form The form
     *
     */
    private function buildEditPcaForm(Pca $pca)
    {
        $form = $this->createForm(
            'homecare_pca',
            $pca,
            array(
                'action' => $this->generateUrl('Homecare_HomecareBundle_edit_pca', array('pcaId' => $pca->getId())),
                'method' => 'POST',
                'validation_groups' => ['Default'],
            )
        );

        $form->add(
            'submit',
            'submit',
            [
                'label' => 'Save Changes',
                'attr'  => ['class' => 'btn btn-default'],
            ]
        );

        return $form;
    }


    /**
     * @param User $user
     *
     * @return Response
     */
    private function sendPasswordResetEmail(User $user)
    {

        if (null === $user) {
            return $this->container->get('templating')->renderResponse(
                'FOSUserBundle:Resetting:request.html.'.$this->getEngine(),
                array('invalid_username' => $user->getUsername())
            );
        }

        if ($user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            return $this->container->get('templating')->renderResponse(
                'FOSUserBundle:Resetting:passwordAlreadyRequested.html.'.$this->getEngine()
            );
        }

        if (null === $user->getConfirmationToken()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $tokenGenerator = $this->container->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        $this->container->get('session')->set(static::SESSION_EMAIL, $this->getObfuscatedEmail($user));
        $this->container->get('fos_user.mailer')->sendResettingEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->container->get('fos_user.user_manager')->updateUser($user);
    }


    /**
     * Get the truncated email displayed when requesting the resetting.
     *
     * The default implementation only keeps the part following @ in the address.
     *
     * @param \FOS\UserBundle\Model\UserInterface $user
     *
     * @return string
     */
    protected function getObfuscatedEmail(UserInterface $user)
    {
        $email = $user->getEmail();
        if (false !== $pos = strpos($email, '@')) {
            $email = '...'.substr($email, $pos);
        }

        return $email;
    }

}
