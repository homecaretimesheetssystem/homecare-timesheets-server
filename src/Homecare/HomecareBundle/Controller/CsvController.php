<?php

namespace Homecare\HomecareBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;


class CsvController extends Controller
{
    public function generateCsvAction( $timesheetId )
    {
        $em = $this->getDoctrine()->getManager();
        $timesheet = $em->getRepository( "HomecareHomecareBundle:Timesheet" )->find( $timesheetId );

        // check for download access: calls all voters
        $this->denyAccessUnlessGranted('download', $timesheet);

        $csvFileName = $timesheet->getFile()->getTimesheetCsvFile();
        $amazonS3 = $this->get( 'homecare.homecarebundle.amazons3' );
        $result = $amazonS3->downloadFile( $csvFileName );
        $response = new Response();
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $this->get( 'homecare_homecarebundle.model.filenaming' )->createFileName( $timesheet, '.csv' ) . '";');
        $response->headers->set( 'Content-type', $result['ContentType'] );
        $response->setContent( $result['Body'] );
        return $response;
    }
}
