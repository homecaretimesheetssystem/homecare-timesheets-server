<?php

namespace Homecare\HomecareBundle\Controller;

use Proxies\__CG__\Homecare\HomecareBundle\Entity\RecipientSignature;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class CareGoalComparisonController extends Controller
{
    public function showComparisonAction(Request $request)
    {



        //grab the current logged in user;
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();

        //if the timesheetDateIds have been json encoded then decode them
        if (null !== $request->query->get('jsonEncoded')) {

            $timesheetIds = json_decode($request->query->get('pdfsOrCsvs'));

        } else {

            $timesheetIds = $request->query->get('pdfsOrCsvs');

        }


        //grab the session
        $session = $request->getSession();

        // store the timesheets in a session
        $session->remove('timesheets');

        $session->set('timesheets', $timesheetIds);

        $timesheets = $session->get('timesheets');



        // find out whos goals you are going to check
        if ($request->query->has('CompareOwnGoalsSubmitButton')) {
            $match = 'adminGoals';
        } else if ($request->query->has('CompareAgencyGoalsSubmitButton')) {
            $match = 'agencyGoals';
        } else if ($request->query->has('CompareRecipientGoalsSubmitButton')) {
            $match = 'recipientGoals';
        } else {

            //if none of the submit buttons have been pressed then show the goals for whomever is logged in
            if ($user->hasRole('ROLE_ADMIN')) {
                $match = 'adminGoals';
            } elseif ($user->hasRole('ROLE_AGENCY')) {
                $match = 'agencyGoals';
            } elseif ($user->hasRole('ROLE_RECIPIENT')) {
                $match = 'recipientGoals';
            }

        }


        //grab all the timesheets for the specified date
        $careOptionTimesheets = $em->getRepository(
            "HomecareHomecareBundle:CareOptionTimesheet"
        )->getCareOptionTimesheets($timesheetIds);


        // check for access: calls all voters
        foreach ($careOptionTimesheets as $c) {
            $this->denyAccessUnlessGranted('compare', $c->getTimesheet());
        }




        //grab the recipient object for the timesheets
        $recipientObj = $careOptionTimesheets[0]->getTimesheet()->getRecipient();


        //$this->checkIfCareGoalsHaveBeenSet( $recipientObj, $em );


        // care goals have not been set by the logged in user so redirect them to the recipient profile page
        // so that they can set some care goals on the recipient
        if ( ! $this->checkIfCareGoalsHaveBeenSet($recipientObj, $em)) {


            // \Doctrine\Common\Util\Debug::dump( $careOptionTimesheets[0]->getTimesheet()->getRecipient() );
            //return new Response( "goals not set yet" );


            //set a flash message to show users whey they were redirected
            $session->getFlashBag()->add(
                'error1',
                'You must enter in care goals for the recipient before you can compare to care plan goals'
            );

            return $this->redirect(
                $this->generateUrl(
                    'Homecare_HomecareBundle_recipient_profile',
                    array('recipientId' => $recipientObj->getId(), 'timesheetDateIds' => json_encode($timesheetIds))
                )
            );

        }


        return $this->render(
            'HomecareHomecareBundle:CareGoalComparison:showComparison.html.twig',
            array(
                'recipientId' => $recipientObj->getId(),
                'match' => $match,
                'timesheetDateIds' => $timesheetIds,
            )
        );
    }


    /**
     * this method checks if care goals have been set for a recipient from the currently logged in user
     *
     */
    public function checkIfCareGoalsHaveBeenSet($recipient, $em)
    {


        //grab the current logged in user;
        $user = $this->get('security.token_storage')->getToken()->getUser();

        //grab the repository
        $careGoalsRepository = $em->getRepository("HomecareHomecareBundle:CareGoals");


        if ($user->hasRole('ROLE_ADMIN')) {


            $adminType = $user->getAdmin();
            //use this to grab all the caregoals_careoptions


            if ( ! $careGoalsRepository->getTheCareGoal($recipient, $adminType, 'admin')) {

                return false;
            } else {
                return true;
            }

        } elseif ($user->hasRole('ROLE_AGENCY')) {
            $agencyType = $user->getAgency();


            if ( ! $careGoalsRepository->getTheCareGoal($recipient, $agencyType, 'agency')) {

                return false;
            } else {
                return true;
            }

        } elseif ($user->hasRole('ROLE_RECIPIENT')) {

            $setByRecipient = true;

            if ( ! $careGoalsRepository->getTheCareGoal($recipient, $setByRecipient, 'recipient')) {

                return false;
            } else {
                return true;
            }


        }


    }


}