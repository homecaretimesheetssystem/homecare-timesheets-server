<?php

namespace Homecare\HomecareBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ZipArchive;
use Homecare\HomecareBundle\Entity\User;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class PdfMultipleController extends Controller
{
    private $pdfFileNames = [];

    public function generatePdfsAction( Request $request ) {
        $timesheetIds = $request->query->get( 'pdfsOrCsvs' );
        $em = $this->getDoctrine()->getManager();

        //download s3 files to uploads directory
        foreach ( $timesheetIds as $timesheetId ) {
            $timesheet = $em->getRepository( "HomecareHomecareBundle:Timesheet" )->find( $timesheetId );

            // check for download access: calls all voters
            $this->denyAccessUnlessGranted('download', $timesheet);

            $pdfFileName = $timesheet->getFile()->getTimesheetPdfFile();
            $this->get( 'homecare.homecarebundle.amazons3' )->downloadFile( $pdfFileName, true );
            $this->pdfFileNames[ $pdfFileName ] = $this->get( 'homecare_homecarebundle.model.filenaming' )->createFileName( $timesheet, '.pdf' );
        }

        //create zip file and add files to it
        $customZipFileObj = $this->get( 'homecare_homecarebundle.model.zipfile' );
        $customZipFileObj->addFilesToZip( $this->pdfFileNames );

        //send the zip file to the browser
        $zipFileName = $this->get('kernel')->getRootDir().'/../web/images/files.zip';
        $shouldDelete = true;
        $response = new BinaryFileResponse( $zipFileName );
        $response->deleteFileAfterSend( $shouldDelete );
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'pdfFiles.zip'
        );
        return $response;
    }
}
