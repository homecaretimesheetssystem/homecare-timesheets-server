<?php

namespace Homecare\HomecareBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DateFilterController extends Controller
{
    public function showDateFilterAction( $filter, $textToFilterBy, $dateFilter, $startDate, $endDate )
    {
        return $this->render('HomecareHomecareBundle:DateFilter:showDateFilter.html.twig', array(
            'filter' => $filter, 
						'textToFilterBy' => $textToFilterBy,
						'dateFilter' => $dateFilter,
						'startDate' => $startDate,
						'endDate' => $endDate,
            ));    }

}
