<?php
namespace Homecare\HomecareBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ZipArchive;
use Homecare\HomecareBundle\Entity\User;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class CsvMultipleController extends Controller
{
	private $csvFileNames = [];

	
    public function generateCsvsAction( Request $request )
    {
        $timesheetIds = $request->query->get( 'pdfsOrCsvs' );
        $em = $this->getDoctrine()->getManager();
        foreach ( $timesheetIds as $timesheetId ) {
            //download s3 files to uploads directory
            $timesheet = $em->getRepository( "HomecareHomecareBundle:Timesheet" )->find( $timesheetId );

            // check for download access: calls all voters
            $this->denyAccessUnlessGranted('download', $timesheet);

            $csvFileName = $timesheet->getFile()->getTimesheetCsvFile();
            $this->get( 'homecare.homecarebundle.amazons3' )->downloadFile( $csvFileName, true );
            $this->csvFileNames[ $csvFileName ] = $this->get( 'homecare_homecarebundle.model.filenaming' )->createFileName( $timesheet, '.csv' );
        }

        //create zip file and add files to it
        $customZipFileObj = $this->get( 'homecare_homecarebundle.model.zipfile' );
        $customZipFileObj->addFilesToZip( $this->csvFileNames );

        //send the zip file to the browser
        $zipFileName = $this->get('kernel')->getRootDir().'/../web/images/files.zip';
        $shouldDelete = true;
        $response = new BinaryFileResponse( $zipFileName );
        $response->deleteFileAfterSend( $shouldDelete );
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'csvFiles.zip'
        );
        return $response;
    }
}