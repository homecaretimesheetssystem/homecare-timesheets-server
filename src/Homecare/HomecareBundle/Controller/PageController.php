<?php

namespace Homecare\HomecareBundle\Controller;

use Homecare\HomecareBundle\Data\StripeAdapter;
use Homecare\HomecareBundle\Entity\Agency;
use Homecare\HomecareBundle\Entity\User;
use Homecare\HomecareBundle\Form\model\BillingData;
use Homecare\HomecareBundle\Form\model\RegistrationData;
use Homecare\HomecareBundle\Form\model\TermsAccepted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Aws\S3\S3Client;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class PageController extends Controller
{

    const SIGN_UP_SUCCESS_MESSAGE = "Congrats \"%s\". Your agency is now signed up and can login to your account";


    public function indexAction(Request $request)
    {

        return new RedirectResponse($this->generateUrl('fos_user_security_login'));
    }


    /**
     * @param Request $request
     *
     * @return Response
     */
    public function signUpAction(Request $request)
    {
        $registrationData = new RegistrationData();
        $termsAccepted    = new TermsAccepted();
        $billingData      = new BillingData(new Agency());
        $registrationData->setTermsAccepted($termsAccepted);
        $registrationData->setBillingData($billingData);

        $form = $this->createForm(
            'homecare_registration_data',
            $registrationData,
            [
                'action'            => $this->generateUrl('Homecare_HomecareBundle_signup'),
                'method'            => 'POST',
                'validation_groups' => array('Default'),
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Submit']);


        $form->handleRequest($request);


        if ($form->isValid()) {
            $registrationData = $form->getData();
            $agency           = $registrationData->getBillingData()->getAgency();
            $user = new User();

            $agency->initializeAgency(
                $registrationData->getAgencyName(),
                $registrationData->getPhoneNumber(),
                $registrationData->getState(),
                $registrationData->getBillingData()->getPlan()
            );

            $user->initializeUser(
                $registrationData->getFirstName(),
                $registrationData->getLastName(),
                $registrationData->getUsername(),
                $registrationData->getEmail(),
                $registrationData->getPassword()
            );

            $plainPassword = $registrationData->getPassword();
            $encoded       = $this->container->get('security.password_encoder')
                                             ->encodePassword($user, $plainPassword);
            $user->setPassword($encoded);
            // initialize the recipient user
            $user->initializeAgency($agency);

            $em = $this->getDoctrine()->getManager();

            $em->persist($agency);
            $em->persist($user);
            $em->flush();

            $this->addFlash('notice', sprintf(self::SIGN_UP_SUCCESS_MESSAGE, $agency->getAgencyName()));

            return $this->redirectToRoute('index');
        }

        return $this->render(
            '@HomecareHomecare/Page/signUp.html.twig',
            [
                'form' => $form->createView(),
            ]
        );

    }


    public function termsAndConditionsAction()
    {

        return $this->render(
            '@HomecareHomecare/Page/termsandconditions.html.twig',
            [
            ]
        );
    }


}