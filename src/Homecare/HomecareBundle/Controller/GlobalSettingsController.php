<?php

namespace Homecare\HomecareBundle\Controller;

use Homecare\HomecareBundle\Entity\Qualifications;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class GlobalSettingsController
 * @package Homecare\HomecareBundle\Controller
 */
class GlobalSettingsController extends Controller
{


    public function qualificationsAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $qualifications = $em->getRepository("HomecareHomecareBundle:Qualifications")->findAll();

        //if the form has been submitted
        if ($request->getMethod() == "POST") {

            $qualificationName = $request->request->get("qualificationName");

            $qualification = new Qualifications();
            $qualification->setQualification($qualificationName);
            $em->persist($qualification);
            $em->flush();


            $request->getSession()->getFlashBag()->add(
                'notice',
                'Qualification successfully created'
            );

            return $this->redirectToRoute("Homecare_HomecareBundle_qualifications");


        }

        return $this->render(
            'HomecareHomecareBundle:GlobalSettings:qualifications.html.twig',
            array(
                'qualifications' => $qualifications,
            )
        );


    }


    public function deleteQualificationsAction(Request $request)
    {

        //if the form has been submitted
        if ($request->getMethod() == "POST") {

            $em = $this->getDoctrine()->getManager();


            $qualificationId = $request->request->get("qualification");

            $qualification = $em->getRepository("HomecareHomecareBundle:Qualifications")->find($qualificationId);


            if (count($qualification->getPcas()) > 0 || count($qualification->getRequests()) > 0) {
                $request->getSession()->getFlashBag()->add(
                    'notice',
                    'Qualification cannot be deleted because it is currently attached to a PCA or a Request'
                );

                return $this->redirectToRoute("Homecare_HomecareBundle_qualifications");
            }


            $em->remove($qualification);
            $em->flush();


            $request->getSession()->getFlashBag()->add(
                'notice',
                'Qualification successfully deleted'
            );

        }


        return $this->redirectToRoute("Homecare_HomecareBundle_qualifications");

    }


}
