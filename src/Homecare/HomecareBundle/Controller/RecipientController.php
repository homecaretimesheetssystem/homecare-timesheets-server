<?php

namespace Homecare\HomecareBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Homecare\HomecareBundle\Entity\Recipient;
use Homecare\HomecareBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


/**
 * Class RecipientController
 * @package Homecare\HomecareBundle\Controller
 *
 * @author Josh Crawmer <joshcrawmer4@gmail.com>
 */
class RecipientController extends Controller
{

    const SESSION_EMAIL = 'fos_user_send_resetting_email/email';


    /**
     * This action creates a new recipient
     *
     * @param Request $request
     *
     * @Security("has_role('ROLE_AGENCY') or has_role('ROLE_ADMIN')")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function createRecipientAction(Request $request)
    {

        $recipient = new Recipient();
        $user      = new User();
        $recipient->setUser($user);

        $form = $this->buildCreateRecipientForm($recipient);


        if ($request->getMethod() == 'POST') {

            $form->handleRequest($request);

            if ($form->isValid()) {

                $recipient = $form->getData();
                $agency    = ($recipient->getAgency()) ? $recipient->getAgency() : $this->get(
                    'security.token_storage'
                )->getToken()->getUser()->getAgency();

                $recipient->setAgency($agency);

                $user = $recipient->getUser();

                $plainPassword = $user->getPassword();
                $encoded       = $this->container->get('security.password_encoder')
                                                 ->encodePassword($user, $plainPassword);
                $user->setPassword($encoded);

                // initialize the recipient user
                $user->initializeRecipient($recipient);


                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($recipient);
                $em->flush();

                //set a flash message
                $session = $request->getSession();
                $session->getFlashBag()->add(
                    'message',
                    'Recipient Successfully Created!'
                );

                $this->sendPasswordResetEmail($user);


                return $this->redirect($this->generateUrl('Homecare_HomecareBundle_create_recipient'));
            }


        }


        return $this->render(
            'HomecareHomecareBundle:Recipient:createRecipient.html.twig',
            array(
                'form' => $form->createView(),
            )
        );


    }


    /**
     * This method is used to edit a recipient
     *
     * @param $recipientId
     * @param Request $request
     *
     * @Security("has_role('ROLE_AGENCY') or has_role('ROLE_ADMIN')")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editRecipientAction($recipientId, Request $request)
    {
        $recipient = $this->getDoctrine()->getRepository('HomecareHomecareBundle:Recipient')->find($recipientId);


        // check for edit access: calls all voters
        $this->denyAccessUnlessGranted('edit', $recipient);

        $form = $this->buildEditRecipientForm($recipient);

        $originalPassword = $recipient->getUser()->getPassword();

        if ($request->getMethod() == 'POST') {

            $form->handleRequest($request);

            if ($form->isValid()) {

                $recipient = $form->getData();

                $user          = $recipient->getUser();
                $plainPassword = $user->getPassword();
                if ( ! empty($plainPassword)) {
                    //encode the password
                    $encoded = $this->container->get('security.password_encoder')
                                               ->encodePassword($user, $plainPassword);
                    $user->setPassword($encoded);
                } else {
                    $user->setPassword($originalPassword);
                }

                $user->initializeRecipient($recipient);

                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($recipient);
                $em->flush();

                //set a flash message
                $session = $request->getSession();
                $session->getFlashBag()->add(
                    'message',
                    'Recipient Successfully Modified!'
                );

                return $this->redirect(
                    $this->generateUrl(
                        'Homecare_HomecareBundle_edit_recipient',
                        array('recipientId' => $recipientId)
                    )
                );
            }


        }


        return $this->render(
            'HomecareHomecareBundle:Recipient:editRecipient.html.twig',
            array(
                'form' => $form->createView(),
            )
        );

    }


    /**
     * Builds a form to create a recipient.
     *
     * @param Recipient $recipient
     *
     * @return Form The form
     */
    private function buildCreateRecipientForm(Recipient $recipient)
    {
        $form = $this->createForm(
            'homecare_recipient',
            $recipient,
            array(
                'validation_groups' => ['Default', 'PASSWORD'],
            )

        );

        $form->add(
            'submit',
            'submit',
            [
                'label' => 'Create Recipient',
                'attr'  => ['class' => 'btn btn-default'],
            ]
        );

        return $form;
    }


    /**
     * Builds a form to edit a recipient.
     *
     * @param Recipient $recipient
     *
     * @return Form The form
     */
    private function buildEditRecipientForm(Recipient $recipient)
    {
        $form = $this->createForm(
            'homecare_recipient',
            $recipient,
            array(
                'action'            => $this->generateUrl(
                    'Homecare_HomecareBundle_edit_recipient',
                    array('recipientId' => $recipient->getId())
                ),
                'method'            => 'POST',
                'validation_groups' => ['Default'],
            )

        );

        $form->add(
            'submit',
            'submit',
            [
                'label' => 'Edit Recipient',
                'attr'  => ['class' => 'btn btn-default'],
            ]
        );

        return $form;
    }


    /**
     * @param User $user
     *
     * @return Response
     */
    private function sendPasswordResetEmail(User $user)
    {

        if (null === $user) {
            return $this->container->get('templating')->renderResponse(
                'FOSUserBundle:Resetting:request.html.'.$this->getEngine(),
                array('invalid_username' => $user->getUsername())
            );
        }

        if ($user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            return $this->container->get('templating')->renderResponse(
                'FOSUserBundle:Resetting:passwordAlreadyRequested.html.'.$this->getEngine()
            );
        }

        if (null === $user->getConfirmationToken()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $tokenGenerator = $this->container->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        $this->container->get('session')->set(static::SESSION_EMAIL, $this->getObfuscatedEmail($user));
        $this->container->get('fos_user.mailer')->sendResettingEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->container->get('fos_user.user_manager')->updateUser($user);
    }


    /**
     * Get the truncated email displayed when requesting the resetting.
     *
     * The default implementation only keeps the part following @ in the address.
     *
     * @param \FOS\UserBundle\Model\UserInterface $user
     *
     * @return string
     */
    protected function getObfuscatedEmail(UserInterface $user)
    {
        $email = $user->getEmail();
        if (false !== $pos = strpos($email, '@')) {
            $email = '...'.substr($email, $pos);
        }

        return $email;
    }

}
