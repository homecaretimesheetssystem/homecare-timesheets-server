<?php

namespace Homecare\HomecareBundle\Controller;

use Homecare\HomecareBundle\Entity\User;
use Homecare\HomecareBundle\Form\Agency2Type;
use Homecare\HomecareBundle\Form\model\BillingData;
use Homecare\HomecareBundle\Form\model\RegistrationData;
use Homecare\HomecareBundle\Form\model\TermsAccepted;
use Homecare\HomecareBundle\Form\User2Type;
use Homecare\HomecareBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Homecare\HomecareBundle\Entity\Agency;
use Homecare\HomecareBundle\Entity\Services;
use Homecare\HomecareBundle\Form\AgencyType;
use Doctrine\Common\Collections\ArrayCollection;
use Homecare\HomecareBundle\Entity\EmailSettings;
use Homecare\HomecareBundle\Form\EmailSettingsType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class AgencyController extends Controller
{


    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function settingsAction(Request $request)
    {


        $registrationData = new RegistrationData();
        $agency           = $this->getUser()->getAgency();
        //$plan             = $agency->getPlan() ? $agency->getPlan() : 1;
        //$registrationData->setPlan($plan);
        //$agency->setRegistrationData($registrationData);
        $registrationData->setAgency($agency);
        $registrationData->setUser($this->getUser());


        $agencyForm             = $this->buildAgencyForm($agency);
        $billingForm            = $this->buildBillingForm($agency);
        $cancelSubscriptionForm = $this->buildCancelSubscriptionForm();


        $em = $this->getDoctrine()->getEntityManager();

        $agencyForm->handleRequest($request);
        $billingForm->handleRequest($request);
        $cancelSubscriptionForm->handleRequest($request);

        if ($agencyForm->isValid()) {

            $agency = $agencyForm->getData();
            $em->persist($agency);
            $em->flush();

            //set a flash message
            $session = $request->getSession();
            $session->getFlashBag()->add(
                'message',
                'Your agency settings have been successfully updated!'
            );


            $data = $agencyForm->getData();
            if ( ! is_null($data->getImageFile())) {
                $this->get('homecare.homecarebundle.amazons3')->submitFileToAmazonS3($agency->getImageName());
            }


            return $this->redirect($this->generateUrl('Homecare_HomecareBundle_agency_settings'));
        }


        if ($billingForm->isValid()) {
            $data = $billingForm->getData();
            $data->getAgency();

            //$agency->
            $em->persist($agency);
            $em->flush();

            //set a flash message
            $session = $request->getSession();
            $session->getFlashBag()->add(
                'message',
                'Your agency settings have been successfully updated!'
            );

            return $this->redirect($this->generateUrl('Homecare_HomecareBundle_agency_settings'));


        }


        if ($cancelSubscriptionForm->isValid()) {
            $agency = $this->getUser()->getAgency();
            if ( ! $agency) {
                throw $this->createNotFoundException('This account is not an agency account');
            }

            $em = $this->getDoctrine()->getEntityManager();

            $pcaUsers = [];
            foreach ($agency->getPcas() as $pca) {
                array_push($pcaUsers, $pca->getUser());
            }
            $recipientUsers = [];
            foreach ($agency->getRecipients() as $recipient) {
                array_push($recipientUsers, $recipient->getUser());
            }

            $userCollection = new ArrayCollection(
                array_merge(
                    $recipientUsers,
                    $pcaUsers
                )
            );

            // cancel all the pcas and recipients for the agency
            foreach ($userCollection as $user) {
                // we are using the expired flag as determining if the account has been cancelled or not
                $user->setExpired(true);
                $em->persist($user);
                $em->flush($user);
            }


            if (null !== $agency->getStripeSubscriptionId()) {
                $this->container->get('homecare_homecare.data.stripe_adapter')->cancelSubscription(
                    $agency->getStripeSubscriptionId()

                );

                $agency->setStripeSubscriptionId(null);
                $agency->setPlan(0);
                $em->persist($agency);
                $em->flush($agency);

            }

            //set a flash message
            $session = $request->getSession();
            $session->getFlashBag()->add(
                'message',
                'Your agency subscription has been successfully cancelled. Return to this page to resign up at any point.'
            );


            return $this->redirect($this->generateUrl('Homecare_HomecareBundle_agency_settings'));
        }


        return $this->render(
            'HomecareHomecareBundle:Agency:settings.html.twig',
            array(
                'agencyForm'             => $agencyForm->createView(),
                'billingForm'            => $billingForm->createView(),
                'cancelSubscriptionForm' => $cancelSubscriptionForm->createView(),
                'agency'                 => $agency,
            )
        );
    }


    public function createAgencyUserAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        //grab the current logged in user;
        $user = $this->get('security.token_storage')->getToken()->getUser();
        //create a new user object
        $newUser = new User();
        $form    = $this->createForm(
            'homecare_homecarebundle_user',
            $newUser,
            array('validation_groups' => array('Default', 'PASSWORD'))
        );

        $form->add('submit', 'submit', ['label' => 'Submit']);

        //if the form has been submitted
        if ($request->getMethod() == "POST") {
            //If the form was submitted then bind the request variables to the form User object
            $form->handleRequest($request);

            if ($form->isValid()) {
                $newUser->addRole('ROLE_AGENCY');
                //grab the password from the form
                $plainPassword = $newUser->getPassword();
                //encode the password
                $encoded = $this->container->get('security.password_encoder')
                                           ->encodePassword($newUser, $plainPassword);
                $newUser->setPassword($encoded);
                $newUser->setEnabled(true);
                $newUser->setAgency($user->getAgency());
                $em->persist($newUser);
                $em->flush();
                $request->getSession()->getFlashBag()->add(
                    'notice',
                    'Agency User Successfully Created!'
                );

                return $this->redirectToRoute('Homecare_HomecareBundle_agency_create_agency_user');
            }
        }

        return $this->render(
            'HomecareHomecareBundle:Agency:createAgencyUser.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }


    public function editAgencyUserAction($userId, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        //grab the current logged in user;
        $user = $this->get('security.token_storage')->getToken()->getUser();
        //create a new user object
        $editUser = $this->getDoctrine()->getRepository('HomecareHomecareBundle:User')->find($userId);


        // check for edit access: calls all voters
        $this->denyAccessUnlessGranted('edit', $editUser);


        $form = $this->createForm(
            'homecare_homecarebundle_user',
            $editUser,
            array('validation_groups' => array('Default'))
        );

        $form->add('submit', 'submit', ['label' => 'Submit']);


        $originalPassword = $user->getPassword();

        //if the form has been submitted
        if ($request->getMethod() == "POST") {
            //If the form was submitted then bind the request variables to the form User object
            $form->handleRequest($request);

            if ($form->isValid()) {
                $editUser->addRole('ROLE_AGENCY');
                //grab the password from the form
                $plainPassword = $editUser->getPassword();
                if ( ! empty($plainPassword)) {
                    //encode the password
                    $encoded = $this->container->get('security.password_encoder')
                                               ->encodePassword($editUser, $plainPassword);


                    //return new Response($encoded);
                    $editUser->setPassword($encoded);
                } else {
                    $editUser->setPassword($originalPassword);
                }

                $editUser->setEnabled(true);
                $editUser->setAgency($user->getAgency());
                $em->persist($editUser);
                $em->flush();
                $request->getSession()->getFlashBag()->add(
                    'notice',
                    'Agency User Successfully Modified!'
                );

                return $this->redirectToRoute(
                    'Homecare_HomecareBundle_agency_edit_agency_user',
                    array('userId' => $userId)
                );
            }
        }

        return $this->render(
            'HomecareHomecareBundle:Agency:editAgencyUser.html.twig',
            array(
                'form' => $form->createView(),
            )
        );

    }


    public function emailSettingsAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $newEmailSettings = new EmailSettings();
        $form             = $this->createForm(new EmailSettingsType(), $newEmailSettings);

        //grab the current logged in user;
        $user   = $this->get('security.token_storage')->getToken()->getUser();
        $agency = $user->getAgency();


        //get all the emails related to the logged in agency so you can pass to the view

        $emails = $agency->getEmails();


        //if the form has been submitted
        if ($request->getMethod() == "POST") {
            //If the form was submitted then bind the request variables to the form User object
            $form->handleRequest($request);

            if ($form->isValid()) {
                $newEmailSettings->setAgency($agency);
                $em->persist($newEmailSettings);
                $em->flush();
                $this->addFlash('message', 'Email has been successfuly added');

                return $this->redirectToRoute('Homecare_HomecareBundle_agency_email_settings');
            }
        }

        return $this->render(
            'HomecareHomecareBundle:Agency:emailSettings.html.twig',
            array(
                'form'   => $form->createView(),
                'emails' => $emails,
            )
        );
    }


    public function deleteEmailAction($emailId)
    {
        $em    = $this->getDoctrine()->getManager();
        $email = $em->getRepository("HomecareHomecareBundle:EmailSettings")->find($emailId);
        $em->remove($email);
        $em->flush();
        $this->addFlash('message', 'Email Was Successfully Deleted');

        return $this->redirectToRoute('Homecare_HomecareBundle_agency_email_settings');

    }


    private function getRandomString()
    {
        $length     = 5;
        $characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWZYZ";

        $real_string_length = strlen($characters);
        $string             = "id";

        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, $real_string_length - 1)];
        }

        return strtolower($string);
    }


    public function createAgencyAction(Request $request)
    {

        $registrationData = new RegistrationData();
        $termsAccepted    = new TermsAccepted();
        $billingData      = new BillingData(new Agency());
        $registrationData->setTermsAccepted($termsAccepted);
        $registrationData->setBillingData($billingData);

        $form = $this->createForm(
            'homecare_registration_data',
            $registrationData,
            [
                'action'            => $this->generateUrl('Homecare_HomecareBundle_create_agency'),
                'method'            => 'POST',
                'validation_groups' => array('Default'),
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Submit']);


        $form->handleRequest($request);


        if ($form->isValid()) {
            $registrationData = $form->getData();
            $agency           = $registrationData->getBillingData()->getAgency();
            $user             = new User();

            $agency->initializeAgency(
                $registrationData->getAgencyName(),
                $registrationData->getPhoneNumber(),
                $registrationData->getState(),
                $registrationData->getBillingData()->getPlan()
            );

            $user->initializeUser(
                $registrationData->getFirstName(),
                $registrationData->getLastName(),
                $registrationData->getUsername(),
                $registrationData->getEmail(),
                $registrationData->getPassword()
            );

            $plainPassword = $registrationData->getPassword();
            $encoded       = $this->container->get('security.password_encoder')
                                             ->encodePassword($user, $plainPassword);
            $user->setPassword($encoded);
            // initialize the recipient user
            $user->initializeAgency($agency);

            $em = $this->getDoctrine()->getManager();

            $em->persist($agency);
            $em->persist($user);
            $em->flush();

            $this->addFlash('notice', 'Agency has been successfully created.');

            return $this->redirectToRoute('Homecare_HomecareBundle_create_agency');
        }


        return $this->render(
            'HomecareHomecareBundle:Admin:createAgency.html.twig',
            array(
                'form' => $form->createView(),
            )
        );


    }


    public function editAgencyAction(Request $request, Agency $agency)
    {

        $registrationData = new RegistrationData();
        $termsAccepted    = new TermsAccepted();
        $billingData      = new BillingData(new Agency());
        $registrationData->setTermsAccepted($termsAccepted);
        $registrationData->setBillingData($billingData);


        $agencyForm  = $this->buildAdminAgencyForm($agency);
        $billingForm = $this->buildAdminBillingForm($agency);


        $em = $this->getDoctrine()->getEntityManager();

        $agencyForm->handleRequest($request);
        $billingForm->handleRequest($request);


        if ($agencyForm->isValid()) {

            $agency = $agencyForm->getData();
            $em->persist($agency);
            $em->flush();

            //set a flash message
            $session = $request->getSession();
            $session->getFlashBag()->add(
                'message',
                'Your agency settings have been successfully updated!'
            );


            $data = $agencyForm->getData();
            if ( ! is_null($data->getImageFile())) {
                $this->get('homecare.homecarebundle.amazons3')->submitFileToAmazonS3($agency->getImageName());
            }


            return $this->redirect(
                $this->generateUrl('Homecare_HomecareBundle_edit_agency', array('id' => $agency->getId()))
            );
        }


        if ($billingForm->isValid()) {
            $data   = $billingForm->getData();
            $agency = $data->getAgency();
            $em->persist($agency);
            $em->flush();

            //set a flash message
            $session = $request->getSession();
            $session->getFlashBag()->add(
                'message',
                'Your agency settings have been successfully updated!'
            );

            return $this->redirect(
                $this->generateUrl('Homecare_HomecareBundle_edit_agency', array('id' => $agency->getId()))
            );

        }


        return $this->render(
            'HomecareHomecareBundle:Admin:editAgency.html.twig',
            array(
                'agencyForm'  => $agencyForm->createView(),
                'billingForm' => $billingForm->createView(),
            )
        );


    }


    /**
     * @param Agency $agency
     *
     * @return \Symfony\Component\Form\Form
     */
    private function buildAgencyForm(Agency $agency)
    {

        $form = $this->createForm(
            'homecare_homecarebundle_agency',
            $agency,
            [
                'action' => $this->generateUrl('Homecare_HomecareBundle_agency_settings'),
                'method' => 'POST',
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Submit']);


        return $form;

    }


    /**
     * @param Agency $agency
     *
     * @return \Symfony\Component\Form\Form
     */
    private function buildAdminAgencyForm(Agency $agency)
    {

        $form = $this->createForm(
            'homecare_homecarebundle_agency',
            $agency,
            [
                'action' => $this->generateUrl(
                    'Homecare_HomecareBundle_edit_agency',
                    array('id' => $agency->getId())
                ),
                'method' => 'POST',
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Submit']);


        return $form;

    }


    /**
     * @param Agency $agency
     *
     * @return \Symfony\Component\Form\Form
     */
    private function buildBillingForm(Agency $agency)
    {


        $billingData = new BillingData($agency);
        $billingData->setPlan($agency->getPlan());

        $form = $this->createForm(
            'homecare_billing_data',
            $billingData,
            [
                'action'            => $this->generateUrl('Homecare_HomecareBundle_agency_settings'),
                'method'            => 'POST',
                'validation_groups' => array('Default'),
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Submit']);


        return $form;

    }


    /**
     * @param Agency $agency
     *
     * @return \Symfony\Component\Form\Form
     */
    private function buildAdminBillingForm(Agency $agency)
    {


        $billingData = new BillingData($agency);
        $billingData->setPlan($agency->getPlan());

        $form = $this->createForm(
            'homecare_billing_data',
            $billingData,
            [
                'action'            => $this->generateUrl(
                    'Homecare_HomecareBundle_edit_agency',
                    array('id' => $agency->getId())
                ),
                'method'            => 'POST',
                'validation_groups' => array('Default'),
            ]
        );

        $form->add('submit', 'submit', ['label' => 'Submit']);


        return $form;

    }


    /**
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function buildCancelSubscriptionForm()
    {

        return $this->createFormBuilder()
                    ->add('cancel', SubmitType::class, array('label' => 'Cancel Subscription'))
                    ->setAction($this->generateUrl('Homecare_HomecareBundle_agency_settings'))
                    ->setMethod('POST')
                    ->getForm();

    }


}
