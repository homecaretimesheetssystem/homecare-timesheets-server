<?php

namespace Homecare\HomecareBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class FilesController extends Controller
{

    /**
     * this action shows all the files for a given timesheet
     *
     * @param $timesheetId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showFilesAction($timesheetId)
    {
        $timesheet = $this->getDoctrine()->getManager()->getRepository('HomecareHomecareBundle:Timesheet')->find(
            $timesheetId
        );

        // check for download access: calls all voters
        $this->denyAccessUnlessGranted('files', $timesheet);

        $files = $timesheet->getFile();

        return $this->render(
            'HomecareHomecareBundle:Files:showFiles.html.twig',
            array(
                'files'       => $files,
                'timesheetId' => $timesheetId,
                'timesheet'   => $timesheet,
            )
        );
    }

    /**
     * this action downloads a single file
     *
     * @param $fileName
     * @param $timesheetId
     * @param $kindOfFile
     *
     * @return Response
     */
    public function downloadFileAction($fileName, $timesheetId)
    {
        $info      = new \SplFileInfo($fileName);
        $em        = $this->getDoctrine()->getManager();
        $timesheet = $em->getRepository("HomecareHomecareBundle:Timesheet")->find($timesheetId);

        // check for File access: calls all voters
        $this->denyAccessUnlessGranted('files', $timesheet);

        $amazonS3 = $this->get('homecare.homecarebundle.amazons3');
        $result   = $amazonS3->downloadFile($fileName);
        $response = new Response();
        $response->headers->set(
            'Content-Disposition',
            'attachment; filename="'.$this->get('homecare_homecarebundle.model.filenaming')->createFileName(
                $timesheet,
                '.'.$info->getExtension()
            ).'";'
        );
        $response->headers->set('Content-type', $result['ContentType']);
        $response->setContent($result['Body']);

        return $response;
    }


    public function downloadMultipleFilesAction(Request $request)
    {
        $allTheFileNames = [];
        $files           = $request->query->get('files');
        $timesheetId     = $request->query->get('timesheetId');
        $em              = $this->getDoctrine()->getManager();
        $timesheet       = $em->getRepository("HomecareHomecareBundle:Timesheet")->find($timesheetId);


        // check for File access: calls all voters
        $this->denyAccessUnlessGranted('files', $timesheet);

        //create a variable to add onto the end of the name of the file to make sure each file has a unique name
        $i = 1;
        foreach ($files as $file) {
            $info = new \SplFileInfo($file);
            //download s3 files to uploads directory
            $this->get('homecare.homecarebundle.amazons3')->downloadFile($file, true);
            $allTheFileNames[$file] = $this->get('homecare_homecarebundle.model.filenaming')->createFileName(
                $timesheet,
                ' file#'.$i.'.'.$info->getExtension()
            );
            $i++;
        }

        //create zip file and add files to it
        $customZipFileObj = $this->get('homecare_homecarebundle.model.zipfile');
        $customZipFileObj->addFilesToZip($allTheFileNames);

        //send the zip file to the browser
        $zipFileName  = $this->get('kernel')->getRootDir().'/../web/images/files.zip';
        $shouldDelete = true;
        $response     = new BinaryFileResponse($zipFileName);
        $response->deleteFileAfterSend($shouldDelete);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'files.zip'
        );

        return $response;
    }
}
