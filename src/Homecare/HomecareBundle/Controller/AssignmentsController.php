<?php

namespace Homecare\HomecareBundle\Controller;

use Homecare\HomecareBundle\Form\RecipientPcaFilterType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Homecare\HomecareBundle\Entity\RecipientPca;
use Homecare\HomecareBundle\Form\RecipientPcaType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Homecare\HomecareBundle\Entity\RandomVerification;
use Symfony\Component\HttpFoundation\Response;


class AssignmentsController extends Controller
{


    public function assignPcaToRecipientAction(Request $request)
    {
        $em           = $this->getDoctrine()->getManager();
        $recipientPca = new RecipientPca();
        $form         = $this->createForm(new RecipientPcaType($this->getUser()), $recipientPca);

        //if the form has been submitted
        if ($request->getMethod() == "POST") {
            //If the form was submitted then bind the request variables to the form User object
            $form->handleRequest($request);


            //do some testing here to make sure that the pca/recipient combination is not in use yet
            $recipientPca = $this->getDoctrine()
                                 ->getRepository('HomecareHomecareBundle:RecipientPca')
                                 ->checkIfPcaRecipientCombinationExists(
                                     $recipientPca->getRecipient(),
                                     $recipientPca->getPca()
                                 );

            // if a recipient is already assigned to that pca and it is not a temporary assignment then return an error


            if ($recipientPca && $recipientPca->getIsTemp() == false) {

                $form->addError(
                    new FormError(
                        'That Recipient Has Already Been
                 Assigned To That Pca. Select A Different Recipient Or Pca'
                    )
                );

            }


            if ($form->isValid()) {

                // if there was a temporary assignment then delete it
                if ($recipientPca && $recipientPca->getIsTemp() == true) {

                    $em->remove($recipientPca);
                    $em->flush();

                }

                $assignment = $form->getData();

                // create a new verification
                $verification = $this->get('homecare_homecare.model.verification');
                $verification->setRecipientPca($assignment);
                $verification->setFirstTimeVerification();

                $request->getSession()->getFlashBag()->add(
                    'message',
                    'Recipient Has Been Assigned To Pca'
                );

                return $this->redirectToRoute('Homecare_HomecareBundle_assign_pca_to_recipient');
            }
        }

        return $this->render(
            'HomecareHomecareBundle:Assignments:assignPcaToRecipient.html.twig',
            array(
                'form' => $form->createView(),
            )
        );
    }


    public function deleteAssignmentAction($recipientPcaId)
    {
        $em           = $this->getDoctrine()->getManager();
        $recipientPca = $em->getRepository("HomecareHomecareBundle:RecipientPca")->find($recipientPcaId);


        foreach ($recipientPca->getVerification() as $verification) {
            if ($verification) {
                $verification->setRecipientPca(null);
                $verification->setArchivedRecipient($recipientPca->getRecipient());
                $verification->setArchivedPca($recipientPca->getPca());
                $em->persist($verification);
                $em->flush($verification);
            }
        }

        $pca = $recipientPca->getPca();
        $em->remove($recipientPca);
        $em->flush();

        $this->get('homecare_homecare.model.verification')->decideFrequency($pca);

        $this->addFlash('message', 'Recipient Pca Combination Successfully Deleted');

        return $this->redirectToRoute('Homecare_HomecareBundle_show_assignments');
    }


    public function showAssignmentsAction(Request $request, $filter, $order)
    {

        $agency     = $this->getUser()->getAgency();
        $pcas       = $agency->getPcas();
        $recipients = $agency->getRecipients();

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(
            new RecipientPcaFilterType(),
            null,
            [
                'action' => $this->generateUrl('Homecare_HomecareBundle_show_assignments'),
                'method' => 'GET',
            ]
        );


        $filterBuilder = $this->getDoctrine()
                              ->getManager()
                              ->getRepository(
                                  'HomecareHomecareBundle:RecipientPca'
                              )->createQueryBuilder('rp')
                              ->innerJoin('rp.pca', 'pc')
                              ->innerJoin('rp.recipient', 're')
                              ->innerJoin('re.user', 'ru', 'WITH', 'ru.archived = 0')
                              ->innerJoin('pc.user', 'pu', 'WITH', 'pu.archived = 0')
                              ->where('rp.recipient IN (:recipients)')
                              ->andWhere('rp.pca IN (:pcas)')
                              ->andWhere('rp.isTemp = false')
                              ->setParameter('recipients', $recipients)
                              ->setParameter('pcas', $pcas);


        $form->handleRequest($request);

        if ($form->isValid()) {

            $this->get('lexik_form_filter.query_builder_updater')
                 ->addFilterConditions($form, $filterBuilder);

            $filterBuilder->getQuery()->getResult();

        }


        /*
        $recipientPcas = $em->getRepository("HomecareHomecareBundle:RecipientPca")->findAgencyAssignments(
            $pcas,
            $recipients
        );
*/

        //dump($recipientPcas);


        $query      = $filterBuilder->getQuery();
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

        $sortedRecipientPcas = $this->sortRecipientPcas($pagination, $filter, $order);

        // swap the order
        $order = $order == 'asc' ? 'desc' : 'asc';

        return $this->render(
            'HomecareHomecareBundle:Assignments:showAssignments.html.twig',
            array(
                'recipientPcas' => $sortedRecipientPcas,
                'order'         => $order,
                'form'          => $form->createView(),
                'pagination'    => $pagination,
            )
        );
    }


    public function verificationsAction($recipientPcaId)
    {

        $em            = $this->getDoctrine()->getManager();
        $verifications = $em->getRepository("HomecareHomecareBundle:Verification")->findBy(
            array(
                'recipientPca' => $recipientPcaId,
            )
        );

        return $this->render(
            'HomecareHomecareBundle:Assignments:verifications.html.twig',
            array(
                'verifications' => $verifications,
            )
        );

    }


    // PRIVATE METHODS

    private function sortRecipientPcas($recipientPcas, $filter, $order)
    {
        $array = [];
        foreach ($recipientPcas as $rp) {
            $array[] = [
                'recipientPcaId'     => $rp->getId(),
                'pcaFirstName'       => $rp->getPca()->getFirstName(),
                'pcaLastName'        => $rp->getPca()->getLastName(),
                'agencyName'         => $rp->getPca()->getAgency()->getAgencyName(),
                'recipientFirstName' => $rp->getRecipient()->getFirstName(),
                'recipientLastName'  => $rp->getRecipient()->getLastName(),
            ];
        }


        usort(
            $array,
            function ($item1, $item2) use ($filter, $order) {
                if ($order == 'asc') {
                    return $item1[$filter.'LastName'] < $item2[$filter.'LastName'] ? -1 : 1;
                } else {
                    return $item1[$filter.'LastName'] < $item2[$filter.'LastName'] ? 1 : -1;
                }

            }
        );

        return $array;
    }


}
