<?php
// src/Homecare/HomecareBundle/Twig/AppExtension.php
namespace Homecare\HomecareBundle\Twig;
/**
* this class is a custom twig extension that i created which gets all the unique items out of an array and 
* returns an array of the unique items
*/
class AppExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('unique', array($this, 'uniqueFilter')),
        );
    }

    public function uniqueFilter( $arrayToFilter )
    {
       $uniqueArray = array_unique( $arrayToFilter );

        return $uniqueArray;
    }

    public function getName()
    {
        return 'app_extension';
    }
}


?>