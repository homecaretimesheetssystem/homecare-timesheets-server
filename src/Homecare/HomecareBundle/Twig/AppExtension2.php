<?php
// src/Homecare/HomecareBundle/Twig/AppExtension.php
namespace Homecare\HomecareBundle\Twig;
/**
* this class is a custom twig extension that i created which gets all the unique items out of an array and 
* returns an array of the unique items
*/
class AppExtension2 extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('json_decode', array($this, 'jsonDecodeFilter')),
        );
    }

    public function jsonDecodeFilter( $jsonToDecode )
    {
       $decodedJson = json_decode( $jsonToDecode );

        return $decodedJson;
    }

    public function getName()
    {
        return 'app_extension2';
    }
}


?>