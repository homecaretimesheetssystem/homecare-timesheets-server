<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 5/30/16
 * Time: 3:32 PM
 */

namespace Homecare\HomecareBundle\Security;

use Homecare\HomecareBundle\Entity\Pca;
use Homecare\HomecareBundle\Entity\Recipient;
use Homecare\HomecareBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class RecipientVoter extends Voter
{

    /**
     * these strings are just invented. You can use anything
     */
    const EDIT = 'edit';


    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }


    protected function supports($attribute, $subject)
    {

        // if the attribute isn't one we support, return false
        if ( ! in_array($attribute, array(self::EDIT))) {
            return false;
        }

        // only vote on User objects inside this voter
        if ( ! $subject instanceof Recipient) {
            return false;
        }

        return true;

    }


    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {

        $user = $token->getUser();

        if ( ! $user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is a User object, thanks to supports
        /** @var Pca $pca */
        $recipient = $subject;

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($recipient, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');

    }



    private function canEdit($recipient, $user, $token)
    {

        return $recipient->getAgency() === $user->getAgency() || $this->decisionManager->decide(
            $token,
            array('ROLE_ADMIN')
        );

    }


}