<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 5/30/16
 * Time: 2:32 PM
 */

namespace Homecare\HomecareBundle\Security;


use Homecare\HomecareBundle\Entity\User;
use Homecare\HomecareBundle\Entity\Timesheet;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class TimesheetVoter extends Voter
{

    /**
     * these strings are just invented. You can use anything
     */
    const ARCHIVE = 'archive';
    const DOWNLOAD = 'download';
    const COMPARE = 'compare';
    const FILES = 'files';


    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }


    protected function supports($attribute, $subject)
    {


        // if the attribute isn't one we support, return false
        if ( ! in_array($attribute, array(self::ARCHIVE, self::DOWNLOAD, self::COMPARE, self::FILES))) {
            return false;
        }

        // only vote on Post objects inside this voter
        if ( ! $subject instanceof Timesheet) {
            return false;
        }

        return true;

    }


    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {

        $user = $token->getUser();

        if ( ! $user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is a Post object, thanks to supports
        /** @var Timesheet $post */
        $timesheet = $subject;

        switch ($attribute) {
            case self::ARCHIVE:
                return $this->canArchive($timesheet, $user, $token);
            case self::DOWNLOAD:
                return $this->canDownload($timesheet, $user, $token);
            case self::COMPARE:
                return $this->canCompare($timesheet, $user, $token);
            case self::FILES:
                return $this->canViewFiles($timesheet, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');

    }


    private function canArchive($timesheet, $user, $token)
    {
        return $user->getAgency() === $timesheet->getOwner() ||
               $this->decisionManager->decide($token, array('ROLE_ADMIN'));

    }

    private function canDownload($timesheet, $user, $token)
    {

        if ($user->getAgency()) {
            return $user->getAgency() === $timesheet->getOwner() ||
                   $this->decisionManager->decide($token, array('ROLE_ADMIN'));
        } elseif ($user->getRecipient()) {
            return $user->getRecipient() === $timesheet->getRecipient() ||
                   $this->decisionManager->decide($token, array('ROLE_ADMIN'));
        } elseif ($user->getAdmin()) {
            return $this->decisionManager->decide($token, array('ROLE_ADMIN'));
        }

    }


    private function canCompare($timesheet, $user, $token)
    {
        if ($user->getAgency()) {
            return $user->getAgency() === $timesheet->getOwner() ||
                   $this->decisionManager->decide($token, array('ROLE_ADMIN'));
        } elseif ($user->getRecipient()) {
            return $user->getRecipient() === $timesheet->getRecipient() ||
                   $this->decisionManager->decide($token, array('ROLE_ADMIN'));
        } elseif ($user->getAdmin()) {
            return $this->decisionManager->decide($token, array('ROLE_ADMIN'));
        }

    }



    private function canViewFiles($timesheet, $user, $token)
    {
        return $user->getAgency() === $timesheet->getOwner() ||
               $this->decisionManager->decide($token, array('ROLE_ADMIN'));
    }


}