<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 5/30/16
 * Time: 3:32 PM
 */

namespace Homecare\HomecareBundle\Security;

use Homecare\HomecareBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserVoter extends Voter
{

    /**
     * these strings are just invented. You can use anything
     */
    const ARCHIVE = 'archive';
    const EDIT = 'edit';


    private $decisionManager;

    public function __construct(AccessDecisionManagerInterface $decisionManager)
    {
        $this->decisionManager = $decisionManager;
    }


    protected function supports($attribute, $subject)
    {

        // if the attribute isn't one we support, return false
        if ( ! in_array($attribute, array(self::ARCHIVE, self::EDIT))) {
            return false;
        }

        // only vote on User objects inside this voter
        if ( ! $subject instanceof User) {
            return false;
        }

        return true;

    }


    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {

        $user = $token->getUser();

        if ( ! $user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is a User object, thanks to supports
        /** @var User $userSubject */
        $userSubject = $subject;

        switch ($attribute) {
            case self::ARCHIVE:
                return $this->canArchive($userSubject, $user, $token);
            case self::EDIT:
                return $this->canEdit($userSubject, $user, $token);
        }

        throw new \LogicException('This code should not be reached!');

    }


    private function canArchive($userSubject, $user, $token)
    {

        // what type of user are you trying to delete
        if ($userSubject->getAgency()) {
            return $userSubject->getAgency() === $user->getAgency() || $this->decisionManager->decide(
                $token,
                array('ROLE_ADMIN')
            );
        } elseif ($userSubject->getRecipient()) {
            return $userSubject->getRecipient()->getAgency() === $user->getAgency() || $this->decisionManager->decide(
                $token,
                array('ROLE_ADMIN')
            );
        } elseif ($userSubject->getPca()) {
            return $userSubject->getPca()->getAgency() === $user->getAgency() || $this->decisionManager->decide(
                $token,
                array('ROLE_ADMIN')
            );
        } elseif ($userSubject->getAdmin()) {
            return $this->decisionManager->decide($token, array('ROLE_ADMIN'));
        }

    }


    private function canEdit($userSubject, $user, $token)
    {

        return $userSubject->getAgency() === $user->getAgency() || $this->decisionManager->decide(
            $token,
            array('ROLE_ADMIN')
        );

    }


}