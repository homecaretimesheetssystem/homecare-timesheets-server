<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 2/21/17
 * Time: 9:49 AM
 */

namespace Homecare\HomecareBundle\Data;


use paragraph1\phpFCM\Client;
use paragraph1\phpFCM\Message;
use paragraph1\phpFCM\Recipient\Device as FcmDevice;
use paragraph1\phpFCM\Notification;
use Homecare\HomecareBundle\Entity\Device;


/**
 * Class FirebaseCloudMessagingAdapter
 * @package Homecare\HomecareBundle\Data
 *
 * @author Josh Crawmer <jcrawmer@smp.org>
 */
class FirebaseCloudMessagingAdapter
{

    /**
     * Notification icon name
     */
    const NOTIFICATION_ICON = 'notification_icon_resource_name';

    /**
     * Notification color
     */
    const NOTIFICATION_COLOR = '#ffffff';

    /**
     * Notification badge
     */
    const NOTIFICATION_BADGE = 1;


    /**
     * Notification Sound
     */
    const NOTIFICATION_SOUND = "default";


    private $apiKey;

    /**
     * FirebaseCloudMessagingAdapter constructor.
     *
     * @param $apiKey
     */
    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }


    /**
     * Attach a $notification object to send a notification which will cause the notification popup on the app
     * Attach a $data array to send data in the background quietly. You can do either or, or just one of them
     *
     * @param Notification|null $notification (new Notification('test title', 'testing body'))
     * @param Device $device
     * @param array $data (Example: ['someId' => 111, 'requestCreated' => true,])
     */
    public function sendToDevice(
        Notification $notification = null,
        Device $device,
        $data = array()
    ) {

        $client = new Client();
        $client->setApiKey($this->apiKey);
        $client->injectHttpClient(new \GuzzleHttp\Client());

        $message = new Message();
        $message->addRecipient(new FcmDevice($device->getDeviceToken()));


        if ($notification) {
            $notification->setIcon(self::NOTIFICATION_ICON)
                         ->setColor(self::NOTIFICATION_COLOR)
                         ->setBadge(self::NOTIFICATION_BADGE)
                         ->setSound(self::NOTIFICATION_SOUND);
            $message->setNotification($notification);
        }

        if ( ! empty($data)) {
            $message->setData($data);
        }


        $response = $client->send($message);
        //var_dump($response->getStatusCode());

    }


}