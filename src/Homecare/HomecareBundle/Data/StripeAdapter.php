<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 2/22/17
 * Time: 1:39 PM
 */

namespace Homecare\HomecareBundle\Data;


use InvalidArgumentException;
use Stripe\Stripe;
use Stripe\Customer;
use \Stripe\Subscription;


/**
 * Class StripeAdapter
 * @package Homecare\HomecareBundle\Data
 * @author Josh Crawmer <joshcrawmer4@gmail.com>
 */
class StripeAdapter
{

    /**
     * 1 - 100 employees
     */
    const PLAN_1_100_EMPLOYEES = 1;

    /**
     * 101 - 200 employees
     */
    const PLAN_101_200_EMPLOYEES = 2;

    /**
     * over 200 employees
     */
    const PLAN_OVER_200_EMPLOYEES = 3;

    /**
     * Invalid argument exception error message
     */
    const INVALID_ARGUMENT_ERROR = "\$plan argument must be one of the following options %s";

    /**
     * available plans
     *
     * @var array
     */
    public static $availablePlans = [

        self::PLAN_1_100_EMPLOYEES,
        self::PLAN_101_200_EMPLOYEES,
        self::PLAN_OVER_200_EMPLOYEES,
    ];


    /**
     * @var string
     */
    private $secretKey;

    /**
     * @var string
     */
    private $publishableKey;


    /**
     * Depending on whether or not your in development or production the service container
     * will automatically inject the corresponding test and live keys that way a live charge will never happen
     * while in development
     *
     * StripeAdapter constructor.
     *
     * @param $secretKey
     * @param $publishableKey
     */
    public function __construct($secretKey, $publishableKey)
    {
        $this->secretKey      = $secretKey;
        $this->publishableKey = $publishableKey;

        // set the Api Key
        Stripe::setApiKey($this->secretKey);


    }

    /**
     * @param array $data associative array of data to save on the customer
     * 'description' => '',
     * 'email' => ''
     *
     *
     * @return Customer
     */
    public function createCustomer($data = array())
    {
        return Customer::create($data);
    }


    /**
     * returns a customer object
     *
     * @param $customerId
     *
     * @return Customer
     */
    public function getCustomer($customerId)
    {
        return Customer::retrieve($customerId);
    }

    /**
     * @param Customer $customer
     * @param array $data
     *
     * @return array|\Stripe\StripeObject
     */
    public function createCard(Customer $customer, $data = array())
    {
        return $customer->sources->create(array("source" => $data));
    }


    /**
     * @param Customer $customer
     * @param $plan
     *
     * @return Subscription
     */
    public function createSubscription(Customer $customer, $plan = self::PLAN_1_100_EMPLOYEES)
    {

        // if trying to create a plan that does not exist then throw an exception
        if ( ! in_array($plan, self::$availablePlans)) {
            throw new InvalidArgumentException(
                sprintf(self::INVALID_ARGUMENT_ERROR, implode(",", self::$availablePlans))
            );
        }

        return Subscription::create(
            array(
                "customer" => $customer->id,
                "plan"     => $plan,
            )
        );
    }


    /**
     * returns a subscription object
     *
     * @param $subscriptionId
     *
     * @return Customer
     */
    public function getSubscription($subscriptionId)
    {
        return Subscription::retrieve($subscriptionId);
    }


    /**
     * @param $subscriptionId
     *
     * @return Subscription
     */
    public function cancelSubscription($subscriptionId)
    {
        $subscription = Subscription::retrieve($subscriptionId);

        return $subscription->cancel();
    }

}