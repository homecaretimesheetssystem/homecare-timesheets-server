<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 11/30/16
 * Time: 11:54 AM
 */

namespace Homecare\HomecareBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\ReversedTransformer;
use Symfony\Component\Form\Exception\InvalidConfigurationException;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToTimestampTransformer;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToArrayTransformer;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomTimeType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        // build the minutes
        $minutes = [];
        for ($i = 0; $i <= 60; $i++) {
            if (strlen((string)$i) == 1) {
                $minutes[sprintf("%d$i", 0)] = sprintf("%d$i", 0);
            } else {
                $minutes[$i] = $i;
            }

        }


        // build the hours
        $hours = [];
        for ($i = 1; $i <= 12; $i++) {
            if (strlen((string)$i) == 1) {
                $hours[sprintf("%d$i", 0)] = sprintf("%d$i", 0);
            } else {
                $hours[$i] = $i;
            }

        }


        $builder->add(
            'hour',
            ChoiceType::class,
            array(
                'choices' => $hours,
            )
        );

        $builder->add('minute', ChoiceType::class, array('choices' => $minutes));

        $builder->add(
            'period',
            ChoiceType::class,
            array(
                'choices' => array(
                    'AM' => 'AM',
                    'PM' => 'PM',

                ),
            )
        );


    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'custom_time';
    }
}
