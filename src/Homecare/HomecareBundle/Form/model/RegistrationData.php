<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 2/19/17
 * Time: 2:13 PM
 */

namespace Homecare\HomecareBundle\Form\model;


use Homecare\HomecareBundle\Entity\Agency;
use Homecare\HomecareBundle\Entity\States;
use Homecare\HomecareBundle\Entity\User;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Validator\Constraints as Assert;
use Homecare\HomecareBundle\Validator\Constraints as CustomAssert;

/**
 * Class RegistrationData
 * @package Homecare\HomecareBundle\Form\model
 *
 */
class RegistrationData
{


    /**
     * @var Agency
     */
    private $agency;


    /**
     * @var User
     */
    private $user;


    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $agencyName;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $phoneNumber;

    /**
     * @var States
     * @Assert\NotBlank()
     */
    private $state;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $firstName;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $lastName;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $password;


    /**
     * @var BillingData
     * @Assert\Valid()
     */
    private $billingData;


    /**
     * @var TermsAccepted
     * @Assert\Valid()
     */
    private $termsAccepted;


    /**
     * @return string
     */
    public function getAgencyName()
    {
        return $this->agencyName;
    }

    /**
     * @param string $agencyName
     */
    public function setAgencyName($agencyName)
    {
        $this->agencyName = $agencyName;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }


    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return States
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param States $state
     */
    public function setState(States $state)
    {
        $this->state = $state;
    }

    /**
     * @return Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * @param Agency $agency
     */
    public function setAgency($agency)
    {
        $this->agency = $agency;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }


    /**
     * @return BillingData
     */
    public function getBillingData()
    {
        return $this->billingData;
    }

    /**
     * @param BillingData $billingData
     */
    public function setBillingData($billingData)
    {
        $this->billingData = $billingData;
    }

    /**
     * @return TermsAccepted
     */
    public function getTermsAccepted()
    {
        return $this->termsAccepted;
    }

    /**
     * @param TermsAccepted $termsAccepted
     */
    public function setTermsAccepted($termsAccepted)
    {
        $this->termsAccepted = $termsAccepted;
    }


}