<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 3/26/17
 * Time: 12:40 PM
 */

namespace Homecare\HomecareBundle\Form\model;

use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class TermsAccepted
 * @package Homecare\HomecareBundle\Form\model
 */
class TermsAccepted
{

    /**
     * @var string
     * @Assert\IsTrue(message="Please accept our terms and conditions")
     */
    private $termsAccepted;


    /**
     * @return string
     */
    public function getTermsAccepted()
    {
        return $this->termsAccepted;
    }

    /**
     * @param string $termsAccepted
     */
    public function setTermsAccepted($termsAccepted)
    {
        $this->termsAccepted = $termsAccepted;
    }

}