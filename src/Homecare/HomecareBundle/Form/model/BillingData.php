<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 3/13/17
 * Time: 3:35 PM
 */

namespace Homecare\HomecareBundle\Form\model;

use Homecare\HomecareBundle\Entity\Agency;
use Homecare\HomecareBundle\Entity\User;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Component\Validator\Constraints as Assert;
use Homecare\HomecareBundle\Validator\Constraints as CustomAssert;
use Symfony\Component\Validator\GroupSequenceProviderInterface;


/**
 * Class BillingData
 * @package Homecare\HomecareBundle\Form\model
 *
 * @CustomAssert\CreditCard(groups={"VALIDATE_CARD_STRIPE"})
 * @CustomAssert\CreditCardUpdate(groups={"VALIDATE_CARD_STRIPE_UPDATE"})
 * @CustomAssert\CreditCardRestart(groups={"VALIDATE_CARD_STRIPE_RESTART"})
 * @Assert\GroupSequenceProvider
 *
 */
class BillingData implements GroupSequenceProviderInterface
{

    const CREATE_BILLING = "CREATE_BILLING";

    const UPDATE_BILLING = "UPDATE_BILLING";

    const RESTART_BILLING = "RESTART_BILLING";


    /**
     * The default group sequence in which the
     * @var array
     */
    private static $groupSequences = array(

        // THE SEQUENCE TO USE WHEN CREATING AN ACCOUNT AND VALIDATING THE CARDS
        self::CREATE_BILLING  => array("BillingData", "VALIDATE_CARD", "VALIDATE_CARD_STRIPE"),
        // THE SEQUENCE TO USE WHEN UPDATING AN ACCOUNT AND VALIDATING THE CARDS
        self::UPDATE_BILLING  => array("BilingData", "VALIDATE_CARD", "VALIDATE_CARD_STRIPE_UPDATE"),
        // SEQUENCE TO USE WHEN RESTARTING AN ACCOUNT AFTER CACNELLING
        self::RESTART_BILLING => array("BilingData", "VALIDATE_CARD", "VALIDATE_CARD_STRIPE_RESTART"),

    );


    /**
     * @var Agency
     */
    private $agency;


    /**
     * @var integer
     * @Assert\NotBlank(groups={"VALIDATE_CARD"})
     * @Assert\Regex(
     *     pattern="/^\d*$/",
     *     message="Card number cannot contain spaces, hyphens, or any other symbols",
     *     groups={"VALIDATE_CARD"}
     * )
     * @Assert\CardScheme(
     *     schemes={"VISA","MASTERCARD","DISCOVER","AMEX"},
     *     message="Your credit card number is invalid.",
     *     groups={"VALIDATE_CARD"}
     * )
     */
    private $creditCardNumber;


    /**
     * @var integer
     * @Assert\NotBlank(groups={"VALIDATE_CARD"})
     * @Assert\Regex(
     *     pattern="/^\d{2}$/",
     *     message="Must be a 2 digit month",
     *     groups={"VALIDATE_CARD"}
     * )
     *
     */
    private $expirationMonth;


    /**
     * @var integer
     * @Assert\NotBlank(groups={"VALIDATE_CARD"})
     * @Assert\Regex(
     *     pattern="/^\d{4}$/",
     *     message="Must be a 4 digit year",
     *     groups={"VALIDATE_CARD"}
     * )
     *
     */
    private $expirationYear;


    /**
     * @var integer
     * @Assert\NotBlank(groups={"VALIDATE_CARD"})
     * @Assert\Regex(
     *     pattern="/^\d*$/",
     *     message="CVV must contain only numbers",
     *     groups={"VALIDATE_CARD"}
     * )
     */
    private $vCode;


    /**
     * @var string
     * @Assert\NotBlank(message="Please select a plan", groups={"VALIDATE_CARD"})
     * @Assert\Choice(choices = {"1", "2", "3"}, message="Please select a plan", groups={"VALIDATE_CARD"})
     */
    private $plan;


    /**
     * @var String
     */
    private $stripeCustomerId;


    /**
     * @var String
     */
    private $stripeSource;


    /**
     * @var String
     */
    private $stripeSubscriptionId;


    /**
     * This billing data class has to have a valid agency object to perform some of the
     * tasks it needs to.
     *
     * BillingData constructor.
     *
     * @param Agency $agency
     */
    public function __construct(Agency $agency)
    {
        $this->agency = $agency;
    }


    /**
     * @return int
     */
    public function getCreditCardNumber()
    {
        return $this->creditCardNumber;
    }

    /**
     * @param int $creditCardNumber
     */
    public function setCreditCardNumber($creditCardNumber)
    {
        $this->creditCardNumber = $creditCardNumber;
    }

    /**
     * @return int
     */
    public function getExpirationMonth()
    {
        return $this->expirationMonth;
    }

    /**
     * @param int $expirationMonth
     */
    public function setExpirationMonth($expirationMonth)
    {
        $this->expirationMonth = $expirationMonth;
    }

    /**
     * @return int
     */
    public function getExpirationYear()
    {
        return $this->expirationYear;
    }

    /**
     * @param int $expirationYear
     */
    public function setExpirationYear($expirationYear)
    {
        $this->expirationYear = $expirationYear;
    }

    /**
     * @return int
     */
    public function getVCode()
    {
        return $this->vCode;
    }

    /**
     * @param int $vCode
     */
    public function setVCode($vCode)
    {
        $this->vCode = $vCode;
    }

    /**
     * @return string
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @param string $plan
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;
    }


    /**
     * @return Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }

    /**
     * @param Agency $agency
     */
    public function setAgency($agency)
    {
        $this->agency = $agency;
    }

    /**
     * @return String
     */
    public function getStripeCustomerId()
    {
        return $this->stripeCustomerId;
    }

    /**
     * @param String $stripeCustomerId
     */
    public function setStripeCustomerId($stripeCustomerId)
    {
        $this->stripeCustomerId = $stripeCustomerId;
    }

    /**
     * @return String
     */
    public function getStripeSource()
    {
        return $this->stripeSource;
    }

    /**
     * @param String $stripeSource
     */
    public function setStripeSource($stripeSource)
    {
        $this->stripeSource = $stripeSource;
    }

    /**
     * @return String
     */
    public function getStripeSubscriptionId()
    {
        return $this->stripeSubscriptionId;
    }

    /**
     * @param String $stripeSubscriptionId
     */
    public function setStripeSubscriptionId($stripeSubscriptionId)
    {
        $this->stripeSubscriptionId = $stripeSubscriptionId;
    }


    /**
     * The following group sequence determines which validation sequence to perform
     * which also handles the saving and updating of the credit card info
     * and the user's subscription
     *
     * @return array
     */
    public function getGroupSequence()
    {

        // if it the users first time creating an account then run the following sequence
        if (null === $this->agency->getStripeSubscriptionId() && null === $this->agency->getStripeSource(
            ) && null === $this->agency->getStripeCustomerId()
        ) {
            return self::$groupSequences[self::CREATE_BILLING];
        }


        // if the user has cancelled their subscription and is (re-upping) then perform the following validation
        if (null === $this->agency->getStripeSubscriptionId() && $this->agency->getStripeSource(
            ) && $this->agency->getStripeCustomerId()
        ) {
            return self::$groupSequences[self::RESTART_BILLING];
        }

        // if the user is updating their subscription then perform the following validation
        if ($this->agency->getStripeSubscriptionId() && $this->agency->getStripeSource(
            ) && $this->agency->getStripeCustomerId()
        ) {
            return self::$groupSequences[self::UPDATE_BILLING];
        }


    }


}