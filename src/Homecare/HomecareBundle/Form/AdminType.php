<?php

namespace Homecare\HomecareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AdminType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('firstName');
        $builder->add('lastName');
        $builder->add('save','submit');
        $builder->add('users', 'collection', array('type' => new User4Type(), 'allow_add' => true, 'by_reference' => false, 'error_bubbling' => false  ));

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Homecare\HomecareBundle\Entity\Admin',
            'validation_groups' => array( 'adminCreateAdmin' )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_homecarebundle_admin';
    }
}