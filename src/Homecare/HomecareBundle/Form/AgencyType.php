<?php

namespace Homecare\HomecareBundle\Form;

use Homecare\HomecareBundle\Entity\Agency;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Homecare\HomecareBundle\Form\Type\CustomTimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;

class AgencyType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('agencyName')
            ->add(
                'weekStartDate',
                ChoiceType::class,
                array(
                    'choices' => Agency::$weekStartDates,
                    'label'   => 'Week Start Day (Changing this value will effect your PCA\'s total hours worked for the past week)',

                )
            )
            ->add(
                'startTime',
                CustomTimeType::class,
                array(
                    'label' => 'Week Start Time (Changing this value will effect your PCA\'s total hours worked for the past week)',
                )
            )
            ->add('phoneNumber')
            ->add(
                'state',
                'entity',
                array(
                    'class'    => 'HomecareHomecareBundle:States',
                    'property' => 'stateName',
                )
            );


        $builder->add('imageFile', 'file', array('label' => 'Agency Logo'));



    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Homecare\HomecareBundle\Entity\Agency',
                'attr'               => ['novalidate' => 'novalidate'],
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_homecarebundle_agency';
    }


    /**
     * @return array
     */
    private function getMonths()
    {
        return array(
            'January'   => '01',
            'February'  => '02',
            'March'     => '03',
            'April'     => '04',
            'May'       => '05',
            'June'      => '06',
            'July'      => '07',
            'August'    => '08',
            'September' => '09',
            'October'   => '10',
            'November'  => '11',
            'December'  => '12',
        );
    }


    /**
     * @return array
     */
    private function getYears()
    {

        $years = array();
        for ($i = 0; $i <= 20; $i++) {

            if ($i == 0) {
                $years[(new \DateTime())->format("Y")] = (new \DateTime())->format("Y");

                continue;
            }


            $years[(new \DateTime())->add(new \DateInterval("P".$i."Y"))->format("Y")] = (new \DateTime())->add(
                new \DateInterval("P".$i."Y")
            )->format("Y");
        }

        return $years;
    }
}
