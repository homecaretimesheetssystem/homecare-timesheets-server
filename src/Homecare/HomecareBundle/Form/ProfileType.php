<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 10/19/16
 * Time: 10:31 AM
 */

namespace Homecare\HomecareBundle\Form;


use Homecare\HomecareBundle\Entity\Qualifications;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class ProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('homeAddress')
            ->add('available')
            ->add('profilePhoto')
            ->add('qualifications')
            ->add('counties')
            ->add('gender')
            ->add('yearsOfExperience');

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'         => 'Homecare\HomecareBundle\Entity\Pca',
                'attr'               => ['novalidate' => 'novalidate'],
                'cascade_validation' => true,
                'csrf_protection'    => false,
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_profile';
    }
}
