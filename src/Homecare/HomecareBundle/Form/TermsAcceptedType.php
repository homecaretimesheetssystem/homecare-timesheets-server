<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 2/19/17
 * Time: 2:17 PM
 */

namespace Homecare\HomecareBundle\Form;


use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\IsTrue;

/**
 * Class TermsAcceptedType
 * @package Homecare\HomecareBundle\Form
 */
class TermsAcceptedType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'termsAccepted',
            CheckboxType::class,
            array(
                'label'    => 'Accept Terms and Agreement',
                'required' => false,
            )
        );


    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Homecare\HomecareBundle\Form\model\TermsAccepted',
                'attr'       => ['novalidate' => 'novalidate'],


            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_terms_accepted';
    }


}