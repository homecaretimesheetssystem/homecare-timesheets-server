<?php

namespace Homecare\HomecareBundle\Form;

use Homecare\HomecareBundle\Entity\Agency;
use Homecare\HomecareBundle\Form\Type\CustomTimeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class Agency2Type extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('agencyName')
            ->add(
                'weekStartDate',
                ChoiceType::class,
                array(
                    'choices' => Agency::$weekStartDates,
                    'label'   => 'Week Start Day (Changing this value will effect your PCA\'s total hours worked for the past week)',

                )
            )
            ->add(
                'startTime',
                CustomTimeType::class,
                array(
                    'label' => 'Week Start Time (Changing this value will effect your PCA\'s total hours worked for the past week)',
                )
            )
            ->add('phoneNumber')
            ->add(
                'state',
                'entity',
                array(
                    'class'    => 'HomecareHomecareBundle:States',
                    'property' => 'stateName',
                )
            );

        $builder->add('imageFile', 'file', array('label' => 'Agency Logo'));

        /*
                $builder->add(
                    'plan',
                    ChoiceType::class,
                    array(
                        'choices'       => array(
                            1 => '1-100 employees',
                            2 => '101-200 employees',
                            3 => 'Over 200 employees',
                        ),
                        'label'         => 'Agency Plan',
                        'property_path' => 'registrationData.plan',
                    )
                );
        */

        /*
        $builder->add(
            'creditCardNumber',
            'text',
            [
                'label'         => 'Credit Card (No hyphens, spaces, or dashes)',
                'attr'          => ['placeholder' => '4444555566667777'],
                'property_path' => 'registrationData.creditCardNumber',

            ]
        );


        $builder->add(
            'expiration_date',
            'date',
            [
                'format'        => 'MMM-yyy d',
                'years'         => range(date('Y'), date('Y') + 12),
                'days'          => array(1),
                'empty_value'   => array('year' => 'Select Year', 'month' => 'Select Month', 'day' => false),
                'label'         => 'Expiration Date',
                'property_path' => 'registrationData.expirationDate',
            ]
        );


        $builder->add(
            'vCode',
            'text',
            [
                'label'         => 'CVV (Code on back of card)',
                'property_path' => 'registrationData.vCode',

            ]
        );
*/

        $builder->add('save', 'submit');

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {

        /*
        $resolver->setDefaults(
            array(
                'data_class' => 'Homecare\HomecareBundle\Entity\Agency',
            )
        );
        */


        $resolver->setDefaults(
            array(
                'data_class' => 'Homecare\HomecareBundle\Entity\Agency',
                /*
                'validation_groups' => function (FormInterface $form) {

                    $agency = $form->getData();
                    // if a credit card number is attached to the form then enable validation
                    if ($agency->getRegistrationData()->getCreditCardNumber()) {
                        return array('VALIDATE_CARD');
                    } else {
                        return array();
                    }

                },
                */
            )
        );


    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_homecarebundle_agency2';
    }
}
