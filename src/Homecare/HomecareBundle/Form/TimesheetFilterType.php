<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 3/23/16
 * Time: 4:49 PM
 */

namespace Homecare\HomecareBundle\Form;

use Ambta\DoctrineEncryptBundle\Encryptors\Rijndael256Encryptor;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderExecuterInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;


class TimesheetFilterType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder->add(
            'dateFilter',
            Filters\ChoiceFilterType::class,
            array(
                'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {

                    $encryptor = new Rijndael256Encryptor('ABCDEFGHIJKLMNOPQRSTUVWXZY123456');

                    if (empty($values['value'])) {
                        return null;
                    }

                    //$values['value'] = $encryptor->encrypt($values['value']);


                    //$paramName = sprintf('p_%s', str_replace('.', '_', $field));

                    // expression that represent the condition
                    //$expression = $filterQuery->getExpr()->eq($field, ':'.$paramName);

                    // expression parameters
                    //$parameters = array($paramName => $values['value']); // [ name => value ]
                    // or if you need to define the parameter's type
                    // $parameters = array($paramName => array($values['value'], \PDO::PARAM_STR)); // [ name => [value, type] ]

                    $filterQuery->getQueryBuilder()
                                ->andWhere('sd.dateEnding >= :lastMonth')
                                ->setParameter('lastMonth', new \DateTime($values['value'] . ' DAYS AGO'));

                    //return $filterQuery->createCondition($expression, $parameters);
                },
                'expanded'     => true,
                'multiple'     => false,
                'label'        => 'Choose Date',
                'choices'      => array(
                    '7'   => '7 DAYS',
                    '14'  => '14 DAYS',
                    '30'  => '30 DAYS',
                    '60'  => '60 DAYS',
                    '90'  => '90 DAYS',
                    '180' => '180 DAYS',
                    '365' => '365 DAYS',
                ),

            )
        );


        $builder->add(
            'sessionData',
            Filters\CollectionAdapterFilterType::class,
            array(
                'entry_type' => new SessionDataFilterType(),
                'label'      => false,
                'add_shared' => function (FilterBuilderExecuterInterface $qbe) {

                    $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                        // add the join clause to the doctrine query builder
                        // the where clause for the label and color fields will be added automatically with the right alias later by the Lexik\Filter\QueryBuilderUpdater
                        $filterBuilder->join($alias.'.sessionData', $joinAlias);
                        //$filterBuilder->join($alias . '.options', $joinAlias);
                    };

                    // then use the query builder executor to define the join and its alias.
                    $qbe->addOnce($qbe->getAlias().'.sessionData', 'sd', $closure);
                },
            )
        );


        $builder->add(
            'recipient',
            Filters\CollectionAdapterFilterType::class,
            array(
                'entry_type' => new RecipientFilterType(),
                'label'      => false,
                'add_shared' => function (FilterBuilderExecuterInterface $qbe) {

                    $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                        // add the join clause to the doctrine query builder
                        // the where clause for the label and color fields will be added automatically with the right alias later by the Lexik\Filter\QueryBuilderUpdater
                        $filterBuilder->join($alias.'.recipient', $joinAlias);
                        //$filterBuilder->join($alias . '.options', $joinAlias);
                    };

                    // then use the query builder executor to define the join and its alias.
                    $qbe->addOnce($qbe->getAlias().'.recipient', 'r', $closure);
                },
            )
        );



        $builder->add(
            'verification',
            Filters\CheckboxFilterType::class,
            array(
                'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {

                    $encryptor = new Rijndael256Encryptor('ABCDEFGHIJKLMNOPQRSTUVWXZY123456');

                    if (empty($values['value'])) {
                        return null;
                    }

                    $filterQuery->getQueryBuilder()->andWhere('t.verification IS NOT NULL');

                },
            )
        );

        $builder->add('archived', Filters\BooleanFilterType::class);






    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'csrf_protection' => false,
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'timesheet_filter';
    }

}