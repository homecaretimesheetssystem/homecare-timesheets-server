<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 3/24/16
 * Time: 12:22 PM
 */

namespace Homecare\HomecareBundle\Form;



use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderExecuterInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Ambta\DoctrineEncryptBundle\Encryptors\Rijndael256Encryptor;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;

class SessionDataFilterType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'dateEnding',
            Filters\DateRangeFilterType::class, array(
            'label' => 'Date Range'
        ));


        $builder->add('pca', Filters\CollectionAdapterFilterType::class, array(
            'entry_type'       => new PcaFilterType(),
            'label' => false,
            'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {

                $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                    // add the join clause to the doctrine query builder
                    // the where clause for the label and color fields will be added automatically with the right alias later by the Lexik\Filter\QueryBuilderUpdater
                    $filterBuilder->join($alias . '.pca', $joinAlias);
                };

                // then use the query builder executor to define the join and its alias.
                $qbe->addOnce($qbe->getAlias().'.pca', 'p', $closure);
            },
        ));

        $builder->add('service', Filters\CollectionAdapterFilterType::class, array(
            'entry_type'       => new ServiceFilterType(),
            'label' => false,
            'add_shared' => function (FilterBuilderExecuterInterface $qbe)  {

                $closure = function (QueryBuilder $filterBuilder, $alias, $joinAlias, Expr $expr) {
                    // add the join clause to the doctrine query builder
                    // the where clause for the label and color fields will be added automatically with the right alias later by the Lexik\Filter\QueryBuilderUpdater
                    $filterBuilder->join($alias . '.service', $joinAlias);
                };

                // then use the query builder executor to define the join and its alias.
                $qbe->addOnce($qbe->getAlias().'.service', 's', $closure);
            },
        ));




    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'session_data_filter';
    }

}