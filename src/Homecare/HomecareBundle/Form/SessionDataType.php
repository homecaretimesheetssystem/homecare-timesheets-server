<?php

namespace Homecare\HomecareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SessionDataType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('continueTimesheetNumber')
            ->add('flagForFurtherInvestigation')
            ->add('currentTimesheetId')
            ->add('currentTimesheetNumber')
            ->add('pca')
            ->add('service')
            ->add('timeIn')
            ->add('timeOut')
            ->add('dateEnding','datetime', array(
                'widget' => 'single_text',
                'date_format' => 'yyyy-MM-dd',
            ))
            ->add('ratio')
            ->add('sharedCareLocation')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Homecare\HomecareBundle\Entity\SessionData',
            'csrf_protection'   => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_homecarebundle_sessiondata';
    }
}
