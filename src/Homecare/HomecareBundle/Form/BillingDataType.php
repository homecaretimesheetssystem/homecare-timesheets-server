<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 2/19/17
 * Time: 2:17 PM
 */

namespace Homecare\HomecareBundle\Form;


use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class BillingDataType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'plan',
            ChoiceType::class,
            array(
                'choices' => array(
                    0 => 'Subscription Not Activated',
                    1 => '1-100 employees',
                    2 => '101-200 employees',
                    3 => 'Over 200 employees',
                ),
                'label'   => 'Agency Plan',
            )
        );


        $builder->add(
            'creditCardNumber',
            'text',
            [
                'label' => 'Credit Card (No hyphens, spaces, or dashes)',
                'attr'  => ['placeholder' => '4444555566667777'],

            ]
        );


        $builder->add(
            'expirationMonth',
            ChoiceType::class,
            array(
                'choices'           => $this->getMonths(),
                // *this line is important*
                'choices_as_values' => true,
                'property_path'     => 'expirationMonth',
            )
        );

        $builder->add(
            'expirationYear',
            ChoiceType::class,
            array(
                'choices'           => $this->getYears(),
                // *this line is important*
                'choices_as_values' => true,
                'property_path'     => 'expirationYear',
            )
        );


        $builder->add(
            'vCode',
            'text',
            [
                'label' => 'CVV (Code on back of card)',

            ]
        );


    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Homecare\HomecareBundle\Form\model\BillingData',
                'attr'       => ['novalidate' => 'novalidate'],


            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_billing_data';
    }


    /**
     * @return array
     */
    private function getMonths()
    {
        return array(
            'January'   => '01',
            'February'  => '02',
            'March'     => '03',
            'April'     => '04',
            'May'       => '05',
            'June'      => '06',
            'July'      => '07',
            'August'    => '08',
            'September' => '09',
            'October'   => '10',
            'November'  => '11',
            'December'  => '12',
        );
    }


    /**
     * @return array
     */
    private function getYears()
    {

        $years = array();
        for ($i = 0; $i <= 20; $i++) {

            if ($i == 0) {
                $years[(new \DateTime())->format("Y")] = (new \DateTime())->format("Y");

                continue;
            }


            $years[(new \DateTime())->add(new \DateInterval("P".$i."Y"))->format("Y")] = (new \DateTime())->add(
                new \DateInterval("P".$i."Y")
            )->format("Y");
        }

        return $years;
    }

}