<?php

namespace Homecare\HomecareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Homecare\HomecareBundle\Entity\Recipient;
use Homecare\HomecareBundle\Entity\Pca;
use Homecare\HomecareBundle\Entity\User;

class User2Type extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('username')
            ->add('email', 'email')
            ->add('password', 'password')
            ->add('agency', 'entity', array( 'class' => 'HomecareHomecareBundle:Agency', 'property' => 'agencyName', 'placeholder' => 'N/A' ) )
            ->add('save','submit')
        ;
				
						
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Homecare\HomecareBundle\Entity\User',
            'validation_groups' => array( 'adminCreateAdmin', 'adminCreateAgency' )
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_homecarebundle_user2';
    }
}