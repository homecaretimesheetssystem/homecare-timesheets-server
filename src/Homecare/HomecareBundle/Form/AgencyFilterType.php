<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 3/24/16
 * Time: 1:43 PM
 */

namespace Homecare\HomecareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type as Filters;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderExecuterInterface;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Ambta\DoctrineEncryptBundle\Encryptors\Rijndael256Encryptor;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;


class AgencyFilterType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('agencyName', Filters\TextFilterType::class, array(
            'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {

                $encryptor = new Rijndael256Encryptor( 'ABCDEFGHIJKLMNOPQRSTUVWXZY123456' );

                if (empty($values['value'])) {
                    return null;
                }

                $values['value'] = $encryptor->encrypt(strtolower($values['value']));

                $paramName = sprintf('p_%s', str_replace('.', '_', $field));

                // expression that represent the condition
                $expression = $filterQuery->getExpr()->eq($field, ':'.$paramName);

                // expression parameters
                $parameters = array($paramName => $values['value']); // [ name => value ]
                // or if you need to define the parameter's type
                // $parameters = array($paramName => array($values['value'], \PDO::PARAM_STR)); // [ name => [value, type] ]

                return $filterQuery->createCondition($expression, $parameters);
            },
            'attr' => array(
                'placeholder' => 'Exact match required',
            ),
        ));

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'agency_filter';
    }



}