<?php

namespace Homecare\HomecareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class User4Type extends AbstractType
{
	
	

	
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', array('error_bubbling' => false, 'attr'=> array('autocomplete'=>'off')))
            ->add('email', 'email', array('error_bubbling' => false, 'attr'=> array('autocomplete'=>'off')))
            ->add('password', 'password', array('error_bubbling' => false, 'attr'=> array('autocomplete'=>'off')))
							
					;
				
			
			}
			
			
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Homecare\HomecareBundle\Entity\User',
            'validation_groups' => array( 'adminCreatePca', 'adminCreateRecipient', 'agencyCreatePca', 'agencyCreateRecipient', 'adminCreateAdmin' ),

        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_homecarebundle_user4';
    }
}