<?php

namespace Homecare\HomecareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;
use Homecare\HomecareBundle\Entity\User;

class RecipientPcaType extends AbstractType
{



    protected $user;

    public function __construct ( User $user )
    {
        $this->user = $user;
    }



    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /*
        $builder->add('recipient', 'entity', array(
            'class' => 'HomecareHomecareBundle:Recipient',
            'property' => 'recipientName',
        ));
*/

        /*
        $builder->add('pca', 'entity', array(
            'class' => 'HomecareHomecareBundle:Pca',
            'property' => 'firstName',
        ));
        */
        $builder->add('recipient', 'entity', array(
            'class' => 'HomecareHomecareBundle:Recipient',
            'property' => 'uniqueName',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('r')
                    ->innerJoin('r.agency', 'a')
                    ->innerJoin('r.user', 'u', 'WITH', 'u.archived = 0')
                    ->where('a = :agency')
                    ->setParameter('agency', $this->user->getAgency() );
            },
        ));


        $builder->add('pca', 'entity', array(
            'label' => 'Worker',
            'class' => 'HomecareHomecareBundle:Pca',
            'property' => 'uniqueName',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('p')
                    ->innerJoin('p.agency', 'a')
                    ->innerJoin('p.user', 'u', 'WITH', 'u.archived = 0')
                    ->where('a = :agency')
                    ->setParameter('agency', $this->user->getAgency() );
            },
        ));



        $builder->add('save', 'submit');

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Homecare\HomecareBundle\Entity\RecipientPca',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_homecarebundle_recipient_pca';
    }
}
