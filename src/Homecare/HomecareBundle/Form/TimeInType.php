<?php

namespace Homecare\HomecareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TimeInType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('timeIn','datetime', array(
                'widget' => 'single_text',
                'date_format' => 'yyyy-MM-dd',
            ))
							
            ->add('estimatedShiftLength')
            ->add('timeInAddress')
            ->add('latitude')
            ->add('longitude')
            ->add('timeInPictureTime','datetime', array(
                'widget' => 'single_text',
                'date_format' => 'yyyy-MM-dd',
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Homecare\HomecareBundle\Entity\TimeIn',
						'csrf_protection'   => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_homecarebundle_timein';
    }
}
