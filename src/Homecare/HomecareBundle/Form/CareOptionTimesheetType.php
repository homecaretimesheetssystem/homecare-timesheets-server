<?php

namespace Homecare\HomecareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CareOptionTimesheetType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
			 $builder->add('timesheet', 'entity', array(
				    'class' => 'HomecareHomecareBundle:Timesheet',
				    'property' => 'id',
						'expanded' => true,
						'label' => false,
						'by_reference' => false,
					
				));
					
				$builder->add('careOption', 'entity', array(
				    'class' => 'HomecareHomecareBundle:CareOptions',
				    'property' => 'careOption',
						'expanded' => true,
						'label' => false,
						'by_reference' => false,
					
				));
				
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Homecare\HomecareBundle\Entity\CareOptionTimesheet',
						'csrf_protection'   => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_homecarebundle_careoptiontimesheet';
    }
}
