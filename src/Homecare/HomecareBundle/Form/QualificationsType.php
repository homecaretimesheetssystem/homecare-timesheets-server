<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 10/19/16
 * Time: 1:58 PM
 */

namespace Homecare\HomecareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QualificationsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('qualification');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Homecare\HomecareBundle\Entity\Qualifications',
            )
        );
    }
}