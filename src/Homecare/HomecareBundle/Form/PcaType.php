<?php

namespace Homecare\HomecareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;


/**
 * Class PcaType
 * @package Homecare\HomecareBundle\Form
 */
class PcaType extends AbstractType
{


    private $authorizationChecker;

    public function __construct(AuthorizationChecker $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('umpi')
            ->add('companyAssignedId', 'text', array('label' => 'Company Assigned ID'))
            ->add('phoneNumber')
            ->add('yearsOfExperience', IntegerType::class)
            ->add('firstName')
            ->add('lastName');

        $builder->add(
            'gender',
            ChoiceType::class,
            array(
                'choices' => array(
                    1 => 'Female',
                    2 => 'Male',
                ),
            )
        );


        $builder->add(
            'username',
            'text',
            [
                'label'         => 'Username',
                'property_path' => 'user.username',
            ]
        );


        $builder->add(
            'email',
            'email',
            [
                'label'         => 'Email',
                'property_path' => 'user.email',
            ]
        );


        $builder->add(
            'password',
            'password',
            [
                'label'         => 'Password',
                'property_path' => 'user.password',
            ]
        );


        $builder->add(
            'services',
            'entity',
            array(
                'class'    => 'Homecare\HomecareBundle\Entity\Services',
                'property' => 'serviceName',
                'multiple' => true,
                'expanded' => true,
            )
        );


        // if an admin user is logged in then add the agency field
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {

            $builder->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'addExtraFields']);

        }

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Homecare\HomecareBundle\Entity\Pca',
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_pca';
    }


    /**
     * this method adds any dynamic fields to the form
     *
     * @param FormEvent $event
     */
    public function addExtraFields(FormEvent $event)
    {

        $form = $event->getForm();

        $form->add(
            'agency',
            'entity',
            array('class' => 'HomecareHomecareBundle:Agency', 'property' => 'agencyName')
        );

    }

}
