<?php

namespace Homecare\HomecareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PcaSignatureType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           
    				->add('pcaSigTime','datetime', array(
        					'widget' => 'single_text',
        					'date_format' => 'yyyy-MM-dd',
    						))
            ->add('pcaSigAddress')
            ->add('latitude')
            ->add('longitude')
		    		->add('pcaPhotoTime','datetime', array(
		        			'widget' => 'single_text',
		        			'date_format' => 'yyyy-MM-dd',
		    				))
            ->add('timesheet')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Homecare\HomecareBundle\Entity\PcaSignature',
						'csrf_protection'   => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_homecarebundle_pcasignature';
    }
}
