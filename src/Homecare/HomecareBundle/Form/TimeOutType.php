<?php

namespace Homecare\HomecareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TimeOutType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('timeOut','datetime', array(
            'widget' => 'single_text',
            'date_format' => 'yyyy-MM-dd',
        ))
            ->add('timeOutAddress')
            ->add('latitude')
            ->add('longitude')
        ->add('timeOutPictureTime','datetime', array(
            'widget' => 'single_text',
            'date_format' => 'yyyy-MM-dd',
        ))
            ->add('totalHours')
            ->add('billableHours')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Homecare\HomecareBundle\Entity\TimeOut',
						'csrf_protection'   => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_homecarebundle_timeout';
    }
}
