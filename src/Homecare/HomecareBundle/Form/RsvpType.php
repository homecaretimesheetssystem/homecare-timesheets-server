<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 10/19/16
 * Time: 10:31 AM
 */

namespace Homecare\HomecareBundle\Form;


use Homecare\HomecareBundle\Entity\Qualifications;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class RsvpType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('pca')
            ->add(
                'eta',
                'datetime',
                array(
                    'widget'      => 'single_text',
                    'date_format' => 'yyyy-MM-dd',
                )
            )
            ->add('accepted')
            ->add('declined')
            ->add('cancellationReason')
            ->add('request');

    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'         => 'Homecare\HomecareBundle\Entity\Rsvp',
                'attr'               => ['novalidate' => 'novalidate'],
                //'cascade_validation' => true,
                'csrf_protection'    => false,
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_rsvp';
    }
}
