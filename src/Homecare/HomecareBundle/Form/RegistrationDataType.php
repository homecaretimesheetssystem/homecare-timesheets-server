<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 2/19/17
 * Time: 2:17 PM
 */

namespace Homecare\HomecareBundle\Form;


use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\IsTrue;

class RegistrationDataType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder->add(
            'agencyName',
            'text',
            [
                'label'         => 'Agency Name',
                'property_path' => 'agencyName',
            ]
        );

        $builder->add(
            'phoneNumber',
            'text',
            [
                'label'         => 'Phone Number',
                'property_path' => 'phoneNumber',
            ]
        );


        $builder->add(
            'state',
            'entity',
            array(
                'class'         => 'HomecareHomecareBundle:States',
                'label'         => 'State',
                'property_path' => 'state',
            )
        );

        $builder->add(
            'firstName',
            'text',
            [
                'label'         => 'First Name',
                'property_path' => 'firstName',
            ]
        );


        $builder->add(
            'lastName',
            'text',
            [
                'label'         => 'Last Name',
                'property_path' => 'lastName',
            ]
        );

        $builder->add(
            'username',
            'text',
            [
                'label'         => 'Username',
                'property_path' => 'username',
            ]
        );

        $builder->add(
            'email',
            'email',
            [
                'label'         => 'Email',
                'property_path' => 'email',
            ]
        );


        $builder->add(
            'password',
            RepeatedType::class,
            array(
                'type'            => PasswordType::class,
                'invalid_message' => 'The password fields must match.',
                'options'         => array('attr' => array('class' => 'password-field')),
                'required'        => true,
                'first_options'   => array('label' => 'Password'),
                'second_options'  => array('label' => 'Repeat Password'),
                'property_path'   => 'password',
            )
        );


        $builder->add('billingData', BillingDataType::class);

        $builder->add('termsAccepted', TermsAcceptedType::class);


    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Homecare\HomecareBundle\Form\model\RegistrationData',
                'attr'       => ['novalidate' => 'novalidate'],


            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_registration_data';
    }


    /**
     * @return array
     */
    private function getMonths()
    {
        return array(
            'January'   => '01',
            'February'  => '02',
            'March'     => '03',
            'April'     => '04',
            'May'       => '05',
            'June'      => '06',
            'July'      => '07',
            'August'    => '08',
            'September' => '09',
            'October'   => '10',
            'November'  => '11',
            'December'  => '12',
        );
    }


    /**
     * @return array
     */
    private function getYears()
    {

        $years = array();
        for ($i = 0; $i <= 20; $i++) {

            if ($i == 0) {
                $years[(new \DateTime())->format("Y")] = (new \DateTime())->format("Y");

                continue;
            }


            $years[(new \DateTime())->add(new \DateInterval("P".$i."Y"))->format("Y")] = (new \DateTime())->add(
                new \DateInterval("P".$i."Y")
            )->format("Y");
        }

        return $years;
    }

}