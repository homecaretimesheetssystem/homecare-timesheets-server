<?php

namespace Homecare\HomecareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RecipientSignatureType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           
        		->add('recipientSigTime','datetime', array(
            			'widget' => 'single_text',
            			'date_format' => 'yyyy-MM-dd',
        				))
            ->add('recipientSigAddress')
            ->add('latitude')
            ->add('longitude')
        		->add('recipientPhotoTime','datetime', array(
            			'widget' => 'single_text',
            			'date_format' => 'yyyy-MM-dd',
        				))
            ->add('timesheet')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Homecare\HomecareBundle\Entity\RecipientSignature',
						'csrf_protection'   => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_homecarebundle_recipientsignature';
    }
}
