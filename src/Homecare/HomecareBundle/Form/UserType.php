<?php

namespace Homecare\HomecareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class UserType extends AbstractType
{


    private $authorizationChecker;

    public function __construct(AuthorizationChecker $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('username')
            ->add('email', 'email')
            ->add('password', 'password');


        // if an admin user is logged in then add the agency field
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {

            $builder->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'addExtraFields']);

        }


    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'Homecare\HomecareBundle\Entity\User',
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_homecarebundle_user';
    }


    /**
     * this method adds any dynamic fields to the form
     *
     * @param FormEvent $event
     */
    public function addExtraFields(FormEvent $event)
    {

        $form = $event->getForm();

        $form->add(
            'agency',
            'entity',
            array('class' => 'HomecareHomecareBundle:Agency', 'property' => 'agencyName')
        );

    }
}
