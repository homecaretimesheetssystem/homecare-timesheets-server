<?php

namespace Homecare\HomecareBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class User3Type extends AbstractType
{




    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', 'text', array('error_bubbling' => false))
            ->add('email', 'email', array('error_bubbling' => false))
            ->add('password', 'password', array('error_bubbling' => false))

        ;


    }



    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Homecare\HomecareBundle\Entity\User',
            'validation_groups' => array( 'adminCreateRecipient' ),

        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_homecarebundle_user3';
    }
}