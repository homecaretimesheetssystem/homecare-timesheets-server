<?php

namespace Homecare\HomecareBundle\Form;


use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class RecipientType extends AbstractType
{


    private $authorizationChecker;

    public function __construct(AuthorizationChecker $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }


    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('maNumber')
            ->add(
                'genderPreference',
                ChoiceType::class,
                array(
                    'choices' => array(
                        0 => 'No Preference',
                        1 => 'Female',
                        2 => 'Male',
                    ),
                )
            )
            ->add('companyAssignedId', 'text', array('label' => 'Company Assigned ID'))
            ->add(
                'skipVerification',
                CheckboxType::class,
                array(
                    'label'    => 'Turn Off Verification Photo',
                    'required' => false,
                )
            )
            ->add(
                'skipGps',
                CheckboxType::class,
                array(
                    'label'    => 'Turn Off GPS',
                    'required' => false,
                )
            );


        $builder->add(
            'username',
            'text',
            [
                'label'         => 'Username',
                'property_path' => 'user.username',
            ]
        );


        $builder->add(
            'email',
            'email',
            [
                'label'         => 'Email',
                'property_path' => 'user.email',
            ]
        );


        $builder->add(
            'password',
            'password',
            [
                'label'         => 'Password',
                'property_path' => 'user.password',
            ]
        );


        $builder->add(
            'county',
            'entity',
            array(
                'class'    => 'HomecareHomecareBundle:County',
                'property' => 'name',
            )
        );


        // if a recipient is logged in (on the app) then allow them to upload their county
        if ($this->authorizationChecker->isGranted('ROLE_RECIPIENT')) {

            $builder->add('homeAddress');

        }


        // if an admin user is logged in then add the agency field
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {

            $builder->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'addExtraFields']);

        }


    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'        => 'Homecare\HomecareBundle\Entity\Recipient',
                'validation_groups' => array('agencyCreateRecipient'),
                'csrf_protection'   => false,
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_recipient';
    }


    /**
     * this method adds any dynamic fields to the form
     *
     * @param FormEvent $event
     */
    public function addExtraFields(FormEvent $event)
    {

        $form = $event->getForm();

        $form->add(
            'agency',
            'entity',
            array(
                'class'    => 'HomecareHomecareBundle:Agency',
                'property' => 'agencyName',
            )
        );

    }
}
