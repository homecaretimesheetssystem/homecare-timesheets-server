<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 10/19/16
 * Time: 10:16 AM
 */

namespace Homecare\HomecareBundle\Form;


use AppBundle\Entity\Image;
use AppBundle\Validator\Constraints\Image as ImageConstraint;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * ImageType
 *
 * @author Josh Crawmer <joshcrawmer4@gmail.com>
 */
class ImageType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add(
            'file',
            'file',
            [
                'label' => 'New Image',
            ]
        );

    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class'  => 'Homecare\HomecareBundle\Entity\Image',
                'attr'        => ['novalidate' => 'novalidate'],
                'is_required' => true,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'homecare_image';
    }


}
