<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 2/20/17
 * Time: 7:19 PM
 */

namespace Homecare\HomecareBundle\Form;


use Homecare\HomecareBundle\Entity\Qualifications;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class DeviceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('user')
            ->add('isAndroid')
            ->add('isIos')
            ->add('isActive')
            ->add('deviceToken');
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class'         => 'Homecare\HomecareBundle\Entity\Device',
                'attr'               => ['novalidate' => 'novalidate'],
                'cascade_validation' => true,
                'csrf_protection'    => false,
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'homecare_device';
    }
}
