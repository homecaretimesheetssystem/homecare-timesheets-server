<?php

namespace Homecare\HomecareBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class HomecareHomecareBundle extends Bundle
{
	
	
	public function getParent()
	    {
	        return 'HomecareHomecareApiBundle';
	    }
	
	
}
