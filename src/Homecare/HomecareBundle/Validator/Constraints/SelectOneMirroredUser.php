<?php
namespace Homecare\HomecareBundle\Validator\Constraints;
	

use Symfony\Component\Validator\Constraint;

class SelectOneMirroredUser extends Constraint {
	

public $message = 'You can can only select One role to assign to a user!';


public $emptyMessage = 'You must select a role to assign to the user';


//pass in the acutal class for validating
public function getTargets()
{
    return self::CLASS_CONSTRAINT;
}

	
}	
	
	
?>