<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 3/12/17
 * Time: 9:47 PM
 */

namespace Homecare\HomecareBundle\Validator\Constraints;

use FOS\OAuthServerBundle\Propel\Token;
use Homecare\HomecareBundle\Data\StripeAdapter;
use Homecare\HomecareBundle\Entity\Agency;
use Homecare\HomecareBundle\Entity\User;
use Homecare\HomecareBundle\Form\model\BillingData;
use Homecare\HomecareBundle\Form\model\RegistrationData;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


class CreditCardValidator extends ConstraintValidator
{

    /**
     * the default path to throw the error at
     */
    const DEFAULT_ERROR_PATH = "creditCardNumber";

    /**
     * @var StripeAdapter
     */
    private $stripeAdapter;


    private $tokenStorage;

    private $authorizationChecker;


    /**
     * CreditCardValidator constructor.
     *
     * @param StripeAdapter $stripeAdapter
     * @param $tokenStorage
     * @param $authorizationChecker
     */
    public function __construct(
        StripeAdapter $stripeAdapter,
        TokenStorage $tokenStorage,
        AuthorizationChecker $authorizationChecker
    ) {
        $this->stripeAdapter        = $stripeAdapter;
        $this->tokenStorage         = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
    }


    /**
     * @param mixed $protocol
     * @param Constraint $constraint
     */
    public function validate($protocol, Constraint $constraint)
    {


        // create the customer and setup the credit card on the customer
        try {
            $customer = $this->stripeAdapter->createCustomer(
                array(
                    'source' => array(
                        'object'    => 'card',
                        'exp_month' => $protocol->getExpirationMonth(),
                        'exp_year'  => $protocol->getExpirationYear(),
                        'number'    => $protocol->getCreditCardNumber(),
                        'cvc'       => $protocol->getVCode(),
                    ),
                )
            );
            // store some stripe data on the agency
            $protocol->getAgency()->setStripeCustomerId($customer->id);
            $protocol->getAgency()->setStripeSource($customer->default_source);

        } catch (\Exception $e) {

            $this->attachError($e, $constraint);
        }


        // if a customer was created properly
        if (isset($customer)) {

            try {
                $subscription = $this->stripeAdapter->createSubscription(
                    $customer,
                    $protocol->getPlan()
                );
                $protocol->getAgency()->setStripeSubscriptionId($subscription->id);
                $protocol->getAgency()->setPlan($protocol->getPlan());
            } catch (\Exception $e) {

                $this->attachError($e, $constraint);

            }

        }

    }


    /**
     * Attaches the error message for the stripe billing to the appropriate spot
     * on the form
     *
     * @param \Exception $e
     * @param Constraint $constraint
     */
    private function attachError(\Exception $e, Constraint $constraint)
    {
        $path = isset($constraint->stripeErrorMessages[$e->getMessage(
            )]) ? $constraint->stripeErrorMessages[$e->getMessage()] : self::DEFAULT_ERROR_PATH;

        $this->context->buildViolation($e->getMessage())
                      ->atPath($path)
                      ->addViolation();
    }


}