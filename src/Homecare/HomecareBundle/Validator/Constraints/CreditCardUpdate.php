<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 3/26/17
 * Time: 5:16 PM
 */

namespace Homecare\HomecareBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class CreditCardUpdate
 * @package Homecare\HomecareBundle\Validator\Constraints
 *
 * @Annotation
 *
 */
class CreditCardUpdate extends Constraint
{

    /**
     * A map of stripe error messages and the field that they should render on
     * This will need to be updated if any of the model property names change or the generic
     * stipe error messages change
     *
     * @var array
     */
    public $stripeErrorMessages = [
        'Your card\'s security code is incorrect.'                                 => 'vCode',
        'Your card was declined.'                                                  => 'creditCardNumber',
        'Your card has expired.'                                                   => 'expirationYear',
        'Your card\'s expiration month is invalid.'                                => 'expirationMonth',
        'Your card\'s expiration year is invalid.'                                 => 'expirationYear',
        'An error occurred while processing your card. Try again in a little bit.' => 'creditCardNumber',

    ];


    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

}