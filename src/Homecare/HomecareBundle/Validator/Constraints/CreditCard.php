<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 3/12/17
 * Time: 9:47 PM
 */

namespace Homecare\HomecareBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


/**
 * @Annotation
 */
class CreditCard extends Constraint
{
    /**
     * A map of stripe error messages and the field that they should render on
     * This will need to be updated if any of the model property names change or the generic
     * stipe error messages change
     *
     * @var array
     */
    public $stripeErrorMessages = [
        'Your card\'s security code is incorrect.' => 'vCode',
        'Your card was declined.' => 'creditCardNumber',
        'Your card has expired.' => 'expirationYear',
        'Your card\'s expiration month is invalid.' => 'expirationMonth',
        'Your card\'s expiration year is invalid.' => 'expirationYear',
        'An error occurred while processing your card. Try again in a little bit.' => 'creditCardNumber',

    ];


    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

}