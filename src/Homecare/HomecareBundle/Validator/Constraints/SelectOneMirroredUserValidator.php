<?php
namespace Homecare\HomecareBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


class SelectOneMirroredUserValidator extends ConstraintValidator
{
    public function validate($protocol, Constraint $constraint)
    {
			
			//if the recipient is not empty
        if ( !empty( $protocol->getRecipient() ) ) {
					//make sure there is nothing in the agency or admin otherwise create a validation
					if ( !empty( $protocol->getAgency() ) || !empty( $protocol->getAdmin() ) || !empty( $protocol->getPca() ) ) {
						// If you're using the new 2.5 validation API (you probably are!)
            $this->context->buildViolation($constraint->message)
                ->atPath('recipient')
                ->addViolation();
					}
				} elseif ( !empty( $protocol->getAgency() ) ) {
					//make sure there is nothing in the agency or admin otherwise create a validation
					if ( !empty( $protocol->getRecipient() ) || !empty( $protocol->getAdmin() ) || !empty( $protocol->getPca() ) ) {
						// If you're using the new 2.5 validation API (you probably are!)
            $this->context->buildViolation($constraint->message)
                ->atPath('recipient')
                ->addViolation();
					}
				} elseif ( !empty( $protocol->getAdmin() ) ) {
					//make sure there is nothing in the agency or admin otherwise create a validation
					if ( !empty( $protocol->getRecipient() ) || !empty( $protocol->getAgency() ) || !empty( $protocol->getPca() ) ) {
						// If you're using the new 2.5 validation API (you probably are!)
            $this->context->buildViolation($constraint->message)
                ->atPath('recipient')
                ->addViolation();
					}
				} elseif ( !empty( $protocol->getPca() ) ) {
					//make sure there is nothing in the agency or admin otherwise create a validation
					if ( !empty( $protocol->getRecipient() ) || !empty( $protocol->getAgency() ) || !empty( $protocol->getAdmin() ) ) {
						// If you're using the new 2.5 validation API (you probably are!)
            $this->context->buildViolation($constraint->message)
                ->atPath('recipient')
                ->addViolation();
					}
				} else {
					//no user was selected so show empty error message
          $this->context->buildViolation($constraint->emptyMessage)
              ->atPath('recipient')
              ->addViolation();
				}
				
				
						
				
    }
}



?>