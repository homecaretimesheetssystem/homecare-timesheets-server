<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 3/26/17
 * Time: 6:44 PM
 */

namespace Homecare\HomecareBundle\Validator\Constraints;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use FOS\OAuthServerBundle\Propel\Token;
use Homecare\HomecareBundle\Data\StripeAdapter;
use Homecare\HomecareBundle\Entity\Agency;
use Homecare\HomecareBundle\Entity\User;
use Homecare\HomecareBundle\Form\model\BillingData;
use Homecare\HomecareBundle\Form\model\RegistrationData;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


class CreditCardRestartValidator extends ConstraintValidator
{

    /**
     * the default path to throw the error at
     */
    const DEFAULT_ERROR_PATH = "creditCardNumber";

    /**
     * @var StripeAdapter
     */
    private $stripeAdapter;


    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * @var EntityManager
     */
    private $entityManager;


    /**
     * CreditCardValidator constructor.
     *
     * @param StripeAdapter $stripeAdapter
     * @param TokenStorage $tokenStorage
     * @param AuthorizationChecker $authorizationChecker
     * @param EntityManager $entityManager
     */
    public function __construct(
        StripeAdapter $stripeAdapter,
        TokenStorage $tokenStorage,
        AuthorizationChecker $authorizationChecker,
        EntityManager $entityManager
    ) {
        $this->stripeAdapter        = $stripeAdapter;
        $this->tokenStorage         = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
        $this->entityManager        = $entityManager;
    }


    /**
     * @param mixed $protocol
     * @param Constraint $constraint
     */
    public function validate($protocol, Constraint $constraint)
    {


        try {

            $customer         = $this->stripeAdapter->getCustomer(
                $protocol->getAgency()->getStripeCustomerId()
            );
            $customer->source = array(
                'object'    => 'card',
                'exp_month' => $protocol->getExpirationMonth(),
                'exp_year'  => $protocol->getExpirationYear(),
                'number'    => $protocol->getCreditCardNumber(),
                'cvc'       => $protocol->getVCode(),
            );


            $customer = $customer->save();
            $protocol->getAgency()->setStripeCustomerId($customer->id);
            $protocol->getAgency()->setStripeSource($customer->default_source);

        } catch (\Exception $e) {
            $this->attachError($e, $constraint);
        }


        // if a customer was created properly
        if (isset($customer)) {

            try {
                $subscription = $this->stripeAdapter->createSubscription(
                    $customer,
                    $protocol->getPlan()
                );
                $protocol->getAgency()->setStripeSubscriptionId($subscription->id);
                $protocol->getAgency()->setPlan($protocol->getPlan());
            } catch (\Exception $e) {

                $this->attachError($e, $constraint);

            }

        }


        // reactivate all the pca and recipient users for the agency
        $pcaUsers = [];
        foreach ($protocol->getAgency()->getPcas() as $pca) {
            array_push($pcaUsers, $pca->getUser());
        }
        $recipientUsers = [];
        foreach ($protocol->getAgency()->getRecipients() as $recipient) {
            array_push($recipientUsers, $recipient->getUser());
        }

        $userCollection = new ArrayCollection(
            array_merge(
                $recipientUsers,
                $pcaUsers
            )
        );

        foreach ($userCollection as $user) {
            // we are using the expired flag as determining if the account has been cancelled or not
            // make sure to set it back to false cause we are re-activating their accounts
            $user->setExpired(false);
            $this->entityManager->persist($user);
            $this->entityManager->flush($user);
        }


        return;

    }


    /**
     * Attaches the error message for the stripe billing to the appropriate spot
     * on the form
     *
     * @param \Exception $e
     * @param Constraint $constraint
     */
    private function attachError(
        \Exception $e,
        Constraint $constraint
    ) {
        $path = isset($constraint->stripeErrorMessages[$e->getMessage(
            )]) ? $constraint->stripeErrorMessages[$e->getMessage()] : self::DEFAULT_ERROR_PATH;

        $this->context->buildViolation($e->getMessage())
                      ->atPath($path)
                      ->addViolation();
    }


}