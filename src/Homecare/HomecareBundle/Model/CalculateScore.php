<?php
namespace Homecare\HomecareBundle\Model;
	
	
use Doctrine\ORM\EntityManager;	
	
//this class is used to calculate a scores based on the different care plan goals
//set by either the recipient, agency, or admin
class CalculateScore {
	
	protected $em;
	//this array holds the dates of all the services done
	protected $datesAndServices = array();
	//this property will hold all the desired care goals from the recipient, admin, or agency
	protected $desiredCareGoals = array();
	//this property holds the timesheets
	protected $timesheets = array();

	    public function __construct( EntityManager $em ) {
	        $this->em = $em;
	    }
	
	
	//get all the timesheets for a specificd user and date range
	public function setTimesheets( $timesheets ) {
		$this->timesheets = $timesheets;
	}
	
	
	
	
	public function setDesiredCareGoals( $desiredCareGoals ) {
		$this->desiredCareGoals =  $desiredCareGoals;
	}
	
		

		//this method will grab all the care performed and place it in its respective date
		//  array(0 => ("12/20/14" => array( "behavior", "health_related" ) ) )
		public function sortCarePerformedByEachDate() {
            foreach ( $this->timesheets as $timesheet ) {
                $dateCareWasPerformed = $timesheet->getSessionData()->getDateEnding()->format('m/d/Y');
                $careOptions = $timesheet->getCareOptionTimesheets();
                foreach ( $careOptions as $careOption ) {
                    $this->datesAndServices[ $dateCareWasPerformed ][ 'carePerformed' ][] = $careOption->getCareOption()->getCareOption();
                }
            }
            return $this->datesAndServices;
        }
		
		
		
		
	
		//this method compares the datesAndServices to the desired care goals
		public function calculateTheActualScore() {

            //\Doctrine\Common\Util\Debug::dump( 'test' );

            //return false;

			$desiredCareGoals = $this->desiredCareGoals;
			
			
		
			//grab the total number of days that services were performed for all the shown timesheets
			$totalNumDates = count( $this->datesAndServices );
			
			$totalNumDesiredGoals = count( $desiredCareGoals );

            //\Doctrine\Common\Util\Debug::dump( $totalNumDesiredGoals );

            //return false;
			
			$numOfMissedGoals = 0;
			$numOfSuccessfulGoals = 0;
			foreach ( $this->datesAndServices as $date => $carePerformed ) {
                foreach( $carePerformed as $care ) {
                    //\Doctrine\Common\Util\Debug::dump( $care);
                    //return false;
                    //make sure that the desired care goal is in the actual services performed
                    foreach ( $desiredCareGoals as $desiredCareGoal ) {
                        if ( !in_array( $desiredCareGoal, $care ) ) {
                            $numOfMissedGoals += 1;
                        }
                        if ( in_array( $desiredCareGoal, $care ) ) {
                            $numOfSuccessfulGoals += 1;
                        }
                    }
                }
			}
			
					
			//if there are dates to show 
			if ( $totalNumDates != 0 ) {
				//this is the percentage of care goals that were missed
				$grade = $numOfMissedGoals / ( $totalNumDesiredGoals * $totalNumDates );
				// now create the percentage of care goals that were accomplished
				$actualGrade = 100 - ( $grade * 100 );
				//now format the grade by rounding it
				$formatedGrade = round( $actualGrade );
				return $formatedGrade;
			}
			
			
		}
	
	
	
	
	
	
	
	
}
