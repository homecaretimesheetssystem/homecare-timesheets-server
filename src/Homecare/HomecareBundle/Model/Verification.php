<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 1/21/16
 * Time: 8:31 AM
 */

namespace Homecare\HomecareBundle\Model;

use Doctrine\ORM\EntityManager;
use Homecare\HomecareBundle\Entity\Pca;
use Homecare\HomecareBundle\Entity\Recipient;
use Homecare\HomecareBundle\Entity\RecipientPca;
use Homecare\HomecareBundle\Entity\Verification as V;


class Verification
{

    private $em;
    private $repository;
    private $frequency = 90;
    private $recipientPca;

    public function __construct(EntityManager $entityManager)
    {
        $this->em         = $entityManager;
        $this->repository = $entityManager->getRepository("HomecareHomecareBundle:Verification");
    }


    public function setRecipientPca(RecipientPca $recipientPca)
    {
        $this->recipientPca = $recipientPca;
    }


    public function setFirstTimeVerification()
    {
        $verification = new V();
        $verification->setRecipientPca($this->recipientPca);
        $verification->setCurrent(true);
        $verification->setFirstTime(true);
        $this->em->persist($verification);
        $this->em->flush();

        $pca = $this->recipientPca->getPca();

        // loop through all the verifications and update the frequency
        $this->decideFrequency($pca);
    }


    public function setNonFirstTimeVerification(V $oldVerification)
    {
        $verification = new V();
        $verification->setRecipientPca($this->recipientPca);
        $verification->setFirstTime(false);
        $verification->setCurrent(true);

        $verificationDate = new \DateTime();
        $verificationDate->modify("+".$oldVerification->getFrequency()." day");
        $verificationDate->modify("-".rand(1, 90)."day");
        $verification->setVerificationDate($verificationDate);

        $this->em->persist($verification);
        $this->em->flush();

        $pca = $this->recipientPca->getPca();
        // loop through all the verifications and update the frequency
        $this->decideFrequency($pca);
    }


    public function setVerification(V $verification)
    {
        // it will not be the current verification anymore
        $verification->setCurrent(false);
        $verification->setVerified(true);

        // set today as the verification date if the verification we are setting is the first time
        if ($verification->getFirstTime()) {
            $verificationDate = new \DateTime();
            $verification->setVerificationDate($verificationDate);
        }
    }


    /**
     * reusable method to decide the verification frequency for a pca
     *
     * this must be called after archiving a recipient or pca
     */
    public function decideFrequency(Pca $pca)
    {
        $verifications = $this->repository->getRecentVerifications($pca);

        // decide the frequency of the verifications
        switch (count($verifications)) {

            case 1:
                $this->frequency = 90;
                //$this->newVerification();
                break;
            case 2:
                $this->frequency = 180;
                //$this->createVerification($verifications[0]);
                break;
            case 3:
                $this->frequency = 270;
                //$this->createVerification($verifications[0]);
                break;
            case 4:
                $this->frequency = 360;
                //$this->createVerification($verifications[0]);
                break;
            default:
                $this->frequency = 360;
                break;
        }

        // go through the current verifications and update the frequency

        foreach ($verifications as $verification) {
            $verification->setFrequency($this->frequency);
            $this->em->flush();
        }


    }


}