<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 4/9/15
 * Time: 2:39 PM
 */

namespace Homecare\HomecareBundle\Model;


use ZipArchive;

class ZipFile {


    /**
     * this method creates a zip file and adds an array of files to it
     *
     * @param array $pdfFileNames
     */
    public function addFilesToZip( $fileNames ) {
        $zipFileName = $this->getImagesDir() . '/files.zip';
        $myfile = fopen( $zipFileName, "w") or die("Unable to open file!");
        chmod( $zipFileName, 0777 );
        $zip = new ZipArchive;
        $zip->open( $zipFileName );
        foreach ( $fileNames as $actualFileName => $customFileName ) {
            $zip->addFile( $this->getImagesDir() . '/../uploads/' . $actualFileName, $customFileName );
        }
        $zip->close();
    }


    /**
     * get images directory where you create the zip file
     *
     * @return string
     */
    protected function getImagesDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/images';
    }


}