<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 4/22/15
 * Time: 6:54 PM
 */

namespace Homecare\HomecareBundle\Model;


class FileNaming {

    public function createFileName( $timesheet, $fileExtension ) {
        return $timesheet->getRecipient()->getLastName() . ',' . $timesheet->getRecipient()->getFirstName() . ' ' . substr( $timesheet->getSessionData()->getPca()->getFirstName(), 0, 1 ) . substr( $timesheet->getSessionData()->getPca()->getLastName(), 0, 1 ) . ' ' . $timesheet->getSessionData()->getDateEnding()->format( "m-d-Y h i s" ) . ' ' . $timesheet->getSessionData()->getTimeIn()->getTimeIn()->format( "m-d-Y h i s" ) . ' ' . $timesheet->getSessionData()->getService()->getServiceName() . $fileExtension;
    }

}