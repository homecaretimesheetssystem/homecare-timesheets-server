<?php

namespace Homecare\HomecareBundle\Model;

use Homecare\HomecareBundle\Entity\Pca;
use Homecare\HomecareBundle\Entity\Recipient;
use Homecare\HomecareBundle\Entity\User;

class CustomSwiftMailer
{

    private $swiftMailer;

    private $amazonUrl;


    private $templating;


    /**
     * @param \Swift_Mailer $swiftMailer
     * @param string $amazonUrl
     * @param $templating
     */
    public function __construct(\Swift_Mailer $swiftMailer, $amazonUrl, $templating)
    {
        $this->swiftMailer = $swiftMailer;
        $this->amazonUrl   = $amazonUrl;
        $this->templating  = $templating;
    }


    /**
     * this method takes an email addresses and an array of 2 file Urls. 1 being a pdf file url and the other being a csv file url
     *
     * @param array $emailAddresses
     * @param array $attachmentUrls
     */
    public function sendEmails($emailAddresses, $attachmentUrls)
    {

        $message = \Swift_Message::newInstance()
                                 ->setSubject('Timesheet Pdf and Csv File')
                                 ->setFrom(array('delivery@homecaretimesheetsapp.com' => 'Homecare Timesheets App'))
                                 ->setTo($emailAddresses)
                                 ->setBody("Here are the pdf and csv for the timesheet");


        foreach ($attachmentUrls as $attachmentUrl) {
            $message->attach(\Swift_Attachment::fromPath($this->amazonUrl.$attachmentUrl));
        }


        if ( ! $this->swiftMailer->send($message, $failures)) {
            //\Doctrine\Common\Util\Debug::dump($failures);
        }
    }


    /**
     * @param Recipient $recipient
     */
    public function sendTimesheetAvailableEmail(Recipient $recipient)
    {

        $emailAddress = $recipient->getUser()->getEmail();

        $message = \Swift_Message::newInstance()
                                 ->setSubject('Timesheets need to be signed')
                                 ->setFrom(array('delivery@homecaretimesheetsapp.com' => 'Homecare Timesheets App'))
                                 ->setTo($emailAddress)
                                 ->setBody(
                                     "You have timesheet(s) available to sign. Please login to the app and sign them"
                                 );


        if ( ! $this->swiftMailer->send($message, $failures)) {
            //\Doctrine\Common\Util\Debug::dump($failures);
        }


    }


    /**
     * @param Pca $pca
     * @param Recipient $recipient
     */
    public function sendTimesheetFinishedEmail(Pca $pca, Recipient $recipient)
    {

        $emailAddress = $pca->getUser()->getEmail();

        $message = \Swift_Message::newInstance()
                                 ->setSubject(
                                     'Recipient '.$recipient->getRecipientName().' sent timesheet'
                                 )
                                 ->setFrom(array('delivery@homecaretimesheetsapp.com' => 'Homecare Timesheets App'))
                                 ->setTo($emailAddress)
                                 ->setBody(
                                     'Recipient '.$recipient->getRecipientName().' has signed and sent a timesheet'
                                 );


        if ( ! $this->swiftMailer->send($message, $failures)) {
            //\Doctrine\Common\Util\Debug::dump($failures);
        }


    }


    /**
     * @param User $user
     */
    public function sendAppDownloads(User $user)
    {

        $emailAddress = $user->getEmail();

        $message = \Swift_Message::newInstance()
                                 ->setSubject('Download Our App!')
                                 ->setFrom(array('delivery@homecaretimesheetsapp.com' => 'Homecare Timesheets App'))
                                 ->setTo($emailAddress)
                                 ->setBody(
                                     $this->templating->render(
                                         'HomecareHomecareBundle:Emails:download_app.html.twig',
                                         []
                                     ),
                                     'text/html'
                                 );


        if ( ! $this->swiftMailer->send($message, $failures)) {
            //\Doctrine\Common\Util\Debug::dump($failures);
        }


    }


}