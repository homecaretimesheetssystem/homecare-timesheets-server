var manageUsers = (function(){



    return {


        showModal: function(linkScope) {
            this.userToDelete = linkScope.data("userid");
            this.userType = linkScope.data("usertype");
            $("#modal1").modal('show');
        },
        userToDelete: 0,
        userType: "",
        deleteUser: function() {
            var route = Routing.generate('Homecare_HomecareBundle_delete_user', { typeOfUser: this.userType, userId: this.userToDelete });
           window.location = route;
        }

        }
})();