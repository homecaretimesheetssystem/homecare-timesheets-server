/*set all your initializer code here */
$('document').ready(function() {

//always test for if the element exists before you use it because all the javascript is minified across all the pages of the application
window.userToDelete = 0;

    if( $('.deleteUserLink').length ) {
        $('.deleteUserLink').click(function(){
        manageUsers.showModal( $(this) );
        })
    }

    if( $('#deleteUserButton').length ) {
        $('#deleteUserButton').click(function(){
           manageUsers.deleteUser();
        })
    }



});

