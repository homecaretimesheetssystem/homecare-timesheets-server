<?php
// src/Homecare/HomecareBundle/DataFixtures/ORM/StatesDataFixture.php

namespace Homecare\HomecareBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Homecare\HomecareBundle\Entity\Services;

class ServuceDataFixture implements FixtureInterface
{

    private $services = array(
        'Personal Care Service',
        'Homemaking',
        'Respite',
        'Caregiving Expense',
        'Environmental Modifications',
        'Personal Support',
        'Self-Direction Support',
        'Consumer Support',
        'Personal Assistance',
        'Treatment and Training',
        'Temporary Pca (On Demand)'
    );


    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $serviceNumber = 1;

        foreach ($this->services as $service) {
            $serviceObj = new Services();
            $serviceObj->setServiceName($service);
            $serviceObj->setServiceNumber($serviceNumber);
            $manager->persist($serviceObj);
            $serviceNumber++;
        }
        $manager->flush();
    }
}

?>