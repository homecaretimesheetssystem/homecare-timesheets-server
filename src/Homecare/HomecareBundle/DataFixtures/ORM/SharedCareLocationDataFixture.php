<?php
// src/Homecare/HomecareBundle/DataFixtures/ORM/StatesDataFixture.php

namespace Homecare\HomecareBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Homecare\HomecareBundle\Entity\SharedCareLocation;

class SharedCareLocationDataFixture implements FixtureInterface
{

    private $locations = array(
        'N/A', 'Home', 'Community'
    );


    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        foreach ( $this->locations as $location ) {
            $sharedCareLocation = new SharedCareLocation();
            $sharedCareLocation->setLocation( $location );
            $manager->persist( $sharedCareLocation );
        }
        $manager->flush();
    }
}

?>