<?php
// src/Homecare/HomecareBundle/DataFixtures/ORM/UserDataFixture.php

namespace Homecare\HomecareBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Homecare\HomecareBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;



class UserDataFixture extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface {
	
	/**
	* @var ContainerInterface
	*/
	private $container;
	
  /**
      * {@inheritDoc}
      */
     public function setContainer(ContainerInterface $container = null)
     {
         $this->container = $container;
     }

public function load( ObjectManager $manager )
{
	

	
	//CREATE AN ADMIN USER
	$user2 = new User();
	$user2->setUsername( "admin" );
	$user2->setEmail( "andre.best@pcapartners.net" );
	$user2->setFirstName( "Andre" );
	$user2->setLastName( "Best" );
	//$encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
	//$user->setPassword( $encoder->encodePassword('iluv2rap', $user->getSalt() ) );
	$plainPassword = 'admin';
	$encoded = $this->container->get('security.password_encoder')
	    ->encodePassword($user2, $plainPassword);
	$user2->setPassword($encoded);
	$user2->setEnabled(true) ;
	$user2->addRole( 'ROLE_ADMIN' );
	$user2->setAdmin( $manager->merge( $this->getReference( 'admin' ) ) );
	$manager->persist( $user2 );
	
	
	
	
	
	$manager->flush();
	
}


public function getOrder() {
	return 2;
}	



}
	
	
?>