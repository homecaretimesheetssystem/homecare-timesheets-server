<?php
// src/Homecare/HomecareBundle/DataFixtures/ORM/CareOptionsDataFixture.php

namespace Homecare\HomecareBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Homecare\HomecareBundle\Entity\CareOptions;

class CareOptionsDataFixture implements FixtureInterface
{

    private $care_options = array(

        'Dressing'           => false,
        'Grooming'           => false,
        'Bathing'            => false,
        'Eating'             => false,
        'Transfers'          => false,
        'Mobility'           => false,
        'Positioning'        => false,
        'Toileting'          => false,
        'Health Related'     => false,
        'Behavior'           => false,
        'Light Housekeeping' => true,
        'Laundry'            => true,
        'Other'              => true,

    );


    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {

        foreach ($this->care_options as $care_option => $isIadl) {

            $care_option_obj = new CareOptions();
            $care_option_obj->setCareOption($care_option);
            $care_option_obj->setIsIadl($isIadl);
            $manager->persist($care_option_obj);

        }

        $manager->flush();
    }
}

?>