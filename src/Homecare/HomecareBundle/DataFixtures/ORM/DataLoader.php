<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 7/31/16
 * Time: 8:54 PM
 */

namespace Homecare\HomecareBundle\DataFixtures\ORM\Staging;


use FOS\UserBundle\Entity\User;
use Hautelook\AliceBundle\Doctrine\DataFixtures\AbstractLoader;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DataLoader extends AbstractLoader
{

    /**
     * {@inheritdoc}
     */
    public function getFixtures()
    {
        return [
            __DIR__.'/dummyData.yml',
        ];
    }


    public function encodePassword(User $user, $plainPassword)
    {

        return $this->container->get('security.password_encoder')
                               ->encodePassword($user, $plainPassword);
    }
}