<?php
// src/Homecare/HomecareBundle/DataFixtures/ORM/StatesDataFixture.php

namespace Homecare\HomecareBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Homecare\HomecareBundle\Entity\Ratio;

class RatioDataFixture implements FixtureInterface
{

    private $ratios = array(
    '1:1','1:2','1:3'
    );


    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        foreach ( $this->ratios as $ratio ) {
            $ratioObj = new Ratio();
            $ratioObj->setRatio( $ratio );
            $manager->persist( $ratioObj );
        }
        $manager->flush();
    }
}

?>