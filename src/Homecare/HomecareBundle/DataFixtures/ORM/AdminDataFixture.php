<?php
// src/Homecare/HomecareBundle/DataFixtures/ORM/TimesheetsDataFixture.php

namespace Homecare\HomecareBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Homecare\HomecareBundle\Entity\Admin;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Homecare\HomecareBundle\DataFixtures\ORM\DummyData;



class AdminDataFixture extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface {
	
	/**
	* @var ContainerInterface
	*/
	private $container;
	
	
	
		/**
      * {@inheritDoc}
      */
     public function setContainer(ContainerInterface $container = null)
     {
         $this->container = $container;
     }
		 
		 
		 
		 
		 
		 
		 
		 

public function load( ObjectManager $manager )
{

//API STEP 1!!	
//KICK OFF THE API by entering in the Admin



//ADMIN 1
$admin = new Admin();
$admin->setFirstName( "Andre" );
$admin->setLastName( "Best" );
$manager->persist( $admin );



$manager->flush();	


$this->addReference( 'admin', $admin );


	
}



public function getOrder() {
	return 1;
}	



}