<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 4/8/17
 * Time: 3:11 PM
 */

namespace Homecare\HomecareApiBundle\Data;


use Symfony\Component\Form\FormInterface;

class ApiProblem
{

    const VALIDATION_ERROR = 1;
    const NO_RECIPIENT_COUNTY_ERROR = 2;
    const NO_DEVICE_ID_ATTACHED_TO_USER = 3;


    static private $codes = array(
        self::VALIDATION_ERROR          => [
            'title'  => 'There was a validation error',
            'detail' => 'The request data you passed up did not validate properly',
        ],
        self::NO_RECIPIENT_COUNTY_ERROR => [
            'title'  => 'There was no county set for the recipient',
            'detail' => 'A county must be set on the dashboard for the recipient to post a request',
        ],
        self::NO_DEVICE_ID_ATTACHED_TO_USER => [
            'title'  => 'A device id has not been attached to the user',
            'detail' => 'A device id has not been attached to the user. This error should not be happening and could either be a server error or client error',
        ]
    );


    /**
     * A machine readable integer that never changes
     *
     * @var integer
     */
    private $code;

    /**
     * This should contain a URL to the documentation explaining the code
     * @var string
     */
    private $type;

    /**
     * A short description of the error
     * @var string
     */
    private $title;

    /**
     * A longer description of the error
     * @var string
     */
    private $detail;

    /**
     * Any extra error data that is being added to the response
     * @var array
     */
    private $extraData = array();


    /**
     * ApiProblem constructor.
     *
     * @param $code
     *
     */
    public function __construct($code)
    {

        $this->code = $code;

        if ( ! isset(self::$codes[$code])) {
            throw new \InvalidArgumentException('No api code for '.$code);
        }

        $this->title  = self::$codes[$code]['title'];
        $this->detail = self::$codes[$code]['detail'];

    }


    public function set($name, $value)
    {
        $this->extraData[$name] = $value;
    }


    public function toArray()
    {
        return array_merge(
            [
                'code'   => $this->code,
                'title'  => $this->title,
                'detail' => $this->detail,
            ],
            $this->extraData
        );
    }


    /**
     * @param FormInterface $form
     *
     * @return array
     */
    public static function getErrorsFromForm(FormInterface $form)
    {
        $errors = array();
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = self::getErrorsFromForm($childForm)) {
                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }

        return $errors;
    }


}