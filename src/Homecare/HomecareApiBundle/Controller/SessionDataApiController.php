<?php

namespace Homecare\HomecareApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Homecare\HomecareBundle\Entity\SessionData;
use Homecare\HomecareBundle\Form\SessionDataType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;




class SessionDataApiController extends Controller
{
	

		
		/**
		* @View()
		*/
	    public function postCreateSessiondataAction( Request $request )
	    {
            return $this->createSessionDataForm( $request, new SessionData() );
        }
		
		
		
		
			/**
			* @View()
			*/
		    public function patchSessiondataAction( SessionData $sessionData )
		    {
			
				return $this->updateSessiondataAction( $sessionData );
			
			}   
		
		
		
		
		
		
			private function setMethod( $statusCode, $sessionData = null ){
			
			   switch( $statusCode ) {
				
					case 201:
					return array( 'method' => 'POST' );
					break;
					case 204:
					return array( 
						'action' => $this->generateUrl('patch_sessiondata', array('sessionData' => $sessionData->getId())),
						'method' => 'PATCH' 
					);
				
					break;
				
			}
		
		
		}
		
		
		
		
		
		
			/**
			* @View()
			*/
		  private function createSessionDataForm( $request, SessionData $sessionData )
		     {
	         
				
					  $statusCode = 201;
			
						$form = $this->createForm( new SessionDataType(), $sessionData, $this->setMethod( $statusCode ) );
	 
	 
		         $form->handleRequest( $this->getRequest() );
					 
					 
					 

		         if ( $form->isValid() ) {
						 
						 
						 
		             //$user->save();

								 //$timesheet = $form->getData();

		             $response = new Response();
		             $response->setStatusCode(201);
				 //return 'yo dog';
			 
				 				$em = $this->getDoctrine()->getManager();
								$em->persist( $sessionData );
		            $em->flush();
			 

		             // set the `Location` header only when creating new resources
								 /*
		             if (201 === $statusCode) {
		                 $response->headers->set('Location',
		                     $this->generateUrl(
		                         'acme_demo_user_get', array('id' => $user->getId()),
		                         true // absolute
		                     )
		                 );
		             }
								 */
	           //return V::create($form, 201);
		             //return $response;
							 
					 //return array( $form, array("timesheet" => array("id" => $timesheet->getId() ) ) );
				 
	        

return V::create( array("createdSessionData" => array("id" => $sessionData->getId() ) ), $statusCode );
							 
				 
								// return array( $form, array("timesheet" => array("id" => $timesheet->getId() ) ) );
				 
		         }

		         return V::create($form, 400);
		     }
		
		
			
			
			
			
			
			
 				/**
 				* @View()
 				*/
 				private function updateSessiondataAction( $sessionData )
 				    {
							
						
						
	
 				        $statusCode = 204;

 				        $form = $this->createForm( new SessionDataType(), $sessionData, $this->setMethod( $statusCode, $sessionData ) );
							 
							 
							 
 							 $form->handleRequest( $this->getRequest() );
								
								
 				        if ($form->isValid()) {
									
									  
 					 				$em = $this->getDoctrine()->getManager();
 									$em->persist( $sessionData );
 			            $em->flush();

 				            $response = new Response();
 				            $response->setStatusCode( $statusCode );

 				            // set the `Location` header only when creating new resources
 										/*
 				            if (201 === $statusCode) {
 				                $response->headers->set('Location',
 				                    $this->generateUrl(
 				                        'acme_demo_user_get', array('id' => $user->getId()),
 				                        true // absolute
 				                    )
 				                );
 				            }
 */
 				            return $response;
 				        }

 				        return V::create($form, 400);
 				    }
		
		
			
			
			
			
			
			
			
	
	
	
}