<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 2/20/17
 * Time: 6:08 PM
 */

namespace Homecare\HomecareApiBundle\Controller;

use Homecare\HomecareBundle\Entity\Agency;
use Homecare\HomecareBundle\Entity\Pca;
use Homecare\HomecareBundle\Entity\Recipient;
use Homecare\HomecareBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Homecare\HomecareBundle\Entity\Timesheet;
use Homecare\HomecareBundle\Entity\CareOptionTimesheet;
use Homecare\HomecareBundle\Form\CareOptionTimesheetType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;
use Doctrine\Common\Collections\ArrayCollection;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


/**
 * Class DeviceController
 * @package Homecare\HomecareApiBundle\Controller
 */
class DeviceController extends Controller
{

    /**
     * @param User $user
     *
     * @return array
     * @View()
     * @ApiDoc(
     *  section="device",
     *  output="array<Homecare\HomecareBundle\Entity\Device>",
     *  resource=true,
     *  description="returns an array of Device objects",
     *  requirements={
     *      {
     *          "name"="user",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the user whom you want to get all the devices from"
     *      }
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         400="Bad request",
     *         404="not found",
     *         500="server error"
     *
     *     }
     *
     * )
     * )
     */
    public function getUsersDevicesAction(User $user)
    {

        $em      = $this->getDoctrine()->getEntityManager();
        $devices = $em->getRepository("HomecareHomecareBundle:Device")->getAllByUser($user);

        return $devices;

    }


    /**
     * @View()
     * @ApiDoc(
     *  section="device",
     *  description="Creates a device Object. Has a Location header in the response that indicates where the newly created resource lives",
     *  parameters={
     *      {"name"="isAndroid", "dataType"="boolean", "required"=false, "description"="is the device an android device"},
     *      {"name"="isIos", "dataType"="boolean", "required"=false, "description"="is the device an Ios device"},
     *      {"name"="isActive", "dataType"="boolean", "required"=false, "description"="If the Ios or Android device has blocked or disabled push notifications"},
     *      {"name"="deviceToken", "dataType"="string", "required"=false, "description"="the device token. This is needed to send the push notification to that specific device"},
     *      {"name"="user", "dataType"="integer", "required"=false, "description"="the user id that this device is tied to"},
     *
     *  },
     *  statusCodes={
     *         201="Returned when successful",
     *         400="Bad request or validation failed",
     *         500="Server error"
     *
     *     }
     *  )
     *
     * @param Request $request
     *
     * @return Response
     */
    public function postDevicesAction(Request $request)
    {

        $form = $this->createForm(
            'homecare_device'
        );

        $form->handleRequest($request);


        if ($form->isValid()) {

            $em = $this->getDoctrine()->getEntityManager();

            $device = $form->getData();
            $user   = $device->getUser();

            $oldDevice = $this->getDoctrine()->getRepository('HomecareHomecareBundle:Device')->findOneBy(
                array('deviceToken' => $device->getDeviceToken())
            );


            // if a device object exists with the same token
            // and a different user is logged in then the last one
            if ($oldDevice && $this->getUser()->getId() != $oldDevice->getUser()->getId()) {
                $em->remove($oldDevice);

                $em->persist($device);
                $em->flush();

            } elseif ($oldDevice && $this->getUser()->getId() == $oldDevice->getUser()->getId()) {
                // if an old device exists and it belongs to the same user who is logged in then do nothing
            } elseif ( ! $oldDevice) {
                $em->persist($device);
                $em->flush();
            }


            $response = new Response();
            $response->setStatusCode(201);

            $response->headers->set(
                'Location',
                $this->generateUrl('get_users_devices', array('user' => $device->getUser()->getId()))
            );

            return $response;

        }

        return V::create($form, 400);

    }


}