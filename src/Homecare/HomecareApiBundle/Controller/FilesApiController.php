<?php

namespace Homecare\HomecareApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Homecare\HomecareBundle\Entity\Files;
use Homecare\HomecareBundle\Entity\NewFile;
use Homecare\HomecareBundle\Form\NewFileType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Homecare\HomecareBundle\Entity\Timesheet;




class FilesApiController extends Controller
{
	
	
	
	/**
	* @View()
	*/
    public function postCreateFilesAction( Request $request )
    {

			$em = $this->getDoctrine()->getManager();
			
			$files = new Files();
			
			$em->persist( $files );
			
			$em->flush();
			
			return array( "createdFilesId" => $files->getId() );

		}
	
	
	


		/**
		* @View()
		*/
	    public function postFilesAction( Files $fileObj, Request $request )
	    {

            // get the timesheet id
			$timesheetId = $request->request->get( 'timesheet' );
            // get the timesheet object to use
            $em = $this->getDoctrine()->getManager();
            $timesheetObj = $em->getRepository( "HomecareHomecareBundle:Timesheet" )->find( $timesheetId );


            $fileObj->setTimesheet( $timesheetObj );


				//get all the files
				$file = $request->files->all();

				$pathToFiles = [];
				foreach( $file as $columnName => $file ) {

					$uniqueFileName = $this->saveFileInDatabase( $columnName, $file, $fileObj );

					$this->saveFileToAmazonS3( $uniqueFileName );

					$pathToFiles[] = $uniqueFileName;

				}

				return array( "uploadedFileLocation" => $pathToFiles );

				//return $fileName;

			}





			/**
			* this method handles persisting the file to the database
			*
			* @param UploadedFile $file this is the files object
			* @param string $columnName this is the column name to insert the file name into
			* @param Files $fileObj this is the entity file object that gets param converted
			*
			* @return string this is the unique file name that was generated randomly
			*/
			private function saveFileInDatabase( $columnName, UploadedFile $file, Files $fileObj )
			{


				$dir = $this->get('kernel')->getRootDir() . '/../web/uploads';

				//create the unique file name for the file being uploaded
				$uniqueFileName = $fileObj->getId() . $this->getRandomString() . time() . '.' . $file->getClientOriginalExtension();

				//move the file
				$file->move( $dir, $uniqueFileName );

				//save the file name in the database
				$em = $this->getDoctrine()->getManager();

				call_user_func(array($fileObj, 'set' . ucwords( $columnName ) ), $uniqueFileName );


				$em->persist( $fileObj );

				$em->flush();

				return $uniqueFileName;

			}






			/**
			* this method handles saving the file to amazons S3 server
			*
			* @param string $uniqueFileName this is the unique file name for the method
			*/
			private function saveFileToAmazonS3( $uniqueFileName )
			{

				//save file to amazon S3
				$amazonS3 = $this->get( 'homecare.homecarebundle.amazons3' );

				$amazonS3->submitFileToAmazonS3( $uniqueFileName );

				$amazonS3->deleteTempFile( $uniqueFileName );

			}







			private function getRandomString()
			{
			    $length = 5;
			    $characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWZYZ";

			    $real_string_length = strlen($characters) ;
			    $string="id";

			    for ($p = 0; $p < $length; $p++)
			    {
			        $string .= $characters[mt_rand(0, $real_string_length-1)];
			    }

			    return strtolower($string);
			}


	
	

	
	
	
}