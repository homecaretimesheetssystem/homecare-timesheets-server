<?php

namespace Homecare\HomecareApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Homecare\HomecareBundle\Entity\Timesheet;
use Homecare\HomecareBundle\Entity\CareOptionTimesheet;
use Homecare\HomecareBundle\Form\CareOptionTimesheetType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;
use Doctrine\Common\Collections\ArrayCollection;


class CareOptionTimesheetApiController extends Controller
{
	
	
	
	
	
	
	/**
	* @View()
	*/
    public function postCareoptionsAction( Request $request )
    {
					
		//return $this->updateCareOptionForm( new CareOptionTimesheet() );

		
				
		//$data =  json_decode( $request->getContent(), true );
		
		
		//$timesheetId = $data[ 'homecare_homecarebundle_careoptiontimesheet' ][ 'timesheet' ];
		//$arrayOfCareOptions = $data[ 'homecare_homecarebundle_careoptiontimesheet' ][ 'careOptions' ];


        $timesheetId = $request->request->get( 'homecare_homecarebundle_careoptiontimesheet' )[ 'timesheet' ];
        $arrayOfCareOptions = $request->request->get( 'homecare_homecarebundle_careoptiontimesheet' )[ 'careOptions' ];




        $em = $this->getDoctrine()->getManager();
		
		$timesheetObj = $em->getRepository( "HomecareHomecareBundle:Timesheet" )->findOneById( $timesheetId );
		
		
		$addedCareOptionIds = [];
		foreach ( $arrayOfCareOptions as $careOptionId ) {
			
			
$careOptionObj = $em->getRepository( "HomecareHomecareBundle:CareOptions" )->findOneById( $careOptionId );
		
		

		
		$careOptionTimesheet = new CareOptionTimesheet();
		
		$careOptionTimesheet->setTimesheet( $timesheetObj );
		$careOptionTimesheet->setCareOption( $careOptionObj );	
		
		$em->persist( $careOptionTimesheet );
		
		$em->flush();
		
		$addedCareOptionIds[] = $careOptionTimesheet->getId();
		
		}
		
		
		$statusCode = 201;
		
		return V::create( array( "careOptionTimesheetsCreated" => array("ids" => $addedCareOptionIds ) ), $statusCode );
				
		}
		
		
		
		
		
		
		/**
		* @View()
		*/
	    public function deleteCareoptionDeleteAction( CareOptionTimesheet $careOption )
	    {
					
			//return $this->updateCareOptionForm( new CareOptionTimesheet() );
			
			$idToDelete = $careOption->getId();
			
			$em = $this->getDoctrine()->getManager();
			
			$em->remove( $careOption );
			$em->flush();
			
			return array( 'careOptionIdDeleted' => $idToDelete );
				
			}
	
	
	
	
	
	
			/**
			* @View()
			*/
		    public function deleteCareoptionsDeleteAction( Timesheet $timesheet )
		    {
					
				//return $this->updateCareOptionForm( new CareOptionTimesheet() );
			
				$timesheetId = $timesheet->getId();
			
				$em = $this->getDoctrine()->getManager();
				
				$em->getRepository( "HomecareHomecareBundle:CareOptionTimesheet" )->deleteCareOptionTimesheets( $timesheetId );
			
			
			
//return $timesheetId;
			
	return array( 'message' => 'all care options for timesheet Id ' . $timesheetId . ' have been deleted' );			
				
				}
	
	
	
	
	
	
}