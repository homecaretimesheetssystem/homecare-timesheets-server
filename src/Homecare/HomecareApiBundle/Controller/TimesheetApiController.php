<?php

namespace Homecare\HomecareApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Homecare\HomecareBundle\Entity\Timesheet;
use Homecare\HomecareBundle\Entity\Services;
use Homecare\HomecareBundle\Entity\CareOptions;
use Homecare\HomecareBundle\Form\TimesheetType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\PropertyAccess\PropertyAccess;


class TimesheetApiController extends Controller
{


    //curl -H "Accept: application/json" -H "Content-type: application/json" -i -X POST -d '{"homecare_homecarebundle_timesheet":{"agency":"157"}}' http://www.hc-timesheets-demo.com/app_dev.php/api/v1/agencies


    /**
     * @View()
     */
    public function postCreateTimesheetAction(Request $request)
    {

        return $this->createTimesheetForm($request, new Timesheet());

    }


    /**
     * @View()
     */
    public function patchTimesheetAction(Request $request, Timesheet $timesheet)
    {

        return $this->updateTimesheetForm($timesheet);

    }


    /**
     * @View()
     */
    public function postFinishTimesheetsAction(Request $request)
    {


        $sessionId = $request->request->get('homecare_homecarebundle_timesheet')['sessionId'];
        $em        = $this->getDoctrine()->getManager();


        //first get the session to delete
        $session = $em->getRepository("HomecareHomecareBundle:SessionData")->find($sessionId);

        //if the verification has not been set yet then set the flagForFutherInvestigation to 1

        // stop the flag for further investigation for now
        /*
            if ( !$session[0]->getVerification() ) {
                $session[0]->setFlagForFurtherInvestigation( 1 );
            }
        */


        $timesheets = $session->getTimesheets();


        $emailObjs      = $timesheets[0]->getRecipient()->getAgency()->getEmails();
        $emailAddresses = [];
        foreach ($emailObjs as $emailObj) {
            $emailAddresses[] = $emailObj->getEmail();
        }


        $swiftMailer = $this->get('homecare_homecare.model.custom_swift_mailer');

        $attachmentUrls = [];

        foreach ($timesheets as $timesheet) {
            $timesheet->setFinished(1);

            //send out the pdf and csv file emails to the agencies email addresses
            //$this->sendPdfAndCsvFilesInEmailToAgency( $timesheet );
            if ($timesheet->getFile()) {
                $attachmentUrls[] = $timesheet->getFile()->getTimesheetCsvFile();
                $attachmentUrls[] = $timesheet->getFile()->getTimesheetPdfFile();
            }


            $swiftMailer->sendTimesheetFinishedEmail($session->getPca(), $timesheet->getRecipient());

        }


        $em->flush();


        if (count($emailAddresses) > 0) {
            $swiftMailer->sendEmails($emailAddresses, $attachmentUrls);
        }


        // last

        //return V::create(array("verification_needed" => "false"), 200);


        $statusCode = 201;

        return V::create(array("message" => "timesheets have been marked as finished"), $statusCode);
    }


    /**
     * @View()
     * @param Request $request
     *
     * @return V
     */
    public function postDeleteTimesheetsAction(Request $request)
    {

        $sessionId = $request->request->get('homecare_homecarebundle_timesheet')['sessionId'];

        $em = $this->getDoctrine()->getManager();


        //first get the session to delete
        $session = $em->getRepository("HomecareHomecareBundle:SessionData")->find($sessionId);


        if ( ! $session) {
            $statusCode = 404;

            return V::create(
                array("error" => "the session associated with \"$sessionId\" does not exist"),
                $statusCode
            );
        }


        //next get all the files if they exist and delete them from amazon s3
        $timesheets = $session->getTimesheets();
        $accessor   = PropertyAccess::createPropertyAccessor();
        $fileKeys   = [
            'timesheetCsvFile',
            'timesheetPdfFile',
            'timesheetRecSig',
            'timesheetPcaSig',
        ];
        foreach ($timesheets as $timehseet) {
            $file = $timehseet->getFile();
            //if a file row exists for this timesheet then perform these steps
            if ($file) {
                foreach ($fileKeys as $fileKey) {
                    if ($fileToDelete = $accessor->getValue($file, $fileKey)) {
                        //delete each file from amazon
                        $this->deleteFileFromAmazon($fileToDelete);
                    }
                }
            }
        }

        //delete the timesheet before you delete there constraints
        $em->remove($session);
        $em->flush();

        return V::create(array("message" => "all the timesheets and there session data have been deleted"), 200);
    }


    private function deleteFileFromAmazon($fileToDelete)
    {

        //next remove each file from amazon
        $amazonS3 = $this->get('homecare.homecarebundle.amazons3');
        $amazonS3->deleteDirectory($fileToDelete);


    }


    private function setMethod($statusCode, $timesheet = null)
    {

        switch ($statusCode) {

            case 201:
                return array('method' => 'POST');
                break;

            case 204:
                return array(
                    'action' => $this->generateUrl('patch_timesheet', array('timesheet' => $timesheet->getId())),
                    'method' => 'PATCH',
                );
                break;

        }

    }


    /**
     * @View()
     */
    private function updateTimesheetForm(Timesheet $timesheet)
    {


        $statusCode = 204;

        $form = $this->createForm(new TimesheetType(), $timesheet, $this->setMethod($statusCode, $timesheet));


        $form->handleRequest($this->getRequest());


        if ($form->isValid()) {


            $em = $this->getDoctrine()->getManager();


            //$careOption = $em->getRepository( "HomecareHomecareBundle:CareOptions" )->findOneById(456);


            //$timesheet->addCareOption( $careOption );

            $em->persist($timesheet);
            $em->flush();

            $response = new Response();
            $response->setStatusCode($statusCode);

            // set the `Location` header only when creating new resources
            /*
if (201 === $statusCode) {
    $response->headers->set('Location',
        $this->generateUrl(
            'acme_demo_user_get', array('id' => $user->getId()),
            true // absolute
        )
    );
}
*/

            return $response;
        }

        return V::create($form, 400);
    }


    /**
     * @View()
     */
    private function createTimesheetForm($request, Timesheet $timesheet)
    {


        $statusCode = 201;

        $form = $this->createForm(new TimesheetType(), $timesheet, $this->setMethod($statusCode));


        $form->handleRequest($this->getRequest());

        if ($form->isValid()) {
            //$user->save();

            //$timesheet = $form->getData();

            $response = new Response();
            $response->setStatusCode(201);


            $em = $this->getDoctrine()->getManager();
            $em->persist($timesheet);
            $em->flush();


            // set the `Location` header only when creating new resources
            /*
if (201 === $statusCode) {
    $response->headers->set('Location',
        $this->generateUrl(
            'acme_demo_user_get', array('id' => $user->getId()),
            true // absolute
        )
    );
}
            */
            //return V::create($form, 201);
            //return $response;

            //return array( $form, array("timesheet" => array("id" => $timesheet->getId() ) ) );


            return V::create(array("createdTimesheet" => array("id" => $timesheet->getId())), $statusCode);


            // return array( $form, array("timesheet" => array("id" => $timesheet->getId() ) ) );

        }

        return V::create($form, 400);
    }


}