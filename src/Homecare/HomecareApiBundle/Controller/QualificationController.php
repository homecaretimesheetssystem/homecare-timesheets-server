<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 2/11/17
 * Time: 5:33 PM
 */

namespace Homecare\HomecareApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Homecare\HomecareBundle\Entity\Pca;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Homecare\HomecareBundle\Entity\Qualifications;


class QualificationController extends Controller
{


    /**
     * @return array
     * @View()
     * @ApiDoc(
     *  section="qualifications",
     *  output="array<Homecare\HomecareBundle\Entity\Qualifications>",
     *  resource=true,
     *  description="returns an array of qualification objects",
     *
     *
     *  statusCodes={
     *         200="Returned when successful",
     *         400="Bad request",
     *         404="not found",
     *         500="server error"
     *
     *     }
     *
     * )
     */
    public function getQualificationsAction(Request $request)
    {

        $qualifications = $this->getDoctrine()->getEntityManager()->getRepository(
            'HomecareHomecareBundle:Qualifications'
        )->findAll();

        return $qualifications;
    }


}