<?php

namespace Homecare\HomecareApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Homecare\HomecareBundle\Entity\User;
use Homecare\HomecareBundle\Entity\Pca;
use Homecare\HomecareBundle\Entity\Timesheet;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;


class RecipientPasswordController extends Controller
{

    /**
     * @View()
     */
    public function postRecipientpasswordAction(Request $request)
    {

        $jsonData = $request->getContent();

        //return $jsonData;

        $arrayOfData = json_decode($jsonData, true);

        $recipientId = $arrayOfData['recipientId'];
        $pcaId       = $arrayOfData['pcaId'];
        $password    = $arrayOfData['password'];


        //grab the recipient user
        $user = $this->getDoctrine()->getRepository("HomecareHomecareBundle:User")->getByRecipientId($recipientId);


        //return $user;

        //make sure the recipient user exists
        if ( ! $user) {
            return array(
                'error'             => 'invalidRecipient',
                'errorMessage'      => 'There is not a recipient user with that recipient Id',
                'postedRecipientId' => $recipientId,
            );
        }

        //make sure that the password sent up is correct
        $encoder = $this->container->get('security.password_encoder');
        if ( ! $encoder->isPasswordValid($user, $password)) {

            return array(
                'error'          => 'invalidPassword',
                'errorMessage'   => 'password is incorrect',
                'postedPassword' => $password,
            );

        }

        //make sure that specific pca is actually assigned the the recipient
        $em           = $this->getDoctrine()->getManager();
        $recipientPca = $em->getRepository('HomecareHomecareBundle:RecipientPca')->findBy(
            ['recipient' => $recipientId, 'pca' => $pcaId]
        );


        if ( ! $recipientPca) {
            return array(
                'error'        => 'invalidRecipientPcaCombination',
                'errorMessage' => 'that recipient has not been assigned to that pca',
                'recipientId'  => $recipientId,
                'pcaId'        => $pcaId,
            );
        }


        $statusCode = 200;

        return V::create(
            array("status" => "success", "message" => "recipient password has been successfully verified"),
            $statusCode
        );

    }

}