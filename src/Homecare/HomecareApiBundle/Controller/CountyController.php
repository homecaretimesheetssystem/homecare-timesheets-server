<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 2/24/17
 * Time: 11:38 PM
 */

namespace Homecare\HomecareApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Homecare\HomecareBundle\Entity\Pca;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class CountyController extends Controller
{


    /**
     *
     * @return array
     * @View()
     * @ApiDoc(
     *  section="county",
     *  output="array<Homecare\HomecareBundle\Entity\County>",
     *  resource=true,
     *  description="returns an array of county objects",
     *  statusCodes={
     *         200="Returned when successful",
     *         400="Bad request",
     *         404="not found",
     *         500="server error"
     *
     *     }
     *
     * )
     */
    public function getCountiesAction()
    {
        $em       = $this->getDoctrine()->getEntityManager();
        $counties = $em->getRepository("HomecareHomecareBundle:County")->findAll();

        return $counties;
    }


}