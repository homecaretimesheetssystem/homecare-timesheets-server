<?php

namespace Homecare\HomecareApiBundle\Controller;

use Homecare\HomecareBundle\Entity\Agency;
use Homecare\HomecareBundle\Entity\Pca;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Homecare\HomecareBundle\Entity\TimeIn;
use Homecare\HomecareBundle\Form\TimeInType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;


class TimeInApiController extends Controller
{


    /**
     * @View()
     */
    public function postCreateTimeInAction(Request $request)
    {

        return $this->createTimeInForm($request, new TimeIn());


    }


    /**
     * @View()
     */
    public function patchTimeinAction(TimeIn $timeIn)
    {

        return $this->updateTimeInForm($timeIn);

    }


    private function setMethod($statusCode, $timeIn = null)
    {

        switch ($statusCode) {

            case 201:
                return array('method' => 'POST');
                break;

            case 204:
                return array(
                    'action' => $this->generateUrl('patch_timein', array('timeIn' => $timeIn->getId())),
                    'method' => 'PATCH',
                );

                break;

        }

    }


    /**
     * @View()
     */
    private function updateTimeInForm(TimeIn $timeIn)
    {


        $statusCode = 204;

        $form = $this->createForm(new TimeInType(), $timeIn, $this->setMethod($statusCode, $timeIn));


        $form->handleRequest($this->getRequest());


        if ($form->isValid()) {

            $data = $form->getData();


            $em = $this->getDoctrine()->getManager();
            $em->persist($timeIn);
            $em->flush();

            $response = new Response();
            $response->setStatusCode($statusCode);

            $estimatedShiftLength = $data->getEstimatedShiftLength();

            $pca = $timeIn->getSessionData()->getPca();

            $billableHours = $this->getDoctrine()->getEntityManager()->getRepository(
                'HomecareHomecareBundle:TimeOut'
            )->countBillableHoursForPca(
                $pca
            );

            $seconds = ($billableHours * 60 * 60) + $estimatedShiftLength;

            return V::create(array('seconds' => (int)$seconds), 200);

        }

        return V::create($form, 400);
    }


    /**
     * @View()
     */
    private function createTimeInForm($request, TimeIn $timeIn)
    {


        $statusCode = 201;

        $form = $this->createForm(new TimeInType(), $timeIn, $this->setMethod($statusCode));


        $form->handleRequest($this->getRequest());


        if ($form->isValid()) {


            //$user->save();

            //$timesheet = $form->getData();

            $response = new Response();
            $response->setStatusCode(201);
            //return 'yo dog';

            $em = $this->getDoctrine()->getManager();
            $em->persist($timeIn);
            $em->flush();


            // set the `Location` header only when creating new resources
            /*
if (201 === $statusCode) {
    $response->headers->set('Location',
        $this->generateUrl(
            'acme_demo_user_get', array('id' => $user->getId()),
            true // absolute
        )
    );
}
            */
            //return V::create($form, 201);
            //return $response;

            //return array( $form, array("timesheet" => array("id" => $timesheet->getId() ) ) );


            return V::create(array("createdTimeIn" => array("id" => $timeIn->getId())), $statusCode);


            // return array( $form, array("timesheet" => array("id" => $timesheet->getId() ) ) );

        }

        return V::create($form, 400);
    }


}