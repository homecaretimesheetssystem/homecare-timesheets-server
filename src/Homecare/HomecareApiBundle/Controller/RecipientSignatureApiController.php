<?php

namespace Homecare\HomecareApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Homecare\HomecareBundle\Entity\RecipientSignature;
use Homecare\HomecareBundle\Form\RecipientSignatureType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;


class RecipientSignatureApiController extends Controller
{

    const STATUS_CODE_ALREADY_CREATED = 409;


    /**
     * @View()
     */
    public function postCreateRecipientSignatureAction(Request $request)
    {

        $timesheetId = (int) $request->request->get('homecare_homecarebundle_recipientsignature')['timesheet'];

        $timesheet = $this->getDoctrine()->getRepository('HomecareHomecareBundle:RecipientSignature')->findByTimesheet($timesheetId);


        if($timesheet) {
            return V::create(
                array("error_code" => 1001, "error_message" => "recipient signature already exists for this timesheet" ),
                self::STATUS_CODE_ALREADY_CREATED
            );
        }


        return $this->createRecipientSignatureForm($request, new RecipientSignature());

    }


    private function setMethod($statusCode)
    {

        switch ($statusCode) {

            case 201:
                return array('method' => 'POST');
                break;

        }


    }


    /**
     * @View()
     */
    private function createRecipientSignatureForm($request, RecipientSignature $recipientSignature)
    {


        $statusCode = 201;

        $form = $this->createForm(new RecipientSignatureType(), $recipientSignature, $this->setMethod($statusCode));


        $form->handleRequest($this->getRequest());


        if ($form->isValid()) {


            //$user->save();

            //$timesheet = $form->getData();

            $response = new Response();
            $response->setStatusCode(201);
            //return 'yo dog';

            $em = $this->getDoctrine()->getManager();
            $em->persist($recipientSignature);
            $em->flush();


            // set the `Location` header only when creating new resources
            /*
if (201 === $statusCode) {
    $response->headers->set('Location',
        $this->generateUrl(
            'acme_demo_user_get', array('id' => $user->getId()),
            true // absolute
        )
    );
}
            */
            //return V::create($form, 201);
            //return $response;

            //return array( $form, array("timesheet" => array("id" => $timesheet->getId() ) ) );


            return V::create(
                array("createdRecipientSignature" => array("id" => $recipientSignature->getId())),
                $statusCode
            );


            // return array( $form, array("timesheet" => array("id" => $timesheet->getId() ) ) );

        }

        return V::create($form, 400);
    }


}