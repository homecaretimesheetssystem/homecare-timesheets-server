<?php

namespace Homecare\HomecareApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Homecare\HomecareBundle\Entity\PcaSignature;
use Homecare\HomecareBundle\Form\PcaSignatureType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;


class PcaSignatureApiController extends Controller
{


    /**
     * @View()
     */
    public function postCreatePcaSignatureAction(Request $request)
    {


        return $this->createPcaSignatureForm($request, new PcaSignature());

    }


    private function setMethod($statusCode)
    {

        switch ($statusCode) {

            case 201:
                return array('method' => 'POST');
                break;

        }


    }


    /**
     * @View()
     */
    private function createPcaSignatureForm($request, PcaSignature $pcaSignature)
    {


        $statusCode = 201;

        $form = $this->createForm(new PcaSignatureType(), $pcaSignature, $this->setMethod($statusCode));


        $form->handleRequest($this->getRequest());


        if ($form->isValid()) {


            $response = new Response();
            $response->setStatusCode(201);

            $em = $this->getDoctrine()->getManager();
            $em->persist($pcaSignature);
            $em->flush();


            $recipientSignature = $pcaSignature->getTimesheet()->getRecipientSignature();

            // if no recipient signature exists then send an email to the recipient who needs to sign it
            if ( ! $recipientSignature) {
                $swiftMailer = $this->get('homecare_homecare.model.custom_swift_mailer');

                $recipient = $pcaSignature->getTimesheet()->getRecipient();

                $swiftMailer->sendTimesheetAvailableEmail($recipient);
            }


            return V::create(array("createdPcaSignature" => array("id" => $pcaSignature->getId())), $statusCode);


        }

        return V::create($form, 400);
    }


}