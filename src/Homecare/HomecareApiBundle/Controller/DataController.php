<?php

namespace Homecare\HomecareApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Homecare\HomecareBundle\Entity\User;
use Homecare\HomecareBundle\Entity\Pca;
use Homecare\HomecareBundle\Entity\Timesheet;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;


class DataController extends Controller
{


    /**
     * @View()
     */
    public function getDataAction(Request $request)
    {

        $token = $this->container->get('security.context')->getToken();
        if ( ! $token) {
            return ('anon.');
        }
        $user = $token->getUser();


        $em = $this->getDoctrine()->getManager();

        $user->setLastLogin(new \DateTime());
        $em->flush($user);


        if ($user->hasRole('ROLE_PCA')) {
            return $this->createPcaResponse($user);
        } elseif ($user->hasRole('ROLE_RECIPIENT')) {
            return $this->createRecipientResponse($user);
        } else {
            return new Response("user can not access the app without PCA or Recipient Roles");
        }


        //get the employees(pcas) object


    }


    private function createRecipientResponse($user)
    {


        $recipient = $user->getRecipient();
        // get all the pcas that are in charge of this recipient
        $recipientPcas = $recipient->getRecipientPca();

        $agency = $recipient->getAgency();


        $em = $this->getDoctrine()->getManager();

        // don't need the recipients that the pca is in charge of

        $recipientId = $recipient->getId();

        $arrayResponse = [];


        foreach ($recipientPcas as $rp) {
            $pcaId   = $rp->getPca()->getId();
            $results = $em->getRepository("HomecareHomecareBundle:SessionData")
                          ->getUnfinishedTimesheetsRelatedToRecipientAndPca($recipientId, $pcaId);


            if ($results) {
                foreach ($results as $result) {
                    $arrayResponse['sessionData'][] = $result;
                }
            }

        }


        // return $arrayResponse['sessionData'];

        //return $arrayResponse['sessionData'];

        //$arrayResponse['timesheets']  = $timesheets;
        //$arrayResponse['sessionData'] = ($timesheets) ? $timesheets[0]->getSessionData() : "";

        //get all the care options
        $careOptions = $em->getRepository("HomecareHomecareBundle:CareOptions")->findAll();
        $services    = $em->getRepository("HomecareHomecareBundle:Services")->findAll();


        //get the amazon url
        $amazonUrl = $this->container->getParameter('amazon_url');

        $arrayResponse['user']      = array(
            'id'               => $user->getId(),
            'userType'         => 'recipient',
            'email'            => $user->getEmail(),
            'skipVerification' => $recipient->getSkipVerification(),
        );
        $arrayResponse['recipient'] = $recipient;

        $arrayResponse['pcas']        = $recipientPcas;
        $arrayResponse['agency']      = $agency;
        $arrayResponse['amazonUrl']   = $amazonUrl;
        $arrayResponse['careOptions'] = $careOptions;
        $arrayResponse['services']    = $services;


        return $arrayResponse;

    }


    private function createPcaResponse($user)
    {

        $arrayResponse = [];

        $em = $this->getDoctrine()->getManager();

        $pca   = $user->getPca();
        $pcaId = $pca->getId();

        //get the agency object
        $agency = $pca->getAgency();

        //get the recipients object
        //get the recipients object
        $recipientPcas = $pca->getRecipientPca();
        $recipients    = [];
        foreach ($recipientPcas as $rp) {
            // get recipients
            $recipients[] = $rp->getRecipient();
            // get unfinished timesheets
            $recipientId = $rp->getRecipient()->getId();

            $results = $em->getRepository("HomecareHomecareBundle:SessionData")
                          ->getUnfinishedTimesheetsRelatedToRecipientAndPca($recipientId, $pcaId);


            if ($results) {
                foreach ($results as $result) {
                    $arrayResponse['sessionData'][] = $result;
                }
            }
        }


        //get all the care options
        $careOptions = $em->getRepository("HomecareHomecareBundle:CareOptions")->findAll();

        $ratios = $em->getRepository("HomecareHomecareBundle:Ratio")->findAll();

        $sharedCareLocations = $em->getRepository("HomecareHomecareBundle:SharedCareLocation")->findAll();


        //get any unfinished session timesheets so you can store it in the sessionTimesheets response
        /*
        $timesheets = $em->getRepository("HomecareHomecareBundle:Timesheet")->getUnfinishedTimesheets($pca->getId());

        $sessionData = ($timesheets) ? $timesheets[0]->getSessionData() : "";

        if ($sessionData) {
            $sessionData->setCurrentTimesheetId($timesheets[0]->getId());
        }

        */

        $services = $em->getRepository("HomecareHomecareBundle:Services")->findAll();

        //get the amazon url
        $amazonUrl = $this->container->getParameter('amazon_url');


        $arrayResponse['user']                = array(
            'id'       => $user->getId(),
            'userType' => 'pca',
            'email'    => $user->getEmail(),
        );
        $arrayResponse['pca']                 = $pca;
        $arrayResponse['agency']              = $agency;
        $arrayResponse['recipients']          = $recipients;
        $arrayResponse['careOptions']         = $careOptions;
        $arrayResponse['ratios']              = $ratios;
        $arrayResponse['sharedCareLocations'] = $sharedCareLocations;
        $arrayResponse['amazonUrl']           = $amazonUrl;
        $arrayResponse['services']            = $services;


        /*
        if ($timesheets) {
            //$arrayResponse['timesheets']  = $timesheets;
            $arrayResponse['sessionData'] = $sessionData;
        }
*/

        return $arrayResponse;
    }


}