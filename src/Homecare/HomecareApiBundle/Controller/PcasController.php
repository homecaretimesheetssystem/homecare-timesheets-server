<?php

namespace Homecare\HomecareApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Homecare\HomecareBundle\Entity\Pca;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class PcasController extends Controller
{


    /**
     * @param Pca $pca
     *
     * @return array
     * @View()
     * @ApiDoc(
     *  section="pca",
     *  output="Homecare\HomecareBundle\Entity\Pca",
     *  resource=true,
     *  description="returns a pca object",
     *  requirements={
     *      {
     *          "name"="pca",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the pca to get"
     *      }
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         400="Bad request",
     *         404="not found",
     *         500="server error"
     *
     *     }
     *
     * )
     */
    public function getPcasAction(Pca $pca)
    {
        return $pca;
    }


    /**
     * @View()
     * @param Request $request
     * @param Pca $pca
     * @ApiDoc(
     *  section="pca",
     *  description="Updates a Pca Object",
     *  requirements={
     *      {
     *          "name"="pca",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the pca to update"
     *      }
     *  },
     *  parameters={
     *      {"name"="firstName", "dataType"="string", "required"=false, "description"="first name of pca"},
     *      {"name"="lastName", "dataType"="string", "required"=false, "description"="last name of pca"},
     *      {"name"="homeAddress", "dataType"="string", "required"=false, "description"="home address of pca"},
     *      {"name"="available", "dataType"="boolean", "required"=false, "description"="whether or not pca is available to respond to requests"},
     *      {"name"="qualifications", "dataType"="array", "required"=false, "description"="array of qualification ids"},
     *      {"name"="counties", "dataType"="array", "required"=false, "description"="array of county ids that we are assigning to the Pca"},
     *      {"name"="gender", "dataType"="integer", "required"=false, "description"="1 for female and 2 for male"},
     *
     *  },
     *  statusCodes={
     *         204="Returned when successful",
     *         400="Bad request or validation failed",
     *         404="Not found",
     *         500="Server error"
     *
     *     }
     * )
     *
     * @return Response
     */
    public function patchPcasAction(Request $request, Pca $pca)
    {

        $form = $this->createForm(
            'homecare_profile',
            $pca,
            array(
                'action' => $this->generateUrl('patch_pcas', array('pca' => $pca->getId())),
                'method' => 'PATCH',
            )
        );

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getEntityManager();

            $pca = $form->getData();

            $em->persist($pca);
            $em->flush();

            $response = new Response();
            $response->setStatusCode(204);

            return $response;

        }

        return V::create($form, 404);


    }




    /**
     * @View()
     * @param Request $request
     * @param Pca $pca
     * @ApiDoc(
     *  section="pca",
     *  description="Post the pca profile photo",
     *  requirements={
     *      {
     *          "name"="pca",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the pca to update"
     *      }
     *  },
     *  parameters={
     *      {"name"="profilePhoto", "dataType"="file", "required"=true, "description"="use content type multipart/form-data"}
     *
     *  },
     *  statusCodes={
     *         204="Returned when successful",
     *         400="Bad request or validation failed",
     *         404="Not found",
     *         500="Server error"
     *
     *     }
     * )
     *
     * @return Response
     */
    public function postPcasImageAction(Request $request, Pca $pca)
    {

        //get all the files
        $files = $request->files->all();

        $file = $files["profilePhoto"];

        $uniqueFileName = $this->saveFileInDatabase($file, $pca);


        $this->saveFileToAmazonS3($uniqueFileName);

        return array("uploadedFileLocation" => $uniqueFileName);
    }


    private function saveFileInDatabase(UploadedFile $file, Pca $pca)
    {


        $dir = $this->get('kernel')->getRootDir().'/../web/uploads';

        //create the unique file name for the file being uploaded
        $uniqueFileName = $pca->getId().$this->getRandomString().time().'.'.$file->getClientOriginalExtension();

        //move the file
        $file->move($dir, $uniqueFileName);


        //save the file name in the database
        $em = $this->getDoctrine()->getManager();

        $pca->setProfilePhoto($uniqueFileName);

        $em->persist($pca);

        $em->flush();

        return $uniqueFileName;

    }


    private function saveFileToAmazonS3($uniqueFileName)
    {

        //save file to amazon S3
        $amazonS3 = $this->get('homecare.homecarebundle.amazons3');

        $amazonS3->submitFileToAmazonS3($uniqueFileName);

        $amazonS3->deleteTempFile($uniqueFileName);

    }


    private function getRandomString()
    {
        $length     = 5;
        $characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWZYZ";

        $real_string_length = strlen($characters);
        $string             = "id";

        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, $real_string_length - 1)];
        }

        return strtolower($string);
    }


}