<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 10/25/16
 * Time: 9:58 PM
 */

namespace Homecare\HomecareApiBundle\Controller;

use Hautelook\AliceBundle\Tests\Functional\TestBundle\Entity\Dev;
use Homecare\HomecareApiBundle\Data\ApiProblem;
use Homecare\HomecareBundle\Entity\Agency;
use Homecare\HomecareBundle\Entity\Device;
use Homecare\HomecareBundle\Entity\Pca;
use Homecare\HomecareBundle\Entity\Recipient;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Homecare\HomecareBundle\Entity\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request as HttpRequest;
use Homecare\HomecareBundle\Entity\Timesheet;
use Homecare\HomecareBundle\Entity\CareOptionTimesheet;
use Homecare\HomecareBundle\Form\CareOptionTimesheetType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;
use Doctrine\Common\Collections\ArrayCollection;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use paragraph1\phpFCM\Notification;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\RestBundle\Controller\Annotations\Get;


/**
 * Class RequestController
 * @package Homecare\HomecareApiBundle\Controllernew Notification('test title', 'testing body')
 */
class RequestController extends Controller
{


    /**
     * @param Pca $pca
     *
     * @return array
     * @Get("/pcas/{pca}/accepted_requests")
     * @View()
     * @ApiDoc(
     *  section="request",
     *  output="array<Homecare\HomecareBundle\Entity\Request>",
     *  resource=true,
     *  description="returns an array of Request objects that have been accepted by a pca",
     *  requirements={
     *      {
     *          "name"="pca",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the pca whom you want to get all the requests for"
     *      }
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         400="Bad request",
     *         404="not found",
     *         500="server error"
     *
     *     }
     *
     * )
     * )
     */
    public function getAcceptedRequestsAction(Pca $pca)
    {

        $em       = $this->getDoctrine()->getEntityManager();
        $requests = $em->getRepository("HomecareHomecareBundle:Request")->getAcceptedByPca($pca);

        return $requests;
    }


    /**
     * @param Pca $pca
     *
     * @return array
     * @Get("/pcas/{pca}/available_requests")
     * @View()
     * @ApiDoc(
     *  section="request",
     *  output="array<Homecare\HomecareBundle\Entity\Request>",
     *  resource=true,
     *  description="returns an array of Request objects available for a PCA to accept",
     *  requirements={
     *      {
     *          "name"="pca",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the pca whom you want to get all the available requests for"
     *      }
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         400="Bad request",
     *         404="not found",
     *         500="server error"
     *
     *     }
     *
     * )
     * )
     */
    public function getAvailableRequestsAction(Pca $pca)
    {

        $em       = $this->getDoctrine()->getEntityManager();
        $requests = $em->getRepository("HomecareHomecareBundle:Request")->getAvailableForPca($pca);

        return $requests;
    }


    /**
     * @param Recipient $recipient
     *
     * @return array
     *
     * @View()
     * @ApiDoc(
     *  section="request",
     *  output="array<Homecare\HomecareBundle\Entity\Request>",
     *  resource=true,
     *  description="returns an array of Request objects that are made by a given recipient",
     *  requirements={
     *      {
     *          "name"="recipient",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the recipient whom made the requests"
     *      }
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         400="Bad request",
     *         404="not found",
     *         500="server error"
     *
     *     }
     *
     * )
     * )
     */
    public function getRecipientRequestsAction(Recipient $recipient)
    {

        $em       = $this->getDoctrine()->getEntityManager();
        $requests = $em->getRepository("HomecareHomecareBundle:Request")->getRequestsByRecipient($recipient);

        return $requests;
    }


    /**
     * @param Request $request
     *
     * @return Request
     * @View()
     * @ApiDoc(
     *  section="request",
     *  output="Homecare\HomecareBundle\Entity\Request",
     *  resource=true,
     *  description="returns a specific request object",
     *  requirements={
     *      {
     *          "name"="request",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the request you want to return"
     *      }
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         400="Bad request",
     *         404="not found",
     *         500="server error"
     *
     *     }
     *
     * )
     * )
     */
    public function getRequestsAction(Request $request)
    {
        return $request;
    }


    /**
     * @View()
     * @param HttpRequest $request
     * @ApiDoc(
     *  section="request",
     *  description="Creates a request Object. Has a Location header in the response that indicates where the newly created resource lives",
     *  parameters={
     *      {"name"="recipient", "dataType"="integer", "required"=false, "description"="id of the recipient creating the on demand request"},
     *      {"name"="address", "dataType"="string", "required"=false, "description"="address of the recipient"},
     *      {"name"="genderPreference", "dataType"="string", "required"=false, "description"="gender preference of the request. 0 = no preference, 1 = female, and 2 = male (default is 0)"},
     *      {"name"="startShiftTime", "dataType"="string", "required"=false, "description"="the starting shift time"},
     *      {"name"="endShiftTime", "dataType"="string", "required"=false, "description"="the ending shift time"},
     *      {"name"="qualifications", "dataType"="array", "required"=false, "description"="array of qualification ids"},
     *      {"name"="yearsOfExperience", "dataType"="integer", "required"=false, "description"="years of experience"},
     *  },
     *  statusCodes={
     *         201="Returned when successful",
     *         400="Bad request or validation failed",
     *         500="Server error"
     *
     *     }
     *  )
     *
     * @return Response
     */
    public function postRequestsAction(HttpRequest $request)
    {


        $form = $this->createForm(
            'homecare_request'
        );

        $form->handleRequest($request);


        if ($form->isValid()) {

            $em      = $this->getDoctrine()->getEntityManager();
            $request = $form->getData();

            $recipient = $request->getRecipient();


            //gender preference set on request overrides that of the recipient
            $genderPreference = ($request->getGenderPreference() !== null) ? $request->getGenderPreference(
            ) : $recipient->getGenderPreference();
            if ( ! $request->getYearsOfExperience()) {
                $request->setYearsOfExperience(0);
            }

            if ( ! $recipient->getCounty()) {
                return $this->handleErrorResponse(ApiProblem::NO_RECIPIENT_COUNTY_ERROR);
            }

            $pcas = $this->getDoctrine()->getRepository("HomecareHomecareBundle:Pca")->getAllByAgencyAndCounty(
                $recipient->getAgency(),
                $recipient->getCounty(),
                $genderPreference
            );


            $em->persist($request);
            $em->flush();

            foreach ($pcas as $pca) {

                /*
                                $device = new Device();
                                $device->setDeviceToken(
                                    'ca3ENKVmYAk:APA91bGloky4K2i9ZPP0kBFGEZ-XpfbOKY_8s_hTU3LgnhvHAi0n9mUxGQb78_f2X4_WkaQ4Y3mbbbY4ZX6l91MKOEBmTnb7h0HLdViha1v1cARLpC7ps2HmfItqPpsko6xnZqGFR34_'
                                );
                                $this->sendForegroundPushNotification($device);
                */

                $devices = $pca->getUser()->getDevices();
                if (count($devices) == 0) {
                    continue;
                }
                foreach ($devices as $device) {
                    $this->sendForegroundPushNotification(
                        $device,
                        'New Pca Request Available',
                        array('push_type' => 1, 'request_id' => $request->getId())
                    );
                }
            }

            $response = new Response();
            $response->setStatusCode(201);

            $response->headers->set(
                'Location',
                $this->generateUrl('get_requests', array('request' => $request->getId()))
            );


            return $response;


        }


        $errors = ApiProblem::getErrorsFromForm($form);

        return $this->handleErrorResponse(ApiProblem::VALIDATION_ERROR, Response::HTTP_BAD_REQUEST, $errors);

    }


    /**
     * @View()
     * @param Request $request
     * @ApiDoc(
     *  section="request",
     *  description="deletes a request Object.",
     *  parameters={
     *      {"name"="request", "dataType"="integer", "required"=false, "description"="id of the request to delete"},
     *
     *  },
     *  statusCodes={
     *         204="Returned when successful",
     *         400="Bad request or validation failed",
     *         404="Not found",
     *         500="Server error"
     *
     *     }
     *  )
     *
     * @return Response
     */
    public function deleteRequestsAction(Request $request)
    {
        $em = $this->getDoctrine()->getEntityManager();


        foreach ($request->getRsvps() as $rsvp) {
            // if the rsvp assoicated with the request has a sessionData and a timeIn
            if ($rsvp->getSessionData() && $rsvp->getSessionData()->getTimeIn()) {
                $response = new JsonResponse(
                    array(
                        "pcaAlreadyTimedIn" => true,
                        "message"           => "Request could not be cancelled because the pca has already timed in",
                    )
                );
                $response->setStatusCode(200);

                return $response;
            }

        }


        $em->remove($request);
        $em->flush();

        // remove the sessionData attached to the RSVP and requests

        foreach ($request->getRsvps() as $rsvp) {
            if ($sessionData = $rsvp->getSessionData()) {
                $em->remove($sessionData);
                $em->flush();
            }

        }


        $rsvps = $request->getRsvps();


        foreach ($rsvps as $rsvp) {

            $pca     = $rsvp->getPca();
            $devices = $pca->getUser()->getDevices();
            if (count($devices) == 0) {
                continue;
            }
            foreach ($devices as $device) {
                $this->sendForegroundPushNotification(
                    $device,
                    'Recipient '.ucwords($request->getRecipient()->getRecipientName()).' Has Cancelled The Listing',
                    array(
                        'push_type'  => 3,
                        'request_id' => $request->getId(),
                    )
                );
            }
        }


        $response = new Response();
        $response->setStatusCode(204);

        return $response;

    }


    /**
     * This is For sending a push notification that is not silent
     *
     * If it is a Request added then send the notification
     * If it is a Request deleted then send a silent message
     *
     * @param Device $device
     * @param $notificationBody
     * @param array $data
     */
    private function sendForegroundPushNotification(Device $device, $notificationBody, $data = array())
    {
        $firebaseCloudMessagingAdapter = $this->get('homecare_homecare.data.firebase_cloud_messaging_adapter');
        $notification                  = new Notification('HCTS On Demand', $notificationBody);

        // get all the devices for the given user and send a push notification to them

        $firebaseCloudMessagingAdapter->sendToDevice($notification, $device, $data);


    }


    /**
     * This is For sending a push notification that is silent and in the background
     *
     * @param Pca $pca
     */
    private function sendBackgroundPushNotification(Pca $pca)
    {
        $firebaseCloudMessagingAdapter = $this->get('homecare_homecare.data.firebase_cloud_messaging_adapter');
        //$notification                  = new Notification('test title', 'testing body');

        // get all the devices for the given user and send a push notification to them
        //$devices = $pca->getUser()->getDevices();
        //foreach ($devices as $device) {
        //andriod => 'cZV8dvkzMzM:APA91bEo3reFklpNSg4Ke3Wkjn4Nw2RDzF49-xL6ckM7sFGVV50Pt_hmVdaXGp2c3p5hv8W6IoO-fXgL2J73Z5hegiKRnSDop6I4mxUXU7KacU89IONedtqtGHhe9o_575cEmLyBPX9q'


        $device = new Device();
        $device->setDeviceToken(
            '30adcdc706f531dd99ddd80e68ae2ea152471d16b71595442e309437f9b58268'
        );
        $firebaseCloudMessagingAdapter->sendToDevice(null, $device, array('someId' => 111));

    }


    /**
     * @param $code
     * @param $statusCode
     * @param array $errors
     *
     * @return JsonResponse
     */
    private function handleErrorResponse($code, $statusCode = 400, array $errors = array())
    {
        $apiProblem = new ApiProblem($code);

        $apiProblem->set('errors', $errors);

        $response = new JsonResponse(
            $apiProblem->toArray(),
            $statusCode

        );
        $response->headers->set('Content-Type', 'application/problem+json');

        return $response;
    }


}
