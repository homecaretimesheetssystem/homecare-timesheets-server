<?php

namespace Homecare\HomecareApiBundle\Controller;

use Homecare\HomecareBundle\Entity\Pca;
use Homecare\HomecareBundle\Entity\Recipient;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Homecare\HomecareBundle\Entity\Verification;
use Homecare\HomecareBundle\Form\VerificationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;


class VerificationApiController extends Controller
{

    private $verificationNeeded = false;


    /**
     * @View()
     */
    public function getPcaVerifyAction(Pca $pca, Recipient $recipient)
    {

        //return array('pca' => $pca->getId(), 'recipient' => $recipient->getId());

        //return array('pca' => $pca, 'recipient' => $recipient);

        $em           = $this->getDoctrine()->getManager();
        $verification = $em->getRepository("HomecareHomecareBundle:Verification")->getVerificationForPcaAndRecipient(
            $pca,
            $recipient
        );


        if (true == $recipient->getSkipVerification()) {
            return V::create(array("verification_needed" => "false"), 200);
        }


        if ( ! $verification) {
            return V::create(array("verification_needed" => "false"), 200);
        }


        if ( ! empty($verification[0]->getVerificationDate()) && $verification[0]->getVerificationDate(
            ) instanceof \DateTime
        ) {
            $verificationDate = $verification[0]->getVerificationDate()->format('Y-m-d');
            $date             = new \DateTime();
            $currentDate      = $date->format('Y-m-d');
            if ($currentDate >= $verificationDate) {
                $this->verificationNeeded = true;
            }
        }

        // if it is the pca's first time choosing this recipient
        if ($verification[0]->getfirstTime()) {
            $this->verificationNeeded = true;
        }

        if ($this->verificationNeeded) {
            return V::create(array("verification_needed" => true, "id" => $verification[0]->getId()), 200);

        }


        return V::create(array("verification_needed" => "false"), 200);

    }


    /**
     * @View()
     */
    public function postVerifcationPhotoAction(Request $request, Verification $verification)
    {

        //get all the files
        $files = $request->files->all();

        $file = $files["verificationPhoto"];

        $uniqueFileName = $this->saveFileInDatabase($file, $verification);


        $this->saveFileToAmazonS3($uniqueFileName);

        $verificationService = $this->get('homecare_homecare.model.verification');
        $verificationService->setVerification($verification);

        $em = $this->getDoctrine()->getManager();
        $em->persist($verification);
        $em->flush();


        return array("uploadedFileLocation" => $uniqueFileName);
    }


    private function saveFileInDatabase(UploadedFile $file, $verification)
    {


        $dir = $this->get('kernel')->getRootDir().'/../web/uploads';

        //create the unique file name for the file being uploaded
        $uniqueFileName = $verification->getId().$this->getRandomString().time().'.'.$file->getClientOriginalExtension(
            );

        //move the file
        $file->move($dir, $uniqueFileName);


        //save the file name in the database
        $em = $this->getDoctrine()->getManager();

        $verification->setVerificationPhoto($uniqueFileName);


        $em->persist($verification);

        $em->flush();

        return $uniqueFileName;

    }


    private function saveFileToAmazonS3($uniqueFileName)
    {

        //save file to amazon S3
        $amazonS3 = $this->get('homecare.homecarebundle.amazons3');

        $amazonS3->submitFileToAmazonS3($uniqueFileName);

        $amazonS3->deleteTempFile($uniqueFileName);

    }


    private function getRandomString()
    {
        $length     = 5;
        $characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWZYZ";

        $real_string_length = strlen($characters);
        $string             = "id";

        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, $real_string_length - 1)];
        }

        return strtolower($string);
    }


    /**
     * @View()
     */
    public function patchVerifyAction(Request $request, Verification $verification)
    {
        //return $verification->getId();

        return $this->pcaVerificationForm($request, $verification);
    }


    /**
     * @View()
     */
    private function pcaVerificationForm($request, Verification $verification)
    {


        $statusCode = 204;

        $form = $this->createForm(
            new VerificationType(),
            $verification,
            array(
                'action' => $this->generateUrl('patch_verify', array('verification' => $verification->getId())),
                'method' => 'PATCH',
            )
        );

        $form->handleRequest($request);

        if ($form->isValid()) {


            $verificationService = $this->get('homecare_homecare.model.verification');
            $verificationService->setVerification($verification);
            $verificationService->setRecipientPca($verification->getRecipientPca());
            $verificationService->setNonFirstTimeVerification($verification);

            $response = new Response();
            $response->setStatusCode(201);

            $em = $this->getDoctrine()->getManager();
            $em->persist($verification);
            $em->flush();

            return V::create(
                array(
                    "message" => "you have successfully verified the pca and recipient",
                    array("verification id" => $verification->getId()),
                ),
                $statusCode
            );

        }

        return V::create($form, 400);
    }


}