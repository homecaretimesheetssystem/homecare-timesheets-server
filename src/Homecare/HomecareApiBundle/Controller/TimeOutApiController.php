<?php

namespace Homecare\HomecareApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Homecare\HomecareBundle\Entity\TimeOut;
use Homecare\HomecareBundle\Form\TimeOutType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;


class TimeOutApiController extends Controller
{
	
	
	
	/**
	* @View()
	*/
    public function postCreateTimeOutAction( Request $request )
    {
		
			return $this->createTimeOutForm( $request, new TimeOut() );
		
		}
	
	
	
	
		/**
		* @View()
		*/
	    public function patchTimeoutAction( TimeOut $timeOut )
	    {
			
			return $this->updateTimeOutForm( $timeOut );
			
		 }
	
	
	
	
	
	
		private function setMethod( $statusCode, $timeOut = null ){
			
		   switch( $statusCode ) {
				
				case 201:
				return array( 'method' => 'POST' );
				break;
				
				case 204:
				return array( 
					'action' => $this->generateUrl('patch_timeout', array('timeOut' => $timeOut->getId())),
					'method' => 'PATCH' 
				);
				break;
				
			}
			
		}
	
	
	
	
	
	
	
	
				/**
				* @View()
				*/
				private function updateTimeOutForm( TimeOut $timeOut )
				    {
							
						
				        $statusCode = 204;

				        $form = $this->createForm(new TimeOutType(), $timeOut, $this->setMethod( $statusCode, $timeOut ) );
							 
							 
							 
							 $form->handleRequest( $this->getRequest() );
								
								
				        if ($form->isValid()) {
									
									  
					 				$em = $this->getDoctrine()->getManager();
									$em->persist( $timeOut );
			            $em->flush();

				            $response = new Response();
				            $response->setStatusCode( $statusCode );

				            // set the `Location` header only when creating new resources
										/*
				            if (201 === $statusCode) {
				                $response->headers->set('Location',
				                    $this->generateUrl(
				                        'acme_demo_user_get', array('id' => $user->getId()),
				                        true // absolute
				                    )
				                );
				            }
*/
				            return $response;
				        }

				        return V::create($form, 400);
				    }
		
	
	
	
	
	
	
	
	
	
	
	
			/**
			* @View()
			*/
		  private function createTimeOutForm( $request, TimeOut $timeOut )
		     {
		         
					  
					  $statusCode = 201;
				
						$form = $this->createForm( new TimeOutType(), $timeOut, $this->setMethod( $statusCode ) );
		 
		 
		         $form->handleRequest( $this->getRequest() );
						 
						 
						 

		         if ( $form->isValid() ) {
							 
							 
							 
		             //$user->save();

								 //$timesheet = $form->getData();

		             $response = new Response();
		             $response->setStatusCode(201);
				 //return 'yo dog';
				 
				 				$em = $this->getDoctrine()->getManager();
								$em->persist( $timeOut );
		            $em->flush();
				 

		             // set the `Location` header only when creating new resources
								 /*
		             if (201 === $statusCode) {
		                 $response->headers->set('Location',
		                     $this->generateUrl(
		                         'acme_demo_user_get', array('id' => $user->getId()),
		                         true // absolute
		                     )
		                 );
		             }
								 */
	           //return V::create($form, 201);
		             //return $response;
								 
					 //return array( $form, array("timesheet" => array("id" => $timesheet->getId() ) ) );
					 
		        

return V::create( array("createdTimeOut" => array("id" => $timeOut->getId() ) ), $statusCode );
								 
					 
								// return array( $form, array("timesheet" => array("id" => $timesheet->getId() ) ) );
					 
		         }

		         return V::create($form, 400);
		     }
	
	
	
	
	
	
	
	
}