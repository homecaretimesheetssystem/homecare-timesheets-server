<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 2/24/17
 * Time: 11:38 PM
 */

namespace Homecare\HomecareApiBundle\Controller;

use Homecare\HomecareBundle\Entity\Recipient;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Homecare\HomecareBundle\Entity\Pca;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class RecipientController extends Controller
{


    /**
     * @param Recipient $recipient
     *
     * @return array
     * @View()
     * @ApiDoc(
     *  section="recipient",
     *  output="Homecare\HomecareBundle\Entity\Recipient",
     *  resource=true,
     *  description="returns a recipient object",
     *  requirements={
     *      {
     *          "name"="recipient",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the recipient to get"
     *      }
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         400="Bad request",
     *         404="not found",
     *         500="server error"
     *
     *     }
     *
     * )
     */
    public function getRecipientsAction(Recipient $recipient)
    {
        return $recipient;
    }


    /**
     * @View()
     * @param Request $request
     * @param Recipient $recipient
     * @ApiDoc(
     *  section="recipient",
     *  description="Updates a Recipient Object",
     *  requirements={
     *      {
     *          "name"="recipient",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the recipient to update"
     *      }
     *  },
     *  parameters={
     *      {"name"="firstName", "dataType"="string", "required"=false, "description"="first name of recipient"},
     *      {"name"="lastName", "dataType"="string", "required"=false, "description"="last name of recipient"},
     *      {"name"="maNumber'", "dataType"="string", "required"=false, "description"="ma number or the recipient"},
     *      {"name"="genderPreference", "dataType"="integer", "required"=false, "description"="Overrides the gender preference on the request. 0 = no preference, 1 = female, 2 = male"},
     *      {"name"="companyAssignedId", "dataType"="string", "required"=false, "description"="The company assigned id to the recipient"},
     *      {"name"="skipVerification", "dataType"="boolean", "required"=false, "description"="Whether or not to skip verification on the recipient"},
     *      {"name"="skipGps", "dataType"="boolean", "required"=false, "description"="Whether or not to skip gps on the recipient"},
     *      {"name"="user", "dataType"="integer", "required"=false, "description"="The user id being assigned to this recipient"},
     *      {"name"="county", "dataType"="integer", "required"=false, "description"="The county id that this user lives in"},
     *
     *
     *  },
     *  statusCodes={
     *         204="Returned when successful",
     *         400="Bad request or validation failed",
     *         404="Not found",
     *         500="Server error"
     *
     *     }
     * )
     *
     * @return Response
     */
    public function patchRecipientsAction(Request $request, Recipient $recipient)
    {

        $form = $this->createForm(
            'homecare_recipient',
            $recipient,
            array(
                'action' => $this->generateUrl('patch_recipients', array('recipient' => $recipient->getId())),
                'method' => 'PATCH',
            )
        );

        $form->handleRequest($request);

        if ($form->isValid()) {

            $em = $this->getDoctrine()->getEntityManager();

            $recipient = $form->getData();

            $em->persist($recipient);
            $em->flush();

            $response = new Response();
            $response->setStatusCode(204);

            return $response;

        }

        return V::create($form, 404);


    }


}