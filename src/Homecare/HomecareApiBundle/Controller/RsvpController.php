<?php
/**
 * Created by PhpStorm.
 * User: joshuacrawmer
 * Date: 10/25/16
 * Time: 9:58 PM
 */

namespace Homecare\HomecareApiBundle\Controller;

use Ambta\DoctrineEncryptBundle\Encryptors\Rijndael256Encryptor;
use Homecare\HomecareApiBundle\Data\ApiProblem;
use Homecare\HomecareBundle\Entity\Device;
use Homecare\HomecareBundle\Entity\Pca;
use Homecare\HomecareBundle\Entity\RecipientPca;
use Homecare\HomecareBundle\Entity\Rsvp;
use Homecare\HomecareBundle\Entity\SessionData;
use Homecare\HomecareBundle\Entity\SharedCareLocation;
use paragraph1\phpFCM\Notification;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Homecare\HomecareBundle\Entity\Request;
use Symfony\Component\HttpFoundation\Request as HttpRequest;
use Homecare\HomecareBundle\Entity\Timesheet;
use Homecare\HomecareBundle\Entity\CareOptionTimesheet;
use Homecare\HomecareBundle\Form\CareOptionTimesheetType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use JMS\Serializer\SerializerBuilder;
use FOS\RestBundle\View\View as V;
use Doctrine\Common\Collections\ArrayCollection;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


/**
 * Class RsvpController
 * @package Homecare\HomecareApiBundle\Controller
 */
class RsvpController extends Controller
{


    /**
     * @param Rsvp $rsvp
     *
     * @return Request
     * @View()
     * @ApiDoc(
     *  section="rsvp",
     *  output="Homecare\HomecareBundle\Entity\Rsvp",
     *  resource=true,
     *  description="returns a specific rsvp object",
     *  requirements={
     *      {
     *          "name"="rsvp",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the rsvp you want to return"
     *      }
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         400="Bad request",
     *         404="not found",
     *         500="server error"
     *
     *     }
     *
     * )
     * )
     */
    public function getRsvpAction(Rsvp $rsvp)
    {
        return $rsvp;
    }


    /**
     * @param Pca $pca
     *
     * @return Response
     *
     * @View()
     * @ApiDoc(
     *  section="rsvp",
     *  output="array<Homecare\HomecareBundle\Entity\Rsvp>",
     *  resource=true,
     *  description="returns an array of all rsvp objects for a given pca",
     *  requirements={
     *      {
     *          "name"="pca",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the pca that you want to return all the rsvps for"
     *      }
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         400="Bad request",
     *         404="not found",
     *         500="server error"
     *
     *     }
     *
     * )
     * )
     */
    public function getPcaRsvpsAction(Pca $pca)
    {

        $em    = $this->getDoctrine()->getEntityManager();
        $rsvps = $em->getRepository("HomecareHomecareBundle:Rsvp")->getAllByPca($pca);

        return $rsvps;
    }


    /**
     * @param Request $request
     *
     * @return Response
     * @View()
     * @ApiDoc(
     *  section="rsvp",
     *  output="array<Homecare\HomecareBundle\Entity\Rsvp>",
     *  resource=true,
     *  description="returns an array of all rsvp objects for a given request created by a recipient",
     *  requirements={
     *      {
     *          "name"="request",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the request that you want to return all the rsvps for"
     *      }
     *  },
     *  statusCodes={
     *         200="Returned when successful",
     *         400="Bad request",
     *         404="not found",
     *         500="server error"
     *
     *     }
     *
     * )
     * )
     */
    public function getRequestsRsvpsAction(Request $request)
    {

        $em    = $this->getDoctrine()->getEntityManager();
        $rsvps = $em->getRepository("HomecareHomecareBundle:Rsvp")->getAllByRequest($request);

        return $rsvps;
    }


    /**
     * @View()
     * @param HttpRequest $request
     * @param Rsvp $rsvp
     *
     * @return Response
     * @ApiDoc(
     *  section="rsvp",
     *  description="Updates an Rsvp Object.",
     *  parameters={
     *      {"name"="pca", "dataType"="integer", "required"=false, "description"="id of the pca that is creating an rsvp/accepting the request"},
     *      {"name"="eta", "dataType"="string", "required"=false, "description"="estimated time of arrival until the pca gets to the recipient's house"},
     *      {"name"="accepted", "dataType"="boolean", "required"=false, "description"="set to true if the recipient accepts this rsvp"},
     *      {"name"="declined", "dataType"="boolean", "required"=false, "description"="set to false if the recipient declines the rsvp"},
     *      {"name"="cancellationReason", "dataType"="string", "required"=false, "description"="the cancellation reason"},
     *      {"name"="request", "dataType"="integer", "required"=false, "description"="the id of the request object that this rsvp if for"},
     *
     *  },
     *  statusCodes={
     *         204="Returned when successful",
     *         400="Bad request or validation failed",
     *         404="Not found",
     *         500="Server error"
     *
     *     }
     *  )
     *
     */
    public function patchRsvpsAction(HttpRequest $request, Rsvp $rsvp)
    {

        $form = $this->createForm(
            'homecare_rsvp',
            $rsvp,
            array(
                'action' => $this->generateUrl('patch_rsvps', array('rsvp' => $rsvp->getId())),
                'method' => 'PATCH',
            )
        );

        $form->handleRequest($request);


        if ($form->isValid()) {

            $em = $this->getDoctrine()->getEntityManager();

            $rsvp = $form->getData();

            $em->persist($rsvp);
            $em->flush();

            $response = new Response();
            $response->setStatusCode(204);

            return $response;

        }

        return V::create($form, 400);

    }


    /**
     * @View()
     * @param HttpRequest $request
     * @ApiDoc(
     *  section="rsvp",
     *  description="Creates an Rsvp Object. Has a Location header in the response that indicates where the newly created resource lives",
     *  parameters={
     *      {"name"="pca", "dataType"="integer", "required"=false, "description"="id of the pca that is creating an rsvp/accepting the request"},
     *      {"name"="eta", "dataType"="string", "required"=false, "description"="estimated time of arrival until the pca gets to the recipient's house"},
     *      {"name"="accepted", "dataType"="boolean", "required"=false, "description"="set to true if the recipient accepts this rsvp"},
     *      {"name"="declined", "dataType"="boolean", "required"=false, "description"="set to false if the recipient declines the rsvp"},
     *      {"name"="cancellationReason", "dataType"="string", "required"=false, "description"="the cancellation reason"},
     *      {"name"="request", "dataType"="integer", "required"=false, "description"="the id of the request object that this rsvp if for"},
     *
     *  },
     *  statusCodes={
     *         201="Returned when successful",
     *         400="Bad request or validation failed",
     *         500="Server error"
     *
     *     }
     *  )
     *
     * @return Response
     */
    public function postRsvpsAction(HttpRequest $request)
    {

        $form = $this->createForm(
            'homecare_rsvp'
        );

        $form->handleRequest($request);


        if ($form->isValid()) {

            $em = $this->getDoctrine()->getEntityManager();

            $rsvp = $form->getData();

            // make sure no one else has accepted the request
            // if someone else has accepted the request thejust return
            $allPreviousRsvps = $rsvp->getRequest()->getRsvps();
            foreach ($allPreviousRsvps as $previousRsvp) {
                if ($previousRsvp->getAccepted() == true) {
                    $response = new JsonResponse(array("accepted" => false));
                    $response->setStatusCode(200);

                    return $response;
                }
            }


            // if the code makes it this far then someone has not accepted the request yet
            // create a timesheet with some of the properties pre set
            $sessionData = new SessionData();
            $pca         = $rsvp->getPca();
            $sessionData->setPca($rsvp->getPca());
            $sessionData->setContinueTimesheetNumber(4);
            $encryptor = new Rijndael256Encryptor('ABCDEFGHIJKLMNOPQRSTUVWXZY123456');


            $service = $this->getDoctrine()->getRepository("HomecareHomecareBundle:Services")->findOneBy(
                array('serviceName' => $encryptor->encrypt('personal care service'))
            );

            $ratio = $this->getDoctrine()->getRepository("HomecareHomecareBundle:Ratio")->findOneBy(
                array('ratio' => $encryptor->encrypt('1:1'))
            );

            $sharedCareLocation = $this->getDoctrine()->getRepository(
                "HomecareHomecareBundle:SharedCareLocation"
            )->findOneBy(
                array('location' => 'N/A')
            );

            $sessionData->setService($service);
            $sessionData->setSharedCareLocation($sharedCareLocation);
            $sessionData->setRatio($ratio);

            $timesheet = new Timesheet();
            $recipient = $rsvp->getRequest()->getRecipient();
            $timesheet->setRecipient($rsvp->getRequest()->getRecipient());
            $timesheet->setSessionData($sessionData);
            $em->persist($sessionData);
            $em->flush();
            $em->persist($timesheet);
            $em->flush();


            $rsvp->setAccepted(true);
            $rsvp->setSessionData($sessionData);
            $em->persist($rsvp);
            $em->flush();


            $assignment = $em->getRepository("HomecareHomecareBundle:RecipientPca")->findOneBy(
                array(
                    'recipient' => $recipient->getId(),
                    'pca'       => $pca->getId(),

                )
            );


            // if no assignment exists then create one
            if ( ! $assignment) {

                // create the temporary assignment
                $assignment = new RecipientPca();
                $assignment->setPca($pca);
                $assignment->setRecipient($recipient);
                $assignment->setIsTemp(true);
                $em->persist($assignment);
                $em->flush();
            }


            $recipient = $rsvp->getRequest()->getRecipient();
            $devices   = $recipient->getUser()->getDevices();
            foreach ($devices as $device) {
                $this->sendForegroundPushNotification(
                    $device,
                    'Pca '.ucwords($rsvp->getPca()->getPcaName()).' Has Accepted Your Listing',
                    array('push_type' => 2, 'request_id' => $rsvp->getRequest()->getId())
                );
            }


            $response = new JsonResponse(array("accepted" => true, 'rsvp_id' => $rsvp->getId()));
            $response->setStatusCode(201);
            $response->headers->set(
                'Location',
                $this->generateUrl('get_rsvp', array('rsvp' => $rsvp->getId()))
            );

            return $response;


        }

        return V::create($form, 400);

    }


    /**
     * @View()
     * @param Rsvp $rsvp
     *
     * @return Response
     * @internal param Request $request
     * @ApiDoc(
     *  section="rsvp",
     *  description="deletes a rsvp Object.",
     *  parameters={
     *      {"name"="rsvp", "dataType"="integer", "required"=false, "description"="id of the rsvp to delete"},
     *
     *  },
     *  statusCodes={
     *         204="Returned when successful",
     *         400="Bad request or validation failed",
     *         404="Not found",
     *         500="Server error"
     *
     *     }
     *  )
     *
     */
    public function deleteRsvpAction(Rsvp $rsvp)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $sessionData = $rsvp->getSessionData();


        // if the rsvp already has a session and the PCA has timed in
        // then don't let the pca delete/cancel the RSVP
        if ($sessionData->getTimeIn()) {
            $response = new JsonResponse(
                array(
                    "pcaAlreadyTimedIn" => true,
                    "message"           => "RSVP could not be cancelled because the pca has already timed in",
                )
            );
            $response->setStatusCode(200);

            return $response;
        }


        $recipient = $rsvp->getRequest()->getRecipient();
        $devices   = $recipient->getUser()->getDevices();
        foreach ($devices as $device) {
            $this->sendForegroundPushNotification(
                $device,
                'Pca '.ucwords($rsvp->getPca()->getPcaName()).' Has Cancelled',
                array('push_type' => 4, 'request_id' => $rsvp->getRequest()->getId())
            );
        }


        $em->remove($rsvp);
        $em->remove($sessionData);
        $em->flush();


        $response = new Response();
        $response->setStatusCode(204);

        return $response;

    }


    /**
     * This is For sending a push notification that is not silent
     *
     * If it is a Request added then send the notification
     * If it is a Request deleted then send a silent message
     *
     *
     * @param Device $device
     * @param $notificationBody
     * @param array $data
     */
    private function sendForegroundPushNotification(Device $device, $notificationBody, $data = array())
    {
        $firebaseCloudMessagingAdapter = $this->get('homecare_homecare.data.firebase_cloud_messaging_adapter');
        $notification                  = new Notification('HCTS On Demand', $notificationBody);

        // get all the devices for the given user and send a push notification to them

        $firebaseCloudMessagingAdapter->sendToDevice($notification, $device, $data);


    }


    /**
     * @param array $errors
     *
     * @param $type
     *
     * @return JsonResponse
     */
    private function handleErrorResponse($type, array $errors)
    {
        $apiProblem = new ApiProblem(
            400,
            $type
        );
        $apiProblem->set('errors', $errors);


        $response = new JsonResponse(
            $apiProblem->toArray(),
            $apiProblem->getStatusCode()
        );
        $response->headers->set('Content-Type', 'application/problem+json');

        return $response;
    }


}