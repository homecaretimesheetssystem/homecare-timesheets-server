Homecare Timesheets Version 1.0 04/07/2015

GENERAL USAGE NOTES
---------------------

- The Homecare Timesheets website is completely reliant on the Ios/Android mobile app for sending data to a backend api for processing and data storage.
- The Homecare Timesheets website and api is built upon the symfony framework stack and a lot of its core components. 
- The Homecare Timesheets website is hosted on https://www.heroku.com 
- The Homecare Timesheets database is hosted on a separate server in the cloud by http://www.cleardb.com with nightly backups and the ability to rollback to a previous database version
- The Homecare Timesheets pdf files, csv files, uploaded images from the mobile app, and more are hosted on a separate server in the cloud by http://aws.amazon.com/s3
- The Homecare Timesheets website is version controlled with git
- The Homecare Timesheets website is also remotely version controlled by heroku and previous builds can be rolledback in a matter of seconds
- The Homecare Timesheets is developed locally on a MAMP stack

Homecare Timesheets can be reached at:

website: http://www.homecaretimesheetsapp.com
website 2: http://www.pcapartners.net
email: andre@pcapartners.net
Phone: 612-868-4512
Fax: 763-592-7960



Installing Locally 
-------------------

Fork and clone this repo into your server root. Create database 'hc-timesheets-demo' in PHPMYADMIN. DONT create any tables for it though! Run command 
$app/console doctrine:migrations:migrate 
from the command line, in your root symfony folder so your database is up to date. Boot up your server any visit localhost. Thats it!


API USAGE
----------


client id 41_49v7oce2cv8kg4go8wcc8co4gsgw4sosggcsk4k0wk8o0sgko0, client secret mogyrunrhqo8k8cw04c0k8c4880go88go8gogckoso0s00o0k




MUST AUTHENTICATE VIA OAUTH BEFORE YOU CAN USE ANY OF THE BELOW ENDPOINTS!

1. This is the endpoint to get a fresh access token: (Must have a Client Id Key and a Client Secret Key)

curl -X GET 'http://homecare-timesheet.herokuapp.com/oauth/v1/token?client_id=[clientId]&client_secret=[clientSecret]&grant_type=password&username=employee1&password=employee1'

Notes:
1. Send as get request
2. make sure to get the client Id and client secret from Josh Crawmer on Slack
3. Response will contain the access token



---------------------------------------------------------
FOR EVERY ENDPOINT BELOW YOU NEED TO SET THE AUTHORIZATION BEARER HEADER AND ATTACH THE ACCESS TOKEN TO IT

example endpoint: curl -X GET http://exampleendpoint.com/getUsers -H "Authorization: Bearer [ACCESS_TOKEN_HERE]”
---------------------------------------------------------




live website get all data for a recipient or pca-

curl -H "Accept: application/json” -X GET http://homecare-timesheet.herokuapp.com/api/v1/data

Notes:
1. Set accept headers to application/json
3. Set the method to GET
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/data
5. response is json





live website create blank new timesheet -

curl -H "Accept: application/json" -H "Content-type: application/json" -i -X POST -d '{"homecare_homecarebundle_timesheet":""}' http://homecare-timesheet.herokuapp.com/api/v1/create/timesheet

Notes:
1. Set accept headers to application/json
2. Set content type to appliation/json
3. Set the method to POST
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/create/timesheet
5. the json is '{"homecare_homecarebundle_timesheet":""}'
6. The response is the id of the empty timesheet table row created. You will need this for future requests





live website update timesheet PATCH request

curl -i -H "Accept: application/json" -H "Content-type: application/json" -d '{"homecare_homecarebundle_timesheet":{"recipient":"631","finished":"1","sessionData":"23"}}' -X PATCH  http://homecare-timesheet.herokuapp.com/api/v1/timesheet/1741

Notes:
1. Set accept headers to application/json
2. Set content type to appliation/json
3. Set the method to PATCH
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/timesheet/311
(make sure the endpoint has the timesheet id at the end of the url. You got this id from the response from creating an empty timesheet table row on the previous call)
5. the json is 
'{"homecare_homecarebundle_timesheet":{"recipient":"631","finished":"1","sessionData":"23"}}'
6. You dont have to send up recipient, sessionData, and finished all at the same time. In fact you shouldnt! You dont want to set finished to 1 until the timesheet is finished!. But you have to send them all up at some point for each timesheet created!!
7. The response is 204 on success






live website create time in with POST

curl -H "Accept: application/json" -H "Content-type: application/json" -i -X POST -d '{"homecare_homecarebundle_timein":""}' http://homecare-timesheet.herokuapp.com/api/v1/create/time/in

Notes:
1. Set accept headers to application/json
2. Set content type to appliation/json
3. Set the method to POST
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/create/time/in
5. the json is '{"homecare_homecarebundle_timein":""}'
6. The response is the id of the empty time in table row created. You will need this for future requests





live website update time in with PATCH

curl -H "Accept: application/json" -H "Content-type: application/json" -i -X PATCH -d '{"homecare_homecarebundle_timein":{"timeInAddress":"1752 sequoia lane New Richmond WI 54017","estimatedShiftLength":"43433233","latitude":"-20.43338382","longitude":"40.98765346","timeIn":"2015-03-03 10:20:33","timeInPictureTime":"2015-03-03 10:20:33"}}' http://homecare-timesheet.herokuapp.com/api/v1/timein/1

Notes:
1. Set accept headers to application/json
2. Set content type to appliation/json
3. Set the method to PATCH
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/timein/1
(make sure the endpoint has the time in id at the end of the url. You got this id from the response from creating an empty time in table row on the previous call)
5. the json is '{"homecare_homecarebundle_timein":{"timeInAddress":"1752 sequoia lane New Richmond WI 54017","estimatedShiftLength":"43433233","latitude":"-20.43338382","longitude":"40.98765346","timeIn":"2015-03-03 10:20:33","timeInPictureTime":"2015-03-03 10:20:33"}}'
6. You dont have to send up every json value at once but you have to send them up at some point in time
7. The response is 204 on success





live website create time out with POST

curl -H "Accept: application/json" -H "Content-type: application/json" -i -X POST -d '{"homecare_homecarebundle_timeout":""}' http://homecare-timesheet.herokuapp.com/api/v1/create/time/out

Notes:
1. Set accept headers to application/json
2. Set content type to appliation/json
3. Set the method to POST
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/create/time/out
5. the json is '{"homecare_homecarebundle_timeout":""}'
6. The response is the id of the empty time out table row created. You will need this for future requests





live website update time out with PATCH

curl -H "Accept: application/json" -H "Content-type: application/json" -i -X PATCH -d '{"homecare_homecarebundle_timeout":{"timeOutAddress":"1752 sequoia lane New Richmond WI 54017","latitude":"-20.43338382","longitude":"40.98765346","timeOut":"2015-03-03 10:20:33","timeOutPictureTime":"2015-03-03 10:20:33","totalHours":"5 hours","billableHours":"45 min"}}' http://homecare-timesheet.herokuapp.com/api/v1/timeout/1

Notes:
1. Set accept headers to application/json
2. Set content type to appliation/json
3. Set the method to PATCH
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/timeout/1
(make sure the endpoint has the time out id at the end of the url. You got this id from the response from creating an empty time out table row on the previous call)
5. the json is '{"homecare_homecarebundle_timeout":{"timeOutAddress":"1752 sequoia lane New Richmond WI 54017","latitude":"-20.43338382","longitude":"40.98765346","timeOut":"2015-03-03 10:20:33","timeOutPictureTime":"2015-03-03 10:20:33","totalHours":"5 hours","billableHours":"45 min"}}'
6. You dont have to send up every json value at once but you have to send them up at some point in time
7. The response is 204 on success





Live website create file row with POST

curl -X POST -i -H "Accept: application/json" http://homecare-timesheet.herokuapp.com/api/v1/create/files

Notes:
1. Set accept headers to application/json
2. You should not need to set the content type. Because you are not sending up data. 
3. Set the method to POST
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/create/files
5. the json is: There is no json for you to send up!
6. The response is the id of the empty file table row created. You will need this for future requests





Live website upload files form data with POST

curl -X POST -i -H "Accept: application/json" -F "timeOutPhoto=@holiday.jpg" -F "timeInPhoto=@apitest.txt" -F "timesheetPdfFile=@apitest.txt" -F "timesheetCsvFile=@apitest.txt" -F "timesheet=1741" http://homecare-timesheet.herokuapp.com/api/v1/files/911

Notes:
1. Set accept headers to application/json
2. Set the content type to multipart/form-data 
3. Set the method to POST(NOT PATCH! PATCH requests wont work for file uploads!)
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/files/911
(make sure the endpoint has the file id at the end of the url. You got this id from the response from creating an empty file table row on the previous call)
5. Also make sure you pass the timesheet Id you are currently working on up in the same call.(whichever timesheet the files are for) Example: "timesheet=2"
6. The response is either 200, 201 or 204 on success






Live website create session File row with POST

curl -i -H "Accept: application/json" -X POST  http://homecare-timesheet.herokuapp.com/api/v1/create/session/files

Notes:
1. Set accept headers to application/json
2. You should not need to set the content type. Because you are not sending up data.
3. Set the method to POST
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/create/session/files
5. the json is: There is no json for you to send up!
6. The response is the id of the empty session file table row created. You will need this for future requests






Live website upload session files form data with POST

curl -X POST -i -H "Accept: application/json" -F "timeOutPhoto=@holiday.jpg" -F "timeInPhoto=@apitest.txt" -F "verificationPhoto=@apitest.txt" -F "sessionDataId=51" http://homecare-timesheet.herokuapp.com/api/v1/session/1/files

Notes:
1. Set accept headers to application/json
2. Set the content type to multipart/form-data
3. Set the method to POST(NOT PATCH! PATCH requests wont work for file uploads!)
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/session/1/files
(make sure the endpoint has the sessionFileId at the end of the url inbetween session/ and /files. You got this id from the response from creating an empty session file table row on the previous call)
5. Also make sure you pass the sessionDataId you are currently working on up in the same call.(the sessionDataId is the id you get from hitting this endpoint in a previous call: http://homecare-timesheet.herokuapp.com/api/v1/create/sessiondata  ) Example: "sessionDataId=2"
6. The response is either 200, 201 or 204 on success





live website create careoptions with POST

curl -i -H "Accept: application/json" -H "Content-type: application/json" -d '{"homecare_homecarebundle_careoptiontimesheet":{"timesheet":"11","careOptions":["1691","1711","1701"]}}' -X POST  http://homecare-timesheet.herokuapp.com/api/v1/careoptions

Notes:
1. Set accept headers to application/json
2. Set the content type to application/json 
3. Set the method to POST
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/careoptions
5. You must pass up the current timesheet you are working. And you must pass up the careOption Ids you want to associate with that timesheet
6. The response is either 200, 201 or 204 on success




NOTE! IF YOU WANT TO RE-ENTER THE CARE OPTIONS YOU MUST DELETE THEM FIRST!!!! Below is the method to do that!


LIVE WEBSITE DELETE all care options for a specific timesheet DELETE METHOD

curl -i -H "Accept: application/json" -H "Content-type: application/json" -X DELETE  http://homecare-timesheet.herokuapp.com/api/v1/careoptions/1/delete

Notes:
1. Set accept headers to application/json
2. Set the content type to multipart/form-data 
3. Set the method to DELETE
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/careoptions/1/delete
(make sure the endpoint has the timesheet Id added onto the url. In this case the Id is "1". You get this id in the response from creating an empty timesheet in the beginning of this documentation. Also This endpoint is idempotent(safe) and can be called multiple times without any negative effects. This endpoint deletes all care options for a given timesheet.
6. The response is either 200, 201 or 204 on success





create recepientSignature POST

curl -i -H "Accept: application/json" -H "Content-type: application/json" -d '{"homecare_homecarebundle_recipientsignature":{"timesheet":"1741","recipientSigTime":"2015-03-03 05:22:28","recipientSigAddress":"1752 sequoia lane New Richmond WI 54017","latitude":"-20.82939392","longitude":"60.82939562","recipientPhotoTime":"2015-03-03 10:20:33"}}' -X POST  http://homecare-timesheet.herokuapp.com/api/v1/create/recipient/signature

Notes:
1. Set accept headers to application/json
2. Set the content type to application/json 
3. Set the method to POST
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/create/recipient/signature
5. The json is: '{"homecare_homecarebundle_recipientsignature":{"timesheet":"1741","recipientSigTime":"2015-03-03 05:22:28","recipientSigAddress":"1752 sequoia lane New Richmond WI 54017","latitude":"-20.82939392","longitude":"60.82939562","recipientPhotoTime":"2015-03-03 10:20:33"}}'
(NOTE! YOU MUST SEND UP EVERY VALUE AT THE SAME TIME!)
6. The response is either 200, 201 or 204 on success


There is no PATCH request for the previous call. so make sure you send up every value at the same time in a POST REQUEST!






POST create pcaSignature

curl -i -H "Accept: application/json" -H "Content-type: application/json" -d '{"homecare_homecarebundle_pcasignature":{"timesheet":"1741","pcaSigTime":"2015-03-03 05:22:28","pcaSigAddress":"1752 sequoia lane New Richmond WI 54017","latitude":"-20.82939392","longitude":"60.82939562","pcaPhotoTime":"2015-03-03 10:20:33"}}' -X POST  http://homecare-timesheet.herokuapp.com/api/v1/create/pca/signature

Notes:
1. Set accept headers to application/json
2. Set the content type to application/json 
3. Set the method to POST
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/create/pca/signature
5. The json is: '{"homecare_homecarebundle_pcasignature":{"timesheet":"1741","pcaSigTime":"2015-03-03 05:22:28","pcaSigAddress":"1752 sequoia lane New Richmond WI 54017","latitude":"-20.82939392","longitude":"60.82939562","pcaPhotoTime":"2015-03-03 10:20:33"}}'
(NOTE! YOU MUST SEND UP EVERY VALUE AT THE SAME TIME!)
6. The response is either 200, 201 or 204 on success


There is no PATCH request for the previous call. so make sure you send up every value at the same time in a POST REQUEST!









check if verification is needed

curl -H "Accept: application/json" -i -X GET http://homecare-timesheet.herokuapp.com/api/v1/pca/67/verify/2 -H "Authorization: Bearer NmRhMTJlYzdkNTFjYWQwYWZmODIwNTE0ZWIwMTQzZjRjYzVhOTViNTkyZGQ2NWFhYzRjYjZlMzUzMDdmOTQyNg"

Notes:
1. Set accept headers to application/json
3. Set the method to GET
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/pca/67/verify/2
5. In the url the "67" is the pca Id and the 2 is the recipient Id
6. The response is either 200, 201 or 204 on success and "message" => "verification not needed" or "message" => "verification needed", "id" => 2


Set verification properties

curl -i -H "Accept: application/json" -H "Content-type: application/json" -d '{"homecare_homecarebundle_verification":{"time":"2015-03-03","address":"1752 sequoia lane New Richmond WI 54017","latitude":"-20.82939392","longitude":"60.82939562"}}' -X PATCH  http://homecare-timesheet.herokuapp.com/api/v1/verify/24

Notes:
1. Set accept headers to application/json
2. Set conten type to application/json
3. Set the method to PATCH
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/verify/24
5. In the url the "24" is the id from the verify response endpoint if a verification is needed
6. The response is either 200, 201 or 204 on success



Upload verification photo

curl -X POST -i -H "Accept: application/json" -F "verificationPhoto=@holiday.jpg"  http://homecare-timesheet.herokuapp.com/api/v1/verifcation/24/photo

Notes:
1. Set accept headers to application/json
3. Set the method to POST
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/verifcation/26/photo
5. In the url the "24" is the id from the verify response endpoint if a verification is needed
6. The response is either 200, 201 or 204 on and the new name of the file on the server






POST create blank session data row

curl -i -H "Accept: application/json" -H "Content-type: application/json" -d '{"homecare_homecarebundle_sessiondata":""}' -X POST http://homecare-timesheet.herokuapp.com/api/v1/create/sessiondata

Notes:
1. Set accept headers to application/json
2. Set the content type to application/json 
3. Set the method to POST
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/create/sessiondata
5. The json is: '{"homecare_homecarebundle_sessiondata":""}'
6. this endpoint creates an empty random session data row in the table.
7. The response is the id of the table row created. You will need this for future requests






PATCH session data. add data to the session data row (this now holds all the data that is consistent for each timesheet)

curl -i -H "Accept: application/json" -H "Content-type: application/json" -d '{"homecare_homecarebundle_sessiondata":{"ratio":"11","sharedCareLocation":"11","agency":"561","pca":"11","service":"1","timeIn":"351","timeOut":"301","verification":"1","visitRatio":"1241","currentTimesheetNumber":"2","continueTimesheetNumber":"1","flagForFurtherInvestigation":"0","currentTimesheetId":"1741","dateEnding":"2015-04-09 15:40:41"}}' -X PATCH  http://homecare-timesheet.herokuapp.com/api/v1/sessiondata/1

Notes:
1. Set accept headers to application/json
2. Set the content type to application/json 
3. Set the method to PATCH
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/sessiondata/1
5. The json is: '{"homecare_homecarebundle_sessiondata":{"ratio":"11","sharedCareLocation":"11","pca":"11","service":"1","timeIn":"351","timeOut":"301","verification":"1","visitRatio":"1241","currentTimesheetNumber":"2","continueTimesheetNumber":"1","flagForFurtherInvestigation":"0","currentTimesheetId":"1741","dateEnding":"2015-04-09 15:40:41"}}'
(make sure the endpoint has the id of the session data row added onto the url. In this case that id would be "1", You got this id from the response from creating an empty session data table row on the previous call)
6. You dont have to send up every json value at once but make sure you SEND UP THE PCA VALUE AS SOON AS POSSIBLE!!!
7. The response is 204 on success





DELETE delete any timesheets and all there data associated

curl -i -H "Accept: application/json" -H "Content-type: application/json" -X DELETE -d '{"homecare_homecarebundle_timesheet":{"sessionId":"4"}}' http://homecare-timesheet.herokuapp.com/api/v1/delete/timesheets

Notes:
1. Set accept headers to application/json
2. Set the content type to application/json
3. Set the method to DELETE
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/delete/timesheets
5. the json is: '{"homecare_homecarebundle_timesheet":{"sessionId":"4"}}'
(pass in the session Id . this will delete all the data associated with the timesheet/s)
6. The response is either 200, 201 or 204 on success





Finish timesheets Live Website

curl -i -H "Accept: application/json" -H "Content-type: application/json" -X POST -d '{"homecare_homecarebundle_timesheet":{"sessionId":"5"}}' http://homecare-timesheet.herokuapp.com/api/v1/finish/timesheets

Notes:
1. Set accept headers to application/json
2. Set the content type to application/json
3. Set the method to POST
4. the endpoint is: http://homecare-timesheet.herokuapp.com/api/v1/finish/timesheets
5. the json is: '{"homecare_homecarebundle_timesheet":{"sessionId":"5"}}'
(pass in the session Id. this will save the timesheets associated with the session and mark them as finished. they will then appear on the website)
6. The response is either 200, 201 or 204 on success





Get the total number of hours for a PCA since the Week Start Date.

curl -H "Accept: application/json" -H "Content-type: application/json" -X GET http://www.hc-timesheets-demo.com/app_dev.php/api/v1/pca/7/hours?estimatedShiftLength=6 -H "Authorization: Bearer YWQ1M2IyZDEzYWRlNjZmMmIwNmI4MGI5YjgxMzgzNWEyZDk3MzViOGQzZmEzZTNhZDdjZTM2MzcxZGJlZDMyNg"

Notes:
1. Set accept headers to application/json
2. Set the content type to application/json
3. Set the method to GET
4. the endpoint is: http://www.hc-timesheets-demo.com/app_dev.php/api/v1/pca/7/hours?estimatedShiftLength=6
5. make sure to pass up the ?estimatedShiftLength=value as a query (GET) parameter.
6. The response is either 200, 201 or 204 on success
7. The response will contain the (estimated shift length + total hours worked since the last Week Start Date)
8. Response will resemble {hours: totalHours}
9. Make sure to pass up the Id of the pca. In this case it is 7. Exmample: http://www.hc-timesheets-demo.com/app_dev.php/api/v1/pca/7

