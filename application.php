#!/usr/bin/env php
<?php
// application.php

require __DIR__.'/vendor/autoload.php';

use Homecare\HomecareBundle\Command\AgencyCommand;
use Symfony\Component\Console\Application;
//use Symfony\Bundle\FrameworkBundle\Console\Application

$application = new Application();
$application->add(new AgencyCommand);
$application->run();