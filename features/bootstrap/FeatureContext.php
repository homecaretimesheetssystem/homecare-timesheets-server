<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
// extend raw mink context to be able to access the session
use Behat\MinkExtension\Context\RawMinkContext;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Hautelook\AliceBundle\Alice\DataFixtures\Loader;
use Hautelook\AliceBundle\Doctrine\DataFixtures\Executor\ORMExecutor;
use Homecare\HomecareBundle\Entity\States;
use Homecare\HomecareBundle\Entity\User;
use Behat\Symfony2Extension\Context\KernelDictionary;
use Homecare\HomecareBundle\Entity\Agency;
use Symfony\Bridge\Doctrine\DataFixtures\ContainerAwareLoader;


require_once __DIR__ . '/../../vendor/phpunit/phpunit/src/Framework/Assert/Functions.php';

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawMinkContext implements Context, SnippetAcceptingContext {

	use KernelDictionary;


	private $currentUser;

	/**
	 * Initializes context.
	 *
	 * Every scenario gets its own context instance.
	 * You can also pass arbitrary arguments to the
	 * context constructor through behat.yml.
	 */
	public function __construct() {
	}


	/**
	 * @When I am logged in as an agency
	 */
	public function iAmLoggedInAsAnAgency() {

		$this->visitPath( "/login" );
		$this->getSession()->getPage()->fillField( "_username", "agency1" );
		$this->getSession()->getPage()->fillField( "_password", "agency1" );
		$this->getSession()->getPage()->pressButton( "Login" );
	}


	/**
	 * @When I fill in agency name with :agencyName
	 */
	public function iFillInAgencyNameWith( $agencyName ) {
		$agencyField = $this->getPage()
		                    ->find( 'css', '[name="homecare_homecarebundle_agency[agencyName]"]' );
		assertNotNull( $agencyField, "agency name field could not be found" );
		$agencyField->setValue( $agencyName );

	}





	/**
	 * @When I fill in state with :state
	 */
	public function iFillInStateWith( $state ) {

		$form = $this->getPage()->find('css', 'form');

		$form->selectFieldOption("homecare_homecarebundle_agency_state", 1);

	}



	/**
	 * @Then the agency field should be :agencyName
	 */
	public function theAgencyFieldShouldBe($agencyName)
	{
		$chosenAgencyName = $this->getPage()->find("css", "#homecare_homecarebundle_agency_agencyName")->getValue();

		assertEquals($agencyName, $chosenAgencyName);
	}



	/**
	 * @Then the state field should be :state
	 */
	public function theStateFieldShouldBe($state)
	{
		$chosenState = $this->getPage()->find("css", "#homecare_homecarebundle_agency_state option[selected='selected']")->getText();

		assertEquals($state, $chosenState);
	}


	/**
	 * @Then the phone number field should be :phoneNumber
	 */
	public function thePhoneNumberFieldShouldBe($phoneNumber)
	{
		$chosenPhoneNumber = $this->getPage()->find("css", "#homecare_homecarebundle_agency_phoneNumber")->getValue();

		assertEquals($phoneNumber, $chosenPhoneNumber);
	}


	/**
	 * @When I fill in service name with :serviceName
	 */
	public function iFillInServiceNameWith( $serviceName ) {
		$serviceNameField = $this->getPage()
		                         ->find( 'css', '[name="homecare_homecarebundle_agency[services][0][serviceName]"]' );
		assertNotNull( $serviceNameField, "service name field could not be found" );
		$serviceNameField->setValue( $serviceName );
	}


	/**
	 * @Then the service name field should be :serviceName
	 */
	public function theServiceNameFieldShouldBe($serviceName)
	{
		$chosenServiceName = $this->getPage()->find("css", "#homecare_homecarebundle_agency_services_0_serviceName")->getValue();

		assertEquals($serviceName, $chosenServiceName);
	}




	/**
	 * @When I fill in phone number with :phoneNumber
	 */
	public function iFillInPhoneNumberWith( $phoneNumber ) {
		$phoneNumberField = $this->getPage()
		                    ->find( 'css', '[name="homecare_homecarebundle_agency[phoneNumber]"]' );
		assertNotNull( $phoneNumberField, "phone number field could not be found" );
		$phoneNumberField->setValue( $phoneNumber );
	}




	/**
	 * Pauses the scenario until the user presses a key. Useful when debugging a scenario.
	 *
	 * @Then (I )break
	 */
	public function iPutABreakpoint()
	{
		fwrite(STDOUT, "\033[s    \033[93m[Breakpoint] Press \033[1;93m[RETURN]\033[0;93m to continue...\033[0m");
		while (fgets(STDIN, 1024) == '') {
		}
		fwrite(STDOUT, "\033[u");

		return;
	}




	private function getEntityManager() {
		//return $this->getContainer()->get('doctrine')->getManager();
		return $this->getContainer()->get( 'doctrine.orm.entity_manager' );
	}


	private function getPage() {
		return $this->getSession()->getPage();
	}




}
