Feature: Edit Agency Settings
  In order to show agency specific settings on the mobile app
  as an agency user
  I need to be able to edit the settings

  # this test has to run with javascript cause of adding services nested form
  @javascript
  Scenario: Edit Agency Settings
    Given the database is empty
    Given the following fixtures files are loaded:
      | states.yml   |
      | agencies.yml |
      | users.yml    |
    When I am logged in as an agency
    And I am on "/agency/settings"
    And I fill in agency name with "Best Homecare"
    And I fill in state with "Wisconsin"
    And I fill in phone number with "715-977-1299"
    And I attach the file "images/holiday.jpg" to "homecare_homecarebundle_agency[imageFile]"
    When I follow "Add a Service"
    And I fill in service name with "homemaking"
    When I press "Save"
    Then the agency field should be "Best Homecare"
    Then the state field should be "Wisconsin"
    Then the phone number field should be "715-977-1299"
    Then the service name field should be "homemaking"
    And I should see "Your agency settings have been successfully updated!"

    #And print last response
    #Then I break